`include "tetris_define.vh"

module seven_segment (
	input 	   [3:0]  							  digit,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk1,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk2,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk3,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk4,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk5,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk6,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk7,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk8,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk9,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk10,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk11,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk12,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk13,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk14,
	input  	   [`BITS_BLK_POS-1:0]				  score_blk15,
	output reg [`BLOCKS_WIDTH*`BLOCKS_HEIGHT-1:0] score_board
);

	always@(*) begin 
		if(digit == 0) begin 
			score_board[score_blk1] = 0;
			score_board[score_blk2] = 1;
			score_board[score_blk3] = 0;
			score_board[score_blk4] = 1;
			score_board[score_blk5] = 0;
			score_board[score_blk6] = 1;
			score_board[score_blk7] = 1;
			score_board[score_blk8] = 0;
			score_board[score_blk9] = 1;
			score_board[score_blk10] = 1;
			score_board[score_blk11] = 0;
			score_board[score_blk12] = 1;
			score_board[score_blk13] = 0;
			score_board[score_blk14] = 1;
			score_board[score_blk15] = 0;
		end
		else if(digit == 1) begin 
			score_board[score_blk1] = 0;
			score_board[score_blk2] = 1;
			score_board[score_blk3] = 0;
			score_board[score_blk4] = 0;
			score_board[score_blk5] = 1;
			score_board[score_blk6] = 0;
			score_board[score_blk7] = 0;
			score_board[score_blk8] = 1;
			score_board[score_blk9] = 0;
			score_board[score_blk10] = 0;
			score_board[score_blk11] = 1;
			score_board[score_blk12] = 0;
			score_board[score_blk13] = 0;
			score_board[score_blk14] = 1;
			score_board[score_blk15] = 0;			
		end
		else if(digit == 2) begin 
			score_board[score_blk1] = 1;
			score_board[score_blk2] = 1;
			score_board[score_blk3] = 1;
			score_board[score_blk4] = 0;
			score_board[score_blk5] = 0;
			score_board[score_blk6] = 1;
			score_board[score_blk7] = 1;
			score_board[score_blk8] = 1;
			score_board[score_blk9] = 1;
			score_board[score_blk10] = 1;
			score_board[score_blk11] = 0;
			score_board[score_blk12] = 0;
			score_board[score_blk13] = 1;
			score_board[score_blk14] = 1;
			score_board[score_blk15] = 1;
		end
		else if(digit == 3) begin 
			score_board[score_blk1] = 1;
			score_board[score_blk2] = 1;
			score_board[score_blk3] = 1;
			score_board[score_blk4] = 0;
			score_board[score_blk5] = 0;
			score_board[score_blk6] = 1;
			score_board[score_blk7] = 1;
			score_board[score_blk8] = 1;
			score_board[score_blk9] = 1;
			score_board[score_blk10] = 0;
			score_board[score_blk11] = 0;
			score_board[score_blk12] = 1;
			score_board[score_blk13] = 1;
			score_board[score_blk14] = 1;
			score_board[score_blk15] = 1;
		end
		else if(digit == 4) begin 
			score_board[score_blk1] = 1;
			score_board[score_blk2] = 0;
			score_board[score_blk3] = 1;
			score_board[score_blk4] = 1;
			score_board[score_blk5] = 0;
			score_board[score_blk6] = 1;
			score_board[score_blk7] = 1;
			score_board[score_blk8] = 1;
			score_board[score_blk9] = 1;
			score_board[score_blk10] = 0;
			score_board[score_blk11] = 0;
			score_board[score_blk12] = 1;
			score_board[score_blk13] = 0;
			score_board[score_blk14] = 0;
			score_board[score_blk15] = 1;
		end
		else if(digit == 5) begin 
			score_board[score_blk1] = 1;
			score_board[score_blk2] = 1;
			score_board[score_blk3] = 1;
			score_board[score_blk4] = 1;
			score_board[score_blk5] = 0;
			score_board[score_blk6] = 0;
			score_board[score_blk7] = 1;
			score_board[score_blk8] = 1;
			score_board[score_blk9] = 1;
			score_board[score_blk10] = 0;
			score_board[score_blk11] = 0;
			score_board[score_blk12] = 1;
			score_board[score_blk13] = 1;
			score_board[score_blk14] = 1;
			score_board[score_blk15] = 1;
		end
		else if(digit == 6) begin 
			score_board[score_blk1] = 1;
			score_board[score_blk2] = 1;
			score_board[score_blk3] = 1;
			score_board[score_blk4] = 1;
			score_board[score_blk5] = 0;
			score_board[score_blk6] = 0;
			score_board[score_blk7] = 1;
			score_board[score_blk8] = 1;
			score_board[score_blk9] = 1;
			score_board[score_blk10] = 1;
			score_board[score_blk11] = 0;
			score_board[score_blk12] = 1;
			score_board[score_blk13] = 1;
			score_board[score_blk14] = 1;
			score_board[score_blk15] = 1;
		end
		else if(digit == 7) begin 
			score_board[score_blk1] = 1;
			score_board[score_blk2] = 1;
			score_board[score_blk3] = 1;
			score_board[score_blk4] = 0;
			score_board[score_blk5] = 0;
			score_board[score_blk6] = 1;
			score_board[score_blk7] = 0;
			score_board[score_blk8] = 0;
			score_board[score_blk9] = 1;
			score_board[score_blk10] = 0;
			score_board[score_blk11] = 0;
			score_board[score_blk12] = 1;
			score_board[score_blk13] = 0;
			score_board[score_blk14] = 0;
			score_board[score_blk15] = 1;
		end
		else if(digit == 8) begin 
			score_board[score_blk1] = 1;
			score_board[score_blk2] = 1;
			score_board[score_blk3] = 1;
			score_board[score_blk4] = 1;
			score_board[score_blk5] = 0;
			score_board[score_blk6] = 1;
			score_board[score_blk7] = 1;
			score_board[score_blk8] = 1;
			score_board[score_blk9] = 1;
			score_board[score_blk10] = 1;
			score_board[score_blk11] = 0;
			score_board[score_blk12] = 1;
			score_board[score_blk13] = 1;
			score_board[score_blk14] = 1;
			score_board[score_blk15] = 1;
		end
		else begin 
			score_board[score_blk1] = 1;
			score_board[score_blk2] = 1;
			score_board[score_blk3] = 1;
			score_board[score_blk4] = 1;
			score_board[score_blk5] = 0;
			score_board[score_blk6] = 1;
			score_board[score_blk7] = 1;
			score_board[score_blk8] = 1;
			score_board[score_blk9] = 1;
			score_board[score_blk10] = 0;
			score_board[score_blk11] = 0;
			score_board[score_blk12] = 1;
			score_board[score_blk13] = 0;
			score_board[score_blk14] = 0;
			score_board[score_blk15] = 1;
		end
	end
endmodule