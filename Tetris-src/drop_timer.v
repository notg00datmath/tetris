module auto_fall_timer (
	input		clk,    // Clock
	input		rst,
	input		pause,
	output reg	auto_fall 	
);

	reg	[31:0]	counter;
	always@(posedge clk) begin
		if(!pause) begin
			if(rst) begin
				counter <= 0;
				auto_fall <= 0;
			end
			else begin
				if(counter >= 25000000) begin
					counter <= 0;
					auto_fall <= 1;
				end
				else begin
					counter <= counter + 1;
					auto_fall <= 0;
				end	
			end	
		end
		else begin
			auto_fall <= auto_fall;
			counter <= counter;
		end	
	end
endmodule