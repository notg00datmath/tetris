`include "tetris_define.vh"

module vga (
	input 											pixel_clk,    // Clock	
	input 	wire [`BITS_PER_BLOCK-1:0]				cur_tetris,
	input	wire [`MODE_BITS-1:0]					mode,
	input	wire [`BITS_PER_BLOCK-1:0]				next_tetris,
	input 	wire [`BITS_BLK_POS-1:0]				blk1,	
	input 	wire [`BITS_BLK_POS-1:0]				blk2,	
	input 	wire [`BITS_BLK_POS-1:0]				blk3,	
	input 	wire [`BITS_BLK_POS-1:0]				blk4,	
	input 	wire [`BITS_BLK_POS-1:0]				next_blk1,	
	input 	wire [`BITS_BLK_POS-1:0]				next_blk2,	
	input 	wire [`BITS_BLK_POS-1:0]				next_blk3,	
	input 	wire [`BITS_BLK_POS-1:0]				next_blk4,
	input   wire [`BITS_BLK_POS-1:0]				score1_blk1,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk2,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk3,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk4,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk5,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk6,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk7,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk8,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk9,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk10,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk11,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk12,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk13,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk14,
 	input   wire [`BITS_BLK_POS-1:0]				score1_blk15,	
	input   wire [`BITS_BLK_POS-1:0]				score0_blk1,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk2,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk3,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk4,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk5,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk6,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk7,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk8,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk9,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk10,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk11,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk12,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk13,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk14,
 	input   wire [`BITS_BLK_POS-1:0]				score0_blk15,
	input	wire [`BLOCKS_WIDTH*`BLOCKS_HEIGHT-1:0]	fallen,
	input	wire [`BLOCKS_WIDTH*`BLOCKS_HEIGHT-1:0] score_board1,
	input	wire [`BLOCKS_WIDTH*`BLOCKS_HEIGHT-1:0] score_board0,
	// input	[3:0]									digit0,
	// input	[3:0]									digit1,
	output	reg	 [11:0]								rgb,
	output											hs, vs
);

	reg  [9:0] counterX = 0;
	reg  [9:0] counterY = 0;
	wire [9:0] cur_blk_index; 
	reg		   v_enabled;

	//synchronization signals
	assign hs = (counterX < `HSYNC_PULSE_WIDTH) ? 1'b0 : 1'b1;
	assign vs = (counterY < `VSYNC_PULSE_WIDTH) ? 1'b0 : 1'b1;

	//select current block
	assign cur_blk_index = ((counterX-`BOARD_X)>>4) + (((counterY-`BOARD_Y)>>4)*`BLOCKS_WIDTH);

	always@(*) begin
		if(mode == `MODE_IDLE)
			rgb = `ORANGE;
		else begin
			//with in the game board
			if(counterX >= `BOARD_X && counterX <= `BOARD_X + `BOARD_WIDTH && 
			    counterY >= `BOARD_Y && counterY <= `BOARD_Y + `BOARD_HEIGHT) begin
				if(counterX == `BOARD_X || counterX == `BOARD_X + `BOARD_WIDTH || 
			       counterY == `BOARD_Y || counterY == `BOARD_Y + `BOARD_HEIGHT) begin
				   	//the game board is white
			   		rgb = `GREEN;
			    end
			    else begin
			   		//if current tetrimino is reached, drive rgb
			   		if(cur_blk_index == blk1 ||
			   		   cur_blk_index == blk2 ||
			   		   cur_blk_index == blk3 ||
			   		   cur_blk_index == blk4) begin
			   		   	case(cur_tetris) 
			   		   		`EMPTY_BLOCK: rgb = `BLACK;
			                `I_BLOCK: rgb = `CYAN;
			                `O_BLOCK: rgb = `YELLOW;
			                `T_BLOCK: rgb = `PURPLE;
			                `S_BLOCK: rgb = `GREEN;
			                `Z_BLOCK: rgb = `RED;
			                `J_BLOCK: rgb = `BLUE;
			                `L_BLOCK: rgb = `ORANGE;
			   		   	endcase 
				    end
				    else begin
				    	//if fallen tetriminos reached, drive monochrome
				    	rgb = fallen[cur_blk_index] ? `GRAY : `BLACK;
				    end
			    end
			end

			//score board	
		    else if(counterX >= `SCORE1_X && counterX <= `SCORE1_X + (2*`SCORE_WIDTH) && 
				    counterY >= `SCORE1_Y && counterY <= `SCORE1_Y + `SCORE_HEIGHT) begin
					if(cur_blk_index == score0_blk1  ||
					   cur_blk_index == score0_blk2  ||
					   cur_blk_index == score0_blk3  ||
					   cur_blk_index == score0_blk4  ||
					   cur_blk_index == score0_blk5  ||
					   cur_blk_index == score0_blk6  ||
					   cur_blk_index == score0_blk7  ||
					   cur_blk_index == score0_blk8  ||
					   cur_blk_index == score0_blk9  ||
					   cur_blk_index == score0_blk10 ||
					   cur_blk_index == score0_blk11 ||
					   cur_blk_index == score0_blk12 ||
					   cur_blk_index == score0_blk13 ||
					   cur_blk_index == score0_blk14 ||
					   cur_blk_index == score0_blk15 ||
					   cur_blk_index == score0_blk15 ) 
					   		rgb = score_board0[cur_blk_index] ? `ORANGE : `BLACK;

				    else if(cur_blk_index == score1_blk1  ||
						    cur_blk_index == score1_blk2  ||
						    cur_blk_index == score1_blk3  ||
						    cur_blk_index == score1_blk4  ||
						    cur_blk_index == score1_blk5  ||
						    cur_blk_index == score1_blk6  ||
						    cur_blk_index == score1_blk7  ||
						    cur_blk_index == score1_blk8  ||
						    cur_blk_index == score1_blk9  ||
						    cur_blk_index == score1_blk10 ||
						    cur_blk_index == score1_blk11 ||
						    cur_blk_index == score1_blk12 ||
						    cur_blk_index == score1_blk13 ||
						    cur_blk_index == score1_blk14 ||
						    cur_blk_index == score1_blk15 ||
						    cur_blk_index == score1_blk15 )  
					   		rgb = score_board1[cur_blk_index] ? `CYAN : `BLACK;
					else 
						    rgb = `BLACK;
		    end
            
            //next block
			else if(counterX >= `NEXT_BLOCK_X && counterX <= `NEXT_BLOCK_X + `NEXT_BLOCK_WIDTH && 
				    counterY >= `NEXT_BLOCK_Y && counterY <= `NEXT_BLOCK_Y + `NEXT_BLOCK_HEIGHT) begin 
					if(cur_blk_index == next_blk1 ||
			   		   cur_blk_index == next_blk2 ||
			   		   cur_blk_index == next_blk3 ||
			   		   cur_blk_index == next_blk4) begin
							case(next_tetris) 
				   		   		`EMPTY_BLOCK: rgb = `WHITE;
				                `I_BLOCK: rgb = `CYAN;
				                `O_BLOCK: rgb = `YELLOW;
				                `T_BLOCK: rgb = `PURPLE;
				                `S_BLOCK: rgb = `GREEN;
				                `Z_BLOCK: rgb = `RED;
				                `J_BLOCK: rgb = `BLUE;
				                `L_BLOCK: rgb = `ORANGE;
				   		   	endcase 
				   	end
				   	else	
				   		rgb = `BLACK;
			end

			else 
				rgb = `BLACK;
		end
	end

    always@(posedge pixel_clk) begin
		if(counterX<(`PIXEL_WIDTH + `HSYNC_FRONT_PORCH + `HSYNC_PULSE_WIDTH + `HSYNC_BACK_PORCH - 1)) begin
			counterX <= counterX + 1;
			v_enabled <= 0;
		end
		else begin
			counterX <= 0;
			v_enabled <= 1;
		end
    end

    always@(posedge pixel_clk) begin
    	if(v_enabled==1'b1) begin
    		if(counterY<(`PIXEL_HEIGHT + `VSYNC_PULSE_WIDTH + `VSYNC_FRONT_PORCH + `VSYNC_BACK_PORCH - 1)) 
    			counterY <= counterY + 1;
    		else
    			counterY <= 0;
    	end
    	else 
    		counterY <= counterY;
    end

endmodule