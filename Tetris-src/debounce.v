module debounce (
	input 	clk,    // Clock
	input 	raw,
	output	btn_debounced,	
	output	sw_flipped
);

	reg			debounced, debounced_prev;
	reg	[15:0]	counter;
	wire 		enabled, disabled;

	assign enabled = debounced &&!debounced_prev;
	assign disabled = !debounced && debounced_prev;

	assign btn_debounced = enabled;
	assign sw_flipped = enabled;

	initial begin
		debounced = 0;
		debounced_prev = 0;
		counter = 0;
	end

	always@(posedge clk) begin 
		debounced_prev <= debounced;
	end

	always@(posedge clk) begin
		if (counter == 15000) begin
			counter <= 0;
			debounced <= raw;
		end
		else begin
			counter <= counter + 1;
			debounced <= debounced;
		end
	end

endmodule

