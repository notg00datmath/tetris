-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Tue Mar 17 12:32:14 2020
-- Host        : notg00datmath running 64-bit Ubuntu 18.04.4 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ tetris_pl_interrupt_0_3_sim_netlist.vhdl
-- Design      : tetris_pl_interrupt_0_3
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pl_interrupt_v1_0_pl_interrupt is
  port (
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    pl_interrupt_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    pl_interrupt_rvalid : out STD_LOGIC;
    pl_interrupt_bvalid : out STD_LOGIC;
    pl_interrupt_aclk : in STD_LOGIC;
    pl_interrupt_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    pl_interrupt_arvalid : in STD_LOGIC;
    pl_interrupt_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    pl_interrupt_wvalid : in STD_LOGIC;
    pl_interrupt_awvalid : in STD_LOGIC;
    pl_interrupt_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    mid : in STD_LOGIC;
    right : in STD_LOGIC;
    left : in STD_LOGIC;
    down : in STD_LOGIC;
    up : in STD_LOGIC;
    pl_interrupt_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pl_interrupt_aresetn : in STD_LOGIC;
    pl_interrupt_bready : in STD_LOGIC;
    pl_interrupt_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pl_interrupt_v1_0_pl_interrupt;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pl_interrupt_v1_0_pl_interrupt is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal axi_araddr : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_awready_i_1_n_0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^pl_interrupt_bvalid\ : STD_LOGIC;
  signal \^pl_interrupt_rvalid\ : STD_LOGIC;
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slv_reg1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg1[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg2 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg2[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg3 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg3[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal \slv_reg_wren__0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \axi_araddr[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of axi_arready_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \slv_reg3[31]_i_2\ : label is "soft_lutpair0";
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  pl_interrupt_bvalid <= \^pl_interrupt_bvalid\;
  pl_interrupt_rvalid <= \^pl_interrupt_rvalid\;
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFC4CCC4CCC4CC"
    )
        port map (
      I0 => pl_interrupt_awvalid,
      I1 => aw_en_reg_n_0,
      I2 => \^s_axi_awready\,
      I3 => pl_interrupt_wvalid,
      I4 => pl_interrupt_bready,
      I5 => \^pl_interrupt_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => pl_interrupt_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => axi_awready_i_1_n_0
    );
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => pl_interrupt_araddr(0),
      I1 => pl_interrupt_arvalid,
      I2 => \^s_axi_arready\,
      I3 => axi_araddr(2),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => pl_interrupt_araddr(1),
      I1 => pl_interrupt_arvalid,
      I2 => \^s_axi_arready\,
      I3 => axi_araddr(3),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => axi_araddr(2),
      R => axi_awready_i_1_n_0
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => axi_araddr(3),
      R => axi_awready_i_1_n_0
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => pl_interrupt_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => axi_awready_i_1_n_0
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBFFFFFF08000000"
    )
        port map (
      I0 => pl_interrupt_awaddr(0),
      I1 => pl_interrupt_wvalid,
      I2 => \^s_axi_awready\,
      I3 => aw_en_reg_n_0,
      I4 => pl_interrupt_awvalid,
      I5 => p_0_in(0),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBFFFFFF08000000"
    )
        port map (
      I0 => pl_interrupt_awaddr(1),
      I1 => pl_interrupt_wvalid,
      I2 => \^s_axi_awready\,
      I3 => aw_en_reg_n_0,
      I4 => pl_interrupt_awvalid,
      I5 => p_0_in(1),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => p_0_in(0),
      R => axi_awready_i_1_n_0
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => p_0_in(1),
      R => axi_awready_i_1_n_0
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pl_interrupt_aresetn,
      O => axi_awready_i_1_n_0
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => pl_interrupt_wvalid,
      I1 => \^s_axi_awready\,
      I2 => aw_en_reg_n_0,
      I3 => pl_interrupt_awvalid,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => axi_awready_i_1_n_0
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => pl_interrupt_awvalid,
      I1 => pl_interrupt_wvalid,
      I2 => \^s_axi_awready\,
      I3 => \^s_axi_wready\,
      I4 => pl_interrupt_bready,
      I5 => \^pl_interrupt_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^pl_interrupt_bvalid\,
      R => axi_awready_i_1_n_0
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(0),
      I1 => slv_reg1(0),
      I2 => axi_araddr(2),
      I3 => slv_reg2(0),
      I4 => axi_araddr(3),
      I5 => mid,
      O => reg_data_out(0)
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(10),
      I1 => slv_reg1(10),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(10),
      O => reg_data_out(10)
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(11),
      I1 => slv_reg1(11),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(11),
      O => reg_data_out(11)
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(12),
      I1 => slv_reg1(12),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(12),
      O => reg_data_out(12)
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(13),
      I1 => slv_reg1(13),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(13),
      O => reg_data_out(13)
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(14),
      I1 => slv_reg1(14),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(14),
      O => reg_data_out(14)
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(15),
      I1 => slv_reg1(15),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(15),
      O => reg_data_out(15)
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(16),
      I1 => slv_reg1(16),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(16),
      O => reg_data_out(16)
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(17),
      I1 => slv_reg1(17),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(17),
      O => reg_data_out(17)
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(18),
      I1 => slv_reg1(18),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(18),
      O => reg_data_out(18)
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(19),
      I1 => slv_reg1(19),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(19),
      O => reg_data_out(19)
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(1),
      I1 => slv_reg1(1),
      I2 => axi_araddr(2),
      I3 => slv_reg2(1),
      I4 => axi_araddr(3),
      I5 => right,
      O => reg_data_out(1)
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(20),
      I1 => slv_reg1(20),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(20),
      O => reg_data_out(20)
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(21),
      I1 => slv_reg1(21),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(21),
      O => reg_data_out(21)
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(22),
      I1 => slv_reg1(22),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(22),
      O => reg_data_out(22)
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(23),
      I1 => slv_reg1(23),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(23),
      O => reg_data_out(23)
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(24),
      I1 => slv_reg1(24),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(24),
      O => reg_data_out(24)
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(25),
      I1 => slv_reg1(25),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(25),
      O => reg_data_out(25)
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(26),
      I1 => slv_reg1(26),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(26),
      O => reg_data_out(26)
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(27),
      I1 => slv_reg1(27),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(27),
      O => reg_data_out(27)
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(28),
      I1 => slv_reg1(28),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(28),
      O => reg_data_out(28)
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(29),
      I1 => slv_reg1(29),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(29),
      O => reg_data_out(29)
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(2),
      I1 => slv_reg1(2),
      I2 => axi_araddr(2),
      I3 => slv_reg2(2),
      I4 => axi_araddr(3),
      I5 => left,
      O => reg_data_out(2)
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(30),
      I1 => slv_reg1(30),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(30),
      O => reg_data_out(30)
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(31),
      I1 => slv_reg1(31),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(31),
      O => reg_data_out(31)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(3),
      I1 => slv_reg1(3),
      I2 => axi_araddr(2),
      I3 => slv_reg2(3),
      I4 => axi_araddr(3),
      I5 => down,
      O => reg_data_out(3)
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg3(4),
      I1 => slv_reg1(4),
      I2 => axi_araddr(2),
      I3 => slv_reg2(4),
      I4 => axi_araddr(3),
      I5 => up,
      O => reg_data_out(4)
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(5),
      I1 => slv_reg1(5),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(5),
      O => reg_data_out(5)
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(6),
      I1 => slv_reg1(6),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(6),
      O => reg_data_out(6)
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(7),
      I1 => slv_reg1(7),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(7),
      O => reg_data_out(7)
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(8),
      I1 => slv_reg1(8),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(8),
      O => reg_data_out(8)
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => slv_reg3(9),
      I1 => slv_reg1(9),
      I2 => axi_araddr(2),
      I3 => axi_araddr(3),
      I4 => slv_reg2(9),
      O => reg_data_out(9)
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(0),
      Q => pl_interrupt_rdata(0),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(10),
      Q => pl_interrupt_rdata(10),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(11),
      Q => pl_interrupt_rdata(11),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(12),
      Q => pl_interrupt_rdata(12),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(13),
      Q => pl_interrupt_rdata(13),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(14),
      Q => pl_interrupt_rdata(14),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(15),
      Q => pl_interrupt_rdata(15),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(16),
      Q => pl_interrupt_rdata(16),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(17),
      Q => pl_interrupt_rdata(17),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(18),
      Q => pl_interrupt_rdata(18),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(19),
      Q => pl_interrupt_rdata(19),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(1),
      Q => pl_interrupt_rdata(1),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(20),
      Q => pl_interrupt_rdata(20),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(21),
      Q => pl_interrupt_rdata(21),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(22),
      Q => pl_interrupt_rdata(22),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(23),
      Q => pl_interrupt_rdata(23),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(24),
      Q => pl_interrupt_rdata(24),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(25),
      Q => pl_interrupt_rdata(25),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(26),
      Q => pl_interrupt_rdata(26),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(27),
      Q => pl_interrupt_rdata(27),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(28),
      Q => pl_interrupt_rdata(28),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(29),
      Q => pl_interrupt_rdata(29),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(2),
      Q => pl_interrupt_rdata(2),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(30),
      Q => pl_interrupt_rdata(30),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(31),
      Q => pl_interrupt_rdata(31),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(3),
      Q => pl_interrupt_rdata(3),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(4),
      Q => pl_interrupt_rdata(4),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(5),
      Q => pl_interrupt_rdata(5),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(6),
      Q => pl_interrupt_rdata(6),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(7),
      Q => pl_interrupt_rdata(7),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(8),
      Q => pl_interrupt_rdata(8),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(9),
      Q => pl_interrupt_rdata(9),
      R => axi_awready_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => pl_interrupt_arvalid,
      I2 => \^pl_interrupt_rvalid\,
      I3 => pl_interrupt_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^pl_interrupt_rvalid\,
      R => axi_awready_i_1_n_0
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => pl_interrupt_awvalid,
      I1 => pl_interrupt_wvalid,
      I2 => \^s_axi_wready\,
      I3 => aw_en_reg_n_0,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => axi_awready_i_1_n_0
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => pl_interrupt_wstrb(1),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg1[15]_i_1_n_0\
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => pl_interrupt_wstrb(2),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg1[23]_i_1_n_0\
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => pl_interrupt_wstrb(3),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg1[31]_i_1_n_0\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => pl_interrupt_wstrb(0),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg1[7]_i_1_n_0\
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => pl_interrupt_wdata(0),
      Q => slv_reg1(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => pl_interrupt_wdata(10),
      Q => slv_reg1(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => pl_interrupt_wdata(11),
      Q => slv_reg1(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => pl_interrupt_wdata(12),
      Q => slv_reg1(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => pl_interrupt_wdata(13),
      Q => slv_reg1(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => pl_interrupt_wdata(14),
      Q => slv_reg1(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => pl_interrupt_wdata(15),
      Q => slv_reg1(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => pl_interrupt_wdata(16),
      Q => slv_reg1(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => pl_interrupt_wdata(17),
      Q => slv_reg1(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => pl_interrupt_wdata(18),
      Q => slv_reg1(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => pl_interrupt_wdata(19),
      Q => slv_reg1(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => pl_interrupt_wdata(1),
      Q => slv_reg1(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => pl_interrupt_wdata(20),
      Q => slv_reg1(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => pl_interrupt_wdata(21),
      Q => slv_reg1(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => pl_interrupt_wdata(22),
      Q => slv_reg1(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => pl_interrupt_wdata(23),
      Q => slv_reg1(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => pl_interrupt_wdata(24),
      Q => slv_reg1(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => pl_interrupt_wdata(25),
      Q => slv_reg1(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => pl_interrupt_wdata(26),
      Q => slv_reg1(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => pl_interrupt_wdata(27),
      Q => slv_reg1(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => pl_interrupt_wdata(28),
      Q => slv_reg1(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => pl_interrupt_wdata(29),
      Q => slv_reg1(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => pl_interrupt_wdata(2),
      Q => slv_reg1(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => pl_interrupt_wdata(30),
      Q => slv_reg1(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => pl_interrupt_wdata(31),
      Q => slv_reg1(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => pl_interrupt_wdata(3),
      Q => slv_reg1(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => pl_interrupt_wdata(4),
      Q => slv_reg1(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => pl_interrupt_wdata(5),
      Q => slv_reg1(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => pl_interrupt_wdata(6),
      Q => slv_reg1(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => pl_interrupt_wdata(7),
      Q => slv_reg1(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => pl_interrupt_wdata(8),
      Q => slv_reg1(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => pl_interrupt_wdata(9),
      Q => slv_reg1(9),
      R => axi_awready_i_1_n_0
    );
\slv_reg2[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => pl_interrupt_wstrb(1),
      I3 => p_0_in(0),
      O => \slv_reg2[15]_i_1_n_0\
    );
\slv_reg2[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => pl_interrupt_wstrb(2),
      I3 => p_0_in(0),
      O => \slv_reg2[23]_i_1_n_0\
    );
\slv_reg2[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => pl_interrupt_wstrb(3),
      I3 => p_0_in(0),
      O => \slv_reg2[31]_i_1_n_0\
    );
\slv_reg2[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(1),
      I2 => pl_interrupt_wstrb(0),
      I3 => p_0_in(0),
      O => \slv_reg2[7]_i_1_n_0\
    );
\slv_reg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => pl_interrupt_wdata(0),
      Q => slv_reg2(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => pl_interrupt_wdata(10),
      Q => slv_reg2(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => pl_interrupt_wdata(11),
      Q => slv_reg2(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => pl_interrupt_wdata(12),
      Q => slv_reg2(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => pl_interrupt_wdata(13),
      Q => slv_reg2(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => pl_interrupt_wdata(14),
      Q => slv_reg2(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => pl_interrupt_wdata(15),
      Q => slv_reg2(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => pl_interrupt_wdata(16),
      Q => slv_reg2(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => pl_interrupt_wdata(17),
      Q => slv_reg2(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => pl_interrupt_wdata(18),
      Q => slv_reg2(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => pl_interrupt_wdata(19),
      Q => slv_reg2(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => pl_interrupt_wdata(1),
      Q => slv_reg2(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => pl_interrupt_wdata(20),
      Q => slv_reg2(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => pl_interrupt_wdata(21),
      Q => slv_reg2(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => pl_interrupt_wdata(22),
      Q => slv_reg2(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => pl_interrupt_wdata(23),
      Q => slv_reg2(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => pl_interrupt_wdata(24),
      Q => slv_reg2(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => pl_interrupt_wdata(25),
      Q => slv_reg2(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => pl_interrupt_wdata(26),
      Q => slv_reg2(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => pl_interrupt_wdata(27),
      Q => slv_reg2(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => pl_interrupt_wdata(28),
      Q => slv_reg2(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => pl_interrupt_wdata(29),
      Q => slv_reg2(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => pl_interrupt_wdata(2),
      Q => slv_reg2(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => pl_interrupt_wdata(30),
      Q => slv_reg2(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => pl_interrupt_wdata(31),
      Q => slv_reg2(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => pl_interrupt_wdata(3),
      Q => slv_reg2(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => pl_interrupt_wdata(4),
      Q => slv_reg2(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => pl_interrupt_wdata(5),
      Q => slv_reg2(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => pl_interrupt_wdata(6),
      Q => slv_reg2(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => pl_interrupt_wdata(7),
      Q => slv_reg2(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => pl_interrupt_wdata(8),
      Q => slv_reg2(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => pl_interrupt_wdata(9),
      Q => slv_reg2(9),
      R => axi_awready_i_1_n_0
    );
\slv_reg3[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => pl_interrupt_wstrb(1),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg3[15]_i_1_n_0\
    );
\slv_reg3[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => pl_interrupt_wstrb(2),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg3[23]_i_1_n_0\
    );
\slv_reg3[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => pl_interrupt_wstrb(3),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg3[31]_i_1_n_0\
    );
\slv_reg3[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^s_axi_wready\,
      I1 => \^s_axi_awready\,
      I2 => pl_interrupt_awvalid,
      I3 => pl_interrupt_wvalid,
      O => \slv_reg_wren__0\
    );
\slv_reg3[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => pl_interrupt_wstrb(0),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      O => \slv_reg3[7]_i_1_n_0\
    );
\slv_reg3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => pl_interrupt_wdata(0),
      Q => slv_reg3(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => pl_interrupt_wdata(10),
      Q => slv_reg3(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => pl_interrupt_wdata(11),
      Q => slv_reg3(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => pl_interrupt_wdata(12),
      Q => slv_reg3(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => pl_interrupt_wdata(13),
      Q => slv_reg3(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => pl_interrupt_wdata(14),
      Q => slv_reg3(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => pl_interrupt_wdata(15),
      Q => slv_reg3(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => pl_interrupt_wdata(16),
      Q => slv_reg3(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => pl_interrupt_wdata(17),
      Q => slv_reg3(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => pl_interrupt_wdata(18),
      Q => slv_reg3(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => pl_interrupt_wdata(19),
      Q => slv_reg3(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => pl_interrupt_wdata(1),
      Q => slv_reg3(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => pl_interrupt_wdata(20),
      Q => slv_reg3(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => pl_interrupt_wdata(21),
      Q => slv_reg3(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => pl_interrupt_wdata(22),
      Q => slv_reg3(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => pl_interrupt_wdata(23),
      Q => slv_reg3(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => pl_interrupt_wdata(24),
      Q => slv_reg3(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => pl_interrupt_wdata(25),
      Q => slv_reg3(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => pl_interrupt_wdata(26),
      Q => slv_reg3(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => pl_interrupt_wdata(27),
      Q => slv_reg3(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => pl_interrupt_wdata(28),
      Q => slv_reg3(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => pl_interrupt_wdata(29),
      Q => slv_reg3(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => pl_interrupt_wdata(2),
      Q => slv_reg3(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => pl_interrupt_wdata(30),
      Q => slv_reg3(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => pl_interrupt_wdata(31),
      Q => slv_reg3(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => pl_interrupt_wdata(3),
      Q => slv_reg3(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => pl_interrupt_wdata(4),
      Q => slv_reg3(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => pl_interrupt_wdata(5),
      Q => slv_reg3(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => pl_interrupt_wdata(6),
      Q => slv_reg3(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => pl_interrupt_wdata(7),
      Q => slv_reg3(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => pl_interrupt_wdata(8),
      Q => slv_reg3(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pl_interrupt_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => pl_interrupt_wdata(9),
      Q => slv_reg3(9),
      R => axi_awready_i_1_n_0
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => pl_interrupt_arvalid,
      I1 => \^pl_interrupt_rvalid\,
      I2 => \^s_axi_arready\,
      O => \slv_reg_rden__0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pl_interrupt_v1_0 is
  port (
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    pl_interrupt_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    pl_interrupt_rvalid : out STD_LOGIC;
    pl_interrupt_bvalid : out STD_LOGIC;
    pl_interrupt_aclk : in STD_LOGIC;
    pl_interrupt_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    pl_interrupt_arvalid : in STD_LOGIC;
    pl_interrupt_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    pl_interrupt_wvalid : in STD_LOGIC;
    pl_interrupt_awvalid : in STD_LOGIC;
    pl_interrupt_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    mid : in STD_LOGIC;
    right : in STD_LOGIC;
    left : in STD_LOGIC;
    down : in STD_LOGIC;
    up : in STD_LOGIC;
    pl_interrupt_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pl_interrupt_aresetn : in STD_LOGIC;
    pl_interrupt_bready : in STD_LOGIC;
    pl_interrupt_rready : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pl_interrupt_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pl_interrupt_v1_0 is
begin
pl_interrupt_v1_0_pl_interrupt_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pl_interrupt_v1_0_pl_interrupt
     port map (
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WREADY => S_AXI_WREADY,
      down => down,
      left => left,
      mid => mid,
      pl_interrupt_aclk => pl_interrupt_aclk,
      pl_interrupt_araddr(1 downto 0) => pl_interrupt_araddr(1 downto 0),
      pl_interrupt_aresetn => pl_interrupt_aresetn,
      pl_interrupt_arvalid => pl_interrupt_arvalid,
      pl_interrupt_awaddr(1 downto 0) => pl_interrupt_awaddr(1 downto 0),
      pl_interrupt_awvalid => pl_interrupt_awvalid,
      pl_interrupt_bready => pl_interrupt_bready,
      pl_interrupt_bvalid => pl_interrupt_bvalid,
      pl_interrupt_rdata(31 downto 0) => pl_interrupt_rdata(31 downto 0),
      pl_interrupt_rready => pl_interrupt_rready,
      pl_interrupt_rvalid => pl_interrupt_rvalid,
      pl_interrupt_wdata(31 downto 0) => pl_interrupt_wdata(31 downto 0),
      pl_interrupt_wstrb(3 downto 0) => pl_interrupt_wstrb(3 downto 0),
      pl_interrupt_wvalid => pl_interrupt_wvalid,
      right => right,
      up => up
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    up : in STD_LOGIC;
    down : in STD_LOGIC;
    left : in STD_LOGIC;
    right : in STD_LOGIC;
    mid : in STD_LOGIC;
    interrupt_up : out STD_LOGIC;
    interrupt_down : out STD_LOGIC;
    interrupt_left : out STD_LOGIC;
    interrupt_right : out STD_LOGIC;
    interrupt_mid : out STD_LOGIC;
    pl_interrupt_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pl_interrupt_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    pl_interrupt_awvalid : in STD_LOGIC;
    pl_interrupt_awready : out STD_LOGIC;
    pl_interrupt_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    pl_interrupt_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pl_interrupt_wvalid : in STD_LOGIC;
    pl_interrupt_wready : out STD_LOGIC;
    pl_interrupt_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pl_interrupt_bvalid : out STD_LOGIC;
    pl_interrupt_bready : in STD_LOGIC;
    pl_interrupt_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pl_interrupt_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    pl_interrupt_arvalid : in STD_LOGIC;
    pl_interrupt_arready : out STD_LOGIC;
    pl_interrupt_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    pl_interrupt_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pl_interrupt_rvalid : out STD_LOGIC;
    pl_interrupt_rready : in STD_LOGIC;
    pl_interrupt_aclk : in STD_LOGIC;
    pl_interrupt_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "tetris_pl_interrupt_0_3,pl_interrupt_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "pl_interrupt_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \^down\ : STD_LOGIC;
  signal \^left\ : STD_LOGIC;
  signal \^mid\ : STD_LOGIC;
  signal \^right\ : STD_LOGIC;
  signal \^up\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of pl_interrupt_aclk : signal is "xilinx.com:signal:clock:1.0 pl_interrupt_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of pl_interrupt_aclk : signal is "XIL_INTERFACENAME pl_interrupt_CLK, ASSOCIATED_BUSIF pl_interrupt, ASSOCIATED_RESET pl_interrupt_aresetn:pl_interrupt_RST, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN tetris_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of pl_interrupt_aresetn : signal is "xilinx.com:signal:reset:1.0 pl_interrupt_RST RST";
  attribute X_INTERFACE_PARAMETER of pl_interrupt_aresetn : signal is "XIL_INTERFACENAME pl_interrupt_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of pl_interrupt_arready : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt ARREADY";
  attribute X_INTERFACE_INFO of pl_interrupt_arvalid : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt ARVALID";
  attribute X_INTERFACE_INFO of pl_interrupt_awready : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt AWREADY";
  attribute X_INTERFACE_INFO of pl_interrupt_awvalid : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt AWVALID";
  attribute X_INTERFACE_INFO of pl_interrupt_bready : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt BREADY";
  attribute X_INTERFACE_INFO of pl_interrupt_bvalid : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt BVALID";
  attribute X_INTERFACE_INFO of pl_interrupt_rready : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt RREADY";
  attribute X_INTERFACE_PARAMETER of pl_interrupt_rready : signal is "XIL_INTERFACENAME pl_interrupt, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN tetris_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of pl_interrupt_rvalid : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt RVALID";
  attribute X_INTERFACE_INFO of pl_interrupt_wready : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt WREADY";
  attribute X_INTERFACE_INFO of pl_interrupt_wvalid : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt WVALID";
  attribute X_INTERFACE_INFO of pl_interrupt_araddr : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt ARADDR";
  attribute X_INTERFACE_INFO of pl_interrupt_arprot : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt ARPROT";
  attribute X_INTERFACE_INFO of pl_interrupt_awaddr : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt AWADDR";
  attribute X_INTERFACE_INFO of pl_interrupt_awprot : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt AWPROT";
  attribute X_INTERFACE_INFO of pl_interrupt_bresp : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt BRESP";
  attribute X_INTERFACE_INFO of pl_interrupt_rdata : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt RDATA";
  attribute X_INTERFACE_INFO of pl_interrupt_rresp : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt RRESP";
  attribute X_INTERFACE_INFO of pl_interrupt_wdata : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt WDATA";
  attribute X_INTERFACE_INFO of pl_interrupt_wstrb : signal is "xilinx.com:interface:aximm:1.0 pl_interrupt WSTRB";
begin
  \^down\ <= down;
  \^left\ <= left;
  \^mid\ <= mid;
  \^right\ <= right;
  \^up\ <= up;
  interrupt_down <= \^down\;
  interrupt_left <= \^left\;
  interrupt_mid <= \^mid\;
  interrupt_right <= \^right\;
  interrupt_up <= \^up\;
  pl_interrupt_bresp(1) <= \<const0>\;
  pl_interrupt_bresp(0) <= \<const0>\;
  pl_interrupt_rresp(1) <= \<const0>\;
  pl_interrupt_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pl_interrupt_v1_0
     port map (
      S_AXI_ARREADY => pl_interrupt_arready,
      S_AXI_AWREADY => pl_interrupt_awready,
      S_AXI_WREADY => pl_interrupt_wready,
      down => \^down\,
      left => \^left\,
      mid => \^mid\,
      pl_interrupt_aclk => pl_interrupt_aclk,
      pl_interrupt_araddr(1 downto 0) => pl_interrupt_araddr(3 downto 2),
      pl_interrupt_aresetn => pl_interrupt_aresetn,
      pl_interrupt_arvalid => pl_interrupt_arvalid,
      pl_interrupt_awaddr(1 downto 0) => pl_interrupt_awaddr(3 downto 2),
      pl_interrupt_awvalid => pl_interrupt_awvalid,
      pl_interrupt_bready => pl_interrupt_bready,
      pl_interrupt_bvalid => pl_interrupt_bvalid,
      pl_interrupt_rdata(31 downto 0) => pl_interrupt_rdata(31 downto 0),
      pl_interrupt_rready => pl_interrupt_rready,
      pl_interrupt_rvalid => pl_interrupt_rvalid,
      pl_interrupt_wdata(31 downto 0) => pl_interrupt_wdata(31 downto 0),
      pl_interrupt_wstrb(3 downto 0) => pl_interrupt_wstrb(3 downto 0),
      pl_interrupt_wvalid => pl_interrupt_wvalid,
      right => \^right\,
      up => \^up\
    );
end STRUCTURE;
