// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Tue Mar 17 12:32:14 2020
// Host        : notg00datmath running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ tetris_pl_interrupt_0_3_stub.v
// Design      : tetris_pl_interrupt_0_3
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "pl_interrupt_v1_0,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(up, down, left, right, mid, interrupt_up, 
  interrupt_down, interrupt_left, interrupt_right, interrupt_mid, pl_interrupt_awaddr, 
  pl_interrupt_awprot, pl_interrupt_awvalid, pl_interrupt_awready, pl_interrupt_wdata, 
  pl_interrupt_wstrb, pl_interrupt_wvalid, pl_interrupt_wready, pl_interrupt_bresp, 
  pl_interrupt_bvalid, pl_interrupt_bready, pl_interrupt_araddr, pl_interrupt_arprot, 
  pl_interrupt_arvalid, pl_interrupt_arready, pl_interrupt_rdata, pl_interrupt_rresp, 
  pl_interrupt_rvalid, pl_interrupt_rready, pl_interrupt_aclk, pl_interrupt_aresetn)
/* synthesis syn_black_box black_box_pad_pin="up,down,left,right,mid,interrupt_up,interrupt_down,interrupt_left,interrupt_right,interrupt_mid,pl_interrupt_awaddr[3:0],pl_interrupt_awprot[2:0],pl_interrupt_awvalid,pl_interrupt_awready,pl_interrupt_wdata[31:0],pl_interrupt_wstrb[3:0],pl_interrupt_wvalid,pl_interrupt_wready,pl_interrupt_bresp[1:0],pl_interrupt_bvalid,pl_interrupt_bready,pl_interrupt_araddr[3:0],pl_interrupt_arprot[2:0],pl_interrupt_arvalid,pl_interrupt_arready,pl_interrupt_rdata[31:0],pl_interrupt_rresp[1:0],pl_interrupt_rvalid,pl_interrupt_rready,pl_interrupt_aclk,pl_interrupt_aresetn" */;
  input up;
  input down;
  input left;
  input right;
  input mid;
  output interrupt_up;
  output interrupt_down;
  output interrupt_left;
  output interrupt_right;
  output interrupt_mid;
  input [3:0]pl_interrupt_awaddr;
  input [2:0]pl_interrupt_awprot;
  input pl_interrupt_awvalid;
  output pl_interrupt_awready;
  input [31:0]pl_interrupt_wdata;
  input [3:0]pl_interrupt_wstrb;
  input pl_interrupt_wvalid;
  output pl_interrupt_wready;
  output [1:0]pl_interrupt_bresp;
  output pl_interrupt_bvalid;
  input pl_interrupt_bready;
  input [3:0]pl_interrupt_araddr;
  input [2:0]pl_interrupt_arprot;
  input pl_interrupt_arvalid;
  output pl_interrupt_arready;
  output [31:0]pl_interrupt_rdata;
  output [1:0]pl_interrupt_rresp;
  output pl_interrupt_rvalid;
  input pl_interrupt_rready;
  input pl_interrupt_aclk;
  input pl_interrupt_aresetn;
endmodule
