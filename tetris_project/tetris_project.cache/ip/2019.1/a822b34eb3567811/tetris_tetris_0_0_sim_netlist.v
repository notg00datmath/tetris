// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Thu Mar 19 19:52:24 2020
// Host        : notg00datmath running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ tetris_tetris_0_0_sim_netlist.v
// Design      : tetris_tetris_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce
   (down_o,
    pixel_clk,
    btn_down);
  output down_o;
  input pixel_clk;
  input btn_down;

  wire \PB_cnt[0]_i_1_n_0 ;
  wire \PB_cnt[0]_i_3_n_0 ;
  wire [15:0]PB_cnt_reg;
  wire \PB_cnt_reg[0]_i_2_n_0 ;
  wire \PB_cnt_reg[0]_i_2_n_1 ;
  wire \PB_cnt_reg[0]_i_2_n_2 ;
  wire \PB_cnt_reg[0]_i_2_n_3 ;
  wire \PB_cnt_reg[0]_i_2_n_4 ;
  wire \PB_cnt_reg[0]_i_2_n_5 ;
  wire \PB_cnt_reg[0]_i_2_n_6 ;
  wire \PB_cnt_reg[0]_i_2_n_7 ;
  wire \PB_cnt_reg[12]_i_1_n_1 ;
  wire \PB_cnt_reg[12]_i_1_n_2 ;
  wire \PB_cnt_reg[12]_i_1_n_3 ;
  wire \PB_cnt_reg[12]_i_1_n_4 ;
  wire \PB_cnt_reg[12]_i_1_n_5 ;
  wire \PB_cnt_reg[12]_i_1_n_6 ;
  wire \PB_cnt_reg[12]_i_1_n_7 ;
  wire \PB_cnt_reg[4]_i_1_n_0 ;
  wire \PB_cnt_reg[4]_i_1_n_1 ;
  wire \PB_cnt_reg[4]_i_1_n_2 ;
  wire \PB_cnt_reg[4]_i_1_n_3 ;
  wire \PB_cnt_reg[4]_i_1_n_4 ;
  wire \PB_cnt_reg[4]_i_1_n_5 ;
  wire \PB_cnt_reg[4]_i_1_n_6 ;
  wire \PB_cnt_reg[4]_i_1_n_7 ;
  wire \PB_cnt_reg[8]_i_1_n_0 ;
  wire \PB_cnt_reg[8]_i_1_n_1 ;
  wire \PB_cnt_reg[8]_i_1_n_2 ;
  wire \PB_cnt_reg[8]_i_1_n_3 ;
  wire \PB_cnt_reg[8]_i_1_n_4 ;
  wire \PB_cnt_reg[8]_i_1_n_5 ;
  wire \PB_cnt_reg[8]_i_1_n_6 ;
  wire \PB_cnt_reg[8]_i_1_n_7 ;
  wire PB_state;
  wire PB_state_i_1_n_0;
  wire PB_sync_0;
  wire PB_sync_1;
  wire btn_down;
  wire down_o;
  wire down_o_INST_0_i_2_n_0;
  wire down_o_INST_0_i_3_n_0;
  wire down_o_INST_0_i_4_n_0;
  wire p_0_in;
  wire p_2_in;
  wire pixel_clk;
  wire [3:3]\NLW_PB_cnt_reg[12]_i_1_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h9)) 
    \PB_cnt[0]_i_1 
       (.I0(PB_state),
        .I1(PB_sync_1),
        .O(\PB_cnt[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \PB_cnt[0]_i_3 
       (.I0(PB_cnt_reg[0]),
        .O(\PB_cnt[0]_i_3_n_0 ));
  FDRE \PB_cnt_reg[0] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2_n_7 ),
        .Q(PB_cnt_reg[0]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  CARRY4 \PB_cnt_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\PB_cnt_reg[0]_i_2_n_0 ,\PB_cnt_reg[0]_i_2_n_1 ,\PB_cnt_reg[0]_i_2_n_2 ,\PB_cnt_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\PB_cnt_reg[0]_i_2_n_4 ,\PB_cnt_reg[0]_i_2_n_5 ,\PB_cnt_reg[0]_i_2_n_6 ,\PB_cnt_reg[0]_i_2_n_7 }),
        .S({PB_cnt_reg[3:1],\PB_cnt[0]_i_3_n_0 }));
  FDRE \PB_cnt_reg[10] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1_n_5 ),
        .Q(PB_cnt_reg[10]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  FDRE \PB_cnt_reg[11] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1_n_4 ),
        .Q(PB_cnt_reg[11]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  FDRE \PB_cnt_reg[12] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1_n_7 ),
        .Q(PB_cnt_reg[12]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  CARRY4 \PB_cnt_reg[12]_i_1 
       (.CI(\PB_cnt_reg[8]_i_1_n_0 ),
        .CO({\NLW_PB_cnt_reg[12]_i_1_CO_UNCONNECTED [3],\PB_cnt_reg[12]_i_1_n_1 ,\PB_cnt_reg[12]_i_1_n_2 ,\PB_cnt_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[12]_i_1_n_4 ,\PB_cnt_reg[12]_i_1_n_5 ,\PB_cnt_reg[12]_i_1_n_6 ,\PB_cnt_reg[12]_i_1_n_7 }),
        .S(PB_cnt_reg[15:12]));
  FDRE \PB_cnt_reg[13] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1_n_6 ),
        .Q(PB_cnt_reg[13]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  FDRE \PB_cnt_reg[14] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1_n_5 ),
        .Q(PB_cnt_reg[14]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  FDRE \PB_cnt_reg[15] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1_n_4 ),
        .Q(PB_cnt_reg[15]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  FDRE \PB_cnt_reg[1] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2_n_6 ),
        .Q(PB_cnt_reg[1]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  FDRE \PB_cnt_reg[2] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2_n_5 ),
        .Q(PB_cnt_reg[2]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  FDRE \PB_cnt_reg[3] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2_n_4 ),
        .Q(PB_cnt_reg[3]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  FDRE \PB_cnt_reg[4] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1_n_7 ),
        .Q(PB_cnt_reg[4]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  CARRY4 \PB_cnt_reg[4]_i_1 
       (.CI(\PB_cnt_reg[0]_i_2_n_0 ),
        .CO({\PB_cnt_reg[4]_i_1_n_0 ,\PB_cnt_reg[4]_i_1_n_1 ,\PB_cnt_reg[4]_i_1_n_2 ,\PB_cnt_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[4]_i_1_n_4 ,\PB_cnt_reg[4]_i_1_n_5 ,\PB_cnt_reg[4]_i_1_n_6 ,\PB_cnt_reg[4]_i_1_n_7 }),
        .S(PB_cnt_reg[7:4]));
  FDRE \PB_cnt_reg[5] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1_n_6 ),
        .Q(PB_cnt_reg[5]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  FDRE \PB_cnt_reg[6] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1_n_5 ),
        .Q(PB_cnt_reg[6]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  FDRE \PB_cnt_reg[7] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1_n_4 ),
        .Q(PB_cnt_reg[7]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  FDRE \PB_cnt_reg[8] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1_n_7 ),
        .Q(PB_cnt_reg[8]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  CARRY4 \PB_cnt_reg[8]_i_1 
       (.CI(\PB_cnt_reg[4]_i_1_n_0 ),
        .CO({\PB_cnt_reg[8]_i_1_n_0 ,\PB_cnt_reg[8]_i_1_n_1 ,\PB_cnt_reg[8]_i_1_n_2 ,\PB_cnt_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[8]_i_1_n_4 ,\PB_cnt_reg[8]_i_1_n_5 ,\PB_cnt_reg[8]_i_1_n_6 ,\PB_cnt_reg[8]_i_1_n_7 }),
        .S(PB_cnt_reg[11:8]));
  FDRE \PB_cnt_reg[9] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1_n_6 ),
        .Q(PB_cnt_reg[9]),
        .R(\PB_cnt[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h6)) 
    PB_state_i_1
       (.I0(p_2_in),
        .I1(PB_state),
        .O(PB_state_i_1_n_0));
  FDRE PB_state_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_state_i_1_n_0),
        .Q(PB_state),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    PB_sync_0_i_1
       (.I0(btn_down),
        .O(p_0_in));
  FDRE PB_sync_0_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(p_0_in),
        .Q(PB_sync_0),
        .R(1'b0));
  FDRE PB_sync_1_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0),
        .Q(PB_sync_1),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    down_o_INST_0
       (.I0(p_2_in),
        .I1(PB_state),
        .O(down_o));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    down_o_INST_0_i_1
       (.I0(down_o_INST_0_i_2_n_0),
        .I1(PB_cnt_reg[2]),
        .I2(PB_cnt_reg[3]),
        .I3(PB_cnt_reg[4]),
        .I4(PB_cnt_reg[15]),
        .I5(down_o_INST_0_i_3_n_0),
        .O(p_2_in));
  LUT4 #(
    .INIT(16'h7FFF)) 
    down_o_INST_0_i_2
       (.I0(PB_cnt_reg[5]),
        .I1(PB_cnt_reg[8]),
        .I2(PB_cnt_reg[1]),
        .I3(PB_cnt_reg[9]),
        .O(down_o_INST_0_i_2_n_0));
  LUT6 #(
    .INIT(64'hEFFFFFFFFFFFFFFF)) 
    down_o_INST_0_i_3
       (.I0(\PB_cnt[0]_i_1_n_0 ),
        .I1(down_o_INST_0_i_4_n_0),
        .I2(PB_cnt_reg[10]),
        .I3(PB_cnt_reg[12]),
        .I4(PB_cnt_reg[11]),
        .I5(PB_cnt_reg[0]),
        .O(down_o_INST_0_i_3_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    down_o_INST_0_i_4
       (.I0(PB_cnt_reg[6]),
        .I1(PB_cnt_reg[13]),
        .I2(PB_cnt_reg[7]),
        .I3(PB_cnt_reg[14]),
        .O(down_o_INST_0_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "debounce" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_0
   (drop_o,
    pixel_clk,
    btn_drop);
  output drop_o;
  input pixel_clk;
  input btn_drop;

  wire \PB_cnt[0]_i_1__3_n_0 ;
  wire \PB_cnt[0]_i_3__3_n_0 ;
  wire [15:0]PB_cnt_reg;
  wire \PB_cnt_reg[0]_i_2__3_n_0 ;
  wire \PB_cnt_reg[0]_i_2__3_n_1 ;
  wire \PB_cnt_reg[0]_i_2__3_n_2 ;
  wire \PB_cnt_reg[0]_i_2__3_n_3 ;
  wire \PB_cnt_reg[0]_i_2__3_n_4 ;
  wire \PB_cnt_reg[0]_i_2__3_n_5 ;
  wire \PB_cnt_reg[0]_i_2__3_n_6 ;
  wire \PB_cnt_reg[0]_i_2__3_n_7 ;
  wire \PB_cnt_reg[12]_i_1__3_n_1 ;
  wire \PB_cnt_reg[12]_i_1__3_n_2 ;
  wire \PB_cnt_reg[12]_i_1__3_n_3 ;
  wire \PB_cnt_reg[12]_i_1__3_n_4 ;
  wire \PB_cnt_reg[12]_i_1__3_n_5 ;
  wire \PB_cnt_reg[12]_i_1__3_n_6 ;
  wire \PB_cnt_reg[12]_i_1__3_n_7 ;
  wire \PB_cnt_reg[4]_i_1__3_n_0 ;
  wire \PB_cnt_reg[4]_i_1__3_n_1 ;
  wire \PB_cnt_reg[4]_i_1__3_n_2 ;
  wire \PB_cnt_reg[4]_i_1__3_n_3 ;
  wire \PB_cnt_reg[4]_i_1__3_n_4 ;
  wire \PB_cnt_reg[4]_i_1__3_n_5 ;
  wire \PB_cnt_reg[4]_i_1__3_n_6 ;
  wire \PB_cnt_reg[4]_i_1__3_n_7 ;
  wire \PB_cnt_reg[8]_i_1__3_n_0 ;
  wire \PB_cnt_reg[8]_i_1__3_n_1 ;
  wire \PB_cnt_reg[8]_i_1__3_n_2 ;
  wire \PB_cnt_reg[8]_i_1__3_n_3 ;
  wire \PB_cnt_reg[8]_i_1__3_n_4 ;
  wire \PB_cnt_reg[8]_i_1__3_n_5 ;
  wire \PB_cnt_reg[8]_i_1__3_n_6 ;
  wire \PB_cnt_reg[8]_i_1__3_n_7 ;
  wire PB_state;
  wire PB_state_i_1__1_n_0;
  wire PB_sync_0_i_1__3_n_0;
  wire PB_sync_0_reg_n_0;
  wire PB_sync_1_reg_n_0;
  wire btn_drop;
  wire drop_o;
  wire drop_o_INST_0_i_2_n_0;
  wire drop_o_INST_0_i_3_n_0;
  wire drop_o_INST_0_i_4_n_0;
  wire p_2_in;
  wire pixel_clk;
  wire [3:3]\NLW_PB_cnt_reg[12]_i_1__3_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h9)) 
    \PB_cnt[0]_i_1__3 
       (.I0(PB_state),
        .I1(PB_sync_1_reg_n_0),
        .O(\PB_cnt[0]_i_1__3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \PB_cnt[0]_i_3__3 
       (.I0(PB_cnt_reg[0]),
        .O(\PB_cnt[0]_i_3__3_n_0 ));
  FDRE \PB_cnt_reg[0] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__3_n_7 ),
        .Q(PB_cnt_reg[0]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  CARRY4 \PB_cnt_reg[0]_i_2__3 
       (.CI(1'b0),
        .CO({\PB_cnt_reg[0]_i_2__3_n_0 ,\PB_cnt_reg[0]_i_2__3_n_1 ,\PB_cnt_reg[0]_i_2__3_n_2 ,\PB_cnt_reg[0]_i_2__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\PB_cnt_reg[0]_i_2__3_n_4 ,\PB_cnt_reg[0]_i_2__3_n_5 ,\PB_cnt_reg[0]_i_2__3_n_6 ,\PB_cnt_reg[0]_i_2__3_n_7 }),
        .S({PB_cnt_reg[3:1],\PB_cnt[0]_i_3__3_n_0 }));
  FDRE \PB_cnt_reg[10] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__3_n_5 ),
        .Q(PB_cnt_reg[10]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  FDRE \PB_cnt_reg[11] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__3_n_4 ),
        .Q(PB_cnt_reg[11]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  FDRE \PB_cnt_reg[12] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__3_n_7 ),
        .Q(PB_cnt_reg[12]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  CARRY4 \PB_cnt_reg[12]_i_1__3 
       (.CI(\PB_cnt_reg[8]_i_1__3_n_0 ),
        .CO({\NLW_PB_cnt_reg[12]_i_1__3_CO_UNCONNECTED [3],\PB_cnt_reg[12]_i_1__3_n_1 ,\PB_cnt_reg[12]_i_1__3_n_2 ,\PB_cnt_reg[12]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[12]_i_1__3_n_4 ,\PB_cnt_reg[12]_i_1__3_n_5 ,\PB_cnt_reg[12]_i_1__3_n_6 ,\PB_cnt_reg[12]_i_1__3_n_7 }),
        .S(PB_cnt_reg[15:12]));
  FDRE \PB_cnt_reg[13] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__3_n_6 ),
        .Q(PB_cnt_reg[13]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  FDRE \PB_cnt_reg[14] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__3_n_5 ),
        .Q(PB_cnt_reg[14]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  FDRE \PB_cnt_reg[15] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__3_n_4 ),
        .Q(PB_cnt_reg[15]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  FDRE \PB_cnt_reg[1] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__3_n_6 ),
        .Q(PB_cnt_reg[1]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  FDRE \PB_cnt_reg[2] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__3_n_5 ),
        .Q(PB_cnt_reg[2]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  FDRE \PB_cnt_reg[3] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__3_n_4 ),
        .Q(PB_cnt_reg[3]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  FDRE \PB_cnt_reg[4] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__3_n_7 ),
        .Q(PB_cnt_reg[4]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  CARRY4 \PB_cnt_reg[4]_i_1__3 
       (.CI(\PB_cnt_reg[0]_i_2__3_n_0 ),
        .CO({\PB_cnt_reg[4]_i_1__3_n_0 ,\PB_cnt_reg[4]_i_1__3_n_1 ,\PB_cnt_reg[4]_i_1__3_n_2 ,\PB_cnt_reg[4]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[4]_i_1__3_n_4 ,\PB_cnt_reg[4]_i_1__3_n_5 ,\PB_cnt_reg[4]_i_1__3_n_6 ,\PB_cnt_reg[4]_i_1__3_n_7 }),
        .S(PB_cnt_reg[7:4]));
  FDRE \PB_cnt_reg[5] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__3_n_6 ),
        .Q(PB_cnt_reg[5]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  FDRE \PB_cnt_reg[6] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__3_n_5 ),
        .Q(PB_cnt_reg[6]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  FDRE \PB_cnt_reg[7] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__3_n_4 ),
        .Q(PB_cnt_reg[7]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  FDRE \PB_cnt_reg[8] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__3_n_7 ),
        .Q(PB_cnt_reg[8]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  CARRY4 \PB_cnt_reg[8]_i_1__3 
       (.CI(\PB_cnt_reg[4]_i_1__3_n_0 ),
        .CO({\PB_cnt_reg[8]_i_1__3_n_0 ,\PB_cnt_reg[8]_i_1__3_n_1 ,\PB_cnt_reg[8]_i_1__3_n_2 ,\PB_cnt_reg[8]_i_1__3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[8]_i_1__3_n_4 ,\PB_cnt_reg[8]_i_1__3_n_5 ,\PB_cnt_reg[8]_i_1__3_n_6 ,\PB_cnt_reg[8]_i_1__3_n_7 }),
        .S(PB_cnt_reg[11:8]));
  FDRE \PB_cnt_reg[9] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__3_n_6 ),
        .Q(PB_cnt_reg[9]),
        .R(\PB_cnt[0]_i_1__3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h6)) 
    PB_state_i_1__1
       (.I0(p_2_in),
        .I1(PB_state),
        .O(PB_state_i_1__1_n_0));
  FDRE PB_state_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_state_i_1__1_n_0),
        .Q(PB_state),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    PB_sync_0_i_1__3
       (.I0(btn_drop),
        .O(PB_sync_0_i_1__3_n_0));
  FDRE PB_sync_0_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0_i_1__3_n_0),
        .Q(PB_sync_0_reg_n_0),
        .R(1'b0));
  FDRE PB_sync_1_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0_reg_n_0),
        .Q(PB_sync_1_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    drop_o_INST_0
       (.I0(p_2_in),
        .I1(PB_state),
        .O(drop_o));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    drop_o_INST_0_i_1
       (.I0(drop_o_INST_0_i_2_n_0),
        .I1(PB_cnt_reg[2]),
        .I2(PB_cnt_reg[3]),
        .I3(PB_cnt_reg[4]),
        .I4(PB_cnt_reg[15]),
        .I5(drop_o_INST_0_i_3_n_0),
        .O(p_2_in));
  LUT4 #(
    .INIT(16'h7FFF)) 
    drop_o_INST_0_i_2
       (.I0(PB_cnt_reg[5]),
        .I1(PB_cnt_reg[8]),
        .I2(PB_cnt_reg[1]),
        .I3(PB_cnt_reg[9]),
        .O(drop_o_INST_0_i_2_n_0));
  LUT6 #(
    .INIT(64'hEFFFFFFFFFFFFFFF)) 
    drop_o_INST_0_i_3
       (.I0(\PB_cnt[0]_i_1__3_n_0 ),
        .I1(drop_o_INST_0_i_4_n_0),
        .I2(PB_cnt_reg[10]),
        .I3(PB_cnt_reg[12]),
        .I4(PB_cnt_reg[11]),
        .I5(PB_cnt_reg[0]),
        .O(drop_o_INST_0_i_3_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    drop_o_INST_0_i_4
       (.I0(PB_cnt_reg[6]),
        .I1(PB_cnt_reg[13]),
        .I2(PB_cnt_reg[7]),
        .I3(PB_cnt_reg[14]),
        .O(drop_o_INST_0_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "debounce" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_1
   (left_o,
    pixel_clk,
    btn_left);
  output left_o;
  input pixel_clk;
  input btn_left;

  wire \PB_cnt[0]_i_1__0_n_0 ;
  wire \PB_cnt[0]_i_3__0_n_0 ;
  wire [15:0]PB_cnt_reg;
  wire \PB_cnt_reg[0]_i_2__0_n_0 ;
  wire \PB_cnt_reg[0]_i_2__0_n_1 ;
  wire \PB_cnt_reg[0]_i_2__0_n_2 ;
  wire \PB_cnt_reg[0]_i_2__0_n_3 ;
  wire \PB_cnt_reg[0]_i_2__0_n_4 ;
  wire \PB_cnt_reg[0]_i_2__0_n_5 ;
  wire \PB_cnt_reg[0]_i_2__0_n_6 ;
  wire \PB_cnt_reg[0]_i_2__0_n_7 ;
  wire \PB_cnt_reg[12]_i_1__0_n_1 ;
  wire \PB_cnt_reg[12]_i_1__0_n_2 ;
  wire \PB_cnt_reg[12]_i_1__0_n_3 ;
  wire \PB_cnt_reg[12]_i_1__0_n_4 ;
  wire \PB_cnt_reg[12]_i_1__0_n_5 ;
  wire \PB_cnt_reg[12]_i_1__0_n_6 ;
  wire \PB_cnt_reg[12]_i_1__0_n_7 ;
  wire \PB_cnt_reg[4]_i_1__0_n_0 ;
  wire \PB_cnt_reg[4]_i_1__0_n_1 ;
  wire \PB_cnt_reg[4]_i_1__0_n_2 ;
  wire \PB_cnt_reg[4]_i_1__0_n_3 ;
  wire \PB_cnt_reg[4]_i_1__0_n_4 ;
  wire \PB_cnt_reg[4]_i_1__0_n_5 ;
  wire \PB_cnt_reg[4]_i_1__0_n_6 ;
  wire \PB_cnt_reg[4]_i_1__0_n_7 ;
  wire \PB_cnt_reg[8]_i_1__0_n_0 ;
  wire \PB_cnt_reg[8]_i_1__0_n_1 ;
  wire \PB_cnt_reg[8]_i_1__0_n_2 ;
  wire \PB_cnt_reg[8]_i_1__0_n_3 ;
  wire \PB_cnt_reg[8]_i_1__0_n_4 ;
  wire \PB_cnt_reg[8]_i_1__0_n_5 ;
  wire \PB_cnt_reg[8]_i_1__0_n_6 ;
  wire \PB_cnt_reg[8]_i_1__0_n_7 ;
  wire PB_state;
  wire PB_state_i_1__4_n_0;
  wire PB_sync_0_i_1__0_n_0;
  wire PB_sync_0_reg_n_0;
  wire PB_sync_1_reg_n_0;
  wire btn_left;
  wire left_o;
  wire left_o_INST_0_i_1_n_0;
  wire left_o_INST_0_i_2_n_0;
  wire left_o_INST_0_i_3_n_0;
  wire left_o_INST_0_i_4_n_0;
  wire pixel_clk;
  wire [3:3]\NLW_PB_cnt_reg[12]_i_1__0_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h9)) 
    \PB_cnt[0]_i_1__0 
       (.I0(PB_state),
        .I1(PB_sync_1_reg_n_0),
        .O(\PB_cnt[0]_i_1__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \PB_cnt[0]_i_3__0 
       (.I0(PB_cnt_reg[0]),
        .O(\PB_cnt[0]_i_3__0_n_0 ));
  FDRE \PB_cnt_reg[0] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__0_n_7 ),
        .Q(PB_cnt_reg[0]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  CARRY4 \PB_cnt_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\PB_cnt_reg[0]_i_2__0_n_0 ,\PB_cnt_reg[0]_i_2__0_n_1 ,\PB_cnt_reg[0]_i_2__0_n_2 ,\PB_cnt_reg[0]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\PB_cnt_reg[0]_i_2__0_n_4 ,\PB_cnt_reg[0]_i_2__0_n_5 ,\PB_cnt_reg[0]_i_2__0_n_6 ,\PB_cnt_reg[0]_i_2__0_n_7 }),
        .S({PB_cnt_reg[3:1],\PB_cnt[0]_i_3__0_n_0 }));
  FDRE \PB_cnt_reg[10] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__0_n_5 ),
        .Q(PB_cnt_reg[10]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  FDRE \PB_cnt_reg[11] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__0_n_4 ),
        .Q(PB_cnt_reg[11]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  FDRE \PB_cnt_reg[12] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__0_n_7 ),
        .Q(PB_cnt_reg[12]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  CARRY4 \PB_cnt_reg[12]_i_1__0 
       (.CI(\PB_cnt_reg[8]_i_1__0_n_0 ),
        .CO({\NLW_PB_cnt_reg[12]_i_1__0_CO_UNCONNECTED [3],\PB_cnt_reg[12]_i_1__0_n_1 ,\PB_cnt_reg[12]_i_1__0_n_2 ,\PB_cnt_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[12]_i_1__0_n_4 ,\PB_cnt_reg[12]_i_1__0_n_5 ,\PB_cnt_reg[12]_i_1__0_n_6 ,\PB_cnt_reg[12]_i_1__0_n_7 }),
        .S(PB_cnt_reg[15:12]));
  FDRE \PB_cnt_reg[13] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__0_n_6 ),
        .Q(PB_cnt_reg[13]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  FDRE \PB_cnt_reg[14] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__0_n_5 ),
        .Q(PB_cnt_reg[14]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  FDRE \PB_cnt_reg[15] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__0_n_4 ),
        .Q(PB_cnt_reg[15]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  FDRE \PB_cnt_reg[1] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__0_n_6 ),
        .Q(PB_cnt_reg[1]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  FDRE \PB_cnt_reg[2] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__0_n_5 ),
        .Q(PB_cnt_reg[2]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  FDRE \PB_cnt_reg[3] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__0_n_4 ),
        .Q(PB_cnt_reg[3]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  FDRE \PB_cnt_reg[4] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__0_n_7 ),
        .Q(PB_cnt_reg[4]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  CARRY4 \PB_cnt_reg[4]_i_1__0 
       (.CI(\PB_cnt_reg[0]_i_2__0_n_0 ),
        .CO({\PB_cnt_reg[4]_i_1__0_n_0 ,\PB_cnt_reg[4]_i_1__0_n_1 ,\PB_cnt_reg[4]_i_1__0_n_2 ,\PB_cnt_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[4]_i_1__0_n_4 ,\PB_cnt_reg[4]_i_1__0_n_5 ,\PB_cnt_reg[4]_i_1__0_n_6 ,\PB_cnt_reg[4]_i_1__0_n_7 }),
        .S(PB_cnt_reg[7:4]));
  FDRE \PB_cnt_reg[5] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__0_n_6 ),
        .Q(PB_cnt_reg[5]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  FDRE \PB_cnt_reg[6] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__0_n_5 ),
        .Q(PB_cnt_reg[6]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  FDRE \PB_cnt_reg[7] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__0_n_4 ),
        .Q(PB_cnt_reg[7]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  FDRE \PB_cnt_reg[8] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__0_n_7 ),
        .Q(PB_cnt_reg[8]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  CARRY4 \PB_cnt_reg[8]_i_1__0 
       (.CI(\PB_cnt_reg[4]_i_1__0_n_0 ),
        .CO({\PB_cnt_reg[8]_i_1__0_n_0 ,\PB_cnt_reg[8]_i_1__0_n_1 ,\PB_cnt_reg[8]_i_1__0_n_2 ,\PB_cnt_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[8]_i_1__0_n_4 ,\PB_cnt_reg[8]_i_1__0_n_5 ,\PB_cnt_reg[8]_i_1__0_n_6 ,\PB_cnt_reg[8]_i_1__0_n_7 }),
        .S(PB_cnt_reg[11:8]));
  FDRE \PB_cnt_reg[9] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__0_n_6 ),
        .Q(PB_cnt_reg[9]),
        .R(\PB_cnt[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h9)) 
    PB_state_i_1__4
       (.I0(left_o_INST_0_i_1_n_0),
        .I1(PB_state),
        .O(PB_state_i_1__4_n_0));
  FDRE PB_state_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_state_i_1__4_n_0),
        .Q(PB_state),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    PB_sync_0_i_1__0
       (.I0(btn_left),
        .O(PB_sync_0_i_1__0_n_0));
  FDRE PB_sync_0_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0_i_1__0_n_0),
        .Q(PB_sync_0_reg_n_0),
        .R(1'b0));
  FDRE PB_sync_1_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0_reg_n_0),
        .Q(PB_sync_1_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h1)) 
    left_o_INST_0
       (.I0(left_o_INST_0_i_1_n_0),
        .I1(PB_state),
        .O(left_o));
  LUT6 #(
    .INIT(64'hEFFFFFFFFFFFFFFF)) 
    left_o_INST_0_i_1
       (.I0(left_o_INST_0_i_2_n_0),
        .I1(left_o_INST_0_i_3_n_0),
        .I2(PB_cnt_reg[2]),
        .I3(PB_cnt_reg[3]),
        .I4(PB_cnt_reg[4]),
        .I5(PB_cnt_reg[15]),
        .O(left_o_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'hEFFFFFFFFFFFFFFF)) 
    left_o_INST_0_i_2
       (.I0(\PB_cnt[0]_i_1__0_n_0 ),
        .I1(left_o_INST_0_i_4_n_0),
        .I2(PB_cnt_reg[10]),
        .I3(PB_cnt_reg[12]),
        .I4(PB_cnt_reg[11]),
        .I5(PB_cnt_reg[0]),
        .O(left_o_INST_0_i_2_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    left_o_INST_0_i_3
       (.I0(PB_cnt_reg[5]),
        .I1(PB_cnt_reg[8]),
        .I2(PB_cnt_reg[1]),
        .I3(PB_cnt_reg[9]),
        .O(left_o_INST_0_i_3_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    left_o_INST_0_i_4
       (.I0(PB_cnt_reg[6]),
        .I1(PB_cnt_reg[13]),
        .I2(PB_cnt_reg[7]),
        .I3(PB_cnt_reg[14]),
        .O(left_o_INST_0_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "debounce" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_2
   (right_o,
    pixel_clk,
    btn_right);
  output right_o;
  input pixel_clk;
  input btn_right;

  wire \PB_cnt[0]_i_1__1_n_0 ;
  wire \PB_cnt[0]_i_3__1_n_0 ;
  wire [15:0]PB_cnt_reg;
  wire \PB_cnt_reg[0]_i_2__1_n_0 ;
  wire \PB_cnt_reg[0]_i_2__1_n_1 ;
  wire \PB_cnt_reg[0]_i_2__1_n_2 ;
  wire \PB_cnt_reg[0]_i_2__1_n_3 ;
  wire \PB_cnt_reg[0]_i_2__1_n_4 ;
  wire \PB_cnt_reg[0]_i_2__1_n_5 ;
  wire \PB_cnt_reg[0]_i_2__1_n_6 ;
  wire \PB_cnt_reg[0]_i_2__1_n_7 ;
  wire \PB_cnt_reg[12]_i_1__1_n_1 ;
  wire \PB_cnt_reg[12]_i_1__1_n_2 ;
  wire \PB_cnt_reg[12]_i_1__1_n_3 ;
  wire \PB_cnt_reg[12]_i_1__1_n_4 ;
  wire \PB_cnt_reg[12]_i_1__1_n_5 ;
  wire \PB_cnt_reg[12]_i_1__1_n_6 ;
  wire \PB_cnt_reg[12]_i_1__1_n_7 ;
  wire \PB_cnt_reg[4]_i_1__1_n_0 ;
  wire \PB_cnt_reg[4]_i_1__1_n_1 ;
  wire \PB_cnt_reg[4]_i_1__1_n_2 ;
  wire \PB_cnt_reg[4]_i_1__1_n_3 ;
  wire \PB_cnt_reg[4]_i_1__1_n_4 ;
  wire \PB_cnt_reg[4]_i_1__1_n_5 ;
  wire \PB_cnt_reg[4]_i_1__1_n_6 ;
  wire \PB_cnt_reg[4]_i_1__1_n_7 ;
  wire \PB_cnt_reg[8]_i_1__1_n_0 ;
  wire \PB_cnt_reg[8]_i_1__1_n_1 ;
  wire \PB_cnt_reg[8]_i_1__1_n_2 ;
  wire \PB_cnt_reg[8]_i_1__1_n_3 ;
  wire \PB_cnt_reg[8]_i_1__1_n_4 ;
  wire \PB_cnt_reg[8]_i_1__1_n_5 ;
  wire \PB_cnt_reg[8]_i_1__1_n_6 ;
  wire \PB_cnt_reg[8]_i_1__1_n_7 ;
  wire PB_state;
  wire PB_state_i_1__5_n_0;
  wire PB_sync_0_i_1__1_n_0;
  wire PB_sync_0_reg_n_0;
  wire PB_sync_1_reg_n_0;
  wire btn_right;
  wire pixel_clk;
  wire right_o;
  wire right_o_INST_0_i_1_n_0;
  wire right_o_INST_0_i_2_n_0;
  wire right_o_INST_0_i_3_n_0;
  wire right_o_INST_0_i_4_n_0;
  wire [3:3]\NLW_PB_cnt_reg[12]_i_1__1_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h9)) 
    \PB_cnt[0]_i_1__1 
       (.I0(PB_state),
        .I1(PB_sync_1_reg_n_0),
        .O(\PB_cnt[0]_i_1__1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \PB_cnt[0]_i_3__1 
       (.I0(PB_cnt_reg[0]),
        .O(\PB_cnt[0]_i_3__1_n_0 ));
  FDRE \PB_cnt_reg[0] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__1_n_7 ),
        .Q(PB_cnt_reg[0]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  CARRY4 \PB_cnt_reg[0]_i_2__1 
       (.CI(1'b0),
        .CO({\PB_cnt_reg[0]_i_2__1_n_0 ,\PB_cnt_reg[0]_i_2__1_n_1 ,\PB_cnt_reg[0]_i_2__1_n_2 ,\PB_cnt_reg[0]_i_2__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\PB_cnt_reg[0]_i_2__1_n_4 ,\PB_cnt_reg[0]_i_2__1_n_5 ,\PB_cnt_reg[0]_i_2__1_n_6 ,\PB_cnt_reg[0]_i_2__1_n_7 }),
        .S({PB_cnt_reg[3:1],\PB_cnt[0]_i_3__1_n_0 }));
  FDRE \PB_cnt_reg[10] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__1_n_5 ),
        .Q(PB_cnt_reg[10]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  FDRE \PB_cnt_reg[11] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__1_n_4 ),
        .Q(PB_cnt_reg[11]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  FDRE \PB_cnt_reg[12] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__1_n_7 ),
        .Q(PB_cnt_reg[12]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  CARRY4 \PB_cnt_reg[12]_i_1__1 
       (.CI(\PB_cnt_reg[8]_i_1__1_n_0 ),
        .CO({\NLW_PB_cnt_reg[12]_i_1__1_CO_UNCONNECTED [3],\PB_cnt_reg[12]_i_1__1_n_1 ,\PB_cnt_reg[12]_i_1__1_n_2 ,\PB_cnt_reg[12]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[12]_i_1__1_n_4 ,\PB_cnt_reg[12]_i_1__1_n_5 ,\PB_cnt_reg[12]_i_1__1_n_6 ,\PB_cnt_reg[12]_i_1__1_n_7 }),
        .S(PB_cnt_reg[15:12]));
  FDRE \PB_cnt_reg[13] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__1_n_6 ),
        .Q(PB_cnt_reg[13]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  FDRE \PB_cnt_reg[14] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__1_n_5 ),
        .Q(PB_cnt_reg[14]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  FDRE \PB_cnt_reg[15] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__1_n_4 ),
        .Q(PB_cnt_reg[15]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  FDRE \PB_cnt_reg[1] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__1_n_6 ),
        .Q(PB_cnt_reg[1]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  FDRE \PB_cnt_reg[2] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__1_n_5 ),
        .Q(PB_cnt_reg[2]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  FDRE \PB_cnt_reg[3] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__1_n_4 ),
        .Q(PB_cnt_reg[3]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  FDRE \PB_cnt_reg[4] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__1_n_7 ),
        .Q(PB_cnt_reg[4]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  CARRY4 \PB_cnt_reg[4]_i_1__1 
       (.CI(\PB_cnt_reg[0]_i_2__1_n_0 ),
        .CO({\PB_cnt_reg[4]_i_1__1_n_0 ,\PB_cnt_reg[4]_i_1__1_n_1 ,\PB_cnt_reg[4]_i_1__1_n_2 ,\PB_cnt_reg[4]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[4]_i_1__1_n_4 ,\PB_cnt_reg[4]_i_1__1_n_5 ,\PB_cnt_reg[4]_i_1__1_n_6 ,\PB_cnt_reg[4]_i_1__1_n_7 }),
        .S(PB_cnt_reg[7:4]));
  FDRE \PB_cnt_reg[5] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__1_n_6 ),
        .Q(PB_cnt_reg[5]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  FDRE \PB_cnt_reg[6] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__1_n_5 ),
        .Q(PB_cnt_reg[6]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  FDRE \PB_cnt_reg[7] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__1_n_4 ),
        .Q(PB_cnt_reg[7]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  FDRE \PB_cnt_reg[8] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__1_n_7 ),
        .Q(PB_cnt_reg[8]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  CARRY4 \PB_cnt_reg[8]_i_1__1 
       (.CI(\PB_cnt_reg[4]_i_1__1_n_0 ),
        .CO({\PB_cnt_reg[8]_i_1__1_n_0 ,\PB_cnt_reg[8]_i_1__1_n_1 ,\PB_cnt_reg[8]_i_1__1_n_2 ,\PB_cnt_reg[8]_i_1__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[8]_i_1__1_n_4 ,\PB_cnt_reg[8]_i_1__1_n_5 ,\PB_cnt_reg[8]_i_1__1_n_6 ,\PB_cnt_reg[8]_i_1__1_n_7 }),
        .S(PB_cnt_reg[11:8]));
  FDRE \PB_cnt_reg[9] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__1_n_6 ),
        .Q(PB_cnt_reg[9]),
        .R(\PB_cnt[0]_i_1__1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h9)) 
    PB_state_i_1__5
       (.I0(right_o_INST_0_i_1_n_0),
        .I1(PB_state),
        .O(PB_state_i_1__5_n_0));
  FDRE PB_state_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_state_i_1__5_n_0),
        .Q(PB_state),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    PB_sync_0_i_1__1
       (.I0(btn_right),
        .O(PB_sync_0_i_1__1_n_0));
  FDRE PB_sync_0_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0_i_1__1_n_0),
        .Q(PB_sync_0_reg_n_0),
        .R(1'b0));
  FDRE PB_sync_1_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0_reg_n_0),
        .Q(PB_sync_1_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h1)) 
    right_o_INST_0
       (.I0(PB_state),
        .I1(right_o_INST_0_i_1_n_0),
        .O(right_o));
  LUT6 #(
    .INIT(64'hEFFFFFFFFFFFFFFF)) 
    right_o_INST_0_i_1
       (.I0(right_o_INST_0_i_2_n_0),
        .I1(right_o_INST_0_i_3_n_0),
        .I2(PB_cnt_reg[2]),
        .I3(PB_cnt_reg[3]),
        .I4(PB_cnt_reg[4]),
        .I5(PB_cnt_reg[15]),
        .O(right_o_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'hEFFFFFFFFFFFFFFF)) 
    right_o_INST_0_i_2
       (.I0(\PB_cnt[0]_i_1__1_n_0 ),
        .I1(right_o_INST_0_i_4_n_0),
        .I2(PB_cnt_reg[10]),
        .I3(PB_cnt_reg[12]),
        .I4(PB_cnt_reg[11]),
        .I5(PB_cnt_reg[0]),
        .O(right_o_INST_0_i_2_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    right_o_INST_0_i_3
       (.I0(PB_cnt_reg[5]),
        .I1(PB_cnt_reg[8]),
        .I2(PB_cnt_reg[1]),
        .I3(PB_cnt_reg[9]),
        .O(right_o_INST_0_i_3_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    right_o_INST_0_i_4
       (.I0(PB_cnt_reg[6]),
        .I1(PB_cnt_reg[13]),
        .I2(PB_cnt_reg[7]),
        .I3(PB_cnt_reg[14]),
        .O(right_o_INST_0_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "debounce" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_3
   (rotate_o,
    pixel_clk,
    btn_rotate);
  output rotate_o;
  input pixel_clk;
  input btn_rotate;

  wire \PB_cnt[0]_i_1__2_n_0 ;
  wire \PB_cnt[0]_i_3__2_n_0 ;
  wire [15:0]PB_cnt_reg;
  wire \PB_cnt_reg[0]_i_2__2_n_0 ;
  wire \PB_cnt_reg[0]_i_2__2_n_1 ;
  wire \PB_cnt_reg[0]_i_2__2_n_2 ;
  wire \PB_cnt_reg[0]_i_2__2_n_3 ;
  wire \PB_cnt_reg[0]_i_2__2_n_4 ;
  wire \PB_cnt_reg[0]_i_2__2_n_5 ;
  wire \PB_cnt_reg[0]_i_2__2_n_6 ;
  wire \PB_cnt_reg[0]_i_2__2_n_7 ;
  wire \PB_cnt_reg[12]_i_1__2_n_1 ;
  wire \PB_cnt_reg[12]_i_1__2_n_2 ;
  wire \PB_cnt_reg[12]_i_1__2_n_3 ;
  wire \PB_cnt_reg[12]_i_1__2_n_4 ;
  wire \PB_cnt_reg[12]_i_1__2_n_5 ;
  wire \PB_cnt_reg[12]_i_1__2_n_6 ;
  wire \PB_cnt_reg[12]_i_1__2_n_7 ;
  wire \PB_cnt_reg[4]_i_1__2_n_0 ;
  wire \PB_cnt_reg[4]_i_1__2_n_1 ;
  wire \PB_cnt_reg[4]_i_1__2_n_2 ;
  wire \PB_cnt_reg[4]_i_1__2_n_3 ;
  wire \PB_cnt_reg[4]_i_1__2_n_4 ;
  wire \PB_cnt_reg[4]_i_1__2_n_5 ;
  wire \PB_cnt_reg[4]_i_1__2_n_6 ;
  wire \PB_cnt_reg[4]_i_1__2_n_7 ;
  wire \PB_cnt_reg[8]_i_1__2_n_0 ;
  wire \PB_cnt_reg[8]_i_1__2_n_1 ;
  wire \PB_cnt_reg[8]_i_1__2_n_2 ;
  wire \PB_cnt_reg[8]_i_1__2_n_3 ;
  wire \PB_cnt_reg[8]_i_1__2_n_4 ;
  wire \PB_cnt_reg[8]_i_1__2_n_5 ;
  wire \PB_cnt_reg[8]_i_1__2_n_6 ;
  wire \PB_cnt_reg[8]_i_1__2_n_7 ;
  wire PB_state;
  wire PB_state_i_1__0_n_0;
  wire PB_sync_0_i_1__2_n_0;
  wire PB_sync_0_reg_n_0;
  wire PB_sync_1_reg_n_0;
  wire btn_rotate;
  wire p_2_in;
  wire pixel_clk;
  wire rotate_o;
  wire rotate_o_INST_0_i_2_n_0;
  wire rotate_o_INST_0_i_3_n_0;
  wire rotate_o_INST_0_i_4_n_0;
  wire [3:3]\NLW_PB_cnt_reg[12]_i_1__2_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h9)) 
    \PB_cnt[0]_i_1__2 
       (.I0(PB_state),
        .I1(PB_sync_1_reg_n_0),
        .O(\PB_cnt[0]_i_1__2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \PB_cnt[0]_i_3__2 
       (.I0(PB_cnt_reg[0]),
        .O(\PB_cnt[0]_i_3__2_n_0 ));
  FDRE \PB_cnt_reg[0] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__2_n_7 ),
        .Q(PB_cnt_reg[0]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  CARRY4 \PB_cnt_reg[0]_i_2__2 
       (.CI(1'b0),
        .CO({\PB_cnt_reg[0]_i_2__2_n_0 ,\PB_cnt_reg[0]_i_2__2_n_1 ,\PB_cnt_reg[0]_i_2__2_n_2 ,\PB_cnt_reg[0]_i_2__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\PB_cnt_reg[0]_i_2__2_n_4 ,\PB_cnt_reg[0]_i_2__2_n_5 ,\PB_cnt_reg[0]_i_2__2_n_6 ,\PB_cnt_reg[0]_i_2__2_n_7 }),
        .S({PB_cnt_reg[3:1],\PB_cnt[0]_i_3__2_n_0 }));
  FDRE \PB_cnt_reg[10] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__2_n_5 ),
        .Q(PB_cnt_reg[10]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  FDRE \PB_cnt_reg[11] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__2_n_4 ),
        .Q(PB_cnt_reg[11]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  FDRE \PB_cnt_reg[12] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__2_n_7 ),
        .Q(PB_cnt_reg[12]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  CARRY4 \PB_cnt_reg[12]_i_1__2 
       (.CI(\PB_cnt_reg[8]_i_1__2_n_0 ),
        .CO({\NLW_PB_cnt_reg[12]_i_1__2_CO_UNCONNECTED [3],\PB_cnt_reg[12]_i_1__2_n_1 ,\PB_cnt_reg[12]_i_1__2_n_2 ,\PB_cnt_reg[12]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[12]_i_1__2_n_4 ,\PB_cnt_reg[12]_i_1__2_n_5 ,\PB_cnt_reg[12]_i_1__2_n_6 ,\PB_cnt_reg[12]_i_1__2_n_7 }),
        .S(PB_cnt_reg[15:12]));
  FDRE \PB_cnt_reg[13] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__2_n_6 ),
        .Q(PB_cnt_reg[13]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  FDRE \PB_cnt_reg[14] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__2_n_5 ),
        .Q(PB_cnt_reg[14]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  FDRE \PB_cnt_reg[15] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__2_n_4 ),
        .Q(PB_cnt_reg[15]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  FDRE \PB_cnt_reg[1] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__2_n_6 ),
        .Q(PB_cnt_reg[1]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  FDRE \PB_cnt_reg[2] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__2_n_5 ),
        .Q(PB_cnt_reg[2]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  FDRE \PB_cnt_reg[3] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__2_n_4 ),
        .Q(PB_cnt_reg[3]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  FDRE \PB_cnt_reg[4] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__2_n_7 ),
        .Q(PB_cnt_reg[4]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  CARRY4 \PB_cnt_reg[4]_i_1__2 
       (.CI(\PB_cnt_reg[0]_i_2__2_n_0 ),
        .CO({\PB_cnt_reg[4]_i_1__2_n_0 ,\PB_cnt_reg[4]_i_1__2_n_1 ,\PB_cnt_reg[4]_i_1__2_n_2 ,\PB_cnt_reg[4]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[4]_i_1__2_n_4 ,\PB_cnt_reg[4]_i_1__2_n_5 ,\PB_cnt_reg[4]_i_1__2_n_6 ,\PB_cnt_reg[4]_i_1__2_n_7 }),
        .S(PB_cnt_reg[7:4]));
  FDRE \PB_cnt_reg[5] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__2_n_6 ),
        .Q(PB_cnt_reg[5]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  FDRE \PB_cnt_reg[6] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__2_n_5 ),
        .Q(PB_cnt_reg[6]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  FDRE \PB_cnt_reg[7] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__2_n_4 ),
        .Q(PB_cnt_reg[7]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  FDRE \PB_cnt_reg[8] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__2_n_7 ),
        .Q(PB_cnt_reg[8]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  CARRY4 \PB_cnt_reg[8]_i_1__2 
       (.CI(\PB_cnt_reg[4]_i_1__2_n_0 ),
        .CO({\PB_cnt_reg[8]_i_1__2_n_0 ,\PB_cnt_reg[8]_i_1__2_n_1 ,\PB_cnt_reg[8]_i_1__2_n_2 ,\PB_cnt_reg[8]_i_1__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[8]_i_1__2_n_4 ,\PB_cnt_reg[8]_i_1__2_n_5 ,\PB_cnt_reg[8]_i_1__2_n_6 ,\PB_cnt_reg[8]_i_1__2_n_7 }),
        .S(PB_cnt_reg[11:8]));
  FDRE \PB_cnt_reg[9] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__2_n_6 ),
        .Q(PB_cnt_reg[9]),
        .R(\PB_cnt[0]_i_1__2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h6)) 
    PB_state_i_1__0
       (.I0(p_2_in),
        .I1(PB_state),
        .O(PB_state_i_1__0_n_0));
  FDRE PB_state_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_state_i_1__0_n_0),
        .Q(PB_state),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    PB_sync_0_i_1__2
       (.I0(btn_rotate),
        .O(PB_sync_0_i_1__2_n_0));
  FDRE PB_sync_0_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0_i_1__2_n_0),
        .Q(PB_sync_0_reg_n_0),
        .R(1'b0));
  FDRE PB_sync_1_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0_reg_n_0),
        .Q(PB_sync_1_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    rotate_o_INST_0
       (.I0(p_2_in),
        .I1(PB_state),
        .O(rotate_o));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    rotate_o_INST_0_i_1
       (.I0(rotate_o_INST_0_i_2_n_0),
        .I1(PB_cnt_reg[2]),
        .I2(PB_cnt_reg[3]),
        .I3(PB_cnt_reg[4]),
        .I4(PB_cnt_reg[15]),
        .I5(rotate_o_INST_0_i_3_n_0),
        .O(p_2_in));
  LUT4 #(
    .INIT(16'h7FFF)) 
    rotate_o_INST_0_i_2
       (.I0(PB_cnt_reg[5]),
        .I1(PB_cnt_reg[8]),
        .I2(PB_cnt_reg[1]),
        .I3(PB_cnt_reg[9]),
        .O(rotate_o_INST_0_i_2_n_0));
  LUT6 #(
    .INIT(64'hEFFFFFFFFFFFFFFF)) 
    rotate_o_INST_0_i_3
       (.I0(\PB_cnt[0]_i_1__2_n_0 ),
        .I1(rotate_o_INST_0_i_4_n_0),
        .I2(PB_cnt_reg[10]),
        .I3(PB_cnt_reg[12]),
        .I4(PB_cnt_reg[11]),
        .I5(PB_cnt_reg[0]),
        .O(rotate_o_INST_0_i_3_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    rotate_o_INST_0_i_4
       (.I0(PB_cnt_reg[6]),
        .I1(PB_cnt_reg[13]),
        .I2(PB_cnt_reg[7]),
        .I3(PB_cnt_reg[14]),
        .O(rotate_o_INST_0_i_4_n_0));
endmodule

(* ORIG_REF_NAME = "debounce" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_4
   (sw_pause_o,
    pixel_clk,
    sw_pause);
  output sw_pause_o;
  input pixel_clk;
  input sw_pause;

  wire \PB_cnt[0]_i_1__5_n_0 ;
  wire \PB_cnt[0]_i_3__5_n_0 ;
  wire [15:0]PB_cnt_reg;
  wire \PB_cnt_reg[0]_i_2__5_n_0 ;
  wire \PB_cnt_reg[0]_i_2__5_n_1 ;
  wire \PB_cnt_reg[0]_i_2__5_n_2 ;
  wire \PB_cnt_reg[0]_i_2__5_n_3 ;
  wire \PB_cnt_reg[0]_i_2__5_n_4 ;
  wire \PB_cnt_reg[0]_i_2__5_n_5 ;
  wire \PB_cnt_reg[0]_i_2__5_n_6 ;
  wire \PB_cnt_reg[0]_i_2__5_n_7 ;
  wire \PB_cnt_reg[12]_i_1__5_n_1 ;
  wire \PB_cnt_reg[12]_i_1__5_n_2 ;
  wire \PB_cnt_reg[12]_i_1__5_n_3 ;
  wire \PB_cnt_reg[12]_i_1__5_n_4 ;
  wire \PB_cnt_reg[12]_i_1__5_n_5 ;
  wire \PB_cnt_reg[12]_i_1__5_n_6 ;
  wire \PB_cnt_reg[12]_i_1__5_n_7 ;
  wire \PB_cnt_reg[4]_i_1__5_n_0 ;
  wire \PB_cnt_reg[4]_i_1__5_n_1 ;
  wire \PB_cnt_reg[4]_i_1__5_n_2 ;
  wire \PB_cnt_reg[4]_i_1__5_n_3 ;
  wire \PB_cnt_reg[4]_i_1__5_n_4 ;
  wire \PB_cnt_reg[4]_i_1__5_n_5 ;
  wire \PB_cnt_reg[4]_i_1__5_n_6 ;
  wire \PB_cnt_reg[4]_i_1__5_n_7 ;
  wire \PB_cnt_reg[8]_i_1__5_n_0 ;
  wire \PB_cnt_reg[8]_i_1__5_n_1 ;
  wire \PB_cnt_reg[8]_i_1__5_n_2 ;
  wire \PB_cnt_reg[8]_i_1__5_n_3 ;
  wire \PB_cnt_reg[8]_i_1__5_n_4 ;
  wire \PB_cnt_reg[8]_i_1__5_n_5 ;
  wire \PB_cnt_reg[8]_i_1__5_n_6 ;
  wire \PB_cnt_reg[8]_i_1__5_n_7 ;
  wire PB_state;
  wire PB_state_i_1__3_n_0;
  wire PB_sync_0_i_1__5_n_0;
  wire PB_sync_0_reg_n_0;
  wire PB_sync_1_reg_n_0;
  wire pixel_clk;
  wire sw_flipped_i_1__0_n_0;
  wire sw_flipped_i_2__0_n_0;
  wire sw_flipped_i_3__0_n_0;
  wire sw_flipped_i_4__0_n_0;
  wire sw_pause;
  wire sw_pause_o;
  wire [3:3]\NLW_PB_cnt_reg[12]_i_1__5_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h9)) 
    \PB_cnt[0]_i_1__5 
       (.I0(PB_state),
        .I1(PB_sync_1_reg_n_0),
        .O(\PB_cnt[0]_i_1__5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \PB_cnt[0]_i_3__5 
       (.I0(PB_cnt_reg[0]),
        .O(\PB_cnt[0]_i_3__5_n_0 ));
  FDRE \PB_cnt_reg[0] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__5_n_7 ),
        .Q(PB_cnt_reg[0]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  CARRY4 \PB_cnt_reg[0]_i_2__5 
       (.CI(1'b0),
        .CO({\PB_cnt_reg[0]_i_2__5_n_0 ,\PB_cnt_reg[0]_i_2__5_n_1 ,\PB_cnt_reg[0]_i_2__5_n_2 ,\PB_cnt_reg[0]_i_2__5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\PB_cnt_reg[0]_i_2__5_n_4 ,\PB_cnt_reg[0]_i_2__5_n_5 ,\PB_cnt_reg[0]_i_2__5_n_6 ,\PB_cnt_reg[0]_i_2__5_n_7 }),
        .S({PB_cnt_reg[3:1],\PB_cnt[0]_i_3__5_n_0 }));
  FDRE \PB_cnt_reg[10] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__5_n_5 ),
        .Q(PB_cnt_reg[10]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  FDRE \PB_cnt_reg[11] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__5_n_4 ),
        .Q(PB_cnt_reg[11]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  FDRE \PB_cnt_reg[12] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__5_n_7 ),
        .Q(PB_cnt_reg[12]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  CARRY4 \PB_cnt_reg[12]_i_1__5 
       (.CI(\PB_cnt_reg[8]_i_1__5_n_0 ),
        .CO({\NLW_PB_cnt_reg[12]_i_1__5_CO_UNCONNECTED [3],\PB_cnt_reg[12]_i_1__5_n_1 ,\PB_cnt_reg[12]_i_1__5_n_2 ,\PB_cnt_reg[12]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[12]_i_1__5_n_4 ,\PB_cnt_reg[12]_i_1__5_n_5 ,\PB_cnt_reg[12]_i_1__5_n_6 ,\PB_cnt_reg[12]_i_1__5_n_7 }),
        .S(PB_cnt_reg[15:12]));
  FDRE \PB_cnt_reg[13] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__5_n_6 ),
        .Q(PB_cnt_reg[13]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  FDRE \PB_cnt_reg[14] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__5_n_5 ),
        .Q(PB_cnt_reg[14]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  FDRE \PB_cnt_reg[15] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__5_n_4 ),
        .Q(PB_cnt_reg[15]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  FDRE \PB_cnt_reg[1] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__5_n_6 ),
        .Q(PB_cnt_reg[1]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  FDRE \PB_cnt_reg[2] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__5_n_5 ),
        .Q(PB_cnt_reg[2]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  FDRE \PB_cnt_reg[3] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__5_n_4 ),
        .Q(PB_cnt_reg[3]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  FDRE \PB_cnt_reg[4] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__5_n_7 ),
        .Q(PB_cnt_reg[4]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  CARRY4 \PB_cnt_reg[4]_i_1__5 
       (.CI(\PB_cnt_reg[0]_i_2__5_n_0 ),
        .CO({\PB_cnt_reg[4]_i_1__5_n_0 ,\PB_cnt_reg[4]_i_1__5_n_1 ,\PB_cnt_reg[4]_i_1__5_n_2 ,\PB_cnt_reg[4]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[4]_i_1__5_n_4 ,\PB_cnt_reg[4]_i_1__5_n_5 ,\PB_cnt_reg[4]_i_1__5_n_6 ,\PB_cnt_reg[4]_i_1__5_n_7 }),
        .S(PB_cnt_reg[7:4]));
  FDRE \PB_cnt_reg[5] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__5_n_6 ),
        .Q(PB_cnt_reg[5]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  FDRE \PB_cnt_reg[6] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__5_n_5 ),
        .Q(PB_cnt_reg[6]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  FDRE \PB_cnt_reg[7] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__5_n_4 ),
        .Q(PB_cnt_reg[7]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  FDRE \PB_cnt_reg[8] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__5_n_7 ),
        .Q(PB_cnt_reg[8]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  CARRY4 \PB_cnt_reg[8]_i_1__5 
       (.CI(\PB_cnt_reg[4]_i_1__5_n_0 ),
        .CO({\PB_cnt_reg[8]_i_1__5_n_0 ,\PB_cnt_reg[8]_i_1__5_n_1 ,\PB_cnt_reg[8]_i_1__5_n_2 ,\PB_cnt_reg[8]_i_1__5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[8]_i_1__5_n_4 ,\PB_cnt_reg[8]_i_1__5_n_5 ,\PB_cnt_reg[8]_i_1__5_n_6 ,\PB_cnt_reg[8]_i_1__5_n_7 }),
        .S(PB_cnt_reg[11:8]));
  FDRE \PB_cnt_reg[9] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__5_n_6 ),
        .Q(PB_cnt_reg[9]),
        .R(\PB_cnt[0]_i_1__5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    PB_state_i_1__3
       (.I0(sw_flipped_i_1__0_n_0),
        .I1(PB_state),
        .O(PB_state_i_1__3_n_0));
  FDRE PB_state_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_state_i_1__3_n_0),
        .Q(PB_state),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    PB_sync_0_i_1__5
       (.I0(sw_pause),
        .O(PB_sync_0_i_1__5_n_0));
  FDRE PB_sync_0_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0_i_1__5_n_0),
        .Q(PB_sync_0_reg_n_0),
        .R(1'b0));
  FDRE PB_sync_1_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0_reg_n_0),
        .Q(PB_sync_1_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    sw_flipped_i_1__0
       (.I0(sw_flipped_i_2__0_n_0),
        .I1(PB_cnt_reg[2]),
        .I2(PB_cnt_reg[3]),
        .I3(PB_cnt_reg[4]),
        .I4(PB_cnt_reg[15]),
        .I5(sw_flipped_i_3__0_n_0),
        .O(sw_flipped_i_1__0_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    sw_flipped_i_2__0
       (.I0(PB_cnt_reg[5]),
        .I1(PB_cnt_reg[8]),
        .I2(PB_cnt_reg[1]),
        .I3(PB_cnt_reg[9]),
        .O(sw_flipped_i_2__0_n_0));
  LUT6 #(
    .INIT(64'hEFFFFFFFFFFFFFFF)) 
    sw_flipped_i_3__0
       (.I0(\PB_cnt[0]_i_1__5_n_0 ),
        .I1(sw_flipped_i_4__0_n_0),
        .I2(PB_cnt_reg[10]),
        .I3(PB_cnt_reg[12]),
        .I4(PB_cnt_reg[11]),
        .I5(PB_cnt_reg[0]),
        .O(sw_flipped_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    sw_flipped_i_4__0
       (.I0(PB_cnt_reg[6]),
        .I1(PB_cnt_reg[13]),
        .I2(PB_cnt_reg[7]),
        .I3(PB_cnt_reg[14]),
        .O(sw_flipped_i_4__0_n_0));
  FDRE sw_flipped_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(sw_flipped_i_1__0_n_0),
        .Q(sw_pause_o),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "debounce" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_5
   (sw_rst_o,
    pixel_clk,
    sw_rst);
  output sw_rst_o;
  input pixel_clk;
  input sw_rst;

  wire \PB_cnt[0]_i_1__4_n_0 ;
  wire \PB_cnt[0]_i_3__4_n_0 ;
  wire [15:0]PB_cnt_reg;
  wire \PB_cnt_reg[0]_i_2__4_n_0 ;
  wire \PB_cnt_reg[0]_i_2__4_n_1 ;
  wire \PB_cnt_reg[0]_i_2__4_n_2 ;
  wire \PB_cnt_reg[0]_i_2__4_n_3 ;
  wire \PB_cnt_reg[0]_i_2__4_n_4 ;
  wire \PB_cnt_reg[0]_i_2__4_n_5 ;
  wire \PB_cnt_reg[0]_i_2__4_n_6 ;
  wire \PB_cnt_reg[0]_i_2__4_n_7 ;
  wire \PB_cnt_reg[12]_i_1__4_n_1 ;
  wire \PB_cnt_reg[12]_i_1__4_n_2 ;
  wire \PB_cnt_reg[12]_i_1__4_n_3 ;
  wire \PB_cnt_reg[12]_i_1__4_n_4 ;
  wire \PB_cnt_reg[12]_i_1__4_n_5 ;
  wire \PB_cnt_reg[12]_i_1__4_n_6 ;
  wire \PB_cnt_reg[12]_i_1__4_n_7 ;
  wire \PB_cnt_reg[4]_i_1__4_n_0 ;
  wire \PB_cnt_reg[4]_i_1__4_n_1 ;
  wire \PB_cnt_reg[4]_i_1__4_n_2 ;
  wire \PB_cnt_reg[4]_i_1__4_n_3 ;
  wire \PB_cnt_reg[4]_i_1__4_n_4 ;
  wire \PB_cnt_reg[4]_i_1__4_n_5 ;
  wire \PB_cnt_reg[4]_i_1__4_n_6 ;
  wire \PB_cnt_reg[4]_i_1__4_n_7 ;
  wire \PB_cnt_reg[8]_i_1__4_n_0 ;
  wire \PB_cnt_reg[8]_i_1__4_n_1 ;
  wire \PB_cnt_reg[8]_i_1__4_n_2 ;
  wire \PB_cnt_reg[8]_i_1__4_n_3 ;
  wire \PB_cnt_reg[8]_i_1__4_n_4 ;
  wire \PB_cnt_reg[8]_i_1__4_n_5 ;
  wire \PB_cnt_reg[8]_i_1__4_n_6 ;
  wire \PB_cnt_reg[8]_i_1__4_n_7 ;
  wire PB_state;
  wire PB_state_i_1__2_n_0;
  wire PB_sync_0_i_1__4_n_0;
  wire PB_sync_0_reg_n_0;
  wire PB_sync_1_reg_n_0;
  wire pixel_clk;
  wire sw_flipped_i_1_n_0;
  wire sw_flipped_i_2_n_0;
  wire sw_flipped_i_3_n_0;
  wire sw_flipped_i_4_n_0;
  wire sw_rst;
  wire sw_rst_o;
  wire [3:3]\NLW_PB_cnt_reg[12]_i_1__4_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h9)) 
    \PB_cnt[0]_i_1__4 
       (.I0(PB_state),
        .I1(PB_sync_1_reg_n_0),
        .O(\PB_cnt[0]_i_1__4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \PB_cnt[0]_i_3__4 
       (.I0(PB_cnt_reg[0]),
        .O(\PB_cnt[0]_i_3__4_n_0 ));
  FDRE \PB_cnt_reg[0] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__4_n_7 ),
        .Q(PB_cnt_reg[0]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  CARRY4 \PB_cnt_reg[0]_i_2__4 
       (.CI(1'b0),
        .CO({\PB_cnt_reg[0]_i_2__4_n_0 ,\PB_cnt_reg[0]_i_2__4_n_1 ,\PB_cnt_reg[0]_i_2__4_n_2 ,\PB_cnt_reg[0]_i_2__4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\PB_cnt_reg[0]_i_2__4_n_4 ,\PB_cnt_reg[0]_i_2__4_n_5 ,\PB_cnt_reg[0]_i_2__4_n_6 ,\PB_cnt_reg[0]_i_2__4_n_7 }),
        .S({PB_cnt_reg[3:1],\PB_cnt[0]_i_3__4_n_0 }));
  FDRE \PB_cnt_reg[10] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__4_n_5 ),
        .Q(PB_cnt_reg[10]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  FDRE \PB_cnt_reg[11] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__4_n_4 ),
        .Q(PB_cnt_reg[11]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  FDRE \PB_cnt_reg[12] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__4_n_7 ),
        .Q(PB_cnt_reg[12]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  CARRY4 \PB_cnt_reg[12]_i_1__4 
       (.CI(\PB_cnt_reg[8]_i_1__4_n_0 ),
        .CO({\NLW_PB_cnt_reg[12]_i_1__4_CO_UNCONNECTED [3],\PB_cnt_reg[12]_i_1__4_n_1 ,\PB_cnt_reg[12]_i_1__4_n_2 ,\PB_cnt_reg[12]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[12]_i_1__4_n_4 ,\PB_cnt_reg[12]_i_1__4_n_5 ,\PB_cnt_reg[12]_i_1__4_n_6 ,\PB_cnt_reg[12]_i_1__4_n_7 }),
        .S(PB_cnt_reg[15:12]));
  FDRE \PB_cnt_reg[13] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__4_n_6 ),
        .Q(PB_cnt_reg[13]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  FDRE \PB_cnt_reg[14] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__4_n_5 ),
        .Q(PB_cnt_reg[14]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  FDRE \PB_cnt_reg[15] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[12]_i_1__4_n_4 ),
        .Q(PB_cnt_reg[15]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  FDRE \PB_cnt_reg[1] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__4_n_6 ),
        .Q(PB_cnt_reg[1]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  FDRE \PB_cnt_reg[2] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__4_n_5 ),
        .Q(PB_cnt_reg[2]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  FDRE \PB_cnt_reg[3] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[0]_i_2__4_n_4 ),
        .Q(PB_cnt_reg[3]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  FDRE \PB_cnt_reg[4] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__4_n_7 ),
        .Q(PB_cnt_reg[4]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  CARRY4 \PB_cnt_reg[4]_i_1__4 
       (.CI(\PB_cnt_reg[0]_i_2__4_n_0 ),
        .CO({\PB_cnt_reg[4]_i_1__4_n_0 ,\PB_cnt_reg[4]_i_1__4_n_1 ,\PB_cnt_reg[4]_i_1__4_n_2 ,\PB_cnt_reg[4]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[4]_i_1__4_n_4 ,\PB_cnt_reg[4]_i_1__4_n_5 ,\PB_cnt_reg[4]_i_1__4_n_6 ,\PB_cnt_reg[4]_i_1__4_n_7 }),
        .S(PB_cnt_reg[7:4]));
  FDRE \PB_cnt_reg[5] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__4_n_6 ),
        .Q(PB_cnt_reg[5]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  FDRE \PB_cnt_reg[6] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__4_n_5 ),
        .Q(PB_cnt_reg[6]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  FDRE \PB_cnt_reg[7] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[4]_i_1__4_n_4 ),
        .Q(PB_cnt_reg[7]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  FDRE \PB_cnt_reg[8] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__4_n_7 ),
        .Q(PB_cnt_reg[8]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  CARRY4 \PB_cnt_reg[8]_i_1__4 
       (.CI(\PB_cnt_reg[4]_i_1__4_n_0 ),
        .CO({\PB_cnt_reg[8]_i_1__4_n_0 ,\PB_cnt_reg[8]_i_1__4_n_1 ,\PB_cnt_reg[8]_i_1__4_n_2 ,\PB_cnt_reg[8]_i_1__4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\PB_cnt_reg[8]_i_1__4_n_4 ,\PB_cnt_reg[8]_i_1__4_n_5 ,\PB_cnt_reg[8]_i_1__4_n_6 ,\PB_cnt_reg[8]_i_1__4_n_7 }),
        .S(PB_cnt_reg[11:8]));
  FDRE \PB_cnt_reg[9] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\PB_cnt_reg[8]_i_1__4_n_6 ),
        .Q(PB_cnt_reg[9]),
        .R(\PB_cnt[0]_i_1__4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    PB_state_i_1__2
       (.I0(sw_flipped_i_1_n_0),
        .I1(PB_state),
        .O(PB_state_i_1__2_n_0));
  FDRE PB_state_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_state_i_1__2_n_0),
        .Q(PB_state),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    PB_sync_0_i_1__4
       (.I0(sw_rst),
        .O(PB_sync_0_i_1__4_n_0));
  FDRE PB_sync_0_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0_i_1__4_n_0),
        .Q(PB_sync_0_reg_n_0),
        .R(1'b0));
  FDRE PB_sync_1_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(PB_sync_0_reg_n_0),
        .Q(PB_sync_1_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    sw_flipped_i_1
       (.I0(sw_flipped_i_2_n_0),
        .I1(PB_cnt_reg[2]),
        .I2(PB_cnt_reg[3]),
        .I3(PB_cnt_reg[4]),
        .I4(PB_cnt_reg[15]),
        .I5(sw_flipped_i_3_n_0),
        .O(sw_flipped_i_1_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    sw_flipped_i_2
       (.I0(PB_cnt_reg[5]),
        .I1(PB_cnt_reg[8]),
        .I2(PB_cnt_reg[1]),
        .I3(PB_cnt_reg[9]),
        .O(sw_flipped_i_2_n_0));
  LUT6 #(
    .INIT(64'hEFFFFFFFFFFFFFFF)) 
    sw_flipped_i_3
       (.I0(\PB_cnt[0]_i_1__4_n_0 ),
        .I1(sw_flipped_i_4_n_0),
        .I2(PB_cnt_reg[10]),
        .I3(PB_cnt_reg[12]),
        .I4(PB_cnt_reg[11]),
        .I5(PB_cnt_reg[0]),
        .O(sw_flipped_i_3_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    sw_flipped_i_4
       (.I0(PB_cnt_reg[6]),
        .I1(PB_cnt_reg[13]),
        .I2(PB_cnt_reg[7]),
        .I3(PB_cnt_reg[14]),
        .O(sw_flipped_i_4_n_0));
  FDRE sw_flipped_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(sw_flipped_i_1_n_0),
        .Q(sw_rst_o),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_tetris
   (sw_rst_o,
    sw_pause_o,
    g,
    b,
    r,
    \counterX_reg[7] ,
    hs,
    vs,
    cur_blk_index,
    right_o,
    down_o,
    left_o,
    rotate_o,
    drop_o,
    pixel_clk,
    S,
    \g[3]_INST_0_i_8 ,
    \g[3]_INST_0_i_11 ,
    \g[3]_INST_0_i_12 ,
    \g[3]_INST_0_i_10 ,
    \g[3]_INST_0_i_12_0 ,
    \g[3]_INST_0_i_11_0 ,
    \g[3]_INST_0_i_10_0 ,
    \g[3]_INST_0_i_12_1 ,
    \g[3]_INST_0_i_8_0 ,
    \g[3]_INST_0_i_10_1 ,
    \g[3]_INST_0_i_11_1 ,
    \g[3]_INST_0_i_8_1 ,
    \g[3]_INST_0_i_10_2 ,
    \g[3]_INST_0_i_12_2 ,
    \r[1]_INST_0_i_21 ,
    \r[1]_INST_0_i_22 ,
    \r[1]_INST_0_i_20 ,
    \r[1]_INST_0_i_21_0 ,
    \r[1]_INST_0_i_22_0 ,
    \r[1]_INST_0_i_6 ,
    \r[1]_INST_0_i_6_0 ,
    \r[1]_INST_0_i_22_1 ,
    \r[1]_INST_0_i_22_2 ,
    \r[1]_INST_0_i_20_0 ,
    \r[1]_INST_0_i_20_1 ,
    \r[1]_INST_0_i_21_1 ,
    \r[1]_INST_0_i_20_2 ,
    \r[1]_INST_0_i_21_2 ,
    \r[1]_INST_0_i_6_1 ,
    \r[1]_INST_0_i_27 ,
    \r[1]_INST_0_i_27_0 ,
    \r[1]_INST_0_i_27_1 ,
    \r[1]_INST_0_i_27_2 ,
    btn_down,
    btn_left,
    btn_right,
    btn_rotate,
    btn_drop,
    sw_rst,
    sw_pause);
  output sw_rst_o;
  output sw_pause_o;
  output [2:0]g;
  output [1:0]b;
  output [1:0]r;
  output [4:0]\counterX_reg[7] ;
  output hs;
  output vs;
  output [1:0]cur_blk_index;
  output right_o;
  output down_o;
  output left_o;
  output rotate_o;
  output drop_o;
  input pixel_clk;
  input [2:0]S;
  input [2:0]\g[3]_INST_0_i_8 ;
  input [2:0]\g[3]_INST_0_i_11 ;
  input [2:0]\g[3]_INST_0_i_12 ;
  input [2:0]\g[3]_INST_0_i_10 ;
  input [2:0]\g[3]_INST_0_i_12_0 ;
  input [2:0]\g[3]_INST_0_i_11_0 ;
  input [2:0]\g[3]_INST_0_i_10_0 ;
  input [2:0]\g[3]_INST_0_i_12_1 ;
  input [2:0]\g[3]_INST_0_i_8_0 ;
  input [2:0]\g[3]_INST_0_i_10_1 ;
  input [2:0]\g[3]_INST_0_i_11_1 ;
  input [2:0]\g[3]_INST_0_i_8_1 ;
  input [2:0]\g[3]_INST_0_i_10_2 ;
  input [2:0]\g[3]_INST_0_i_12_2 ;
  input [2:0]\r[1]_INST_0_i_21 ;
  input [2:0]\r[1]_INST_0_i_22 ;
  input [2:0]\r[1]_INST_0_i_20 ;
  input [2:0]\r[1]_INST_0_i_21_0 ;
  input [2:0]\r[1]_INST_0_i_22_0 ;
  input [2:0]\r[1]_INST_0_i_6 ;
  input [2:0]\r[1]_INST_0_i_6_0 ;
  input [2:0]\r[1]_INST_0_i_22_1 ;
  input [2:0]\r[1]_INST_0_i_22_2 ;
  input [2:0]\r[1]_INST_0_i_20_0 ;
  input [2:0]\r[1]_INST_0_i_20_1 ;
  input [2:0]\r[1]_INST_0_i_21_1 ;
  input [2:0]\r[1]_INST_0_i_20_2 ;
  input [2:0]\r[1]_INST_0_i_21_2 ;
  input [2:0]\r[1]_INST_0_i_6_1 ;
  input [2:0]\r[1]_INST_0_i_27 ;
  input [2:0]\r[1]_INST_0_i_27_0 ;
  input [2:0]\r[1]_INST_0_i_27_1 ;
  input [2:0]\r[1]_INST_0_i_27_2 ;
  input btn_down;
  input btn_left;
  input btn_right;
  input btn_rotate;
  input btn_drop;
  input sw_rst;
  input sw_pause;

  wire [2:0]S;
  wire [1:0]b;
  wire btn_down;
  wire btn_drop;
  wire btn_left;
  wire btn_right;
  wire btn_rotate;
  wire [4:0]\counterX_reg[7] ;
  wire [1:0]cur_blk_index;
  wire down_o;
  wire drop_o;
  wire [2:0]g;
  wire [2:0]\g[3]_INST_0_i_10 ;
  wire [2:0]\g[3]_INST_0_i_10_0 ;
  wire [2:0]\g[3]_INST_0_i_10_1 ;
  wire [2:0]\g[3]_INST_0_i_10_2 ;
  wire [2:0]\g[3]_INST_0_i_11 ;
  wire [2:0]\g[3]_INST_0_i_11_0 ;
  wire [2:0]\g[3]_INST_0_i_11_1 ;
  wire [2:0]\g[3]_INST_0_i_12 ;
  wire [2:0]\g[3]_INST_0_i_12_0 ;
  wire [2:0]\g[3]_INST_0_i_12_1 ;
  wire [2:0]\g[3]_INST_0_i_12_2 ;
  wire [2:0]\g[3]_INST_0_i_8 ;
  wire [2:0]\g[3]_INST_0_i_8_0 ;
  wire [2:0]\g[3]_INST_0_i_8_1 ;
  wire hs;
  wire left_o;
  wire pixel_clk;
  wire [1:0]r;
  wire [2:0]\r[1]_INST_0_i_20 ;
  wire [2:0]\r[1]_INST_0_i_20_0 ;
  wire [2:0]\r[1]_INST_0_i_20_1 ;
  wire [2:0]\r[1]_INST_0_i_20_2 ;
  wire [2:0]\r[1]_INST_0_i_21 ;
  wire [2:0]\r[1]_INST_0_i_21_0 ;
  wire [2:0]\r[1]_INST_0_i_21_1 ;
  wire [2:0]\r[1]_INST_0_i_21_2 ;
  wire [2:0]\r[1]_INST_0_i_22 ;
  wire [2:0]\r[1]_INST_0_i_22_0 ;
  wire [2:0]\r[1]_INST_0_i_22_1 ;
  wire [2:0]\r[1]_INST_0_i_22_2 ;
  wire [2:0]\r[1]_INST_0_i_27 ;
  wire [2:0]\r[1]_INST_0_i_27_0 ;
  wire [2:0]\r[1]_INST_0_i_27_1 ;
  wire [2:0]\r[1]_INST_0_i_27_2 ;
  wire [2:0]\r[1]_INST_0_i_6 ;
  wire [2:0]\r[1]_INST_0_i_6_0 ;
  wire [2:0]\r[1]_INST_0_i_6_1 ;
  wire right_o;
  wire rotate_o;
  wire sw_pause;
  wire sw_pause_o;
  wire sw_rst;
  wire sw_rst_o;
  wire vs;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce debounce_down
       (.btn_down(btn_down),
        .down_o(down_o),
        .pixel_clk(pixel_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_0 debounce_drop
       (.btn_drop(btn_drop),
        .drop_o(drop_o),
        .pixel_clk(pixel_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_1 debounce_left
       (.btn_left(btn_left),
        .left_o(left_o),
        .pixel_clk(pixel_clk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_2 debounce_right
       (.btn_right(btn_right),
        .pixel_clk(pixel_clk),
        .right_o(right_o));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_3 debounce_rotate
       (.btn_rotate(btn_rotate),
        .pixel_clk(pixel_clk),
        .rotate_o(rotate_o));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_4 debounce_sw_pause
       (.pixel_clk(pixel_clk),
        .sw_pause(sw_pause),
        .sw_pause_o(sw_pause_o));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_5 debounce_sw_rst
       (.pixel_clk(pixel_clk),
        .sw_rst(sw_rst),
        .sw_rst_o(sw_rst_o));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga vga_inst
       (.S(S),
        .b(b),
        .\counterX_reg[7]_0 (\counterX_reg[7] ),
        .cur_blk_index(cur_blk_index),
        .g(g),
        .\g[3]_INST_0_i_10_0 (\g[3]_INST_0_i_10 ),
        .\g[3]_INST_0_i_10_1 (\g[3]_INST_0_i_10_0 ),
        .\g[3]_INST_0_i_10_2 (\g[3]_INST_0_i_10_1 ),
        .\g[3]_INST_0_i_10_3 (\g[3]_INST_0_i_10_2 ),
        .\g[3]_INST_0_i_11_0 (\g[3]_INST_0_i_11 ),
        .\g[3]_INST_0_i_11_1 (\g[3]_INST_0_i_11_0 ),
        .\g[3]_INST_0_i_11_2 (\g[3]_INST_0_i_11_1 ),
        .\g[3]_INST_0_i_12_0 (\g[3]_INST_0_i_12 ),
        .\g[3]_INST_0_i_12_1 (\g[3]_INST_0_i_12_0 ),
        .\g[3]_INST_0_i_12_2 (\g[3]_INST_0_i_12_1 ),
        .\g[3]_INST_0_i_12_3 (\g[3]_INST_0_i_12_2 ),
        .\g[3]_INST_0_i_8_0 (\g[3]_INST_0_i_8 ),
        .\g[3]_INST_0_i_8_1 (\g[3]_INST_0_i_8_0 ),
        .\g[3]_INST_0_i_8_2 (\g[3]_INST_0_i_8_1 ),
        .hs(hs),
        .pixel_clk(pixel_clk),
        .r(r),
        .\r[1]_INST_0_i_20_0 (\r[1]_INST_0_i_20 ),
        .\r[1]_INST_0_i_20_1 (\r[1]_INST_0_i_20_0 ),
        .\r[1]_INST_0_i_20_2 (\r[1]_INST_0_i_20_1 ),
        .\r[1]_INST_0_i_20_3 (\r[1]_INST_0_i_20_2 ),
        .\r[1]_INST_0_i_21_0 (\r[1]_INST_0_i_21 ),
        .\r[1]_INST_0_i_21_1 (\r[1]_INST_0_i_21_0 ),
        .\r[1]_INST_0_i_21_2 (\r[1]_INST_0_i_21_1 ),
        .\r[1]_INST_0_i_21_3 (\r[1]_INST_0_i_21_2 ),
        .\r[1]_INST_0_i_22_0 (\r[1]_INST_0_i_22 ),
        .\r[1]_INST_0_i_22_1 (\r[1]_INST_0_i_22_0 ),
        .\r[1]_INST_0_i_22_2 (\r[1]_INST_0_i_22_1 ),
        .\r[1]_INST_0_i_22_3 (\r[1]_INST_0_i_22_2 ),
        .\r[1]_INST_0_i_27_0 (\r[1]_INST_0_i_27 ),
        .\r[1]_INST_0_i_27_1 (\r[1]_INST_0_i_27_0 ),
        .\r[1]_INST_0_i_27_2 (\r[1]_INST_0_i_27_1 ),
        .\r[1]_INST_0_i_27_3 (\r[1]_INST_0_i_27_2 ),
        .\r[1]_INST_0_i_6_0 (\r[1]_INST_0_i_6 ),
        .\r[1]_INST_0_i_6_1 (\r[1]_INST_0_i_6_0 ),
        .\r[1]_INST_0_i_6_2 (\r[1]_INST_0_i_6_1 ),
        .vs(vs));
endmodule

(* CHECK_LICENSE_TYPE = "tetris_tetris_0_0,tetris,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "package_project" *) 
(* X_CORE_INFO = "tetris,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    pixel_clk,
    btn_down,
    btn_left,
    btn_right,
    btn_rotate,
    btn_drop,
    sw_rst,
    sw_pause,
    rotate_o,
    down_o,
    left_o,
    right_o,
    drop_o,
    sw_rst_o,
    sw_pause_o,
    r,
    g,
    b,
    hs,
    vs);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 pixel_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME pixel_clk, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN tetris_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, FREQ_HZ 25173010, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input pixel_clk;
  input btn_down;
  input btn_left;
  input btn_right;
  input btn_rotate;
  input btn_drop;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 sw_rst RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sw_rst, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input sw_rst;
  input sw_pause;
  output rotate_o;
  output down_o;
  output left_o;
  output right_o;
  output drop_o;
  output sw_rst_o;
  output sw_pause_o;
  output [3:0]r;
  output [3:0]g;
  output [3:0]b;
  output hs;
  output vs;

  wire [3:1]\^b ;
  wire btn_down;
  wire btn_drop;
  wire btn_left;
  wire btn_right;
  wire btn_rotate;
  wire down_o;
  wire drop_o;
  wire [3:0]\^g ;
  wire \g[3]_INST_0_i_28_n_0 ;
  wire \g[3]_INST_0_i_29_n_0 ;
  wire \g[3]_INST_0_i_30_n_0 ;
  wire \g[3]_INST_0_i_32_n_0 ;
  wire \g[3]_INST_0_i_33_n_0 ;
  wire \g[3]_INST_0_i_34_n_0 ;
  wire \g[3]_INST_0_i_36_n_0 ;
  wire \g[3]_INST_0_i_37_n_0 ;
  wire \g[3]_INST_0_i_38_n_0 ;
  wire \g[3]_INST_0_i_40_n_0 ;
  wire \g[3]_INST_0_i_41_n_0 ;
  wire \g[3]_INST_0_i_42_n_0 ;
  wire \g[3]_INST_0_i_44_n_0 ;
  wire \g[3]_INST_0_i_45_n_0 ;
  wire \g[3]_INST_0_i_46_n_0 ;
  wire \g[3]_INST_0_i_48_n_0 ;
  wire \g[3]_INST_0_i_49_n_0 ;
  wire \g[3]_INST_0_i_50_n_0 ;
  wire \g[3]_INST_0_i_52_n_0 ;
  wire \g[3]_INST_0_i_53_n_0 ;
  wire \g[3]_INST_0_i_54_n_0 ;
  wire \g[3]_INST_0_i_56_n_0 ;
  wire \g[3]_INST_0_i_57_n_0 ;
  wire \g[3]_INST_0_i_58_n_0 ;
  wire \g[3]_INST_0_i_60_n_0 ;
  wire \g[3]_INST_0_i_61_n_0 ;
  wire \g[3]_INST_0_i_62_n_0 ;
  wire \g[3]_INST_0_i_64_n_0 ;
  wire \g[3]_INST_0_i_65_n_0 ;
  wire \g[3]_INST_0_i_66_n_0 ;
  wire \g[3]_INST_0_i_68_n_0 ;
  wire \g[3]_INST_0_i_69_n_0 ;
  wire \g[3]_INST_0_i_70_n_0 ;
  wire \g[3]_INST_0_i_72_n_0 ;
  wire \g[3]_INST_0_i_73_n_0 ;
  wire \g[3]_INST_0_i_74_n_0 ;
  wire \g[3]_INST_0_i_76_n_0 ;
  wire \g[3]_INST_0_i_77_n_0 ;
  wire \g[3]_INST_0_i_78_n_0 ;
  wire \g[3]_INST_0_i_80_n_0 ;
  wire \g[3]_INST_0_i_81_n_0 ;
  wire \g[3]_INST_0_i_82_n_0 ;
  wire \g[3]_INST_0_i_84_n_0 ;
  wire \g[3]_INST_0_i_85_n_0 ;
  wire \g[3]_INST_0_i_86_n_0 ;
  wire hs;
  wire left_o;
  wire pixel_clk;
  wire [1:0]\^r ;
  wire \r[1]_INST_0_i_100_n_0 ;
  wire \r[1]_INST_0_i_102_n_0 ;
  wire \r[1]_INST_0_i_103_n_0 ;
  wire \r[1]_INST_0_i_104_n_0 ;
  wire \r[1]_INST_0_i_106_n_0 ;
  wire \r[1]_INST_0_i_107_n_0 ;
  wire \r[1]_INST_0_i_108_n_0 ;
  wire \r[1]_INST_0_i_110_n_0 ;
  wire \r[1]_INST_0_i_111_n_0 ;
  wire \r[1]_INST_0_i_112_n_0 ;
  wire \r[1]_INST_0_i_114_n_0 ;
  wire \r[1]_INST_0_i_115_n_0 ;
  wire \r[1]_INST_0_i_116_n_0 ;
  wire \r[1]_INST_0_i_118_n_0 ;
  wire \r[1]_INST_0_i_119_n_0 ;
  wire \r[1]_INST_0_i_120_n_0 ;
  wire \r[1]_INST_0_i_122_n_0 ;
  wire \r[1]_INST_0_i_123_n_0 ;
  wire \r[1]_INST_0_i_124_n_0 ;
  wire \r[1]_INST_0_i_127_n_0 ;
  wire \r[1]_INST_0_i_128_n_0 ;
  wire \r[1]_INST_0_i_129_n_0 ;
  wire \r[1]_INST_0_i_131_n_0 ;
  wire \r[1]_INST_0_i_132_n_0 ;
  wire \r[1]_INST_0_i_133_n_0 ;
  wire \r[1]_INST_0_i_135_n_0 ;
  wire \r[1]_INST_0_i_136_n_0 ;
  wire \r[1]_INST_0_i_137_n_0 ;
  wire \r[1]_INST_0_i_139_n_0 ;
  wire \r[1]_INST_0_i_140_n_0 ;
  wire \r[1]_INST_0_i_141_n_0 ;
  wire \r[1]_INST_0_i_57_n_0 ;
  wire \r[1]_INST_0_i_58_n_0 ;
  wire \r[1]_INST_0_i_59_n_0 ;
  wire \r[1]_INST_0_i_61_n_0 ;
  wire \r[1]_INST_0_i_62_n_0 ;
  wire \r[1]_INST_0_i_63_n_0 ;
  wire \r[1]_INST_0_i_65_n_0 ;
  wire \r[1]_INST_0_i_66_n_0 ;
  wire \r[1]_INST_0_i_67_n_0 ;
  wire \r[1]_INST_0_i_78_n_0 ;
  wire \r[1]_INST_0_i_79_n_0 ;
  wire \r[1]_INST_0_i_80_n_0 ;
  wire \r[1]_INST_0_i_82_n_0 ;
  wire \r[1]_INST_0_i_83_n_0 ;
  wire \r[1]_INST_0_i_84_n_0 ;
  wire \r[1]_INST_0_i_86_n_0 ;
  wire \r[1]_INST_0_i_87_n_0 ;
  wire \r[1]_INST_0_i_88_n_0 ;
  wire \r[1]_INST_0_i_90_n_0 ;
  wire \r[1]_INST_0_i_91_n_0 ;
  wire \r[1]_INST_0_i_92_n_0 ;
  wire \r[1]_INST_0_i_94_n_0 ;
  wire \r[1]_INST_0_i_95_n_0 ;
  wire \r[1]_INST_0_i_96_n_0 ;
  wire \r[1]_INST_0_i_98_n_0 ;
  wire \r[1]_INST_0_i_99_n_0 ;
  wire right_o;
  wire rotate_o;
  wire sw_pause;
  wire sw_pause_o;
  wire sw_rst;
  wire sw_rst_o;
  wire [9:8]\vga_inst/cur_blk_index ;
  wire [7:3]\vga_inst/cur_blk_index__0 ;
  wire vs;

  assign b[3] = \^b [3];
  assign b[2] = \^b [3];
  assign b[1] = \^b [1];
  assign b[0] = \^b [3];
  assign g[3:2] = \^g [3:2];
  assign g[1] = \^g [0];
  assign g[0] = \^g [0];
  assign r[3] = \^r [1];
  assign r[2] = \^r [1];
  assign r[1:0] = \^r [1:0];
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_28 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_28_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_29 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_29_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \g[3]_INST_0_i_30 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [3]),
        .I2(\vga_inst/cur_blk_index__0 [4]),
        .O(\g[3]_INST_0_i_30_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_32 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_32_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_33 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_33_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \g[3]_INST_0_i_34 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\g[3]_INST_0_i_34_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_36 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_36_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_37 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_37_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \g[3]_INST_0_i_38 
       (.I0(\vga_inst/cur_blk_index__0 [3]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [5]),
        .O(\g[3]_INST_0_i_38_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_40 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_40_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_41 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_41_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \g[3]_INST_0_i_42 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [3]),
        .I2(\vga_inst/cur_blk_index__0 [4]),
        .O(\g[3]_INST_0_i_42_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_44 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_44_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_45 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_45_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \g[3]_INST_0_i_46 
       (.I0(\vga_inst/cur_blk_index__0 [4]),
        .I1(\vga_inst/cur_blk_index__0 [5]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\g[3]_INST_0_i_46_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_48 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_48_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_49 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_49_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \g[3]_INST_0_i_50 
       (.I0(\vga_inst/cur_blk_index__0 [3]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [5]),
        .O(\g[3]_INST_0_i_50_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_52 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_52_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_53 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_53_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \g[3]_INST_0_i_54 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [3]),
        .I2(\vga_inst/cur_blk_index__0 [4]),
        .O(\g[3]_INST_0_i_54_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_56 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_56_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_57 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_57_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \g[3]_INST_0_i_58 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\g[3]_INST_0_i_58_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_60 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_60_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_61 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_61_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \g[3]_INST_0_i_62 
       (.I0(\vga_inst/cur_blk_index__0 [4]),
        .I1(\vga_inst/cur_blk_index__0 [5]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\g[3]_INST_0_i_62_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_64 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_64_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_65 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_65_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \g[3]_INST_0_i_66 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\g[3]_INST_0_i_66_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_68 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_68_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_69 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_69_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \g[3]_INST_0_i_70 
       (.I0(\vga_inst/cur_blk_index__0 [3]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [5]),
        .O(\g[3]_INST_0_i_70_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_72 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_72_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_73 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_73_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \g[3]_INST_0_i_74 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [3]),
        .I2(\vga_inst/cur_blk_index__0 [4]),
        .O(\g[3]_INST_0_i_74_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_76 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_76_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_77 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_77_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \g[3]_INST_0_i_78 
       (.I0(\vga_inst/cur_blk_index__0 [4]),
        .I1(\vga_inst/cur_blk_index__0 [5]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\g[3]_INST_0_i_78_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_80 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_80_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_81 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_81_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \g[3]_INST_0_i_82 
       (.I0(\vga_inst/cur_blk_index__0 [3]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [5]),
        .O(\g[3]_INST_0_i_82_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \g[3]_INST_0_i_84 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\g[3]_INST_0_i_84_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_85 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\g[3]_INST_0_i_85_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \g[3]_INST_0_i_86 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\g[3]_INST_0_i_86_n_0 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_tetris inst
       (.S({\g[3]_INST_0_i_68_n_0 ,\g[3]_INST_0_i_69_n_0 ,\g[3]_INST_0_i_70_n_0 }),
        .b({\^b [3],\^b [1]}),
        .btn_down(btn_down),
        .btn_drop(btn_drop),
        .btn_left(btn_left),
        .btn_right(btn_right),
        .btn_rotate(btn_rotate),
        .\counterX_reg[7] (\vga_inst/cur_blk_index__0 ),
        .cur_blk_index(\vga_inst/cur_blk_index ),
        .down_o(down_o),
        .drop_o(drop_o),
        .g({\^g [3:2],\^g [0]}),
        .\g[3]_INST_0_i_10 ({\g[3]_INST_0_i_44_n_0 ,\g[3]_INST_0_i_45_n_0 ,\g[3]_INST_0_i_46_n_0 }),
        .\g[3]_INST_0_i_10_0 ({\g[3]_INST_0_i_52_n_0 ,\g[3]_INST_0_i_53_n_0 ,\g[3]_INST_0_i_54_n_0 }),
        .\g[3]_INST_0_i_10_1 ({\g[3]_INST_0_i_40_n_0 ,\g[3]_INST_0_i_41_n_0 ,\g[3]_INST_0_i_42_n_0 }),
        .\g[3]_INST_0_i_10_2 ({\g[3]_INST_0_i_48_n_0 ,\g[3]_INST_0_i_49_n_0 ,\g[3]_INST_0_i_50_n_0 }),
        .\g[3]_INST_0_i_11 ({\g[3]_INST_0_i_60_n_0 ,\g[3]_INST_0_i_61_n_0 ,\g[3]_INST_0_i_62_n_0 }),
        .\g[3]_INST_0_i_11_0 ({\g[3]_INST_0_i_56_n_0 ,\g[3]_INST_0_i_57_n_0 ,\g[3]_INST_0_i_58_n_0 }),
        .\g[3]_INST_0_i_11_1 ({\g[3]_INST_0_i_64_n_0 ,\g[3]_INST_0_i_65_n_0 ,\g[3]_INST_0_i_66_n_0 }),
        .\g[3]_INST_0_i_12 ({\g[3]_INST_0_i_76_n_0 ,\g[3]_INST_0_i_77_n_0 ,\g[3]_INST_0_i_78_n_0 }),
        .\g[3]_INST_0_i_12_0 ({\g[3]_INST_0_i_84_n_0 ,\g[3]_INST_0_i_85_n_0 ,\g[3]_INST_0_i_86_n_0 }),
        .\g[3]_INST_0_i_12_1 ({\g[3]_INST_0_i_72_n_0 ,\g[3]_INST_0_i_73_n_0 ,\g[3]_INST_0_i_74_n_0 }),
        .\g[3]_INST_0_i_12_2 ({\g[3]_INST_0_i_80_n_0 ,\g[3]_INST_0_i_81_n_0 ,\g[3]_INST_0_i_82_n_0 }),
        .\g[3]_INST_0_i_8 ({\g[3]_INST_0_i_36_n_0 ,\g[3]_INST_0_i_37_n_0 ,\g[3]_INST_0_i_38_n_0 }),
        .\g[3]_INST_0_i_8_0 ({\g[3]_INST_0_i_28_n_0 ,\g[3]_INST_0_i_29_n_0 ,\g[3]_INST_0_i_30_n_0 }),
        .\g[3]_INST_0_i_8_1 ({\g[3]_INST_0_i_32_n_0 ,\g[3]_INST_0_i_33_n_0 ,\g[3]_INST_0_i_34_n_0 }),
        .hs(hs),
        .left_o(left_o),
        .pixel_clk(pixel_clk),
        .r(\^r ),
        .\r[1]_INST_0_i_20 ({\r[1]_INST_0_i_90_n_0 ,\r[1]_INST_0_i_91_n_0 ,\r[1]_INST_0_i_92_n_0 }),
        .\r[1]_INST_0_i_20_0 ({\r[1]_INST_0_i_86_n_0 ,\r[1]_INST_0_i_87_n_0 ,\r[1]_INST_0_i_88_n_0 }),
        .\r[1]_INST_0_i_20_1 ({\r[1]_INST_0_i_82_n_0 ,\r[1]_INST_0_i_83_n_0 ,\r[1]_INST_0_i_84_n_0 }),
        .\r[1]_INST_0_i_20_2 ({\r[1]_INST_0_i_78_n_0 ,\r[1]_INST_0_i_79_n_0 ,\r[1]_INST_0_i_80_n_0 }),
        .\r[1]_INST_0_i_21 ({\r[1]_INST_0_i_98_n_0 ,\r[1]_INST_0_i_99_n_0 ,\r[1]_INST_0_i_100_n_0 }),
        .\r[1]_INST_0_i_21_0 ({\r[1]_INST_0_i_106_n_0 ,\r[1]_INST_0_i_107_n_0 ,\r[1]_INST_0_i_108_n_0 }),
        .\r[1]_INST_0_i_21_1 ({\r[1]_INST_0_i_102_n_0 ,\r[1]_INST_0_i_103_n_0 ,\r[1]_INST_0_i_104_n_0 }),
        .\r[1]_INST_0_i_21_2 ({\r[1]_INST_0_i_94_n_0 ,\r[1]_INST_0_i_95_n_0 ,\r[1]_INST_0_i_96_n_0 }),
        .\r[1]_INST_0_i_22 ({\r[1]_INST_0_i_122_n_0 ,\r[1]_INST_0_i_123_n_0 ,\r[1]_INST_0_i_124_n_0 }),
        .\r[1]_INST_0_i_22_0 ({\r[1]_INST_0_i_114_n_0 ,\r[1]_INST_0_i_115_n_0 ,\r[1]_INST_0_i_116_n_0 }),
        .\r[1]_INST_0_i_22_1 ({\r[1]_INST_0_i_118_n_0 ,\r[1]_INST_0_i_119_n_0 ,\r[1]_INST_0_i_120_n_0 }),
        .\r[1]_INST_0_i_22_2 ({\r[1]_INST_0_i_110_n_0 ,\r[1]_INST_0_i_111_n_0 ,\r[1]_INST_0_i_112_n_0 }),
        .\r[1]_INST_0_i_27 ({\r[1]_INST_0_i_135_n_0 ,\r[1]_INST_0_i_136_n_0 ,\r[1]_INST_0_i_137_n_0 }),
        .\r[1]_INST_0_i_27_0 ({\r[1]_INST_0_i_127_n_0 ,\r[1]_INST_0_i_128_n_0 ,\r[1]_INST_0_i_129_n_0 }),
        .\r[1]_INST_0_i_27_1 ({\r[1]_INST_0_i_131_n_0 ,\r[1]_INST_0_i_132_n_0 ,\r[1]_INST_0_i_133_n_0 }),
        .\r[1]_INST_0_i_27_2 ({\r[1]_INST_0_i_139_n_0 ,\r[1]_INST_0_i_140_n_0 ,\r[1]_INST_0_i_141_n_0 }),
        .\r[1]_INST_0_i_6 ({\r[1]_INST_0_i_57_n_0 ,\r[1]_INST_0_i_58_n_0 ,\r[1]_INST_0_i_59_n_0 }),
        .\r[1]_INST_0_i_6_0 ({\r[1]_INST_0_i_65_n_0 ,\r[1]_INST_0_i_66_n_0 ,\r[1]_INST_0_i_67_n_0 }),
        .\r[1]_INST_0_i_6_1 ({\r[1]_INST_0_i_61_n_0 ,\r[1]_INST_0_i_62_n_0 ,\r[1]_INST_0_i_63_n_0 }),
        .right_o(right_o),
        .rotate_o(rotate_o),
        .sw_pause(sw_pause),
        .sw_pause_o(sw_pause_o),
        .sw_rst(sw_rst),
        .sw_rst_o(sw_rst_o),
        .vs(vs));
  LUT3 #(
    .INIT(8'h02)) 
    \r[1]_INST_0_i_100 
       (.I0(\vga_inst/cur_blk_index__0 [3]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [5]),
        .O(\r[1]_INST_0_i_100_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_102 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_102_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_103 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_103_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \r[1]_INST_0_i_104 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\r[1]_INST_0_i_104_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_106 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_106_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_107 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_107_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \r[1]_INST_0_i_108 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [3]),
        .I2(\vga_inst/cur_blk_index__0 [4]),
        .O(\r[1]_INST_0_i_108_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_110 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_110_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_111 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_111_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \r[1]_INST_0_i_112 
       (.I0(\vga_inst/cur_blk_index__0 [4]),
        .I1(\vga_inst/cur_blk_index__0 [5]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\r[1]_INST_0_i_112_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_114 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_114_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_115 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_115_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \r[1]_INST_0_i_116 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [3]),
        .I2(\vga_inst/cur_blk_index__0 [4]),
        .O(\r[1]_INST_0_i_116_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_118 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_118_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_119 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_119_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \r[1]_INST_0_i_120 
       (.I0(\vga_inst/cur_blk_index__0 [4]),
        .I1(\vga_inst/cur_blk_index__0 [5]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\r[1]_INST_0_i_120_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_122 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_122_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_123 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_123_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \r[1]_INST_0_i_124 
       (.I0(\vga_inst/cur_blk_index__0 [3]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [5]),
        .O(\r[1]_INST_0_i_124_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_127 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_127_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_128 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_128_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_129 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\r[1]_INST_0_i_129_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_131 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_131_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_132 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_132_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_133 
       (.I0(\vga_inst/cur_blk_index__0 [4]),
        .I1(\vga_inst/cur_blk_index__0 [5]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\r[1]_INST_0_i_133_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_135 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_135_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_136 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_136_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_137 
       (.I0(\vga_inst/cur_blk_index__0 [4]),
        .I1(\vga_inst/cur_blk_index__0 [5]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\r[1]_INST_0_i_137_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_139 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_139_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_140 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_140_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_141 
       (.I0(\vga_inst/cur_blk_index__0 [4]),
        .I1(\vga_inst/cur_blk_index__0 [5]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\r[1]_INST_0_i_141_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_57 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_57_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_58 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_58_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \r[1]_INST_0_i_59 
       (.I0(\vga_inst/cur_blk_index__0 [4]),
        .I1(\vga_inst/cur_blk_index__0 [5]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\r[1]_INST_0_i_59_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_61 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_61_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_62 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_62_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \r[1]_INST_0_i_63 
       (.I0(\vga_inst/cur_blk_index__0 [3]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [5]),
        .O(\r[1]_INST_0_i_63_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_65 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_65_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_66 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_66_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \r[1]_INST_0_i_67 
       (.I0(\vga_inst/cur_blk_index__0 [4]),
        .I1(\vga_inst/cur_blk_index__0 [5]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\r[1]_INST_0_i_67_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_78 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_78_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_79 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_79_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \r[1]_INST_0_i_80 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\r[1]_INST_0_i_80_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_82 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_82_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_83 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_83_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \r[1]_INST_0_i_84 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [3]),
        .I2(\vga_inst/cur_blk_index__0 [4]),
        .O(\r[1]_INST_0_i_84_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_86 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_86_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_87 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_87_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \r[1]_INST_0_i_88 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [3]),
        .I2(\vga_inst/cur_blk_index__0 [4]),
        .O(\r[1]_INST_0_i_88_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_90 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_90_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_91 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_91_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \r[1]_INST_0_i_92 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\r[1]_INST_0_i_92_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_94 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_94_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_95 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_95_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \r[1]_INST_0_i_96 
       (.I0(\vga_inst/cur_blk_index__0 [5]),
        .I1(\vga_inst/cur_blk_index__0 [4]),
        .I2(\vga_inst/cur_blk_index__0 [3]),
        .O(\r[1]_INST_0_i_96_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \r[1]_INST_0_i_98 
       (.I0(\vga_inst/cur_blk_index [9]),
        .O(\r[1]_INST_0_i_98_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_99 
       (.I0(\vga_inst/cur_blk_index [8]),
        .I1(\vga_inst/cur_blk_index__0 [7]),
        .I2(\vga_inst/cur_blk_index__0 [6]),
        .O(\r[1]_INST_0_i_99_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga
   (g,
    b,
    r,
    \counterX_reg[7]_0 ,
    hs,
    vs,
    cur_blk_index,
    pixel_clk,
    S,
    \g[3]_INST_0_i_8_0 ,
    \g[3]_INST_0_i_11_0 ,
    \g[3]_INST_0_i_12_0 ,
    \g[3]_INST_0_i_10_0 ,
    \g[3]_INST_0_i_12_1 ,
    \g[3]_INST_0_i_11_1 ,
    \g[3]_INST_0_i_10_1 ,
    \g[3]_INST_0_i_12_2 ,
    \g[3]_INST_0_i_8_1 ,
    \g[3]_INST_0_i_10_2 ,
    \g[3]_INST_0_i_11_2 ,
    \g[3]_INST_0_i_8_2 ,
    \g[3]_INST_0_i_10_3 ,
    \g[3]_INST_0_i_12_3 ,
    \r[1]_INST_0_i_21_0 ,
    \r[1]_INST_0_i_22_0 ,
    \r[1]_INST_0_i_20_0 ,
    \r[1]_INST_0_i_21_1 ,
    \r[1]_INST_0_i_22_1 ,
    \r[1]_INST_0_i_6_0 ,
    \r[1]_INST_0_i_6_1 ,
    \r[1]_INST_0_i_22_2 ,
    \r[1]_INST_0_i_22_3 ,
    \r[1]_INST_0_i_20_1 ,
    \r[1]_INST_0_i_20_2 ,
    \r[1]_INST_0_i_21_2 ,
    \r[1]_INST_0_i_20_3 ,
    \r[1]_INST_0_i_21_3 ,
    \r[1]_INST_0_i_6_2 ,
    \r[1]_INST_0_i_27_0 ,
    \r[1]_INST_0_i_27_1 ,
    \r[1]_INST_0_i_27_2 ,
    \r[1]_INST_0_i_27_3 );
  output [2:0]g;
  output [1:0]b;
  output [1:0]r;
  output [4:0]\counterX_reg[7]_0 ;
  output hs;
  output vs;
  output [1:0]cur_blk_index;
  input pixel_clk;
  input [2:0]S;
  input [2:0]\g[3]_INST_0_i_8_0 ;
  input [2:0]\g[3]_INST_0_i_11_0 ;
  input [2:0]\g[3]_INST_0_i_12_0 ;
  input [2:0]\g[3]_INST_0_i_10_0 ;
  input [2:0]\g[3]_INST_0_i_12_1 ;
  input [2:0]\g[3]_INST_0_i_11_1 ;
  input [2:0]\g[3]_INST_0_i_10_1 ;
  input [2:0]\g[3]_INST_0_i_12_2 ;
  input [2:0]\g[3]_INST_0_i_8_1 ;
  input [2:0]\g[3]_INST_0_i_10_2 ;
  input [2:0]\g[3]_INST_0_i_11_2 ;
  input [2:0]\g[3]_INST_0_i_8_2 ;
  input [2:0]\g[3]_INST_0_i_10_3 ;
  input [2:0]\g[3]_INST_0_i_12_3 ;
  input [2:0]\r[1]_INST_0_i_21_0 ;
  input [2:0]\r[1]_INST_0_i_22_0 ;
  input [2:0]\r[1]_INST_0_i_20_0 ;
  input [2:0]\r[1]_INST_0_i_21_1 ;
  input [2:0]\r[1]_INST_0_i_22_1 ;
  input [2:0]\r[1]_INST_0_i_6_0 ;
  input [2:0]\r[1]_INST_0_i_6_1 ;
  input [2:0]\r[1]_INST_0_i_22_2 ;
  input [2:0]\r[1]_INST_0_i_22_3 ;
  input [2:0]\r[1]_INST_0_i_20_1 ;
  input [2:0]\r[1]_INST_0_i_20_2 ;
  input [2:0]\r[1]_INST_0_i_21_2 ;
  input [2:0]\r[1]_INST_0_i_20_3 ;
  input [2:0]\r[1]_INST_0_i_21_3 ;
  input [2:0]\r[1]_INST_0_i_6_2 ;
  input [2:0]\r[1]_INST_0_i_27_0 ;
  input [2:0]\r[1]_INST_0_i_27_1 ;
  input [2:0]\r[1]_INST_0_i_27_2 ;
  input [2:0]\r[1]_INST_0_i_27_3 ;

  wire [6:4]A;
  wire [9:9]C;
  wire [2:0]S;
  wire [1:0]b;
  wire \counterX[4]_i_1_n_0 ;
  wire \counterX[7]_i_1_n_0 ;
  wire \counterX[9]_i_1_n_0 ;
  wire \counterX[9]_i_3_n_0 ;
  wire [9:0]counterX_reg;
  wire [4:0]\counterX_reg[7]_0 ;
  wire counterY;
  wire \counterY[0]_i_1_n_0 ;
  wire \counterY[6]_i_1_n_0 ;
  wire \counterY[6]_i_2_n_0 ;
  wire \counterY[9]_i_3_n_0 ;
  wire [9:0]counterY_reg;
  wire [1:0]cur_blk_index;
  wire [9:2]cur_blk_index0;
  wire [2:1]cur_blk_index__0;
  wire [2:0]g;
  wire [2:0]\g[3]_INST_0_i_10_0 ;
  wire [2:0]\g[3]_INST_0_i_10_1 ;
  wire [2:0]\g[3]_INST_0_i_10_2 ;
  wire [2:0]\g[3]_INST_0_i_10_3 ;
  wire \g[3]_INST_0_i_10_n_0 ;
  wire [2:0]\g[3]_INST_0_i_11_0 ;
  wire [2:0]\g[3]_INST_0_i_11_1 ;
  wire [2:0]\g[3]_INST_0_i_11_2 ;
  wire \g[3]_INST_0_i_11_n_0 ;
  wire [2:0]\g[3]_INST_0_i_12_0 ;
  wire [2:0]\g[3]_INST_0_i_12_1 ;
  wire [2:0]\g[3]_INST_0_i_12_2 ;
  wire [2:0]\g[3]_INST_0_i_12_3 ;
  wire \g[3]_INST_0_i_12_n_0 ;
  wire \g[3]_INST_0_i_13_n_1 ;
  wire \g[3]_INST_0_i_13_n_2 ;
  wire \g[3]_INST_0_i_13_n_3 ;
  wire \g[3]_INST_0_i_14_n_1 ;
  wire \g[3]_INST_0_i_14_n_2 ;
  wire \g[3]_INST_0_i_14_n_3 ;
  wire \g[3]_INST_0_i_15_n_1 ;
  wire \g[3]_INST_0_i_15_n_2 ;
  wire \g[3]_INST_0_i_15_n_3 ;
  wire \g[3]_INST_0_i_16_n_1 ;
  wire \g[3]_INST_0_i_16_n_2 ;
  wire \g[3]_INST_0_i_16_n_3 ;
  wire \g[3]_INST_0_i_17_n_1 ;
  wire \g[3]_INST_0_i_17_n_2 ;
  wire \g[3]_INST_0_i_17_n_3 ;
  wire \g[3]_INST_0_i_18_n_1 ;
  wire \g[3]_INST_0_i_18_n_2 ;
  wire \g[3]_INST_0_i_18_n_3 ;
  wire \g[3]_INST_0_i_19_n_1 ;
  wire \g[3]_INST_0_i_19_n_2 ;
  wire \g[3]_INST_0_i_19_n_3 ;
  wire \g[3]_INST_0_i_1_n_0 ;
  wire \g[3]_INST_0_i_20_n_1 ;
  wire \g[3]_INST_0_i_20_n_2 ;
  wire \g[3]_INST_0_i_20_n_3 ;
  wire \g[3]_INST_0_i_21_n_1 ;
  wire \g[3]_INST_0_i_21_n_2 ;
  wire \g[3]_INST_0_i_21_n_3 ;
  wire \g[3]_INST_0_i_22_n_1 ;
  wire \g[3]_INST_0_i_22_n_2 ;
  wire \g[3]_INST_0_i_22_n_3 ;
  wire \g[3]_INST_0_i_23_n_1 ;
  wire \g[3]_INST_0_i_23_n_2 ;
  wire \g[3]_INST_0_i_23_n_3 ;
  wire \g[3]_INST_0_i_24_n_1 ;
  wire \g[3]_INST_0_i_24_n_2 ;
  wire \g[3]_INST_0_i_24_n_3 ;
  wire \g[3]_INST_0_i_25_n_1 ;
  wire \g[3]_INST_0_i_25_n_2 ;
  wire \g[3]_INST_0_i_25_n_3 ;
  wire \g[3]_INST_0_i_26_n_1 ;
  wire \g[3]_INST_0_i_26_n_2 ;
  wire \g[3]_INST_0_i_26_n_3 ;
  wire \g[3]_INST_0_i_27_n_1 ;
  wire \g[3]_INST_0_i_27_n_2 ;
  wire \g[3]_INST_0_i_27_n_3 ;
  wire \g[3]_INST_0_i_2_n_0 ;
  wire \g[3]_INST_0_i_31_n_0 ;
  wire \g[3]_INST_0_i_35_n_0 ;
  wire \g[3]_INST_0_i_39_n_0 ;
  wire \g[3]_INST_0_i_3_n_0 ;
  wire \g[3]_INST_0_i_43_n_0 ;
  wire \g[3]_INST_0_i_47_n_0 ;
  wire \g[3]_INST_0_i_4_n_0 ;
  wire \g[3]_INST_0_i_51_n_0 ;
  wire \g[3]_INST_0_i_55_n_0 ;
  wire \g[3]_INST_0_i_59_n_0 ;
  wire \g[3]_INST_0_i_5_n_0 ;
  wire \g[3]_INST_0_i_63_n_0 ;
  wire \g[3]_INST_0_i_67_n_0 ;
  wire \g[3]_INST_0_i_6_n_0 ;
  wire \g[3]_INST_0_i_71_n_0 ;
  wire \g[3]_INST_0_i_75_n_0 ;
  wire \g[3]_INST_0_i_79_n_0 ;
  wire \g[3]_INST_0_i_7_n_0 ;
  wire \g[3]_INST_0_i_83_n_0 ;
  wire \g[3]_INST_0_i_87_n_0 ;
  wire [2:0]\g[3]_INST_0_i_8_0 ;
  wire [2:0]\g[3]_INST_0_i_8_1 ;
  wire [2:0]\g[3]_INST_0_i_8_2 ;
  wire \g[3]_INST_0_i_8_n_0 ;
  wire \g[3]_INST_0_i_9_n_0 ;
  wire hs;
  wire [9:0]p_0_in;
  wire [9:1]p_0_in__0;
  wire pixel_clk;
  wire [1:0]r;
  wire \r[0]_INST_0_i_1_n_0 ;
  wire \r[0]_INST_0_i_2_n_0 ;
  wire \r[0]_INST_0_i_3_n_0 ;
  wire \r[0]_INST_0_i_4_n_0 ;
  wire \r[0]_INST_0_i_6_n_0 ;
  wire \r[0]_INST_0_i_7_n_0 ;
  wire \r[0]_INST_0_i_8_n_0 ;
  wire \r[1]_INST_0_i_101_n_0 ;
  wire \r[1]_INST_0_i_105_n_0 ;
  wire \r[1]_INST_0_i_109_n_0 ;
  wire \r[1]_INST_0_i_10_n_0 ;
  wire \r[1]_INST_0_i_10_n_1 ;
  wire \r[1]_INST_0_i_10_n_2 ;
  wire \r[1]_INST_0_i_10_n_3 ;
  wire \r[1]_INST_0_i_113_n_0 ;
  wire \r[1]_INST_0_i_117_n_0 ;
  wire \r[1]_INST_0_i_11_n_0 ;
  wire \r[1]_INST_0_i_11_n_1 ;
  wire \r[1]_INST_0_i_11_n_2 ;
  wire \r[1]_INST_0_i_11_n_3 ;
  wire \r[1]_INST_0_i_121_n_0 ;
  wire \r[1]_INST_0_i_125_n_0 ;
  wire \r[1]_INST_0_i_12_n_0 ;
  wire \r[1]_INST_0_i_130_n_0 ;
  wire \r[1]_INST_0_i_134_n_0 ;
  wire \r[1]_INST_0_i_138_n_0 ;
  wire \r[1]_INST_0_i_13_n_0 ;
  wire \r[1]_INST_0_i_142_n_0 ;
  wire \r[1]_INST_0_i_143_n_0 ;
  wire \r[1]_INST_0_i_14_n_0 ;
  wire \r[1]_INST_0_i_15_n_0 ;
  wire \r[1]_INST_0_i_18_n_0 ;
  wire \r[1]_INST_0_i_19_n_0 ;
  wire \r[1]_INST_0_i_1_n_0 ;
  wire [2:0]\r[1]_INST_0_i_20_0 ;
  wire [2:0]\r[1]_INST_0_i_20_1 ;
  wire [2:0]\r[1]_INST_0_i_20_2 ;
  wire [2:0]\r[1]_INST_0_i_20_3 ;
  wire \r[1]_INST_0_i_20_n_0 ;
  wire [2:0]\r[1]_INST_0_i_21_0 ;
  wire [2:0]\r[1]_INST_0_i_21_1 ;
  wire [2:0]\r[1]_INST_0_i_21_2 ;
  wire [2:0]\r[1]_INST_0_i_21_3 ;
  wire \r[1]_INST_0_i_21_n_0 ;
  wire [2:0]\r[1]_INST_0_i_22_0 ;
  wire [2:0]\r[1]_INST_0_i_22_1 ;
  wire [2:0]\r[1]_INST_0_i_22_2 ;
  wire [2:0]\r[1]_INST_0_i_22_3 ;
  wire \r[1]_INST_0_i_22_n_0 ;
  wire \r[1]_INST_0_i_23_n_1 ;
  wire \r[1]_INST_0_i_23_n_2 ;
  wire \r[1]_INST_0_i_23_n_3 ;
  wire \r[1]_INST_0_i_24_n_1 ;
  wire \r[1]_INST_0_i_24_n_2 ;
  wire \r[1]_INST_0_i_24_n_3 ;
  wire \r[1]_INST_0_i_25_n_1 ;
  wire \r[1]_INST_0_i_25_n_2 ;
  wire \r[1]_INST_0_i_25_n_3 ;
  wire \r[1]_INST_0_i_26_n_0 ;
  wire [2:0]\r[1]_INST_0_i_27_0 ;
  wire [2:0]\r[1]_INST_0_i_27_1 ;
  wire [2:0]\r[1]_INST_0_i_27_2 ;
  wire [2:0]\r[1]_INST_0_i_27_3 ;
  wire \r[1]_INST_0_i_27_n_0 ;
  wire \r[1]_INST_0_i_28_n_0 ;
  wire \r[1]_INST_0_i_29_n_0 ;
  wire \r[1]_INST_0_i_2_n_0 ;
  wire \r[1]_INST_0_i_30_n_0 ;
  wire \r[1]_INST_0_i_31_n_0 ;
  wire \r[1]_INST_0_i_32_n_0 ;
  wire \r[1]_INST_0_i_33_n_0 ;
  wire \r[1]_INST_0_i_34_n_0 ;
  wire \r[1]_INST_0_i_35_n_0 ;
  wire \r[1]_INST_0_i_37_n_0 ;
  wire \r[1]_INST_0_i_38_n_0 ;
  wire \r[1]_INST_0_i_39_n_0 ;
  wire \r[1]_INST_0_i_3_n_0 ;
  wire \r[1]_INST_0_i_3_n_1 ;
  wire \r[1]_INST_0_i_3_n_2 ;
  wire \r[1]_INST_0_i_3_n_3 ;
  wire \r[1]_INST_0_i_40_n_0 ;
  wire \r[1]_INST_0_i_41_n_0 ;
  wire \r[1]_INST_0_i_42_n_0 ;
  wire \r[1]_INST_0_i_43_n_2 ;
  wire \r[1]_INST_0_i_43_n_3 ;
  wire \r[1]_INST_0_i_44_n_0 ;
  wire \r[1]_INST_0_i_45_n_1 ;
  wire \r[1]_INST_0_i_45_n_2 ;
  wire \r[1]_INST_0_i_45_n_3 ;
  wire \r[1]_INST_0_i_46_n_1 ;
  wire \r[1]_INST_0_i_46_n_2 ;
  wire \r[1]_INST_0_i_46_n_3 ;
  wire \r[1]_INST_0_i_47_n_1 ;
  wire \r[1]_INST_0_i_47_n_2 ;
  wire \r[1]_INST_0_i_47_n_3 ;
  wire \r[1]_INST_0_i_48_n_1 ;
  wire \r[1]_INST_0_i_48_n_2 ;
  wire \r[1]_INST_0_i_48_n_3 ;
  wire \r[1]_INST_0_i_49_n_1 ;
  wire \r[1]_INST_0_i_49_n_2 ;
  wire \r[1]_INST_0_i_49_n_3 ;
  wire \r[1]_INST_0_i_4_n_0 ;
  wire \r[1]_INST_0_i_50_n_1 ;
  wire \r[1]_INST_0_i_50_n_2 ;
  wire \r[1]_INST_0_i_50_n_3 ;
  wire \r[1]_INST_0_i_51_n_1 ;
  wire \r[1]_INST_0_i_51_n_2 ;
  wire \r[1]_INST_0_i_51_n_3 ;
  wire \r[1]_INST_0_i_52_n_1 ;
  wire \r[1]_INST_0_i_52_n_2 ;
  wire \r[1]_INST_0_i_52_n_3 ;
  wire \r[1]_INST_0_i_53_n_1 ;
  wire \r[1]_INST_0_i_53_n_2 ;
  wire \r[1]_INST_0_i_53_n_3 ;
  wire \r[1]_INST_0_i_54_n_1 ;
  wire \r[1]_INST_0_i_54_n_2 ;
  wire \r[1]_INST_0_i_54_n_3 ;
  wire \r[1]_INST_0_i_55_n_1 ;
  wire \r[1]_INST_0_i_55_n_2 ;
  wire \r[1]_INST_0_i_55_n_3 ;
  wire \r[1]_INST_0_i_56_n_1 ;
  wire \r[1]_INST_0_i_56_n_2 ;
  wire \r[1]_INST_0_i_56_n_3 ;
  wire \r[1]_INST_0_i_5_n_0 ;
  wire \r[1]_INST_0_i_60_n_0 ;
  wire \r[1]_INST_0_i_64_n_0 ;
  wire \r[1]_INST_0_i_68_n_0 ;
  wire \r[1]_INST_0_i_69_n_0 ;
  wire \r[1]_INST_0_i_69_n_1 ;
  wire \r[1]_INST_0_i_69_n_2 ;
  wire \r[1]_INST_0_i_69_n_3 ;
  wire [2:0]\r[1]_INST_0_i_6_0 ;
  wire [2:0]\r[1]_INST_0_i_6_1 ;
  wire [2:0]\r[1]_INST_0_i_6_2 ;
  wire \r[1]_INST_0_i_6_n_0 ;
  wire \r[1]_INST_0_i_70_n_1 ;
  wire \r[1]_INST_0_i_70_n_2 ;
  wire \r[1]_INST_0_i_70_n_3 ;
  wire \r[1]_INST_0_i_71_n_1 ;
  wire \r[1]_INST_0_i_71_n_2 ;
  wire \r[1]_INST_0_i_71_n_3 ;
  wire \r[1]_INST_0_i_72_n_1 ;
  wire \r[1]_INST_0_i_72_n_2 ;
  wire \r[1]_INST_0_i_72_n_3 ;
  wire \r[1]_INST_0_i_73_n_0 ;
  wire \r[1]_INST_0_i_75_n_0 ;
  wire \r[1]_INST_0_i_76_n_0 ;
  wire \r[1]_INST_0_i_77_n_0 ;
  wire \r[1]_INST_0_i_7_n_0 ;
  wire \r[1]_INST_0_i_81_n_0 ;
  wire \r[1]_INST_0_i_85_n_0 ;
  wire \r[1]_INST_0_i_89_n_0 ;
  wire \r[1]_INST_0_i_8_n_0 ;
  wire \r[1]_INST_0_i_93_n_0 ;
  wire \r[1]_INST_0_i_97_n_0 ;
  wire \r[1]_INST_0_i_9_n_0 ;
  wire rgb10;
  wire rgb1011_out;
  wire rgb11;
  wire rgb1112_out;
  wire rgb12;
  wire rgb1213_out;
  wire rgb13;
  wire rgb1314_out;
  wire rgb14;
  wire rgb1415_out;
  wire rgb15;
  wire rgb1516_out;
  wire rgb16;
  wire rgb1617_out;
  wire rgb1618_out;
  wire rgb163_out;
  wire rgb2;
  wire rgb31_out;
  wire rgb34_out;
  wire rgb4;
  wire rgb40_out;
  wire rgb42_out;
  wire rgb45_out;
  wire rgb5;
  wire rgb56_out;
  wire rgb6;
  wire rgb67_out;
  wire rgb7;
  wire rgb78_out;
  wire rgb8;
  wire rgb89_out;
  wire rgb9;
  wire rgb910_out;
  wire v_enabled;
  wire vs;
  wire vs_INST_0_i_1_n_0;
  wire [3:0]\NLW_g[3]_INST_0_i_13_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_14_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_15_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_16_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_17_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_18_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_19_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_20_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_21_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_22_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_23_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_24_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_25_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_26_O_UNCONNECTED ;
  wire [3:0]\NLW_g[3]_INST_0_i_27_O_UNCONNECTED ;
  wire [0:0]\NLW_r[1]_INST_0_i_10_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_126_CO_UNCONNECTED ;
  wire [3:1]\NLW_r[1]_INST_0_i_126_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_23_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_24_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_25_O_UNCONNECTED ;
  wire [2:2]\NLW_r[1]_INST_0_i_43_CO_UNCONNECTED ;
  wire [3:3]\NLW_r[1]_INST_0_i_43_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_45_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_46_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_47_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_48_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_49_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_50_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_51_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_52_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_53_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_54_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_55_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_56_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_69_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_70_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_71_O_UNCONNECTED ;
  wire [3:0]\NLW_r[1]_INST_0_i_72_O_UNCONNECTED ;

  LUT2 #(
    .INIT(4'hE)) 
    \b[0]_INST_0 
       (.I0(\g[3]_INST_0_i_2_n_0 ),
        .I1(\r[1]_INST_0_i_2_n_0 ),
        .O(b[1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \b[1]_INST_0 
       (.I0(\r[1]_INST_0_i_1_n_0 ),
        .I1(\g[3]_INST_0_i_2_n_0 ),
        .I2(\r[1]_INST_0_i_2_n_0 ),
        .O(b[0]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \counterX[0]_i_1 
       (.I0(counterX_reg[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \counterX[1]_i_1 
       (.I0(counterX_reg[0]),
        .I1(counterX_reg[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \counterX[2]_i_1 
       (.I0(counterX_reg[2]),
        .I1(counterX_reg[0]),
        .I2(counterX_reg[1]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counterX[3]_i_1 
       (.I0(counterX_reg[3]),
        .I1(counterX_reg[1]),
        .I2(counterX_reg[0]),
        .I3(counterX_reg[2]),
        .O(p_0_in[3]));
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \counterX[4]_i_1 
       (.I0(counterX_reg[4]),
        .I1(counterX_reg[1]),
        .I2(counterX_reg[2]),
        .I3(counterX_reg[0]),
        .I4(counterX_reg[3]),
        .O(\counterX[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counterX[5]_i_1 
       (.I0(counterX_reg[5]),
        .I1(counterX_reg[3]),
        .I2(counterX_reg[0]),
        .I3(counterX_reg[2]),
        .I4(counterX_reg[1]),
        .I5(counterX_reg[4]),
        .O(p_0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'hA6AA)) 
    \counterX[6]_i_1 
       (.I0(counterX_reg[6]),
        .I1(counterX_reg[4]),
        .I2(\g[3]_INST_0_i_5_n_0 ),
        .I3(counterX_reg[5]),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hAAAA6AAA)) 
    \counterX[7]_i_1 
       (.I0(counterX_reg[7]),
        .I1(counterX_reg[4]),
        .I2(counterX_reg[6]),
        .I3(counterX_reg[5]),
        .I4(\g[3]_INST_0_i_5_n_0 ),
        .O(\counterX[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h9AAAAAAAAAAAAAAA)) 
    \counterX[8]_i_1 
       (.I0(counterX_reg[8]),
        .I1(\g[3]_INST_0_i_5_n_0 ),
        .I2(counterX_reg[5]),
        .I3(counterX_reg[6]),
        .I4(counterX_reg[4]),
        .I5(counterX_reg[7]),
        .O(p_0_in[8]));
  LUT6 #(
    .INIT(64'hFFFE000000000000)) 
    \counterX[9]_i_1 
       (.I0(counterX_reg[5]),
        .I1(counterX_reg[7]),
        .I2(counterX_reg[6]),
        .I3(\counterX[9]_i_3_n_0 ),
        .I4(counterX_reg[9]),
        .I5(counterX_reg[8]),
        .O(\counterX[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counterX[9]_i_2 
       (.I0(counterX_reg[9]),
        .I1(counterX_reg[6]),
        .I2(counterX_reg[8]),
        .I3(counterX_reg[7]),
        .I4(counterX_reg[5]),
        .I5(\counterX[9]_i_3_n_0 ),
        .O(p_0_in[9]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \counterX[9]_i_3 
       (.I0(counterX_reg[4]),
        .I1(counterX_reg[1]),
        .I2(counterX_reg[2]),
        .I3(counterX_reg[0]),
        .I4(counterX_reg[3]),
        .O(\counterX[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counterX_reg[0] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(counterX_reg[0]),
        .R(\counterX[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counterX_reg[1] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(counterX_reg[1]),
        .R(\counterX[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counterX_reg[2] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(counterX_reg[2]),
        .R(\counterX[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counterX_reg[3] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(counterX_reg[3]),
        .R(\counterX[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counterX_reg[4] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\counterX[4]_i_1_n_0 ),
        .Q(counterX_reg[4]),
        .R(\counterX[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counterX_reg[5] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(counterX_reg[5]),
        .R(\counterX[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counterX_reg[6] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(counterX_reg[6]),
        .R(\counterX[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counterX_reg[7] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\counterX[7]_i_1_n_0 ),
        .Q(counterX_reg[7]),
        .R(\counterX[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counterX_reg[8] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(counterX_reg[8]),
        .R(\counterX[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counterX_reg[9] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(counterX_reg[9]),
        .R(\counterX[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000000015FFFF)) 
    \counterY[0]_i_1 
       (.I0(counterY_reg[8]),
        .I1(counterY_reg[3]),
        .I2(counterY_reg[2]),
        .I3(vs_INST_0_i_1_n_0),
        .I4(counterY_reg[9]),
        .I5(counterY_reg[0]),
        .O(\counterY[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \counterY[1]_i_1 
       (.I0(counterY_reg[0]),
        .I1(counterY_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \counterY[2]_i_1 
       (.I0(counterY_reg[2]),
        .I1(counterY_reg[1]),
        .I2(counterY_reg[0]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counterY[3]_i_1 
       (.I0(counterY_reg[3]),
        .I1(counterY_reg[0]),
        .I2(counterY_reg[1]),
        .I3(counterY_reg[2]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \counterY[4]_i_1 
       (.I0(counterY_reg[4]),
        .I1(counterY_reg[0]),
        .I2(counterY_reg[1]),
        .I3(counterY_reg[2]),
        .I4(counterY_reg[3]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counterY[5]_i_1 
       (.I0(counterY_reg[5]),
        .I1(counterY_reg[4]),
        .I2(counterY_reg[1]),
        .I3(counterY_reg[0]),
        .I4(counterY_reg[2]),
        .I5(counterY_reg[3]),
        .O(p_0_in__0[5]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \counterY[6]_i_1 
       (.I0(counterY_reg[6]),
        .I1(counterY_reg[4]),
        .I2(counterY_reg[5]),
        .I3(counterY_reg[3]),
        .I4(counterY_reg[2]),
        .I5(\counterY[6]_i_2_n_0 ),
        .O(\counterY[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \counterY[6]_i_2 
       (.I0(counterY_reg[0]),
        .I1(counterY_reg[1]),
        .O(\counterY[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \counterY[7]_i_1 
       (.I0(counterY_reg[7]),
        .I1(\counterY[9]_i_3_n_0 ),
        .O(p_0_in__0[7]));
  LUT3 #(
    .INIT(8'h6A)) 
    \counterY[8]_i_1 
       (.I0(counterY_reg[8]),
        .I1(counterY_reg[7]),
        .I2(\counterY[9]_i_3_n_0 ),
        .O(p_0_in__0[8]));
  LUT6 #(
    .INIT(64'hFFEA000000000000)) 
    \counterY[9]_i_1 
       (.I0(counterY_reg[8]),
        .I1(counterY_reg[3]),
        .I2(counterY_reg[2]),
        .I3(vs_INST_0_i_1_n_0),
        .I4(v_enabled),
        .I5(counterY_reg[9]),
        .O(counterY));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \counterY[9]_i_2 
       (.I0(counterY_reg[9]),
        .I1(counterY_reg[7]),
        .I2(counterY_reg[8]),
        .I3(\counterY[9]_i_3_n_0 ),
        .O(p_0_in__0[9]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \counterY[9]_i_3 
       (.I0(counterY_reg[6]),
        .I1(counterY_reg[4]),
        .I2(counterY_reg[5]),
        .I3(counterY_reg[3]),
        .I4(counterY_reg[2]),
        .I5(\counterY[6]_i_2_n_0 ),
        .O(\counterY[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counterY_reg[0] 
       (.C(pixel_clk),
        .CE(v_enabled),
        .D(\counterY[0]_i_1_n_0 ),
        .Q(counterY_reg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \counterY_reg[1] 
       (.C(pixel_clk),
        .CE(v_enabled),
        .D(p_0_in__0[1]),
        .Q(counterY_reg[1]),
        .R(counterY));
  FDRE #(
    .INIT(1'b0)) 
    \counterY_reg[2] 
       (.C(pixel_clk),
        .CE(v_enabled),
        .D(p_0_in__0[2]),
        .Q(counterY_reg[2]),
        .R(counterY));
  FDRE #(
    .INIT(1'b0)) 
    \counterY_reg[3] 
       (.C(pixel_clk),
        .CE(v_enabled),
        .D(p_0_in__0[3]),
        .Q(counterY_reg[3]),
        .R(counterY));
  FDRE #(
    .INIT(1'b0)) 
    \counterY_reg[4] 
       (.C(pixel_clk),
        .CE(v_enabled),
        .D(p_0_in__0[4]),
        .Q(counterY_reg[4]),
        .R(counterY));
  FDRE #(
    .INIT(1'b0)) 
    \counterY_reg[5] 
       (.C(pixel_clk),
        .CE(v_enabled),
        .D(p_0_in__0[5]),
        .Q(counterY_reg[5]),
        .R(counterY));
  FDRE #(
    .INIT(1'b0)) 
    \counterY_reg[6] 
       (.C(pixel_clk),
        .CE(v_enabled),
        .D(\counterY[6]_i_1_n_0 ),
        .Q(counterY_reg[6]),
        .R(counterY));
  FDRE #(
    .INIT(1'b0)) 
    \counterY_reg[7] 
       (.C(pixel_clk),
        .CE(v_enabled),
        .D(p_0_in__0[7]),
        .Q(counterY_reg[7]),
        .R(counterY));
  FDRE #(
    .INIT(1'b0)) 
    \counterY_reg[8] 
       (.C(pixel_clk),
        .CE(v_enabled),
        .D(p_0_in__0[8]),
        .Q(counterY_reg[8]),
        .R(counterY));
  FDRE #(
    .INIT(1'b0)) 
    \counterY_reg[9] 
       (.C(pixel_clk),
        .CE(v_enabled),
        .D(p_0_in__0[9]),
        .Q(counterY_reg[9]),
        .R(counterY));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h55555554)) 
    \g[0]_INST_0 
       (.I0(\g[3]_INST_0_i_1_n_0 ),
        .I1(\r[0]_INST_0_i_1_n_0 ),
        .I2(\r[1]_INST_0_i_2_n_0 ),
        .I3(\g[3]_INST_0_i_2_n_0 ),
        .I4(\r[1]_INST_0_i_1_n_0 ),
        .O(g[0]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h0000FFFE)) 
    \g[2]_INST_0 
       (.I0(\r[1]_INST_0_i_1_n_0 ),
        .I1(\g[3]_INST_0_i_2_n_0 ),
        .I2(\r[1]_INST_0_i_2_n_0 ),
        .I3(\r[0]_INST_0_i_1_n_0 ),
        .I4(\g[3]_INST_0_i_1_n_0 ),
        .O(g[1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h5554)) 
    \g[3]_INST_0 
       (.I0(\g[3]_INST_0_i_1_n_0 ),
        .I1(\g[3]_INST_0_i_2_n_0 ),
        .I2(\r[0]_INST_0_i_1_n_0 ),
        .I3(\r[1]_INST_0_i_2_n_0 ),
        .O(g[2]));
  LUT6 #(
    .INIT(64'hAA02AA02AA020000)) 
    \g[3]_INST_0_i_1 
       (.I0(\r[0]_INST_0_i_1_n_0 ),
        .I1(\g[3]_INST_0_i_3_n_0 ),
        .I2(counterY_reg[6]),
        .I3(\g[3]_INST_0_i_4_n_0 ),
        .I4(\g[3]_INST_0_i_5_n_0 ),
        .I5(\g[3]_INST_0_i_6_n_0 ),
        .O(\g[3]_INST_0_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \g[3]_INST_0_i_10 
       (.I0(rgb6),
        .I1(rgb12),
        .I2(rgb31_out),
        .I3(rgb9),
        .O(\g[3]_INST_0_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \g[3]_INST_0_i_11 
       (.I0(rgb10),
        .I1(rgb14),
        .I2(rgb5),
        .I3(rgb16),
        .O(\g[3]_INST_0_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \g[3]_INST_0_i_12 
       (.I0(rgb8),
        .I1(rgb13),
        .I2(rgb163_out),
        .I3(rgb11),
        .O(\g[3]_INST_0_i_12_n_0 ));
  CARRY4 \g[3]_INST_0_i_13 
       (.CI(1'b0),
        .CO({rgb7,\g[3]_INST_0_i_13_n_1 ,\g[3]_INST_0_i_13_n_2 ,\g[3]_INST_0_i_13_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_13_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_8_1 ,\g[3]_INST_0_i_31_n_0 }));
  CARRY4 \g[3]_INST_0_i_14 
       (.CI(1'b0),
        .CO({rgb42_out,\g[3]_INST_0_i_14_n_1 ,\g[3]_INST_0_i_14_n_2 ,\g[3]_INST_0_i_14_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_14_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_8_2 ,\g[3]_INST_0_i_35_n_0 }));
  CARRY4 \g[3]_INST_0_i_15 
       (.CI(1'b0),
        .CO({rgb15,\g[3]_INST_0_i_15_n_1 ,\g[3]_INST_0_i_15_n_2 ,\g[3]_INST_0_i_15_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_15_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_8_0 ,\g[3]_INST_0_i_39_n_0 }));
  CARRY4 \g[3]_INST_0_i_16 
       (.CI(1'b0),
        .CO({rgb6,\g[3]_INST_0_i_16_n_1 ,\g[3]_INST_0_i_16_n_2 ,\g[3]_INST_0_i_16_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_16_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_10_2 ,\g[3]_INST_0_i_43_n_0 }));
  CARRY4 \g[3]_INST_0_i_17 
       (.CI(1'b0),
        .CO({rgb12,\g[3]_INST_0_i_17_n_1 ,\g[3]_INST_0_i_17_n_2 ,\g[3]_INST_0_i_17_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_17_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_10_0 ,\g[3]_INST_0_i_47_n_0 }));
  CARRY4 \g[3]_INST_0_i_18 
       (.CI(1'b0),
        .CO({rgb31_out,\g[3]_INST_0_i_18_n_1 ,\g[3]_INST_0_i_18_n_2 ,\g[3]_INST_0_i_18_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_18_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_10_3 ,\g[3]_INST_0_i_51_n_0 }));
  CARRY4 \g[3]_INST_0_i_19 
       (.CI(1'b0),
        .CO({rgb9,\g[3]_INST_0_i_19_n_1 ,\g[3]_INST_0_i_19_n_2 ,\g[3]_INST_0_i_19_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_19_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_10_1 ,\g[3]_INST_0_i_55_n_0 }));
  LUT6 #(
    .INIT(64'h0000000000000400)) 
    \g[3]_INST_0_i_2 
       (.I0(\counterX_reg[7]_0 [3]),
        .I1(\g[3]_INST_0_i_7_n_0 ),
        .I2(\g[3]_INST_0_i_8_n_0 ),
        .I3(\r[1]_INST_0_i_6_n_0 ),
        .I4(\counterX_reg[7]_0 [4]),
        .I5(\r[1]_INST_0_i_5_n_0 ),
        .O(\g[3]_INST_0_i_2_n_0 ));
  CARRY4 \g[3]_INST_0_i_20 
       (.CI(1'b0),
        .CO({rgb10,\g[3]_INST_0_i_20_n_1 ,\g[3]_INST_0_i_20_n_2 ,\g[3]_INST_0_i_20_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_20_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_11_1 ,\g[3]_INST_0_i_59_n_0 }));
  CARRY4 \g[3]_INST_0_i_21 
       (.CI(1'b0),
        .CO({rgb14,\g[3]_INST_0_i_21_n_1 ,\g[3]_INST_0_i_21_n_2 ,\g[3]_INST_0_i_21_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_21_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_11_0 ,\g[3]_INST_0_i_63_n_0 }));
  CARRY4 \g[3]_INST_0_i_22 
       (.CI(1'b0),
        .CO({rgb5,\g[3]_INST_0_i_22_n_1 ,\g[3]_INST_0_i_22_n_2 ,\g[3]_INST_0_i_22_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_22_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_11_2 ,\g[3]_INST_0_i_67_n_0 }));
  CARRY4 \g[3]_INST_0_i_23 
       (.CI(1'b0),
        .CO({rgb16,\g[3]_INST_0_i_23_n_1 ,\g[3]_INST_0_i_23_n_2 ,\g[3]_INST_0_i_23_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_23_O_UNCONNECTED [3:0]),
        .S({S,\g[3]_INST_0_i_71_n_0 }));
  CARRY4 \g[3]_INST_0_i_24 
       (.CI(1'b0),
        .CO({rgb8,\g[3]_INST_0_i_24_n_1 ,\g[3]_INST_0_i_24_n_2 ,\g[3]_INST_0_i_24_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_24_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_12_2 ,\g[3]_INST_0_i_75_n_0 }));
  CARRY4 \g[3]_INST_0_i_25 
       (.CI(1'b0),
        .CO({rgb13,\g[3]_INST_0_i_25_n_1 ,\g[3]_INST_0_i_25_n_2 ,\g[3]_INST_0_i_25_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_25_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_12_0 ,\g[3]_INST_0_i_79_n_0 }));
  CARRY4 \g[3]_INST_0_i_26 
       (.CI(1'b0),
        .CO({rgb163_out,\g[3]_INST_0_i_26_n_1 ,\g[3]_INST_0_i_26_n_2 ,\g[3]_INST_0_i_26_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_26_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_12_3 ,\g[3]_INST_0_i_83_n_0 }));
  CARRY4 \g[3]_INST_0_i_27 
       (.CI(1'b0),
        .CO({rgb11,\g[3]_INST_0_i_27_n_1 ,\g[3]_INST_0_i_27_n_2 ,\g[3]_INST_0_i_27_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_g[3]_INST_0_i_27_O_UNCONNECTED [3:0]),
        .S({\g[3]_INST_0_i_12_1 ,\g[3]_INST_0_i_87_n_0 }));
  LUT6 #(
    .INIT(64'h0000000000000080)) 
    \g[3]_INST_0_i_3 
       (.I0(counterY_reg[7]),
        .I1(counterY_reg[5]),
        .I2(counterY_reg[8]),
        .I3(counterY_reg[9]),
        .I4(counterY_reg[2]),
        .I5(counterY_reg[3]),
        .O(\g[3]_INST_0_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \g[3]_INST_0_i_31 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\g[3]_INST_0_i_31_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \g[3]_INST_0_i_35 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\g[3]_INST_0_i_35_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \g[3]_INST_0_i_39 
       (.I0(\counterX[4]_i_1_n_0 ),
        .I1(cur_blk_index__0[2]),
        .I2(cur_blk_index__0[1]),
        .O(\g[3]_INST_0_i_39_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFF777777777)) 
    \g[3]_INST_0_i_4 
       (.I0(\counterY[6]_i_2_n_0 ),
        .I1(counterY_reg[4]),
        .I2(counterY_reg[7]),
        .I3(counterY_reg[5]),
        .I4(\g[3]_INST_0_i_9_n_0 ),
        .I5(counterY_reg[6]),
        .O(\g[3]_INST_0_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \g[3]_INST_0_i_43 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\g[3]_INST_0_i_43_n_0 ));
  LUT3 #(
    .INIT(8'h80)) 
    \g[3]_INST_0_i_47 
       (.I0(cur_blk_index__0[2]),
        .I1(cur_blk_index__0[1]),
        .I2(\counterX[4]_i_1_n_0 ),
        .O(\g[3]_INST_0_i_47_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \g[3]_INST_0_i_5 
       (.I0(counterX_reg[3]),
        .I1(counterX_reg[0]),
        .I2(counterX_reg[2]),
        .I3(counterX_reg[1]),
        .O(\g[3]_INST_0_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \g[3]_INST_0_i_51 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\g[3]_INST_0_i_51_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \g[3]_INST_0_i_55 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\g[3]_INST_0_i_55_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    \g[3]_INST_0_i_59 
       (.I0(cur_blk_index__0[2]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[1]),
        .O(\g[3]_INST_0_i_59_n_0 ));
  LUT6 #(
    .INIT(64'hFEFFFFEFFFFFFFFF)) 
    \g[3]_INST_0_i_6 
       (.I0(counterX_reg[4]),
        .I1(counterX_reg[9]),
        .I2(counterX_reg[8]),
        .I3(counterX_reg[6]),
        .I4(counterX_reg[5]),
        .I5(counterX_reg[7]),
        .O(\g[3]_INST_0_i_6_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    \g[3]_INST_0_i_63 
       (.I0(cur_blk_index__0[2]),
        .I1(cur_blk_index__0[1]),
        .I2(\counterX[4]_i_1_n_0 ),
        .O(\g[3]_INST_0_i_63_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \g[3]_INST_0_i_67 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\g[3]_INST_0_i_67_n_0 ));
  LUT6 #(
    .INIT(64'h4404000020221800)) 
    \g[3]_INST_0_i_7 
       (.I0(\counterX_reg[7]_0 [2]),
        .I1(\counterX_reg[7]_0 [1]),
        .I2(\counterX_reg[7]_0 [0]),
        .I3(cur_blk_index__0[1]),
        .I4(\counterX[4]_i_1_n_0 ),
        .I5(cur_blk_index__0[2]),
        .O(\g[3]_INST_0_i_7_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \g[3]_INST_0_i_71 
       (.I0(cur_blk_index__0[2]),
        .I1(cur_blk_index__0[1]),
        .I2(\counterX[4]_i_1_n_0 ),
        .O(\g[3]_INST_0_i_71_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \g[3]_INST_0_i_75 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\g[3]_INST_0_i_75_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \g[3]_INST_0_i_79 
       (.I0(cur_blk_index__0[2]),
        .I1(cur_blk_index__0[1]),
        .I2(\counterX[4]_i_1_n_0 ),
        .O(\g[3]_INST_0_i_79_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \g[3]_INST_0_i_8 
       (.I0(\g[3]_INST_0_i_10_n_0 ),
        .I1(\g[3]_INST_0_i_11_n_0 ),
        .I2(\g[3]_INST_0_i_12_n_0 ),
        .I3(rgb7),
        .I4(rgb42_out),
        .I5(rgb15),
        .O(\g[3]_INST_0_i_8_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \g[3]_INST_0_i_83 
       (.I0(cur_blk_index__0[2]),
        .I1(cur_blk_index__0[1]),
        .I2(\counterX[4]_i_1_n_0 ),
        .O(\g[3]_INST_0_i_83_n_0 ));
  LUT3 #(
    .INIT(8'h80)) 
    \g[3]_INST_0_i_87 
       (.I0(cur_blk_index__0[2]),
        .I1(cur_blk_index__0[1]),
        .I2(\counterX[4]_i_1_n_0 ),
        .O(\g[3]_INST_0_i_87_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \g[3]_INST_0_i_9 
       (.I0(counterY_reg[9]),
        .I1(counterY_reg[8]),
        .I2(counterY_reg[3]),
        .I3(counterY_reg[2]),
        .O(\g[3]_INST_0_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hFFFFFFF8)) 
    hs_INST_0
       (.I0(counterX_reg[6]),
        .I1(counterX_reg[5]),
        .I2(counterX_reg[8]),
        .I3(counterX_reg[9]),
        .I4(counterX_reg[7]),
        .O(hs));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \r[0]_INST_0 
       (.I0(\r[1]_INST_0_i_2_n_0 ),
        .I1(\r[0]_INST_0_i_1_n_0 ),
        .O(r[0]));
  LUT5 #(
    .INIT(32'h00000002)) 
    \r[0]_INST_0_i_1 
       (.I0(\r[0]_INST_0_i_2_n_0 ),
        .I1(\r[0]_INST_0_i_3_n_0 ),
        .I2(\r[0]_INST_0_i_4_n_0 ),
        .I3(counterY_reg[9]),
        .I4(C),
        .O(\r[0]_INST_0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFEEEA)) 
    \r[0]_INST_0_i_2 
       (.I0(counterY_reg[8]),
        .I1(counterY_reg[6]),
        .I2(\r[0]_INST_0_i_6_n_0 ),
        .I3(counterY_reg[5]),
        .I4(counterY_reg[7]),
        .O(\r[0]_INST_0_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h8888888800008000)) 
    \r[0]_INST_0_i_3 
       (.I0(counterY_reg[8]),
        .I1(counterY_reg[7]),
        .I2(counterY_reg[5]),
        .I3(counterY_reg[4]),
        .I4(\r[0]_INST_0_i_7_n_0 ),
        .I5(counterY_reg[6]),
        .O(\r[0]_INST_0_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFE000000)) 
    \r[0]_INST_0_i_4 
       (.I0(counterX_reg[6]),
        .I1(counterX_reg[4]),
        .I2(counterX_reg[5]),
        .I3(counterX_reg[7]),
        .I4(counterX_reg[8]),
        .I5(counterX_reg[9]),
        .O(\r[0]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000FF7F)) 
    \r[0]_INST_0_i_5 
       (.I0(counterX_reg[7]),
        .I1(counterX_reg[6]),
        .I2(counterX_reg[5]),
        .I3(\r[0]_INST_0_i_8_n_0 ),
        .I4(counterX_reg[9]),
        .I5(counterX_reg[8]),
        .O(C));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'hF0F0F080)) 
    \r[0]_INST_0_i_6 
       (.I0(counterY_reg[0]),
        .I1(counterY_reg[1]),
        .I2(counterY_reg[4]),
        .I3(counterY_reg[2]),
        .I4(counterY_reg[3]),
        .O(\r[0]_INST_0_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \r[0]_INST_0_i_7 
       (.I0(counterY_reg[2]),
        .I1(counterY_reg[3]),
        .O(\r[0]_INST_0_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h00007FFF)) 
    \r[0]_INST_0_i_8 
       (.I0(counterX_reg[1]),
        .I1(counterX_reg[2]),
        .I2(counterX_reg[0]),
        .I3(counterX_reg[3]),
        .I4(counterX_reg[4]),
        .O(\r[0]_INST_0_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \r[1]_INST_0 
       (.I0(\r[1]_INST_0_i_1_n_0 ),
        .I1(\r[1]_INST_0_i_2_n_0 ),
        .O(r[1]));
  LUT5 #(
    .INIT(32'h00000001)) 
    \r[1]_INST_0_i_1 
       (.I0(\counterX_reg[7]_0 [3]),
        .I1(\r[1]_INST_0_i_4_n_0 ),
        .I2(\counterX_reg[7]_0 [4]),
        .I3(\r[1]_INST_0_i_5_n_0 ),
        .I4(\r[1]_INST_0_i_6_n_0 ),
        .O(\r[1]_INST_0_i_1_n_0 ));
  CARRY4 \r[1]_INST_0_i_10 
       (.CI(1'b0),
        .CO({\r[1]_INST_0_i_10_n_0 ,\r[1]_INST_0_i_10_n_1 ,\r[1]_INST_0_i_10_n_2 ,\r[1]_INST_0_i_10_n_3 }),
        .CYINIT(1'b0),
        .DI({cur_blk_index0[4:2],\r[1]_INST_0_i_31_n_0 }),
        .O({\counterX_reg[7]_0 [1:0],cur_blk_index__0[2],\NLW_r[1]_INST_0_i_10_O_UNCONNECTED [0]}),
        .S({\r[1]_INST_0_i_32_n_0 ,\r[1]_INST_0_i_33_n_0 ,\r[1]_INST_0_i_34_n_0 ,\r[1]_INST_0_i_35_n_0 }));
  LUT3 #(
    .INIT(8'h80)) 
    \r[1]_INST_0_i_101 
       (.I0(\counterX[4]_i_1_n_0 ),
        .I1(cur_blk_index__0[2]),
        .I2(cur_blk_index__0[1]),
        .O(\r[1]_INST_0_i_101_n_0 ));
  LUT3 #(
    .INIT(8'h10)) 
    \r[1]_INST_0_i_105 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\r[1]_INST_0_i_105_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \r[1]_INST_0_i_109 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\r[1]_INST_0_i_109_n_0 ));
  CARRY4 \r[1]_INST_0_i_11 
       (.CI(1'b0),
        .CO({\r[1]_INST_0_i_11_n_0 ,\r[1]_INST_0_i_11_n_1 ,\r[1]_INST_0_i_11_n_2 ,\r[1]_INST_0_i_11_n_3 }),
        .CYINIT(1'b0),
        .DI({A[4],\r[1]_INST_0_i_37_n_0 ,\r[1]_INST_0_i_38_n_0 ,1'b0}),
        .O(cur_blk_index0[5:2]),
        .S({\r[1]_INST_0_i_39_n_0 ,\r[1]_INST_0_i_40_n_0 ,\r[1]_INST_0_i_41_n_0 ,\r[1]_INST_0_i_42_n_0 }));
  LUT3 #(
    .INIT(8'h04)) 
    \r[1]_INST_0_i_113 
       (.I0(cur_blk_index__0[2]),
        .I1(cur_blk_index__0[1]),
        .I2(\counterX[4]_i_1_n_0 ),
        .O(\r[1]_INST_0_i_113_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_117 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\r[1]_INST_0_i_117_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \r[1]_INST_0_i_12 
       (.I0(C),
        .I1(cur_blk_index0[8]),
        .O(\r[1]_INST_0_i_12_n_0 ));
  LUT3 #(
    .INIT(8'h02)) 
    \r[1]_INST_0_i_121 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\r[1]_INST_0_i_121_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \r[1]_INST_0_i_125 
       (.I0(\counterX[4]_i_1_n_0 ),
        .I1(cur_blk_index__0[2]),
        .I2(cur_blk_index__0[1]),
        .O(\r[1]_INST_0_i_125_n_0 ));
  CARRY4 \r[1]_INST_0_i_126 
       (.CI(\r[1]_INST_0_i_3_n_0 ),
        .CO(\NLW_r[1]_INST_0_i_126_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_r[1]_INST_0_i_126_O_UNCONNECTED [3:1],cur_blk_index[1]}),
        .S({1'b0,1'b0,1'b0,\r[1]_INST_0_i_143_n_0 }));
  LUT2 #(
    .INIT(4'h6)) 
    \r[1]_INST_0_i_13 
       (.I0(C),
        .I1(cur_blk_index0[7]),
        .O(\r[1]_INST_0_i_13_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_130 
       (.I0(cur_blk_index__0[1]),
        .I1(cur_blk_index__0[2]),
        .I2(\counterX[4]_i_1_n_0 ),
        .O(\r[1]_INST_0_i_130_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_134 
       (.I0(cur_blk_index__0[1]),
        .I1(cur_blk_index__0[2]),
        .I2(\counterX[4]_i_1_n_0 ),
        .O(\r[1]_INST_0_i_134_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_138 
       (.I0(\counterX[4]_i_1_n_0 ),
        .I1(cur_blk_index__0[2]),
        .I2(cur_blk_index__0[1]),
        .O(\r[1]_INST_0_i_138_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \r[1]_INST_0_i_14 
       (.I0(C),
        .I1(cur_blk_index0[6]),
        .O(\r[1]_INST_0_i_14_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_142 
       (.I0(cur_blk_index__0[1]),
        .I1(cur_blk_index__0[2]),
        .I2(\counterX[4]_i_1_n_0 ),
        .O(\r[1]_INST_0_i_142_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \r[1]_INST_0_i_143 
       (.I0(cur_blk_index0[9]),
        .I1(C),
        .O(\r[1]_INST_0_i_143_n_0 ));
  LUT4 #(
    .INIT(16'h1EE1)) 
    \r[1]_INST_0_i_15 
       (.I0(counterX_reg[8]),
        .I1(\r[1]_INST_0_i_44_n_0 ),
        .I2(counterX_reg[9]),
        .I3(cur_blk_index0[5]),
        .O(\r[1]_INST_0_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hAA55AA55AA55956A)) 
    \r[1]_INST_0_i_16 
       (.I0(\r[1]_INST_0_i_31_n_0 ),
        .I1(counterY_reg[0]),
        .I2(counterY_reg[1]),
        .I3(counterY_reg[4]),
        .I4(counterY_reg[2]),
        .I5(counterY_reg[3]),
        .O(cur_blk_index__0[1]));
  LUT6 #(
    .INIT(64'h0000000000011111)) 
    \r[1]_INST_0_i_17 
       (.I0(counterY_reg[9]),
        .I1(counterY_reg[7]),
        .I2(counterY_reg[5]),
        .I3(\r[0]_INST_0_i_6_n_0 ),
        .I4(counterY_reg[6]),
        .I5(counterY_reg[8]),
        .O(A[6]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h0007)) 
    \r[1]_INST_0_i_18 
       (.I0(counterY_reg[7]),
        .I1(counterY_reg[6]),
        .I2(counterY_reg[9]),
        .I3(counterY_reg[8]),
        .O(\r[1]_INST_0_i_18_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \r[1]_INST_0_i_19 
       (.I0(counterX_reg[4]),
        .I1(counterX_reg[6]),
        .I2(counterX_reg[5]),
        .I3(counterX_reg[7]),
        .I4(counterX_reg[8]),
        .I5(\r[1]_INST_0_i_26_n_0 ),
        .O(\r[1]_INST_0_i_19_n_0 ));
  LUT5 #(
    .INIT(32'h0000002A)) 
    \r[1]_INST_0_i_2 
       (.I0(\r[1]_INST_0_i_5_n_0 ),
        .I1(counterY_reg[6]),
        .I2(\r[1]_INST_0_i_7_n_0 ),
        .I3(\r[1]_INST_0_i_8_n_0 ),
        .I4(\r[1]_INST_0_i_9_n_0 ),
        .O(\r[1]_INST_0_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \r[1]_INST_0_i_20 
       (.I0(rgb45_out),
        .I1(rgb67_out),
        .I2(rgb78_out),
        .I3(rgb1415_out),
        .O(\r[1]_INST_0_i_20_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \r[1]_INST_0_i_21 
       (.I0(rgb34_out),
        .I1(rgb1617_out),
        .I2(rgb56_out),
        .I3(rgb1314_out),
        .O(\r[1]_INST_0_i_21_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \r[1]_INST_0_i_22 
       (.I0(rgb89_out),
        .I1(rgb1213_out),
        .I2(rgb910_out),
        .I3(rgb1516_out),
        .O(\r[1]_INST_0_i_22_n_0 ));
  CARRY4 \r[1]_INST_0_i_23 
       (.CI(1'b0),
        .CO({rgb1112_out,\r[1]_INST_0_i_23_n_1 ,\r[1]_INST_0_i_23_n_2 ,\r[1]_INST_0_i_23_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_23_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_6_0 ,\r[1]_INST_0_i_60_n_0 }));
  CARRY4 \r[1]_INST_0_i_24 
       (.CI(1'b0),
        .CO({rgb1618_out,\r[1]_INST_0_i_24_n_1 ,\r[1]_INST_0_i_24_n_2 ,\r[1]_INST_0_i_24_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_24_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_6_2 ,\r[1]_INST_0_i_64_n_0 }));
  CARRY4 \r[1]_INST_0_i_25 
       (.CI(1'b0),
        .CO({rgb1011_out,\r[1]_INST_0_i_25_n_1 ,\r[1]_INST_0_i_25_n_2 ,\r[1]_INST_0_i_25_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_25_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_6_1 ,\r[1]_INST_0_i_68_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \r[1]_INST_0_i_26 
       (.I0(counterX_reg[1]),
        .I1(counterX_reg[0]),
        .I2(counterX_reg[3]),
        .I3(counterX_reg[2]),
        .O(\r[1]_INST_0_i_26_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000FFFE)) 
    \r[1]_INST_0_i_27 
       (.I0(\r[1]_INST_0_i_69_n_0 ),
        .I1(rgb2),
        .I2(rgb4),
        .I3(rgb40_out),
        .I4(\r[1]_INST_0_i_73_n_0 ),
        .I5(counterX_reg[9]),
        .O(\r[1]_INST_0_i_27_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \r[1]_INST_0_i_28 
       (.I0(counterX_reg[4]),
        .I1(counterX_reg[5]),
        .O(\r[1]_INST_0_i_28_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \r[1]_INST_0_i_29 
       (.I0(counterX_reg[7]),
        .I1(counterX_reg[8]),
        .O(\r[1]_INST_0_i_29_n_0 ));
  CARRY4 \r[1]_INST_0_i_3 
       (.CI(\r[1]_INST_0_i_10_n_0 ),
        .CO({\r[1]_INST_0_i_3_n_0 ,\r[1]_INST_0_i_3_n_1 ,\r[1]_INST_0_i_3_n_2 ,\r[1]_INST_0_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({C,C,C,cur_blk_index0[5]}),
        .O({cur_blk_index[0],\counterX_reg[7]_0 [4:2]}),
        .S({\r[1]_INST_0_i_12_n_0 ,\r[1]_INST_0_i_13_n_0 ,\r[1]_INST_0_i_14_n_0 ,\r[1]_INST_0_i_15_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hFFFFFFF8)) 
    \r[1]_INST_0_i_30 
       (.I0(counterY_reg[1]),
        .I1(counterY_reg[0]),
        .I2(counterY_reg[4]),
        .I3(counterY_reg[2]),
        .I4(counterY_reg[3]),
        .O(\r[1]_INST_0_i_30_n_0 ));
  LUT6 #(
    .INIT(64'h15555555EAAAAAAA)) 
    \r[1]_INST_0_i_31 
       (.I0(counterX_reg[4]),
        .I1(counterX_reg[3]),
        .I2(counterX_reg[0]),
        .I3(counterX_reg[2]),
        .I4(counterX_reg[1]),
        .I5(counterX_reg[5]),
        .O(\r[1]_INST_0_i_31_n_0 ));
  LUT6 #(
    .INIT(64'hBFFF40004000BFFF)) 
    \r[1]_INST_0_i_32 
       (.I0(\r[0]_INST_0_i_8_n_0 ),
        .I1(counterX_reg[5]),
        .I2(counterX_reg[6]),
        .I3(counterX_reg[7]),
        .I4(counterX_reg[8]),
        .I5(cur_blk_index0[4]),
        .O(\r[1]_INST_0_i_32_n_0 ));
  LUT6 #(
    .INIT(64'h955595956AAA6A6A)) 
    \r[1]_INST_0_i_33 
       (.I0(counterX_reg[7]),
        .I1(counterX_reg[6]),
        .I2(counterX_reg[5]),
        .I3(counterX_reg[4]),
        .I4(\g[3]_INST_0_i_5_n_0 ),
        .I5(cur_blk_index0[3]),
        .O(\r[1]_INST_0_i_33_n_0 ));
  LUT5 #(
    .INIT(32'h95996A66)) 
    \r[1]_INST_0_i_34 
       (.I0(counterX_reg[6]),
        .I1(counterX_reg[5]),
        .I2(counterX_reg[4]),
        .I3(\g[3]_INST_0_i_5_n_0 ),
        .I4(cur_blk_index0[2]),
        .O(\r[1]_INST_0_i_34_n_0 ));
  LUT6 #(
    .INIT(64'hAA55AA55AA55956A)) 
    \r[1]_INST_0_i_35 
       (.I0(\r[1]_INST_0_i_31_n_0 ),
        .I1(counterY_reg[0]),
        .I2(counterY_reg[1]),
        .I3(counterY_reg[4]),
        .I4(counterY_reg[2]),
        .I5(counterY_reg[3]),
        .O(\r[1]_INST_0_i_35_n_0 ));
  LUT5 #(
    .INIT(32'hFEAA0155)) 
    \r[1]_INST_0_i_36 
       (.I0(counterY_reg[7]),
        .I1(counterY_reg[5]),
        .I2(\r[0]_INST_0_i_6_n_0 ),
        .I3(counterY_reg[6]),
        .I4(counterY_reg[8]),
        .O(A[4]));
  LUT4 #(
    .INIT(16'hA955)) 
    \r[1]_INST_0_i_37 
       (.I0(counterY_reg[7]),
        .I1(counterY_reg[5]),
        .I2(\r[0]_INST_0_i_6_n_0 ),
        .I3(counterY_reg[6]),
        .O(\r[1]_INST_0_i_37_n_0 ));
  LUT6 #(
    .INIT(64'h00007F0FFFFF80F0)) 
    \r[1]_INST_0_i_38 
       (.I0(counterY_reg[0]),
        .I1(counterY_reg[1]),
        .I2(counterY_reg[4]),
        .I3(\r[0]_INST_0_i_7_n_0 ),
        .I4(counterY_reg[5]),
        .I5(counterY_reg[6]),
        .O(\r[1]_INST_0_i_38_n_0 ));
  LUT5 #(
    .INIT(32'hA6A6A669)) 
    \r[1]_INST_0_i_39 
       (.I0(counterY_reg[8]),
        .I1(counterY_reg[7]),
        .I2(counterY_reg[6]),
        .I3(counterY_reg[5]),
        .I4(\r[0]_INST_0_i_6_n_0 ),
        .O(\r[1]_INST_0_i_39_n_0 ));
  LUT6 #(
    .INIT(64'hEFFFFDDB7FFFBDFD)) 
    \r[1]_INST_0_i_4 
       (.I0(\counterX_reg[7]_0 [2]),
        .I1(\counterX_reg[7]_0 [1]),
        .I2(cur_blk_index__0[2]),
        .I3(cur_blk_index__0[1]),
        .I4(\counterX[4]_i_1_n_0 ),
        .I5(\counterX_reg[7]_0 [0]),
        .O(\r[1]_INST_0_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h699C)) 
    \r[1]_INST_0_i_40 
       (.I0(counterY_reg[6]),
        .I1(counterY_reg[7]),
        .I2(\r[0]_INST_0_i_6_n_0 ),
        .I3(counterY_reg[5]),
        .O(\r[1]_INST_0_i_40_n_0 ));
  LUT6 #(
    .INIT(64'h5999966655559999)) 
    \r[1]_INST_0_i_41 
       (.I0(counterY_reg[6]),
        .I1(counterY_reg[5]),
        .I2(counterY_reg[0]),
        .I3(counterY_reg[1]),
        .I4(counterY_reg[4]),
        .I5(\r[0]_INST_0_i_7_n_0 ),
        .O(\r[1]_INST_0_i_41_n_0 ));
  LUT6 #(
    .INIT(64'hAA55A955A955A955)) 
    \r[1]_INST_0_i_42 
       (.I0(counterY_reg[5]),
        .I1(counterY_reg[3]),
        .I2(counterY_reg[2]),
        .I3(counterY_reg[4]),
        .I4(counterY_reg[1]),
        .I5(counterY_reg[0]),
        .O(\r[1]_INST_0_i_42_n_0 ));
  CARRY4 \r[1]_INST_0_i_43 
       (.CI(\r[1]_INST_0_i_11_n_0 ),
        .CO({cur_blk_index0[9],\NLW_r[1]_INST_0_i_43_CO_UNCONNECTED [2],\r[1]_INST_0_i_43_n_2 ,\r[1]_INST_0_i_43_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,A[6],A[6:5]}),
        .O({\NLW_r[1]_INST_0_i_43_O_UNCONNECTED [3],cur_blk_index0[8:6]}),
        .S({1'b1,\r[1]_INST_0_i_75_n_0 ,\r[1]_INST_0_i_76_n_0 ,\r[1]_INST_0_i_77_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hD0000000)) 
    \r[1]_INST_0_i_44 
       (.I0(\g[3]_INST_0_i_5_n_0 ),
        .I1(counterX_reg[4]),
        .I2(counterX_reg[5]),
        .I3(counterX_reg[6]),
        .I4(counterX_reg[7]),
        .O(\r[1]_INST_0_i_44_n_0 ));
  CARRY4 \r[1]_INST_0_i_45 
       (.CI(1'b0),
        .CO({rgb45_out,\r[1]_INST_0_i_45_n_1 ,\r[1]_INST_0_i_45_n_2 ,\r[1]_INST_0_i_45_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_45_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_20_3 ,\r[1]_INST_0_i_81_n_0 }));
  CARRY4 \r[1]_INST_0_i_46 
       (.CI(1'b0),
        .CO({rgb67_out,\r[1]_INST_0_i_46_n_1 ,\r[1]_INST_0_i_46_n_2 ,\r[1]_INST_0_i_46_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_46_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_20_2 ,\r[1]_INST_0_i_85_n_0 }));
  CARRY4 \r[1]_INST_0_i_47 
       (.CI(1'b0),
        .CO({rgb78_out,\r[1]_INST_0_i_47_n_1 ,\r[1]_INST_0_i_47_n_2 ,\r[1]_INST_0_i_47_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_47_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_20_1 ,\r[1]_INST_0_i_89_n_0 }));
  CARRY4 \r[1]_INST_0_i_48 
       (.CI(1'b0),
        .CO({rgb1415_out,\r[1]_INST_0_i_48_n_1 ,\r[1]_INST_0_i_48_n_2 ,\r[1]_INST_0_i_48_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_48_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_20_0 ,\r[1]_INST_0_i_93_n_0 }));
  CARRY4 \r[1]_INST_0_i_49 
       (.CI(1'b0),
        .CO({rgb34_out,\r[1]_INST_0_i_49_n_1 ,\r[1]_INST_0_i_49_n_2 ,\r[1]_INST_0_i_49_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_49_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_21_3 ,\r[1]_INST_0_i_97_n_0 }));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFBFFFF)) 
    \r[1]_INST_0_i_5 
       (.I0(A[6]),
        .I1(\r[0]_INST_0_i_4_n_0 ),
        .I2(\r[1]_INST_0_i_7_n_0 ),
        .I3(counterX_reg[9]),
        .I4(\r[1]_INST_0_i_18_n_0 ),
        .I5(\r[1]_INST_0_i_19_n_0 ),
        .O(\r[1]_INST_0_i_5_n_0 ));
  CARRY4 \r[1]_INST_0_i_50 
       (.CI(1'b0),
        .CO({rgb1617_out,\r[1]_INST_0_i_50_n_1 ,\r[1]_INST_0_i_50_n_2 ,\r[1]_INST_0_i_50_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_50_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_21_0 ,\r[1]_INST_0_i_101_n_0 }));
  CARRY4 \r[1]_INST_0_i_51 
       (.CI(1'b0),
        .CO({rgb56_out,\r[1]_INST_0_i_51_n_1 ,\r[1]_INST_0_i_51_n_2 ,\r[1]_INST_0_i_51_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_51_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_21_2 ,\r[1]_INST_0_i_105_n_0 }));
  CARRY4 \r[1]_INST_0_i_52 
       (.CI(1'b0),
        .CO({rgb1314_out,\r[1]_INST_0_i_52_n_1 ,\r[1]_INST_0_i_52_n_2 ,\r[1]_INST_0_i_52_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_52_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_21_1 ,\r[1]_INST_0_i_109_n_0 }));
  CARRY4 \r[1]_INST_0_i_53 
       (.CI(1'b0),
        .CO({rgb89_out,\r[1]_INST_0_i_53_n_1 ,\r[1]_INST_0_i_53_n_2 ,\r[1]_INST_0_i_53_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_53_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_22_3 ,\r[1]_INST_0_i_113_n_0 }));
  CARRY4 \r[1]_INST_0_i_54 
       (.CI(1'b0),
        .CO({rgb1213_out,\r[1]_INST_0_i_54_n_1 ,\r[1]_INST_0_i_54_n_2 ,\r[1]_INST_0_i_54_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_54_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_22_1 ,\r[1]_INST_0_i_117_n_0 }));
  CARRY4 \r[1]_INST_0_i_55 
       (.CI(1'b0),
        .CO({rgb910_out,\r[1]_INST_0_i_55_n_1 ,\r[1]_INST_0_i_55_n_2 ,\r[1]_INST_0_i_55_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_55_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_22_2 ,\r[1]_INST_0_i_121_n_0 }));
  CARRY4 \r[1]_INST_0_i_56 
       (.CI(1'b0),
        .CO({rgb1516_out,\r[1]_INST_0_i_56_n_1 ,\r[1]_INST_0_i_56_n_2 ,\r[1]_INST_0_i_56_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_56_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_22_0 ,\r[1]_INST_0_i_125_n_0 }));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \r[1]_INST_0_i_6 
       (.I0(\r[1]_INST_0_i_20_n_0 ),
        .I1(\r[1]_INST_0_i_21_n_0 ),
        .I2(\r[1]_INST_0_i_22_n_0 ),
        .I3(rgb1112_out),
        .I4(rgb1618_out),
        .I5(rgb1011_out),
        .O(\r[1]_INST_0_i_6_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \r[1]_INST_0_i_60 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\r[1]_INST_0_i_60_n_0 ));
  LUT3 #(
    .INIT(8'h04)) 
    \r[1]_INST_0_i_64 
       (.I0(\counterX[4]_i_1_n_0 ),
        .I1(cur_blk_index__0[2]),
        .I2(cur_blk_index__0[1]),
        .O(\r[1]_INST_0_i_64_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \r[1]_INST_0_i_68 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\r[1]_INST_0_i_68_n_0 ));
  CARRY4 \r[1]_INST_0_i_69 
       (.CI(1'b0),
        .CO({\r[1]_INST_0_i_69_n_0 ,\r[1]_INST_0_i_69_n_1 ,\r[1]_INST_0_i_69_n_2 ,\r[1]_INST_0_i_69_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_69_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_27_1 ,\r[1]_INST_0_i_130_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h88888880)) 
    \r[1]_INST_0_i_7 
       (.I0(counterY_reg[7]),
        .I1(counterY_reg[5]),
        .I2(counterY_reg[4]),
        .I3(counterY_reg[2]),
        .I4(counterY_reg[3]),
        .O(\r[1]_INST_0_i_7_n_0 ));
  CARRY4 \r[1]_INST_0_i_70 
       (.CI(1'b0),
        .CO({rgb2,\r[1]_INST_0_i_70_n_1 ,\r[1]_INST_0_i_70_n_2 ,\r[1]_INST_0_i_70_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_70_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_27_2 ,\r[1]_INST_0_i_134_n_0 }));
  CARRY4 \r[1]_INST_0_i_71 
       (.CI(1'b0),
        .CO({rgb4,\r[1]_INST_0_i_71_n_1 ,\r[1]_INST_0_i_71_n_2 ,\r[1]_INST_0_i_71_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_71_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_27_0 ,\r[1]_INST_0_i_138_n_0 }));
  CARRY4 \r[1]_INST_0_i_72 
       (.CI(1'b0),
        .CO({rgb40_out,\r[1]_INST_0_i_72_n_1 ,\r[1]_INST_0_i_72_n_2 ,\r[1]_INST_0_i_72_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_r[1]_INST_0_i_72_O_UNCONNECTED [3:0]),
        .S({\r[1]_INST_0_i_27_3 ,\r[1]_INST_0_i_142_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \r[1]_INST_0_i_73 
       (.I0(counterY_reg[8]),
        .I1(counterY_reg[9]),
        .O(\r[1]_INST_0_i_73_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEEEA00001115)) 
    \r[1]_INST_0_i_74 
       (.I0(counterY_reg[8]),
        .I1(counterY_reg[6]),
        .I2(\r[0]_INST_0_i_6_n_0 ),
        .I3(counterY_reg[5]),
        .I4(counterY_reg[7]),
        .I5(counterY_reg[9]),
        .O(A[5]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAA88888)) 
    \r[1]_INST_0_i_75 
       (.I0(counterY_reg[9]),
        .I1(counterY_reg[7]),
        .I2(counterY_reg[5]),
        .I3(\r[0]_INST_0_i_6_n_0 ),
        .I4(counterY_reg[6]),
        .I5(counterY_reg[8]),
        .O(\r[1]_INST_0_i_75_n_0 ));
  LUT6 #(
    .INIT(64'hCCCCCCCCC2C2C222)) 
    \r[1]_INST_0_i_76 
       (.I0(counterY_reg[9]),
        .I1(counterY_reg[8]),
        .I2(counterY_reg[6]),
        .I3(\r[0]_INST_0_i_6_n_0 ),
        .I4(counterY_reg[5]),
        .I5(counterY_reg[7]),
        .O(\r[1]_INST_0_i_76_n_0 ));
  LUT6 #(
    .INIT(64'h5A5A5AAAA6A6A666)) 
    \r[1]_INST_0_i_77 
       (.I0(counterY_reg[9]),
        .I1(counterY_reg[8]),
        .I2(counterY_reg[6]),
        .I3(\r[0]_INST_0_i_6_n_0 ),
        .I4(counterY_reg[5]),
        .I5(counterY_reg[7]),
        .O(\r[1]_INST_0_i_77_n_0 ));
  LUT6 #(
    .INIT(64'hF3733333F37FFFFF)) 
    \r[1]_INST_0_i_8 
       (.I0(\r[1]_INST_0_i_26_n_0 ),
        .I1(\r[1]_INST_0_i_27_n_0 ),
        .I2(counterX_reg[6]),
        .I3(\r[1]_INST_0_i_28_n_0 ),
        .I4(\r[1]_INST_0_i_29_n_0 ),
        .I5(counterX_reg[9]),
        .O(\r[1]_INST_0_i_8_n_0 ));
  LUT3 #(
    .INIT(8'h80)) 
    \r[1]_INST_0_i_81 
       (.I0(cur_blk_index__0[2]),
        .I1(cur_blk_index__0[1]),
        .I2(\counterX[4]_i_1_n_0 ),
        .O(\r[1]_INST_0_i_81_n_0 ));
  LUT3 #(
    .INIT(8'h10)) 
    \r[1]_INST_0_i_85 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\r[1]_INST_0_i_85_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \r[1]_INST_0_i_89 
       (.I0(cur_blk_index__0[1]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[2]),
        .O(\r[1]_INST_0_i_89_n_0 ));
  LUT6 #(
    .INIT(64'h0001111101011111)) 
    \r[1]_INST_0_i_9 
       (.I0(counterY_reg[8]),
        .I1(counterY_reg[9]),
        .I2(counterY_reg[6]),
        .I3(counterY_reg[5]),
        .I4(counterY_reg[7]),
        .I5(\r[1]_INST_0_i_30_n_0 ),
        .O(\r[1]_INST_0_i_9_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    \r[1]_INST_0_i_93 
       (.I0(cur_blk_index__0[2]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[1]),
        .O(\r[1]_INST_0_i_93_n_0 ));
  LUT3 #(
    .INIT(8'h20)) 
    \r[1]_INST_0_i_97 
       (.I0(cur_blk_index__0[2]),
        .I1(\counterX[4]_i_1_n_0 ),
        .I2(cur_blk_index__0[1]),
        .O(\r[1]_INST_0_i_97_n_0 ));
  FDRE v_enabled_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\counterX[9]_i_1_n_0 ),
        .Q(v_enabled),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    vs_INST_0
       (.I0(counterY_reg[2]),
        .I1(counterY_reg[3]),
        .I2(counterY_reg[8]),
        .I3(counterY_reg[9]),
        .I4(vs_INST_0_i_1_n_0),
        .I5(counterY_reg[1]),
        .O(vs));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    vs_INST_0_i_1
       (.I0(counterY_reg[7]),
        .I1(counterY_reg[5]),
        .I2(counterY_reg[6]),
        .I3(counterY_reg[4]),
        .O(vs_INST_0_i_1_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
