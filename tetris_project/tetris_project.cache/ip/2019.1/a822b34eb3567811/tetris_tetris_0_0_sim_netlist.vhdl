-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Thu Mar 19 19:52:24 2020
-- Host        : notg00datmath running 64-bit Ubuntu 18.04.4 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ tetris_tetris_0_0_sim_netlist.vhdl
-- Design      : tetris_tetris_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce is
  port (
    down_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_down : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce is
  signal \PB_cnt[0]_i_1_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal PB_state_i_1_n_0 : STD_LOGIC;
  signal PB_sync_0 : STD_LOGIC;
  signal PB_sync_1 : STD_LOGIC;
  signal down_o_INST_0_i_2_n_0 : STD_LOGIC;
  signal down_o_INST_0_i_3_n_0 : STD_LOGIC;
  signal down_o_INST_0_i_4_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal p_2_in : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of PB_state_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of down_o_INST_0 : label is "soft_lutpair0";
begin
\PB_cnt[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1,
      O => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1_n_0\
    );
PB_state_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => PB_state_i_1_n_0
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_state_i_1_n_0,
      Q => PB_state,
      R => '0'
    );
PB_sync_0_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => btn_down,
      O => p_0_in
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in,
      Q => PB_sync_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0,
      Q => PB_sync_1,
      R => '0'
    );
down_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => down_o
    );
down_o_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => down_o_INST_0_i_2_n_0,
      I1 => PB_cnt_reg(2),
      I2 => PB_cnt_reg(3),
      I3 => PB_cnt_reg(4),
      I4 => PB_cnt_reg(15),
      I5 => down_o_INST_0_i_3_n_0,
      O => p_2_in
    );
down_o_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => down_o_INST_0_i_2_n_0
    );
down_o_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1_n_0\,
      I1 => down_o_INST_0_i_4_n_0,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => down_o_INST_0_i_3_n_0
    );
down_o_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => down_o_INST_0_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_0 is
  port (
    drop_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_drop : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_0 : entity is "debounce";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_0 is
  signal \PB_cnt[0]_i_1__3_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3__3_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2__3_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal \PB_state_i_1__1_n_0\ : STD_LOGIC;
  signal \PB_sync_0_i_1__3_n_0\ : STD_LOGIC;
  signal PB_sync_0_reg_n_0 : STD_LOGIC;
  signal PB_sync_1_reg_n_0 : STD_LOGIC;
  signal drop_o_INST_0_i_2_n_0 : STD_LOGIC;
  signal drop_o_INST_0_i_3_n_0 : STD_LOGIC;
  signal drop_o_INST_0_i_4_n_0 : STD_LOGIC;
  signal p_2_in : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \PB_state_i_1__1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of drop_o_INST_0 : label is "soft_lutpair1";
begin
\PB_cnt[0]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1_reg_n_0,
      O => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt[0]_i_3__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3__3_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__3_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[0]_i_2__3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2__3_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2__3_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2__3_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2__3_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2__3_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2__3_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2__3_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3__3_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__3_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__3_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__3_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[12]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1__3_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1__3_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1__3_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1__3_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1__3_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1__3_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1__3_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1__3_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__3_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__3_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__3_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__3_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__3_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__3_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__3_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[4]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2__3_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1__3_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1__3_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1__3_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1__3_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1__3_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1__3_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1__3_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__3_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__3_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__3_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__3_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[8]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1__3_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1__3_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1__3_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1__3_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1__3_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1__3_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1__3_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1__3_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__3_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_state_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => \PB_state_i_1__1_n_0\
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_state_i_1__1_n_0\,
      Q => PB_state,
      R => '0'
    );
\PB_sync_0_i_1__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => btn_drop,
      O => \PB_sync_0_i_1__3_n_0\
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_sync_0_i_1__3_n_0\,
      Q => PB_sync_0_reg_n_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_reg_n_0,
      Q => PB_sync_1_reg_n_0,
      R => '0'
    );
drop_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => drop_o
    );
drop_o_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => drop_o_INST_0_i_2_n_0,
      I1 => PB_cnt_reg(2),
      I2 => PB_cnt_reg(3),
      I3 => PB_cnt_reg(4),
      I4 => PB_cnt_reg(15),
      I5 => drop_o_INST_0_i_3_n_0,
      O => p_2_in
    );
drop_o_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => drop_o_INST_0_i_2_n_0
    );
drop_o_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1__3_n_0\,
      I1 => drop_o_INST_0_i_4_n_0,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => drop_o_INST_0_i_3_n_0
    );
drop_o_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => drop_o_INST_0_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_1 is
  port (
    left_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_left : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_1 : entity is "debounce";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_1 is
  signal \PB_cnt[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3__0_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal \PB_state_i_1__4_n_0\ : STD_LOGIC;
  signal \PB_sync_0_i_1__0_n_0\ : STD_LOGIC;
  signal PB_sync_0_reg_n_0 : STD_LOGIC;
  signal PB_sync_1_reg_n_0 : STD_LOGIC;
  signal left_o_INST_0_i_1_n_0 : STD_LOGIC;
  signal left_o_INST_0_i_2_n_0 : STD_LOGIC;
  signal left_o_INST_0_i_3_n_0 : STD_LOGIC;
  signal left_o_INST_0_i_4_n_0 : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \PB_state_i_1__4\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of left_o_INST_0 : label is "soft_lutpair2";
begin
\PB_cnt[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1_reg_n_0,
      O => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt[0]_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3__0_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__0_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[0]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2__0_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2__0_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2__0_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2__0_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2__0_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2__0_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2__0_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3__0_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__0_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__0_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__0_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1__0_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1__0_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1__0_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1__0_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1__0_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1__0_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1__0_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1__0_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__0_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__0_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__0_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__0_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__0_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__0_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__0_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2__0_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1__0_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1__0_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1__0_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1__0_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1__0_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1__0_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__0_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__0_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__0_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__0_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1__0_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1__0_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1__0_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1__0_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1__0_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1__0_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1__0_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__0_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_state_i_1__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => left_o_INST_0_i_1_n_0,
      I1 => PB_state,
      O => \PB_state_i_1__4_n_0\
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_state_i_1__4_n_0\,
      Q => PB_state,
      R => '0'
    );
\PB_sync_0_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => btn_left,
      O => \PB_sync_0_i_1__0_n_0\
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_sync_0_i_1__0_n_0\,
      Q => PB_sync_0_reg_n_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_reg_n_0,
      Q => PB_sync_1_reg_n_0,
      R => '0'
    );
left_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => left_o_INST_0_i_1_n_0,
      I1 => PB_state,
      O => left_o
    );
left_o_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => left_o_INST_0_i_2_n_0,
      I1 => left_o_INST_0_i_3_n_0,
      I2 => PB_cnt_reg(2),
      I3 => PB_cnt_reg(3),
      I4 => PB_cnt_reg(4),
      I5 => PB_cnt_reg(15),
      O => left_o_INST_0_i_1_n_0
    );
left_o_INST_0_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1__0_n_0\,
      I1 => left_o_INST_0_i_4_n_0,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => left_o_INST_0_i_2_n_0
    );
left_o_INST_0_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => left_o_INST_0_i_3_n_0
    );
left_o_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => left_o_INST_0_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_2 is
  port (
    right_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_right : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_2 : entity is "debounce";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_2 is
  signal \PB_cnt[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3__1_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2__1_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal \PB_state_i_1__5_n_0\ : STD_LOGIC;
  signal \PB_sync_0_i_1__1_n_0\ : STD_LOGIC;
  signal PB_sync_0_reg_n_0 : STD_LOGIC;
  signal PB_sync_1_reg_n_0 : STD_LOGIC;
  signal right_o_INST_0_i_1_n_0 : STD_LOGIC;
  signal right_o_INST_0_i_2_n_0 : STD_LOGIC;
  signal right_o_INST_0_i_3_n_0 : STD_LOGIC;
  signal right_o_INST_0_i_4_n_0 : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \PB_state_i_1__5\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of right_o_INST_0 : label is "soft_lutpair3";
begin
\PB_cnt[0]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1_reg_n_0,
      O => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt[0]_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3__1_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__1_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[0]_i_2__1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2__1_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2__1_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2__1_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2__1_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2__1_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2__1_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2__1_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3__1_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__1_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__1_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__1_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[12]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1__1_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1__1_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1__1_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1__1_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1__1_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1__1_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1__1_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1__1_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__1_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__1_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__1_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__1_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__1_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__1_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__1_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[4]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2__1_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1__1_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1__1_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1__1_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1__1_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1__1_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1__1_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1__1_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__1_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__1_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__1_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__1_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[8]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1__1_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1__1_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1__1_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1__1_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1__1_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1__1_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1__1_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1__1_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__1_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_state_i_1__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => right_o_INST_0_i_1_n_0,
      I1 => PB_state,
      O => \PB_state_i_1__5_n_0\
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_state_i_1__5_n_0\,
      Q => PB_state,
      R => '0'
    );
\PB_sync_0_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => btn_right,
      O => \PB_sync_0_i_1__1_n_0\
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_sync_0_i_1__1_n_0\,
      Q => PB_sync_0_reg_n_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_reg_n_0,
      Q => PB_sync_1_reg_n_0,
      R => '0'
    );
right_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_state,
      I1 => right_o_INST_0_i_1_n_0,
      O => right_o
    );
right_o_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => right_o_INST_0_i_2_n_0,
      I1 => right_o_INST_0_i_3_n_0,
      I2 => PB_cnt_reg(2),
      I3 => PB_cnt_reg(3),
      I4 => PB_cnt_reg(4),
      I5 => PB_cnt_reg(15),
      O => right_o_INST_0_i_1_n_0
    );
right_o_INST_0_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1__1_n_0\,
      I1 => right_o_INST_0_i_4_n_0,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => right_o_INST_0_i_2_n_0
    );
right_o_INST_0_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => right_o_INST_0_i_3_n_0
    );
right_o_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => right_o_INST_0_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_3 is
  port (
    rotate_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_rotate : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_3 : entity is "debounce";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_3 is
  signal \PB_cnt[0]_i_1__2_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3__2_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2__2_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal \PB_state_i_1__0_n_0\ : STD_LOGIC;
  signal \PB_sync_0_i_1__2_n_0\ : STD_LOGIC;
  signal PB_sync_0_reg_n_0 : STD_LOGIC;
  signal PB_sync_1_reg_n_0 : STD_LOGIC;
  signal p_2_in : STD_LOGIC;
  signal rotate_o_INST_0_i_2_n_0 : STD_LOGIC;
  signal rotate_o_INST_0_i_3_n_0 : STD_LOGIC;
  signal rotate_o_INST_0_i_4_n_0 : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \PB_state_i_1__0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of rotate_o_INST_0 : label is "soft_lutpair4";
begin
\PB_cnt[0]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1_reg_n_0,
      O => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt[0]_i_3__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3__2_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__2_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[0]_i_2__2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2__2_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2__2_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2__2_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2__2_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2__2_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2__2_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2__2_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3__2_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__2_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__2_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__2_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[12]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1__2_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1__2_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1__2_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1__2_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1__2_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1__2_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1__2_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1__2_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__2_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__2_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__2_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__2_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__2_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__2_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__2_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[4]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2__2_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1__2_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1__2_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1__2_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1__2_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1__2_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1__2_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1__2_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__2_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__2_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__2_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__2_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[8]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1__2_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1__2_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1__2_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1__2_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1__2_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1__2_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1__2_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1__2_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__2_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_state_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => \PB_state_i_1__0_n_0\
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_state_i_1__0_n_0\,
      Q => PB_state,
      R => '0'
    );
\PB_sync_0_i_1__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => btn_rotate,
      O => \PB_sync_0_i_1__2_n_0\
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_sync_0_i_1__2_n_0\,
      Q => PB_sync_0_reg_n_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_reg_n_0,
      Q => PB_sync_1_reg_n_0,
      R => '0'
    );
rotate_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => rotate_o
    );
rotate_o_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => rotate_o_INST_0_i_2_n_0,
      I1 => PB_cnt_reg(2),
      I2 => PB_cnt_reg(3),
      I3 => PB_cnt_reg(4),
      I4 => PB_cnt_reg(15),
      I5 => rotate_o_INST_0_i_3_n_0,
      O => p_2_in
    );
rotate_o_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => rotate_o_INST_0_i_2_n_0
    );
rotate_o_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1__2_n_0\,
      I1 => rotate_o_INST_0_i_4_n_0,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => rotate_o_INST_0_i_3_n_0
    );
rotate_o_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => rotate_o_INST_0_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_4 is
  port (
    sw_pause_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    sw_pause : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_4 : entity is "debounce";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_4;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_4 is
  signal \PB_cnt[0]_i_1__5_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3__5_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2__5_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal \PB_state_i_1__3_n_0\ : STD_LOGIC;
  signal \PB_sync_0_i_1__5_n_0\ : STD_LOGIC;
  signal PB_sync_0_reg_n_0 : STD_LOGIC;
  signal PB_sync_1_reg_n_0 : STD_LOGIC;
  signal \sw_flipped_i_1__0_n_0\ : STD_LOGIC;
  signal \sw_flipped_i_2__0_n_0\ : STD_LOGIC;
  signal \sw_flipped_i_3__0_n_0\ : STD_LOGIC;
  signal \sw_flipped_i_4__0_n_0\ : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
\PB_cnt[0]_i_1__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1_reg_n_0,
      O => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt[0]_i_3__5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3__5_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__5_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[0]_i_2__5\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2__5_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2__5_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2__5_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2__5_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2__5_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2__5_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2__5_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3__5_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__5_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__5_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__5_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[12]_i_1__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1__5_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1__5_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1__5_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1__5_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1__5_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1__5_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1__5_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1__5_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__5_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__5_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__5_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__5_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__5_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__5_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__5_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[4]_i_1__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2__5_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1__5_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1__5_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1__5_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1__5_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1__5_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1__5_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1__5_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__5_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__5_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__5_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__5_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[8]_i_1__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1__5_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1__5_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1__5_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1__5_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1__5_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1__5_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1__5_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1__5_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__5_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_state_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sw_flipped_i_1__0_n_0\,
      I1 => PB_state,
      O => \PB_state_i_1__3_n_0\
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_state_i_1__3_n_0\,
      Q => PB_state,
      R => '0'
    );
\PB_sync_0_i_1__5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sw_pause,
      O => \PB_sync_0_i_1__5_n_0\
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_sync_0_i_1__5_n_0\,
      Q => PB_sync_0_reg_n_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_reg_n_0,
      Q => PB_sync_1_reg_n_0,
      R => '0'
    );
\sw_flipped_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => \sw_flipped_i_2__0_n_0\,
      I1 => PB_cnt_reg(2),
      I2 => PB_cnt_reg(3),
      I3 => PB_cnt_reg(4),
      I4 => PB_cnt_reg(15),
      I5 => \sw_flipped_i_3__0_n_0\,
      O => \sw_flipped_i_1__0_n_0\
    );
\sw_flipped_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => \sw_flipped_i_2__0_n_0\
    );
\sw_flipped_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1__5_n_0\,
      I1 => \sw_flipped_i_4__0_n_0\,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => \sw_flipped_i_3__0_n_0\
    );
\sw_flipped_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => \sw_flipped_i_4__0_n_0\
    );
sw_flipped_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \sw_flipped_i_1__0_n_0\,
      Q => sw_pause_o,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_5 is
  port (
    sw_rst_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    sw_rst : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_5 : entity is "debounce";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_5;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_5 is
  signal \PB_cnt[0]_i_1__4_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3__4_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2__4_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal \PB_state_i_1__2_n_0\ : STD_LOGIC;
  signal \PB_sync_0_i_1__4_n_0\ : STD_LOGIC;
  signal PB_sync_0_reg_n_0 : STD_LOGIC;
  signal PB_sync_1_reg_n_0 : STD_LOGIC;
  signal sw_flipped_i_1_n_0 : STD_LOGIC;
  signal sw_flipped_i_2_n_0 : STD_LOGIC;
  signal sw_flipped_i_3_n_0 : STD_LOGIC;
  signal sw_flipped_i_4_n_0 : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
\PB_cnt[0]_i_1__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1_reg_n_0,
      O => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt[0]_i_3__4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3__4_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__4_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[0]_i_2__4\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2__4_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2__4_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2__4_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2__4_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2__4_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2__4_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2__4_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3__4_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__4_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__4_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__4_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[12]_i_1__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1__4_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1__4_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1__4_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1__4_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1__4_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1__4_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1__4_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1__4_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__4_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__4_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__4_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__4_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__4_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__4_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__4_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[4]_i_1__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2__4_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1__4_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1__4_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1__4_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1__4_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1__4_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1__4_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1__4_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__4_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__4_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__4_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__4_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[8]_i_1__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1__4_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1__4_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1__4_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1__4_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1__4_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1__4_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1__4_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1__4_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__4_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_state_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => sw_flipped_i_1_n_0,
      I1 => PB_state,
      O => \PB_state_i_1__2_n_0\
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_state_i_1__2_n_0\,
      Q => PB_state,
      R => '0'
    );
\PB_sync_0_i_1__4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sw_rst,
      O => \PB_sync_0_i_1__4_n_0\
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_sync_0_i_1__4_n_0\,
      Q => PB_sync_0_reg_n_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_reg_n_0,
      Q => PB_sync_1_reg_n_0,
      R => '0'
    );
sw_flipped_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => sw_flipped_i_2_n_0,
      I1 => PB_cnt_reg(2),
      I2 => PB_cnt_reg(3),
      I3 => PB_cnt_reg(4),
      I4 => PB_cnt_reg(15),
      I5 => sw_flipped_i_3_n_0,
      O => sw_flipped_i_1_n_0
    );
sw_flipped_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => sw_flipped_i_2_n_0
    );
sw_flipped_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1__4_n_0\,
      I1 => sw_flipped_i_4_n_0,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => sw_flipped_i_3_n_0
    );
sw_flipped_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => sw_flipped_i_4_n_0
    );
sw_flipped_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => sw_flipped_i_1_n_0,
      Q => sw_rst_o,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga is
  port (
    g : out STD_LOGIC_VECTOR ( 2 downto 0 );
    b : out STD_LOGIC_VECTOR ( 1 downto 0 );
    r : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \counterX_reg[7]_0\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    hs : out STD_LOGIC;
    vs : out STD_LOGIC;
    cur_blk_index : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pixel_clk : in STD_LOGIC;
    S : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_8_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_11_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_12_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_10_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_12_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_11_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_10_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_12_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_8_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_10_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_11_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_8_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_10_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_12_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_21_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_22_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_20_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_21_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_22_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_6_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_6_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_22_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_22_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_20_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_20_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_21_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_20_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_21_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_6_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_27_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_27_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_27_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_27_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga is
  signal A : STD_LOGIC_VECTOR ( 6 downto 4 );
  signal C : STD_LOGIC_VECTOR ( 9 to 9 );
  signal \counterX[4]_i_1_n_0\ : STD_LOGIC;
  signal \counterX[7]_i_1_n_0\ : STD_LOGIC;
  signal \counterX[9]_i_1_n_0\ : STD_LOGIC;
  signal \counterX[9]_i_3_n_0\ : STD_LOGIC;
  signal counterX_reg : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \^counterx_reg[7]_0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal counterY : STD_LOGIC;
  signal \counterY[0]_i_1_n_0\ : STD_LOGIC;
  signal \counterY[6]_i_1_n_0\ : STD_LOGIC;
  signal \counterY[6]_i_2_n_0\ : STD_LOGIC;
  signal \counterY[9]_i_3_n_0\ : STD_LOGIC;
  signal counterY_reg : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal cur_blk_index0 : STD_LOGIC_VECTOR ( 9 downto 2 );
  signal \cur_blk_index__0\ : STD_LOGIC_VECTOR ( 2 downto 1 );
  signal \g[3]_INST_0_i_10_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_11_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_12_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_13_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_13_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_13_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_14_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_14_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_14_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_15_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_15_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_15_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_16_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_16_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_16_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_17_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_17_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_17_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_18_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_18_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_18_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_19_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_19_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_19_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_20_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_20_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_20_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_21_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_21_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_21_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_22_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_22_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_22_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_23_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_23_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_23_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_24_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_24_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_24_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_25_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_25_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_25_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_26_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_26_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_26_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_27_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_27_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_27_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_31_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_35_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_39_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_43_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_47_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_51_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_55_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_59_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_63_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_67_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_71_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_75_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_79_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_83_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_87_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_8_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_9_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 9 downto 1 );
  signal \r[0]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \r[0]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \r[0]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \r[0]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \r[0]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \r[0]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \r[0]_INST_0_i_8_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_101_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_105_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_109_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_10_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_10_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_10_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_10_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_113_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_117_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_11_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_11_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_11_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_11_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_121_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_125_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_12_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_130_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_134_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_138_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_13_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_142_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_143_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_14_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_15_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_18_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_19_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_20_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_21_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_22_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_23_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_23_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_23_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_24_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_24_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_24_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_25_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_25_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_25_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_26_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_27_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_28_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_29_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_30_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_31_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_32_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_33_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_34_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_35_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_37_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_38_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_39_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_3_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_3_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_3_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_40_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_41_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_42_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_43_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_43_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_44_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_45_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_45_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_45_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_46_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_46_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_46_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_47_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_47_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_47_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_48_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_48_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_48_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_49_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_49_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_49_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_50_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_50_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_50_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_51_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_51_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_51_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_52_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_52_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_52_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_53_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_53_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_53_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_54_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_54_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_54_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_55_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_55_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_55_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_56_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_56_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_56_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_60_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_64_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_68_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_69_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_69_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_69_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_69_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_70_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_70_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_70_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_71_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_71_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_71_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_72_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_72_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_72_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_73_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_75_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_76_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_77_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_81_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_85_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_89_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_8_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_93_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_97_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_9_n_0\ : STD_LOGIC;
  signal rgb10 : STD_LOGIC;
  signal rgb1011_out : STD_LOGIC;
  signal rgb11 : STD_LOGIC;
  signal rgb1112_out : STD_LOGIC;
  signal rgb12 : STD_LOGIC;
  signal rgb1213_out : STD_LOGIC;
  signal rgb13 : STD_LOGIC;
  signal rgb1314_out : STD_LOGIC;
  signal rgb14 : STD_LOGIC;
  signal rgb1415_out : STD_LOGIC;
  signal rgb15 : STD_LOGIC;
  signal rgb1516_out : STD_LOGIC;
  signal rgb16 : STD_LOGIC;
  signal rgb1617_out : STD_LOGIC;
  signal rgb1618_out : STD_LOGIC;
  signal rgb163_out : STD_LOGIC;
  signal rgb2 : STD_LOGIC;
  signal rgb31_out : STD_LOGIC;
  signal rgb34_out : STD_LOGIC;
  signal rgb4 : STD_LOGIC;
  signal rgb40_out : STD_LOGIC;
  signal rgb42_out : STD_LOGIC;
  signal rgb45_out : STD_LOGIC;
  signal rgb5 : STD_LOGIC;
  signal rgb56_out : STD_LOGIC;
  signal rgb6 : STD_LOGIC;
  signal rgb67_out : STD_LOGIC;
  signal rgb7 : STD_LOGIC;
  signal rgb78_out : STD_LOGIC;
  signal rgb8 : STD_LOGIC;
  signal rgb89_out : STD_LOGIC;
  signal rgb9 : STD_LOGIC;
  signal rgb910_out : STD_LOGIC;
  signal v_enabled : STD_LOGIC;
  signal vs_INST_0_i_1_n_0 : STD_LOGIC;
  signal \NLW_g[3]_INST_0_i_13_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_14_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_15_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_16_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_17_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_18_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_19_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_20_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_21_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_22_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_23_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_24_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_25_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_26_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_27_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_10_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_r[1]_INST_0_i_126_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_126_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_r[1]_INST_0_i_23_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_24_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_25_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_43_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_r[1]_INST_0_i_43_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_r[1]_INST_0_i_45_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_46_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_47_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_48_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_49_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_50_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_51_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_52_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_53_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_54_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_55_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_56_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_69_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_70_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_71_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_72_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \b[1]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \counterX[0]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \counterX[1]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \counterX[2]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \counterX[3]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \counterX[6]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \counterX[7]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \counterX[9]_i_3\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \counterY[2]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \counterY[3]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \counterY[4]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \counterY[6]_i_2\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \counterY[7]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \counterY[9]_i_2\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \g[0]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \g[2]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \g[3]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \g[3]_INST_0_i_5\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \g[3]_INST_0_i_9\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of hs_INST_0 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \r[0]_INST_0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \r[0]_INST_0_i_6\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \r[0]_INST_0_i_7\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \r[0]_INST_0_i_8\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \r[1]_INST_0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_18\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_26\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_28\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_29\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_30\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_44\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_7\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_73\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of vs_INST_0_i_1 : label is "soft_lutpair17";
begin
  \counterX_reg[7]_0\(4 downto 0) <= \^counterx_reg[7]_0\(4 downto 0);
\b[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \g[3]_INST_0_i_2_n_0\,
      I1 => \r[1]_INST_0_i_2_n_0\,
      O => b(1)
    );
\b[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \r[1]_INST_0_i_1_n_0\,
      I1 => \g[3]_INST_0_i_2_n_0\,
      I2 => \r[1]_INST_0_i_2_n_0\,
      O => b(0)
    );
\counterX[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counterX_reg(0),
      O => p_0_in(0)
    );
\counterX[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => counterX_reg(0),
      I1 => counterX_reg(1),
      O => p_0_in(1)
    );
\counterX[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => counterX_reg(2),
      I1 => counterX_reg(0),
      I2 => counterX_reg(1),
      O => p_0_in(2)
    );
\counterX[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => counterX_reg(3),
      I1 => counterX_reg(1),
      I2 => counterX_reg(0),
      I3 => counterX_reg(2),
      O => p_0_in(3)
    );
\counterX[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => counterX_reg(4),
      I1 => counterX_reg(1),
      I2 => counterX_reg(2),
      I3 => counterX_reg(0),
      I4 => counterX_reg(3),
      O => \counterX[4]_i_1_n_0\
    );
\counterX[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => counterX_reg(5),
      I1 => counterX_reg(3),
      I2 => counterX_reg(0),
      I3 => counterX_reg(2),
      I4 => counterX_reg(1),
      I5 => counterX_reg(4),
      O => p_0_in(5)
    );
\counterX[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A6AA"
    )
        port map (
      I0 => counterX_reg(6),
      I1 => counterX_reg(4),
      I2 => \g[3]_INST_0_i_5_n_0\,
      I3 => counterX_reg(5),
      O => p_0_in(6)
    );
\counterX[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAA6AAA"
    )
        port map (
      I0 => counterX_reg(7),
      I1 => counterX_reg(4),
      I2 => counterX_reg(6),
      I3 => counterX_reg(5),
      I4 => \g[3]_INST_0_i_5_n_0\,
      O => \counterX[7]_i_1_n_0\
    );
\counterX[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => counterX_reg(8),
      I1 => \g[3]_INST_0_i_5_n_0\,
      I2 => counterX_reg(5),
      I3 => counterX_reg(6),
      I4 => counterX_reg(4),
      I5 => counterX_reg(7),
      O => p_0_in(8)
    );
\counterX[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFE000000000000"
    )
        port map (
      I0 => counterX_reg(5),
      I1 => counterX_reg(7),
      I2 => counterX_reg(6),
      I3 => \counterX[9]_i_3_n_0\,
      I4 => counterX_reg(9),
      I5 => counterX_reg(8),
      O => \counterX[9]_i_1_n_0\
    );
\counterX[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => counterX_reg(9),
      I1 => counterX_reg(6),
      I2 => counterX_reg(8),
      I3 => counterX_reg(7),
      I4 => counterX_reg(5),
      I5 => \counterX[9]_i_3_n_0\,
      O => p_0_in(9)
    );
\counterX[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => counterX_reg(4),
      I1 => counterX_reg(1),
      I2 => counterX_reg(2),
      I3 => counterX_reg(0),
      I4 => counterX_reg(3),
      O => \counterX[9]_i_3_n_0\
    );
\counterX_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(0),
      Q => counterX_reg(0),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(1),
      Q => counterX_reg(1),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(2),
      Q => counterX_reg(2),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(3),
      Q => counterX_reg(3),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => \counterX[4]_i_1_n_0\,
      Q => counterX_reg(4),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(5),
      Q => counterX_reg(5),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(6),
      Q => counterX_reg(6),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => \counterX[7]_i_1_n_0\,
      Q => counterX_reg(7),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(8),
      Q => counterX_reg(8),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(9),
      Q => counterX_reg(9),
      R => \counterX[9]_i_1_n_0\
    );
\counterY[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000015FFFF"
    )
        port map (
      I0 => counterY_reg(8),
      I1 => counterY_reg(3),
      I2 => counterY_reg(2),
      I3 => vs_INST_0_i_1_n_0,
      I4 => counterY_reg(9),
      I5 => counterY_reg(0),
      O => \counterY[0]_i_1_n_0\
    );
\counterY[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => counterY_reg(0),
      I1 => counterY_reg(1),
      O => \p_0_in__0\(1)
    );
\counterY[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => counterY_reg(2),
      I1 => counterY_reg(1),
      I2 => counterY_reg(0),
      O => \p_0_in__0\(2)
    );
\counterY[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => counterY_reg(3),
      I1 => counterY_reg(0),
      I2 => counterY_reg(1),
      I3 => counterY_reg(2),
      O => \p_0_in__0\(3)
    );
\counterY[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => counterY_reg(4),
      I1 => counterY_reg(0),
      I2 => counterY_reg(1),
      I3 => counterY_reg(2),
      I4 => counterY_reg(3),
      O => \p_0_in__0\(4)
    );
\counterY[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => counterY_reg(5),
      I1 => counterY_reg(4),
      I2 => counterY_reg(1),
      I3 => counterY_reg(0),
      I4 => counterY_reg(2),
      I5 => counterY_reg(3),
      O => \p_0_in__0\(5)
    );
\counterY[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => counterY_reg(6),
      I1 => counterY_reg(4),
      I2 => counterY_reg(5),
      I3 => counterY_reg(3),
      I4 => counterY_reg(2),
      I5 => \counterY[6]_i_2_n_0\,
      O => \counterY[6]_i_1_n_0\
    );
\counterY[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => counterY_reg(0),
      I1 => counterY_reg(1),
      O => \counterY[6]_i_2_n_0\
    );
\counterY[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => counterY_reg(7),
      I1 => \counterY[9]_i_3_n_0\,
      O => \p_0_in__0\(7)
    );
\counterY[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => counterY_reg(8),
      I1 => counterY_reg(7),
      I2 => \counterY[9]_i_3_n_0\,
      O => \p_0_in__0\(8)
    );
\counterY[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEA000000000000"
    )
        port map (
      I0 => counterY_reg(8),
      I1 => counterY_reg(3),
      I2 => counterY_reg(2),
      I3 => vs_INST_0_i_1_n_0,
      I4 => v_enabled,
      I5 => counterY_reg(9),
      O => counterY
    );
\counterY[9]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(7),
      I2 => counterY_reg(8),
      I3 => \counterY[9]_i_3_n_0\,
      O => \p_0_in__0\(9)
    );
\counterY[9]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counterY_reg(6),
      I1 => counterY_reg(4),
      I2 => counterY_reg(5),
      I3 => counterY_reg(3),
      I4 => counterY_reg(2),
      I5 => \counterY[6]_i_2_n_0\,
      O => \counterY[9]_i_3_n_0\
    );
\counterY_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \counterY[0]_i_1_n_0\,
      Q => counterY_reg(0),
      R => '0'
    );
\counterY_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(1),
      Q => counterY_reg(1),
      R => counterY
    );
\counterY_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(2),
      Q => counterY_reg(2),
      R => counterY
    );
\counterY_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(3),
      Q => counterY_reg(3),
      R => counterY
    );
\counterY_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(4),
      Q => counterY_reg(4),
      R => counterY
    );
\counterY_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(5),
      Q => counterY_reg(5),
      R => counterY
    );
\counterY_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \counterY[6]_i_1_n_0\,
      Q => counterY_reg(6),
      R => counterY
    );
\counterY_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(7),
      Q => counterY_reg(7),
      R => counterY
    );
\counterY_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(8),
      Q => counterY_reg(8),
      R => counterY
    );
\counterY_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(9),
      Q => counterY_reg(9),
      R => counterY
    );
\g[0]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55555554"
    )
        port map (
      I0 => \g[3]_INST_0_i_1_n_0\,
      I1 => \r[0]_INST_0_i_1_n_0\,
      I2 => \r[1]_INST_0_i_2_n_0\,
      I3 => \g[3]_INST_0_i_2_n_0\,
      I4 => \r[1]_INST_0_i_1_n_0\,
      O => g(0)
    );
\g[2]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000FFFE"
    )
        port map (
      I0 => \r[1]_INST_0_i_1_n_0\,
      I1 => \g[3]_INST_0_i_2_n_0\,
      I2 => \r[1]_INST_0_i_2_n_0\,
      I3 => \r[0]_INST_0_i_1_n_0\,
      I4 => \g[3]_INST_0_i_1_n_0\,
      O => g(1)
    );
\g[3]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5554"
    )
        port map (
      I0 => \g[3]_INST_0_i_1_n_0\,
      I1 => \g[3]_INST_0_i_2_n_0\,
      I2 => \r[0]_INST_0_i_1_n_0\,
      I3 => \r[1]_INST_0_i_2_n_0\,
      O => g(2)
    );
\g[3]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA02AA02AA020000"
    )
        port map (
      I0 => \r[0]_INST_0_i_1_n_0\,
      I1 => \g[3]_INST_0_i_3_n_0\,
      I2 => counterY_reg(6),
      I3 => \g[3]_INST_0_i_4_n_0\,
      I4 => \g[3]_INST_0_i_5_n_0\,
      I5 => \g[3]_INST_0_i_6_n_0\,
      O => \g[3]_INST_0_i_1_n_0\
    );
\g[3]_INST_0_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => rgb6,
      I1 => rgb12,
      I2 => rgb31_out,
      I3 => rgb9,
      O => \g[3]_INST_0_i_10_n_0\
    );
\g[3]_INST_0_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => rgb10,
      I1 => rgb14,
      I2 => rgb5,
      I3 => rgb16,
      O => \g[3]_INST_0_i_11_n_0\
    );
\g[3]_INST_0_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => rgb8,
      I1 => rgb13,
      I2 => rgb163_out,
      I3 => rgb11,
      O => \g[3]_INST_0_i_12_n_0\
    );
\g[3]_INST_0_i_13\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb7,
      CO(2) => \g[3]_INST_0_i_13_n_1\,
      CO(1) => \g[3]_INST_0_i_13_n_2\,
      CO(0) => \g[3]_INST_0_i_13_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_13_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_8_1\(2 downto 0),
      S(0) => \g[3]_INST_0_i_31_n_0\
    );
\g[3]_INST_0_i_14\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb42_out,
      CO(2) => \g[3]_INST_0_i_14_n_1\,
      CO(1) => \g[3]_INST_0_i_14_n_2\,
      CO(0) => \g[3]_INST_0_i_14_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_14_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_8_2\(2 downto 0),
      S(0) => \g[3]_INST_0_i_35_n_0\
    );
\g[3]_INST_0_i_15\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb15,
      CO(2) => \g[3]_INST_0_i_15_n_1\,
      CO(1) => \g[3]_INST_0_i_15_n_2\,
      CO(0) => \g[3]_INST_0_i_15_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_15_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_8_0\(2 downto 0),
      S(0) => \g[3]_INST_0_i_39_n_0\
    );
\g[3]_INST_0_i_16\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb6,
      CO(2) => \g[3]_INST_0_i_16_n_1\,
      CO(1) => \g[3]_INST_0_i_16_n_2\,
      CO(0) => \g[3]_INST_0_i_16_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_16_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_10_2\(2 downto 0),
      S(0) => \g[3]_INST_0_i_43_n_0\
    );
\g[3]_INST_0_i_17\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb12,
      CO(2) => \g[3]_INST_0_i_17_n_1\,
      CO(1) => \g[3]_INST_0_i_17_n_2\,
      CO(0) => \g[3]_INST_0_i_17_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_17_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_10_0\(2 downto 0),
      S(0) => \g[3]_INST_0_i_47_n_0\
    );
\g[3]_INST_0_i_18\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb31_out,
      CO(2) => \g[3]_INST_0_i_18_n_1\,
      CO(1) => \g[3]_INST_0_i_18_n_2\,
      CO(0) => \g[3]_INST_0_i_18_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_18_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_10_3\(2 downto 0),
      S(0) => \g[3]_INST_0_i_51_n_0\
    );
\g[3]_INST_0_i_19\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb9,
      CO(2) => \g[3]_INST_0_i_19_n_1\,
      CO(1) => \g[3]_INST_0_i_19_n_2\,
      CO(0) => \g[3]_INST_0_i_19_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_19_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_10_1\(2 downto 0),
      S(0) => \g[3]_INST_0_i_55_n_0\
    );
\g[3]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000400"
    )
        port map (
      I0 => \^counterx_reg[7]_0\(3),
      I1 => \g[3]_INST_0_i_7_n_0\,
      I2 => \g[3]_INST_0_i_8_n_0\,
      I3 => \r[1]_INST_0_i_6_n_0\,
      I4 => \^counterx_reg[7]_0\(4),
      I5 => \r[1]_INST_0_i_5_n_0\,
      O => \g[3]_INST_0_i_2_n_0\
    );
\g[3]_INST_0_i_20\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb10,
      CO(2) => \g[3]_INST_0_i_20_n_1\,
      CO(1) => \g[3]_INST_0_i_20_n_2\,
      CO(0) => \g[3]_INST_0_i_20_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_20_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_11_1\(2 downto 0),
      S(0) => \g[3]_INST_0_i_59_n_0\
    );
\g[3]_INST_0_i_21\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb14,
      CO(2) => \g[3]_INST_0_i_21_n_1\,
      CO(1) => \g[3]_INST_0_i_21_n_2\,
      CO(0) => \g[3]_INST_0_i_21_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_21_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_11_0\(2 downto 0),
      S(0) => \g[3]_INST_0_i_63_n_0\
    );
\g[3]_INST_0_i_22\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb5,
      CO(2) => \g[3]_INST_0_i_22_n_1\,
      CO(1) => \g[3]_INST_0_i_22_n_2\,
      CO(0) => \g[3]_INST_0_i_22_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_22_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_11_2\(2 downto 0),
      S(0) => \g[3]_INST_0_i_67_n_0\
    );
\g[3]_INST_0_i_23\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb16,
      CO(2) => \g[3]_INST_0_i_23_n_1\,
      CO(1) => \g[3]_INST_0_i_23_n_2\,
      CO(0) => \g[3]_INST_0_i_23_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_23_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => S(2 downto 0),
      S(0) => \g[3]_INST_0_i_71_n_0\
    );
\g[3]_INST_0_i_24\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb8,
      CO(2) => \g[3]_INST_0_i_24_n_1\,
      CO(1) => \g[3]_INST_0_i_24_n_2\,
      CO(0) => \g[3]_INST_0_i_24_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_24_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_12_2\(2 downto 0),
      S(0) => \g[3]_INST_0_i_75_n_0\
    );
\g[3]_INST_0_i_25\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb13,
      CO(2) => \g[3]_INST_0_i_25_n_1\,
      CO(1) => \g[3]_INST_0_i_25_n_2\,
      CO(0) => \g[3]_INST_0_i_25_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_25_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_12_0\(2 downto 0),
      S(0) => \g[3]_INST_0_i_79_n_0\
    );
\g[3]_INST_0_i_26\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb163_out,
      CO(2) => \g[3]_INST_0_i_26_n_1\,
      CO(1) => \g[3]_INST_0_i_26_n_2\,
      CO(0) => \g[3]_INST_0_i_26_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_26_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_12_3\(2 downto 0),
      S(0) => \g[3]_INST_0_i_83_n_0\
    );
\g[3]_INST_0_i_27\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb11,
      CO(2) => \g[3]_INST_0_i_27_n_1\,
      CO(1) => \g[3]_INST_0_i_27_n_2\,
      CO(0) => \g[3]_INST_0_i_27_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_27_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_12_1\(2 downto 0),
      S(0) => \g[3]_INST_0_i_87_n_0\
    );
\g[3]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000080"
    )
        port map (
      I0 => counterY_reg(7),
      I1 => counterY_reg(5),
      I2 => counterY_reg(8),
      I3 => counterY_reg(9),
      I4 => counterY_reg(2),
      I5 => counterY_reg(3),
      O => \g[3]_INST_0_i_3_n_0\
    );
\g[3]_INST_0_i_31\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \g[3]_INST_0_i_31_n_0\
    );
\g[3]_INST_0_i_35\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \g[3]_INST_0_i_35_n_0\
    );
\g[3]_INST_0_i_39\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \g[3]_INST_0_i_39_n_0\
    );
\g[3]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFF777777777"
    )
        port map (
      I0 => \counterY[6]_i_2_n_0\,
      I1 => counterY_reg(4),
      I2 => counterY_reg(7),
      I3 => counterY_reg(5),
      I4 => \g[3]_INST_0_i_9_n_0\,
      I5 => counterY_reg(6),
      O => \g[3]_INST_0_i_4_n_0\
    );
\g[3]_INST_0_i_43\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \g[3]_INST_0_i_43_n_0\
    );
\g[3]_INST_0_i_47\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \g[3]_INST_0_i_47_n_0\
    );
\g[3]_INST_0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => counterX_reg(3),
      I1 => counterX_reg(0),
      I2 => counterX_reg(2),
      I3 => counterX_reg(1),
      O => \g[3]_INST_0_i_5_n_0\
    );
\g[3]_INST_0_i_51\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \g[3]_INST_0_i_51_n_0\
    );
\g[3]_INST_0_i_55\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \g[3]_INST_0_i_55_n_0\
    );
\g[3]_INST_0_i_59\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(1),
      O => \g[3]_INST_0_i_59_n_0\
    );
\g[3]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFFFFEFFFFFFFFF"
    )
        port map (
      I0 => counterX_reg(4),
      I1 => counterX_reg(9),
      I2 => counterX_reg(8),
      I3 => counterX_reg(6),
      I4 => counterX_reg(5),
      I5 => counterX_reg(7),
      O => \g[3]_INST_0_i_6_n_0\
    );
\g[3]_INST_0_i_63\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \g[3]_INST_0_i_63_n_0\
    );
\g[3]_INST_0_i_67\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \g[3]_INST_0_i_67_n_0\
    );
\g[3]_INST_0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4404000020221800"
    )
        port map (
      I0 => \^counterx_reg[7]_0\(2),
      I1 => \^counterx_reg[7]_0\(1),
      I2 => \^counterx_reg[7]_0\(0),
      I3 => \cur_blk_index__0\(1),
      I4 => \counterX[4]_i_1_n_0\,
      I5 => \cur_blk_index__0\(2),
      O => \g[3]_INST_0_i_7_n_0\
    );
\g[3]_INST_0_i_71\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \g[3]_INST_0_i_71_n_0\
    );
\g[3]_INST_0_i_75\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \g[3]_INST_0_i_75_n_0\
    );
\g[3]_INST_0_i_79\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \g[3]_INST_0_i_79_n_0\
    );
\g[3]_INST_0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000004"
    )
        port map (
      I0 => \g[3]_INST_0_i_10_n_0\,
      I1 => \g[3]_INST_0_i_11_n_0\,
      I2 => \g[3]_INST_0_i_12_n_0\,
      I3 => rgb7,
      I4 => rgb42_out,
      I5 => rgb15,
      O => \g[3]_INST_0_i_8_n_0\
    );
\g[3]_INST_0_i_83\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \g[3]_INST_0_i_83_n_0\
    );
\g[3]_INST_0_i_87\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \g[3]_INST_0_i_87_n_0\
    );
\g[3]_INST_0_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(8),
      I2 => counterY_reg(3),
      I3 => counterY_reg(2),
      O => \g[3]_INST_0_i_9_n_0\
    );
hs_INST_0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFF8"
    )
        port map (
      I0 => counterX_reg(6),
      I1 => counterX_reg(5),
      I2 => counterX_reg(8),
      I3 => counterX_reg(9),
      I4 => counterX_reg(7),
      O => hs
    );
\r[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \r[1]_INST_0_i_2_n_0\,
      I1 => \r[0]_INST_0_i_1_n_0\,
      O => r(0)
    );
\r[0]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => \r[0]_INST_0_i_2_n_0\,
      I1 => \r[0]_INST_0_i_3_n_0\,
      I2 => \r[0]_INST_0_i_4_n_0\,
      I3 => counterY_reg(9),
      I4 => C(9),
      O => \r[0]_INST_0_i_1_n_0\
    );
\r[0]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEEEA"
    )
        port map (
      I0 => counterY_reg(8),
      I1 => counterY_reg(6),
      I2 => \r[0]_INST_0_i_6_n_0\,
      I3 => counterY_reg(5),
      I4 => counterY_reg(7),
      O => \r[0]_INST_0_i_2_n_0\
    );
\r[0]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888800008000"
    )
        port map (
      I0 => counterY_reg(8),
      I1 => counterY_reg(7),
      I2 => counterY_reg(5),
      I3 => counterY_reg(4),
      I4 => \r[0]_INST_0_i_7_n_0\,
      I5 => counterY_reg(6),
      O => \r[0]_INST_0_i_3_n_0\
    );
\r[0]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFE000000"
    )
        port map (
      I0 => counterX_reg(6),
      I1 => counterX_reg(4),
      I2 => counterX_reg(5),
      I3 => counterX_reg(7),
      I4 => counterX_reg(8),
      I5 => counterX_reg(9),
      O => \r[0]_INST_0_i_4_n_0\
    );
\r[0]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000FF7F"
    )
        port map (
      I0 => counterX_reg(7),
      I1 => counterX_reg(6),
      I2 => counterX_reg(5),
      I3 => \r[0]_INST_0_i_8_n_0\,
      I4 => counterX_reg(9),
      I5 => counterX_reg(8),
      O => C(9)
    );
\r[0]_INST_0_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F0F080"
    )
        port map (
      I0 => counterY_reg(0),
      I1 => counterY_reg(1),
      I2 => counterY_reg(4),
      I3 => counterY_reg(2),
      I4 => counterY_reg(3),
      O => \r[0]_INST_0_i_6_n_0\
    );
\r[0]_INST_0_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counterY_reg(2),
      I1 => counterY_reg(3),
      O => \r[0]_INST_0_i_7_n_0\
    );
\r[0]_INST_0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00007FFF"
    )
        port map (
      I0 => counterX_reg(1),
      I1 => counterX_reg(2),
      I2 => counterX_reg(0),
      I3 => counterX_reg(3),
      I4 => counterX_reg(4),
      O => \r[0]_INST_0_i_8_n_0\
    );
\r[1]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \r[1]_INST_0_i_1_n_0\,
      I1 => \r[1]_INST_0_i_2_n_0\,
      O => r(1)
    );
\r[1]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \^counterx_reg[7]_0\(3),
      I1 => \r[1]_INST_0_i_4_n_0\,
      I2 => \^counterx_reg[7]_0\(4),
      I3 => \r[1]_INST_0_i_5_n_0\,
      I4 => \r[1]_INST_0_i_6_n_0\,
      O => \r[1]_INST_0_i_1_n_0\
    );
\r[1]_INST_0_i_10\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \r[1]_INST_0_i_10_n_0\,
      CO(2) => \r[1]_INST_0_i_10_n_1\,
      CO(1) => \r[1]_INST_0_i_10_n_2\,
      CO(0) => \r[1]_INST_0_i_10_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => cur_blk_index0(4 downto 2),
      DI(0) => \r[1]_INST_0_i_31_n_0\,
      O(3 downto 2) => \^counterx_reg[7]_0\(1 downto 0),
      O(1) => \cur_blk_index__0\(2),
      O(0) => \NLW_r[1]_INST_0_i_10_O_UNCONNECTED\(0),
      S(3) => \r[1]_INST_0_i_32_n_0\,
      S(2) => \r[1]_INST_0_i_33_n_0\,
      S(1) => \r[1]_INST_0_i_34_n_0\,
      S(0) => \r[1]_INST_0_i_35_n_0\
    );
\r[1]_INST_0_i_101\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_101_n_0\
    );
\r[1]_INST_0_i_105\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \r[1]_INST_0_i_105_n_0\
    );
\r[1]_INST_0_i_109\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \r[1]_INST_0_i_109_n_0\
    );
\r[1]_INST_0_i_11\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \r[1]_INST_0_i_11_n_0\,
      CO(2) => \r[1]_INST_0_i_11_n_1\,
      CO(1) => \r[1]_INST_0_i_11_n_2\,
      CO(0) => \r[1]_INST_0_i_11_n_3\,
      CYINIT => '0',
      DI(3) => A(4),
      DI(2) => \r[1]_INST_0_i_37_n_0\,
      DI(1) => \r[1]_INST_0_i_38_n_0\,
      DI(0) => '0',
      O(3 downto 0) => cur_blk_index0(5 downto 2),
      S(3) => \r[1]_INST_0_i_39_n_0\,
      S(2) => \r[1]_INST_0_i_40_n_0\,
      S(1) => \r[1]_INST_0_i_41_n_0\,
      S(0) => \r[1]_INST_0_i_42_n_0\
    );
\r[1]_INST_0_i_113\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_113_n_0\
    );
\r[1]_INST_0_i_117\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \r[1]_INST_0_i_117_n_0\
    );
\r[1]_INST_0_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => C(9),
      I1 => cur_blk_index0(8),
      O => \r[1]_INST_0_i_12_n_0\
    );
\r[1]_INST_0_i_121\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \r[1]_INST_0_i_121_n_0\
    );
\r[1]_INST_0_i_125\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_125_n_0\
    );
\r[1]_INST_0_i_126\: unisim.vcomponents.CARRY4
     port map (
      CI => \r[1]_INST_0_i_3_n_0\,
      CO(3 downto 0) => \NLW_r[1]_INST_0_i_126_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_r[1]_INST_0_i_126_O_UNCONNECTED\(3 downto 1),
      O(0) => cur_blk_index(1),
      S(3 downto 1) => B"000",
      S(0) => \r[1]_INST_0_i_143_n_0\
    );
\r[1]_INST_0_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => C(9),
      I1 => cur_blk_index0(7),
      O => \r[1]_INST_0_i_13_n_0\
    );
\r[1]_INST_0_i_130\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \cur_blk_index__0\(2),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_130_n_0\
    );
\r[1]_INST_0_i_134\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \cur_blk_index__0\(2),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_134_n_0\
    );
\r[1]_INST_0_i_138\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_138_n_0\
    );
\r[1]_INST_0_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => C(9),
      I1 => cur_blk_index0(6),
      O => \r[1]_INST_0_i_14_n_0\
    );
\r[1]_INST_0_i_142\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \cur_blk_index__0\(2),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_142_n_0\
    );
\r[1]_INST_0_i_143\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cur_blk_index0(9),
      I1 => C(9),
      O => \r[1]_INST_0_i_143_n_0\
    );
\r[1]_INST_0_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => counterX_reg(8),
      I1 => \r[1]_INST_0_i_44_n_0\,
      I2 => counterX_reg(9),
      I3 => cur_blk_index0(5),
      O => \r[1]_INST_0_i_15_n_0\
    );
\r[1]_INST_0_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA55AA55AA55956A"
    )
        port map (
      I0 => \r[1]_INST_0_i_31_n_0\,
      I1 => counterY_reg(0),
      I2 => counterY_reg(1),
      I3 => counterY_reg(4),
      I4 => counterY_reg(2),
      I5 => counterY_reg(3),
      O => \cur_blk_index__0\(1)
    );
\r[1]_INST_0_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000011111"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(7),
      I2 => counterY_reg(5),
      I3 => \r[0]_INST_0_i_6_n_0\,
      I4 => counterY_reg(6),
      I5 => counterY_reg(8),
      O => A(6)
    );
\r[1]_INST_0_i_18\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0007"
    )
        port map (
      I0 => counterY_reg(7),
      I1 => counterY_reg(6),
      I2 => counterY_reg(9),
      I3 => counterY_reg(8),
      O => \r[1]_INST_0_i_18_n_0\
    );
\r[1]_INST_0_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => counterX_reg(4),
      I1 => counterX_reg(6),
      I2 => counterX_reg(5),
      I3 => counterX_reg(7),
      I4 => counterX_reg(8),
      I5 => \r[1]_INST_0_i_26_n_0\,
      O => \r[1]_INST_0_i_19_n_0\
    );
\r[1]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000002A"
    )
        port map (
      I0 => \r[1]_INST_0_i_5_n_0\,
      I1 => counterY_reg(6),
      I2 => \r[1]_INST_0_i_7_n_0\,
      I3 => \r[1]_INST_0_i_8_n_0\,
      I4 => \r[1]_INST_0_i_9_n_0\,
      O => \r[1]_INST_0_i_2_n_0\
    );
\r[1]_INST_0_i_20\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => rgb45_out,
      I1 => rgb67_out,
      I2 => rgb78_out,
      I3 => rgb1415_out,
      O => \r[1]_INST_0_i_20_n_0\
    );
\r[1]_INST_0_i_21\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => rgb34_out,
      I1 => rgb1617_out,
      I2 => rgb56_out,
      I3 => rgb1314_out,
      O => \r[1]_INST_0_i_21_n_0\
    );
\r[1]_INST_0_i_22\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => rgb89_out,
      I1 => rgb1213_out,
      I2 => rgb910_out,
      I3 => rgb1516_out,
      O => \r[1]_INST_0_i_22_n_0\
    );
\r[1]_INST_0_i_23\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1112_out,
      CO(2) => \r[1]_INST_0_i_23_n_1\,
      CO(1) => \r[1]_INST_0_i_23_n_2\,
      CO(0) => \r[1]_INST_0_i_23_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_23_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_6_0\(2 downto 0),
      S(0) => \r[1]_INST_0_i_60_n_0\
    );
\r[1]_INST_0_i_24\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1618_out,
      CO(2) => \r[1]_INST_0_i_24_n_1\,
      CO(1) => \r[1]_INST_0_i_24_n_2\,
      CO(0) => \r[1]_INST_0_i_24_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_24_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_6_2\(2 downto 0),
      S(0) => \r[1]_INST_0_i_64_n_0\
    );
\r[1]_INST_0_i_25\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1011_out,
      CO(2) => \r[1]_INST_0_i_25_n_1\,
      CO(1) => \r[1]_INST_0_i_25_n_2\,
      CO(0) => \r[1]_INST_0_i_25_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_25_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_6_1\(2 downto 0),
      S(0) => \r[1]_INST_0_i_68_n_0\
    );
\r[1]_INST_0_i_26\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => counterX_reg(1),
      I1 => counterX_reg(0),
      I2 => counterX_reg(3),
      I3 => counterX_reg(2),
      O => \r[1]_INST_0_i_26_n_0\
    );
\r[1]_INST_0_i_27\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000FFFE"
    )
        port map (
      I0 => \r[1]_INST_0_i_69_n_0\,
      I1 => rgb2,
      I2 => rgb4,
      I3 => rgb40_out,
      I4 => \r[1]_INST_0_i_73_n_0\,
      I5 => counterX_reg(9),
      O => \r[1]_INST_0_i_27_n_0\
    );
\r[1]_INST_0_i_28\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => counterX_reg(4),
      I1 => counterX_reg(5),
      O => \r[1]_INST_0_i_28_n_0\
    );
\r[1]_INST_0_i_29\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => counterX_reg(7),
      I1 => counterX_reg(8),
      O => \r[1]_INST_0_i_29_n_0\
    );
\r[1]_INST_0_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \r[1]_INST_0_i_10_n_0\,
      CO(3) => \r[1]_INST_0_i_3_n_0\,
      CO(2) => \r[1]_INST_0_i_3_n_1\,
      CO(1) => \r[1]_INST_0_i_3_n_2\,
      CO(0) => \r[1]_INST_0_i_3_n_3\,
      CYINIT => '0',
      DI(3) => C(9),
      DI(2) => C(9),
      DI(1) => C(9),
      DI(0) => cur_blk_index0(5),
      O(3) => cur_blk_index(0),
      O(2 downto 0) => \^counterx_reg[7]_0\(4 downto 2),
      S(3) => \r[1]_INST_0_i_12_n_0\,
      S(2) => \r[1]_INST_0_i_13_n_0\,
      S(1) => \r[1]_INST_0_i_14_n_0\,
      S(0) => \r[1]_INST_0_i_15_n_0\
    );
\r[1]_INST_0_i_30\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFF8"
    )
        port map (
      I0 => counterY_reg(1),
      I1 => counterY_reg(0),
      I2 => counterY_reg(4),
      I3 => counterY_reg(2),
      I4 => counterY_reg(3),
      O => \r[1]_INST_0_i_30_n_0\
    );
\r[1]_INST_0_i_31\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"15555555EAAAAAAA"
    )
        port map (
      I0 => counterX_reg(4),
      I1 => counterX_reg(3),
      I2 => counterX_reg(0),
      I3 => counterX_reg(2),
      I4 => counterX_reg(1),
      I5 => counterX_reg(5),
      O => \r[1]_INST_0_i_31_n_0\
    );
\r[1]_INST_0_i_32\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFF40004000BFFF"
    )
        port map (
      I0 => \r[0]_INST_0_i_8_n_0\,
      I1 => counterX_reg(5),
      I2 => counterX_reg(6),
      I3 => counterX_reg(7),
      I4 => counterX_reg(8),
      I5 => cur_blk_index0(4),
      O => \r[1]_INST_0_i_32_n_0\
    );
\r[1]_INST_0_i_33\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"955595956AAA6A6A"
    )
        port map (
      I0 => counterX_reg(7),
      I1 => counterX_reg(6),
      I2 => counterX_reg(5),
      I3 => counterX_reg(4),
      I4 => \g[3]_INST_0_i_5_n_0\,
      I5 => cur_blk_index0(3),
      O => \r[1]_INST_0_i_33_n_0\
    );
\r[1]_INST_0_i_34\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"95996A66"
    )
        port map (
      I0 => counterX_reg(6),
      I1 => counterX_reg(5),
      I2 => counterX_reg(4),
      I3 => \g[3]_INST_0_i_5_n_0\,
      I4 => cur_blk_index0(2),
      O => \r[1]_INST_0_i_34_n_0\
    );
\r[1]_INST_0_i_35\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA55AA55AA55956A"
    )
        port map (
      I0 => \r[1]_INST_0_i_31_n_0\,
      I1 => counterY_reg(0),
      I2 => counterY_reg(1),
      I3 => counterY_reg(4),
      I4 => counterY_reg(2),
      I5 => counterY_reg(3),
      O => \r[1]_INST_0_i_35_n_0\
    );
\r[1]_INST_0_i_36\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEAA0155"
    )
        port map (
      I0 => counterY_reg(7),
      I1 => counterY_reg(5),
      I2 => \r[0]_INST_0_i_6_n_0\,
      I3 => counterY_reg(6),
      I4 => counterY_reg(8),
      O => A(4)
    );
\r[1]_INST_0_i_37\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A955"
    )
        port map (
      I0 => counterY_reg(7),
      I1 => counterY_reg(5),
      I2 => \r[0]_INST_0_i_6_n_0\,
      I3 => counterY_reg(6),
      O => \r[1]_INST_0_i_37_n_0\
    );
\r[1]_INST_0_i_38\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00007F0FFFFF80F0"
    )
        port map (
      I0 => counterY_reg(0),
      I1 => counterY_reg(1),
      I2 => counterY_reg(4),
      I3 => \r[0]_INST_0_i_7_n_0\,
      I4 => counterY_reg(5),
      I5 => counterY_reg(6),
      O => \r[1]_INST_0_i_38_n_0\
    );
\r[1]_INST_0_i_39\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A6A6A669"
    )
        port map (
      I0 => counterY_reg(8),
      I1 => counterY_reg(7),
      I2 => counterY_reg(6),
      I3 => counterY_reg(5),
      I4 => \r[0]_INST_0_i_6_n_0\,
      O => \r[1]_INST_0_i_39_n_0\
    );
\r[1]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFDDB7FFFBDFD"
    )
        port map (
      I0 => \^counterx_reg[7]_0\(2),
      I1 => \^counterx_reg[7]_0\(1),
      I2 => \cur_blk_index__0\(2),
      I3 => \cur_blk_index__0\(1),
      I4 => \counterX[4]_i_1_n_0\,
      I5 => \^counterx_reg[7]_0\(0),
      O => \r[1]_INST_0_i_4_n_0\
    );
\r[1]_INST_0_i_40\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"699C"
    )
        port map (
      I0 => counterY_reg(6),
      I1 => counterY_reg(7),
      I2 => \r[0]_INST_0_i_6_n_0\,
      I3 => counterY_reg(5),
      O => \r[1]_INST_0_i_40_n_0\
    );
\r[1]_INST_0_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5999966655559999"
    )
        port map (
      I0 => counterY_reg(6),
      I1 => counterY_reg(5),
      I2 => counterY_reg(0),
      I3 => counterY_reg(1),
      I4 => counterY_reg(4),
      I5 => \r[0]_INST_0_i_7_n_0\,
      O => \r[1]_INST_0_i_41_n_0\
    );
\r[1]_INST_0_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA55A955A955A955"
    )
        port map (
      I0 => counterY_reg(5),
      I1 => counterY_reg(3),
      I2 => counterY_reg(2),
      I3 => counterY_reg(4),
      I4 => counterY_reg(1),
      I5 => counterY_reg(0),
      O => \r[1]_INST_0_i_42_n_0\
    );
\r[1]_INST_0_i_43\: unisim.vcomponents.CARRY4
     port map (
      CI => \r[1]_INST_0_i_11_n_0\,
      CO(3) => cur_blk_index0(9),
      CO(2) => \NLW_r[1]_INST_0_i_43_CO_UNCONNECTED\(2),
      CO(1) => \r[1]_INST_0_i_43_n_2\,
      CO(0) => \r[1]_INST_0_i_43_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => A(6),
      DI(1 downto 0) => A(6 downto 5),
      O(3) => \NLW_r[1]_INST_0_i_43_O_UNCONNECTED\(3),
      O(2 downto 0) => cur_blk_index0(8 downto 6),
      S(3) => '1',
      S(2) => \r[1]_INST_0_i_75_n_0\,
      S(1) => \r[1]_INST_0_i_76_n_0\,
      S(0) => \r[1]_INST_0_i_77_n_0\
    );
\r[1]_INST_0_i_44\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D0000000"
    )
        port map (
      I0 => \g[3]_INST_0_i_5_n_0\,
      I1 => counterX_reg(4),
      I2 => counterX_reg(5),
      I3 => counterX_reg(6),
      I4 => counterX_reg(7),
      O => \r[1]_INST_0_i_44_n_0\
    );
\r[1]_INST_0_i_45\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb45_out,
      CO(2) => \r[1]_INST_0_i_45_n_1\,
      CO(1) => \r[1]_INST_0_i_45_n_2\,
      CO(0) => \r[1]_INST_0_i_45_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_45_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_20_3\(2 downto 0),
      S(0) => \r[1]_INST_0_i_81_n_0\
    );
\r[1]_INST_0_i_46\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb67_out,
      CO(2) => \r[1]_INST_0_i_46_n_1\,
      CO(1) => \r[1]_INST_0_i_46_n_2\,
      CO(0) => \r[1]_INST_0_i_46_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_46_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_20_2\(2 downto 0),
      S(0) => \r[1]_INST_0_i_85_n_0\
    );
\r[1]_INST_0_i_47\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb78_out,
      CO(2) => \r[1]_INST_0_i_47_n_1\,
      CO(1) => \r[1]_INST_0_i_47_n_2\,
      CO(0) => \r[1]_INST_0_i_47_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_47_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_20_1\(2 downto 0),
      S(0) => \r[1]_INST_0_i_89_n_0\
    );
\r[1]_INST_0_i_48\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1415_out,
      CO(2) => \r[1]_INST_0_i_48_n_1\,
      CO(1) => \r[1]_INST_0_i_48_n_2\,
      CO(0) => \r[1]_INST_0_i_48_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_48_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_20_0\(2 downto 0),
      S(0) => \r[1]_INST_0_i_93_n_0\
    );
\r[1]_INST_0_i_49\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb34_out,
      CO(2) => \r[1]_INST_0_i_49_n_1\,
      CO(1) => \r[1]_INST_0_i_49_n_2\,
      CO(0) => \r[1]_INST_0_i_49_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_49_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_21_3\(2 downto 0),
      S(0) => \r[1]_INST_0_i_97_n_0\
    );
\r[1]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFBFFFF"
    )
        port map (
      I0 => A(6),
      I1 => \r[0]_INST_0_i_4_n_0\,
      I2 => \r[1]_INST_0_i_7_n_0\,
      I3 => counterX_reg(9),
      I4 => \r[1]_INST_0_i_18_n_0\,
      I5 => \r[1]_INST_0_i_19_n_0\,
      O => \r[1]_INST_0_i_5_n_0\
    );
\r[1]_INST_0_i_50\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1617_out,
      CO(2) => \r[1]_INST_0_i_50_n_1\,
      CO(1) => \r[1]_INST_0_i_50_n_2\,
      CO(0) => \r[1]_INST_0_i_50_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_50_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_21_0\(2 downto 0),
      S(0) => \r[1]_INST_0_i_101_n_0\
    );
\r[1]_INST_0_i_51\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb56_out,
      CO(2) => \r[1]_INST_0_i_51_n_1\,
      CO(1) => \r[1]_INST_0_i_51_n_2\,
      CO(0) => \r[1]_INST_0_i_51_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_51_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_21_2\(2 downto 0),
      S(0) => \r[1]_INST_0_i_105_n_0\
    );
\r[1]_INST_0_i_52\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1314_out,
      CO(2) => \r[1]_INST_0_i_52_n_1\,
      CO(1) => \r[1]_INST_0_i_52_n_2\,
      CO(0) => \r[1]_INST_0_i_52_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_52_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_21_1\(2 downto 0),
      S(0) => \r[1]_INST_0_i_109_n_0\
    );
\r[1]_INST_0_i_53\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb89_out,
      CO(2) => \r[1]_INST_0_i_53_n_1\,
      CO(1) => \r[1]_INST_0_i_53_n_2\,
      CO(0) => \r[1]_INST_0_i_53_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_53_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_22_3\(2 downto 0),
      S(0) => \r[1]_INST_0_i_113_n_0\
    );
\r[1]_INST_0_i_54\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1213_out,
      CO(2) => \r[1]_INST_0_i_54_n_1\,
      CO(1) => \r[1]_INST_0_i_54_n_2\,
      CO(0) => \r[1]_INST_0_i_54_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_54_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_22_1\(2 downto 0),
      S(0) => \r[1]_INST_0_i_117_n_0\
    );
\r[1]_INST_0_i_55\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb910_out,
      CO(2) => \r[1]_INST_0_i_55_n_1\,
      CO(1) => \r[1]_INST_0_i_55_n_2\,
      CO(0) => \r[1]_INST_0_i_55_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_55_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_22_2\(2 downto 0),
      S(0) => \r[1]_INST_0_i_121_n_0\
    );
\r[1]_INST_0_i_56\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1516_out,
      CO(2) => \r[1]_INST_0_i_56_n_1\,
      CO(1) => \r[1]_INST_0_i_56_n_2\,
      CO(0) => \r[1]_INST_0_i_56_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_56_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_22_0\(2 downto 0),
      S(0) => \r[1]_INST_0_i_125_n_0\
    );
\r[1]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000004"
    )
        port map (
      I0 => \r[1]_INST_0_i_20_n_0\,
      I1 => \r[1]_INST_0_i_21_n_0\,
      I2 => \r[1]_INST_0_i_22_n_0\,
      I3 => rgb1112_out,
      I4 => rgb1618_out,
      I5 => rgb1011_out,
      O => \r[1]_INST_0_i_6_n_0\
    );
\r[1]_INST_0_i_60\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \r[1]_INST_0_i_60_n_0\
    );
\r[1]_INST_0_i_64\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_64_n_0\
    );
\r[1]_INST_0_i_68\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \r[1]_INST_0_i_68_n_0\
    );
\r[1]_INST_0_i_69\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \r[1]_INST_0_i_69_n_0\,
      CO(2) => \r[1]_INST_0_i_69_n_1\,
      CO(1) => \r[1]_INST_0_i_69_n_2\,
      CO(0) => \r[1]_INST_0_i_69_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_69_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_27_1\(2 downto 0),
      S(0) => \r[1]_INST_0_i_130_n_0\
    );
\r[1]_INST_0_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88888880"
    )
        port map (
      I0 => counterY_reg(7),
      I1 => counterY_reg(5),
      I2 => counterY_reg(4),
      I3 => counterY_reg(2),
      I4 => counterY_reg(3),
      O => \r[1]_INST_0_i_7_n_0\
    );
\r[1]_INST_0_i_70\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb2,
      CO(2) => \r[1]_INST_0_i_70_n_1\,
      CO(1) => \r[1]_INST_0_i_70_n_2\,
      CO(0) => \r[1]_INST_0_i_70_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_70_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_27_2\(2 downto 0),
      S(0) => \r[1]_INST_0_i_134_n_0\
    );
\r[1]_INST_0_i_71\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb4,
      CO(2) => \r[1]_INST_0_i_71_n_1\,
      CO(1) => \r[1]_INST_0_i_71_n_2\,
      CO(0) => \r[1]_INST_0_i_71_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_71_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_27_0\(2 downto 0),
      S(0) => \r[1]_INST_0_i_138_n_0\
    );
\r[1]_INST_0_i_72\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb40_out,
      CO(2) => \r[1]_INST_0_i_72_n_1\,
      CO(1) => \r[1]_INST_0_i_72_n_2\,
      CO(0) => \r[1]_INST_0_i_72_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_72_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_27_3\(2 downto 0),
      S(0) => \r[1]_INST_0_i_142_n_0\
    );
\r[1]_INST_0_i_73\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => counterY_reg(8),
      I1 => counterY_reg(9),
      O => \r[1]_INST_0_i_73_n_0\
    );
\r[1]_INST_0_i_74\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEEEA00001115"
    )
        port map (
      I0 => counterY_reg(8),
      I1 => counterY_reg(6),
      I2 => \r[0]_INST_0_i_6_n_0\,
      I3 => counterY_reg(5),
      I4 => counterY_reg(7),
      I5 => counterY_reg(9),
      O => A(5)
    );
\r[1]_INST_0_i_75\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAA88888"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(7),
      I2 => counterY_reg(5),
      I3 => \r[0]_INST_0_i_6_n_0\,
      I4 => counterY_reg(6),
      I5 => counterY_reg(8),
      O => \r[1]_INST_0_i_75_n_0\
    );
\r[1]_INST_0_i_76\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCCCCCC2C2C222"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(8),
      I2 => counterY_reg(6),
      I3 => \r[0]_INST_0_i_6_n_0\,
      I4 => counterY_reg(5),
      I5 => counterY_reg(7),
      O => \r[1]_INST_0_i_76_n_0\
    );
\r[1]_INST_0_i_77\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5A5A5AAAA6A6A666"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(8),
      I2 => counterY_reg(6),
      I3 => \r[0]_INST_0_i_6_n_0\,
      I4 => counterY_reg(5),
      I5 => counterY_reg(7),
      O => \r[1]_INST_0_i_77_n_0\
    );
\r[1]_INST_0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F3733333F37FFFFF"
    )
        port map (
      I0 => \r[1]_INST_0_i_26_n_0\,
      I1 => \r[1]_INST_0_i_27_n_0\,
      I2 => counterX_reg(6),
      I3 => \r[1]_INST_0_i_28_n_0\,
      I4 => \r[1]_INST_0_i_29_n_0\,
      I5 => counterX_reg(9),
      O => \r[1]_INST_0_i_8_n_0\
    );
\r[1]_INST_0_i_81\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_81_n_0\
    );
\r[1]_INST_0_i_85\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \r[1]_INST_0_i_85_n_0\
    );
\r[1]_INST_0_i_89\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(2),
      O => \r[1]_INST_0_i_89_n_0\
    );
\r[1]_INST_0_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001111101011111"
    )
        port map (
      I0 => counterY_reg(8),
      I1 => counterY_reg(9),
      I2 => counterY_reg(6),
      I3 => counterY_reg(5),
      I4 => counterY_reg(7),
      I5 => \r[1]_INST_0_i_30_n_0\,
      O => \r[1]_INST_0_i_9_n_0\
    );
\r[1]_INST_0_i_93\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_93_n_0\
    );
\r[1]_INST_0_i_97\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \counterX[4]_i_1_n_0\,
      I2 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_97_n_0\
    );
v_enabled_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \counterX[9]_i_1_n_0\,
      Q => v_enabled,
      R => '0'
    );
vs_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => counterY_reg(2),
      I1 => counterY_reg(3),
      I2 => counterY_reg(8),
      I3 => counterY_reg(9),
      I4 => vs_INST_0_i_1_n_0,
      I5 => counterY_reg(1),
      O => vs
    );
vs_INST_0_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => counterY_reg(7),
      I1 => counterY_reg(5),
      I2 => counterY_reg(6),
      I3 => counterY_reg(4),
      O => vs_INST_0_i_1_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_tetris is
  port (
    sw_rst_o : out STD_LOGIC;
    sw_pause_o : out STD_LOGIC;
    g : out STD_LOGIC_VECTOR ( 2 downto 0 );
    b : out STD_LOGIC_VECTOR ( 1 downto 0 );
    r : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \counterX_reg[7]\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    hs : out STD_LOGIC;
    vs : out STD_LOGIC;
    cur_blk_index : out STD_LOGIC_VECTOR ( 1 downto 0 );
    right_o : out STD_LOGIC;
    down_o : out STD_LOGIC;
    left_o : out STD_LOGIC;
    rotate_o : out STD_LOGIC;
    drop_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    S : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_8\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_11\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_12\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_10\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_12_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_11_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_10_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_12_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_8_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_10_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_11_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_8_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_10_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_12_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_21\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_22\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_20\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_21_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_22_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_6\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_6_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_22_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_22_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_20_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_20_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_21_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_20_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_21_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_6_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_27\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_27_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_27_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_27_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    btn_down : in STD_LOGIC;
    btn_left : in STD_LOGIC;
    btn_right : in STD_LOGIC;
    btn_rotate : in STD_LOGIC;
    btn_drop : in STD_LOGIC;
    sw_rst : in STD_LOGIC;
    sw_pause : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_tetris;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_tetris is
begin
debounce_down: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce
     port map (
      btn_down => btn_down,
      down_o => down_o,
      pixel_clk => pixel_clk
    );
debounce_drop: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_0
     port map (
      btn_drop => btn_drop,
      drop_o => drop_o,
      pixel_clk => pixel_clk
    );
debounce_left: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_1
     port map (
      btn_left => btn_left,
      left_o => left_o,
      pixel_clk => pixel_clk
    );
debounce_right: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_2
     port map (
      btn_right => btn_right,
      pixel_clk => pixel_clk,
      right_o => right_o
    );
debounce_rotate: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_3
     port map (
      btn_rotate => btn_rotate,
      pixel_clk => pixel_clk,
      rotate_o => rotate_o
    );
debounce_sw_pause: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_4
     port map (
      pixel_clk => pixel_clk,
      sw_pause => sw_pause,
      sw_pause_o => sw_pause_o
    );
debounce_sw_rst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_5
     port map (
      pixel_clk => pixel_clk,
      sw_rst => sw_rst,
      sw_rst_o => sw_rst_o
    );
vga_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga
     port map (
      S(2 downto 0) => S(2 downto 0),
      b(1 downto 0) => b(1 downto 0),
      \counterX_reg[7]_0\(4 downto 0) => \counterX_reg[7]\(4 downto 0),
      cur_blk_index(1 downto 0) => cur_blk_index(1 downto 0),
      g(2 downto 0) => g(2 downto 0),
      \g[3]_INST_0_i_10_0\(2 downto 0) => \g[3]_INST_0_i_10\(2 downto 0),
      \g[3]_INST_0_i_10_1\(2 downto 0) => \g[3]_INST_0_i_10_0\(2 downto 0),
      \g[3]_INST_0_i_10_2\(2 downto 0) => \g[3]_INST_0_i_10_1\(2 downto 0),
      \g[3]_INST_0_i_10_3\(2 downto 0) => \g[3]_INST_0_i_10_2\(2 downto 0),
      \g[3]_INST_0_i_11_0\(2 downto 0) => \g[3]_INST_0_i_11\(2 downto 0),
      \g[3]_INST_0_i_11_1\(2 downto 0) => \g[3]_INST_0_i_11_0\(2 downto 0),
      \g[3]_INST_0_i_11_2\(2 downto 0) => \g[3]_INST_0_i_11_1\(2 downto 0),
      \g[3]_INST_0_i_12_0\(2 downto 0) => \g[3]_INST_0_i_12\(2 downto 0),
      \g[3]_INST_0_i_12_1\(2 downto 0) => \g[3]_INST_0_i_12_0\(2 downto 0),
      \g[3]_INST_0_i_12_2\(2 downto 0) => \g[3]_INST_0_i_12_1\(2 downto 0),
      \g[3]_INST_0_i_12_3\(2 downto 0) => \g[3]_INST_0_i_12_2\(2 downto 0),
      \g[3]_INST_0_i_8_0\(2 downto 0) => \g[3]_INST_0_i_8\(2 downto 0),
      \g[3]_INST_0_i_8_1\(2 downto 0) => \g[3]_INST_0_i_8_0\(2 downto 0),
      \g[3]_INST_0_i_8_2\(2 downto 0) => \g[3]_INST_0_i_8_1\(2 downto 0),
      hs => hs,
      pixel_clk => pixel_clk,
      r(1 downto 0) => r(1 downto 0),
      \r[1]_INST_0_i_20_0\(2 downto 0) => \r[1]_INST_0_i_20\(2 downto 0),
      \r[1]_INST_0_i_20_1\(2 downto 0) => \r[1]_INST_0_i_20_0\(2 downto 0),
      \r[1]_INST_0_i_20_2\(2 downto 0) => \r[1]_INST_0_i_20_1\(2 downto 0),
      \r[1]_INST_0_i_20_3\(2 downto 0) => \r[1]_INST_0_i_20_2\(2 downto 0),
      \r[1]_INST_0_i_21_0\(2 downto 0) => \r[1]_INST_0_i_21\(2 downto 0),
      \r[1]_INST_0_i_21_1\(2 downto 0) => \r[1]_INST_0_i_21_0\(2 downto 0),
      \r[1]_INST_0_i_21_2\(2 downto 0) => \r[1]_INST_0_i_21_1\(2 downto 0),
      \r[1]_INST_0_i_21_3\(2 downto 0) => \r[1]_INST_0_i_21_2\(2 downto 0),
      \r[1]_INST_0_i_22_0\(2 downto 0) => \r[1]_INST_0_i_22\(2 downto 0),
      \r[1]_INST_0_i_22_1\(2 downto 0) => \r[1]_INST_0_i_22_0\(2 downto 0),
      \r[1]_INST_0_i_22_2\(2 downto 0) => \r[1]_INST_0_i_22_1\(2 downto 0),
      \r[1]_INST_0_i_22_3\(2 downto 0) => \r[1]_INST_0_i_22_2\(2 downto 0),
      \r[1]_INST_0_i_27_0\(2 downto 0) => \r[1]_INST_0_i_27\(2 downto 0),
      \r[1]_INST_0_i_27_1\(2 downto 0) => \r[1]_INST_0_i_27_0\(2 downto 0),
      \r[1]_INST_0_i_27_2\(2 downto 0) => \r[1]_INST_0_i_27_1\(2 downto 0),
      \r[1]_INST_0_i_27_3\(2 downto 0) => \r[1]_INST_0_i_27_2\(2 downto 0),
      \r[1]_INST_0_i_6_0\(2 downto 0) => \r[1]_INST_0_i_6\(2 downto 0),
      \r[1]_INST_0_i_6_1\(2 downto 0) => \r[1]_INST_0_i_6_0\(2 downto 0),
      \r[1]_INST_0_i_6_2\(2 downto 0) => \r[1]_INST_0_i_6_1\(2 downto 0),
      vs => vs
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk : in STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_down : in STD_LOGIC;
    btn_left : in STD_LOGIC;
    btn_right : in STD_LOGIC;
    btn_rotate : in STD_LOGIC;
    btn_drop : in STD_LOGIC;
    sw_rst : in STD_LOGIC;
    sw_pause : in STD_LOGIC;
    rotate_o : out STD_LOGIC;
    down_o : out STD_LOGIC;
    left_o : out STD_LOGIC;
    right_o : out STD_LOGIC;
    drop_o : out STD_LOGIC;
    sw_rst_o : out STD_LOGIC;
    sw_pause_o : out STD_LOGIC;
    r : out STD_LOGIC_VECTOR ( 3 downto 0 );
    g : out STD_LOGIC_VECTOR ( 3 downto 0 );
    b : out STD_LOGIC_VECTOR ( 3 downto 0 );
    hs : out STD_LOGIC;
    vs : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "tetris_tetris_0_0,tetris,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "package_project";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "tetris,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \^b\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \^g\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \g[3]_INST_0_i_28_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_29_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_30_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_32_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_33_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_34_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_36_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_37_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_38_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_40_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_41_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_42_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_44_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_45_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_46_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_48_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_49_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_50_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_52_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_53_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_54_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_56_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_57_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_58_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_60_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_61_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_62_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_64_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_65_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_66_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_68_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_69_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_70_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_72_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_73_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_74_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_76_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_77_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_78_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_80_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_81_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_82_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_84_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_85_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_86_n_0\ : STD_LOGIC;
  signal \^r\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \r[1]_INST_0_i_100_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_102_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_103_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_104_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_106_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_107_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_108_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_110_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_111_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_112_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_114_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_115_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_116_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_118_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_119_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_120_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_122_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_123_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_124_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_127_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_128_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_129_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_131_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_132_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_133_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_135_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_136_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_137_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_139_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_140_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_141_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_57_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_58_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_59_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_61_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_62_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_63_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_65_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_66_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_67_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_78_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_79_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_80_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_82_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_83_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_84_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_86_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_87_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_88_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_90_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_91_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_92_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_94_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_95_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_96_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_98_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_99_n_0\ : STD_LOGIC;
  signal \vga_inst/cur_blk_index\ : STD_LOGIC_VECTOR ( 9 downto 8 );
  signal \vga_inst/cur_blk_index__0\ : STD_LOGIC_VECTOR ( 7 downto 3 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 pixel_clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME pixel_clk, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN tetris_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of pixel_clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER of pixel_clk : signal is "XIL_INTERFACENAME clk, FREQ_HZ 25173010, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of sw_rst : signal is "xilinx.com:signal:reset:1.0 sw_rst RST";
  attribute X_INTERFACE_PARAMETER of sw_rst : signal is "XIL_INTERFACENAME sw_rst, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  b(3) <= \^b\(3);
  b(2) <= \^b\(3);
  b(1) <= \^b\(1);
  b(0) <= \^b\(3);
  g(3 downto 2) <= \^g\(3 downto 2);
  g(1) <= \^g\(0);
  g(0) <= \^g\(0);
  r(3) <= \^r\(1);
  r(2) <= \^r\(1);
  r(1 downto 0) <= \^r\(1 downto 0);
\g[3]_INST_0_i_28\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_28_n_0\
    );
\g[3]_INST_0_i_29\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_29_n_0\
    );
\g[3]_INST_0_i_30\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(3),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \g[3]_INST_0_i_30_n_0\
    );
\g[3]_INST_0_i_32\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_32_n_0\
    );
\g[3]_INST_0_i_33\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_33_n_0\
    );
\g[3]_INST_0_i_34\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_34_n_0\
    );
\g[3]_INST_0_i_36\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_36_n_0\
    );
\g[3]_INST_0_i_37\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_37_n_0\
    );
\g[3]_INST_0_i_38\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \g[3]_INST_0_i_38_n_0\
    );
\g[3]_INST_0_i_40\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_40_n_0\
    );
\g[3]_INST_0_i_41\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_41_n_0\
    );
\g[3]_INST_0_i_42\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(3),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \g[3]_INST_0_i_42_n_0\
    );
\g[3]_INST_0_i_44\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_44_n_0\
    );
\g[3]_INST_0_i_45\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_45_n_0\
    );
\g[3]_INST_0_i_46\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_46_n_0\
    );
\g[3]_INST_0_i_48\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_48_n_0\
    );
\g[3]_INST_0_i_49\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_49_n_0\
    );
\g[3]_INST_0_i_50\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \g[3]_INST_0_i_50_n_0\
    );
\g[3]_INST_0_i_52\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_52_n_0\
    );
\g[3]_INST_0_i_53\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_53_n_0\
    );
\g[3]_INST_0_i_54\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(3),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \g[3]_INST_0_i_54_n_0\
    );
\g[3]_INST_0_i_56\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_56_n_0\
    );
\g[3]_INST_0_i_57\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_57_n_0\
    );
\g[3]_INST_0_i_58\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_58_n_0\
    );
\g[3]_INST_0_i_60\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_60_n_0\
    );
\g[3]_INST_0_i_61\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_61_n_0\
    );
\g[3]_INST_0_i_62\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_62_n_0\
    );
\g[3]_INST_0_i_64\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_64_n_0\
    );
\g[3]_INST_0_i_65\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_65_n_0\
    );
\g[3]_INST_0_i_66\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_66_n_0\
    );
\g[3]_INST_0_i_68\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_68_n_0\
    );
\g[3]_INST_0_i_69\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_69_n_0\
    );
\g[3]_INST_0_i_70\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \g[3]_INST_0_i_70_n_0\
    );
\g[3]_INST_0_i_72\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_72_n_0\
    );
\g[3]_INST_0_i_73\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_73_n_0\
    );
\g[3]_INST_0_i_74\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(3),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \g[3]_INST_0_i_74_n_0\
    );
\g[3]_INST_0_i_76\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_76_n_0\
    );
\g[3]_INST_0_i_77\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_77_n_0\
    );
\g[3]_INST_0_i_78\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_78_n_0\
    );
\g[3]_INST_0_i_80\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_80_n_0\
    );
\g[3]_INST_0_i_81\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_81_n_0\
    );
\g[3]_INST_0_i_82\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \g[3]_INST_0_i_82_n_0\
    );
\g[3]_INST_0_i_84\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_84_n_0\
    );
\g[3]_INST_0_i_85\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_85_n_0\
    );
\g[3]_INST_0_i_86\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_86_n_0\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_tetris
     port map (
      S(2) => \g[3]_INST_0_i_68_n_0\,
      S(1) => \g[3]_INST_0_i_69_n_0\,
      S(0) => \g[3]_INST_0_i_70_n_0\,
      b(1) => \^b\(3),
      b(0) => \^b\(1),
      btn_down => btn_down,
      btn_drop => btn_drop,
      btn_left => btn_left,
      btn_right => btn_right,
      btn_rotate => btn_rotate,
      \counterX_reg[7]\(4 downto 0) => \vga_inst/cur_blk_index__0\(7 downto 3),
      cur_blk_index(1 downto 0) => \vga_inst/cur_blk_index\(9 downto 8),
      down_o => down_o,
      drop_o => drop_o,
      g(2 downto 1) => \^g\(3 downto 2),
      g(0) => \^g\(0),
      \g[3]_INST_0_i_10\(2) => \g[3]_INST_0_i_44_n_0\,
      \g[3]_INST_0_i_10\(1) => \g[3]_INST_0_i_45_n_0\,
      \g[3]_INST_0_i_10\(0) => \g[3]_INST_0_i_46_n_0\,
      \g[3]_INST_0_i_10_0\(2) => \g[3]_INST_0_i_52_n_0\,
      \g[3]_INST_0_i_10_0\(1) => \g[3]_INST_0_i_53_n_0\,
      \g[3]_INST_0_i_10_0\(0) => \g[3]_INST_0_i_54_n_0\,
      \g[3]_INST_0_i_10_1\(2) => \g[3]_INST_0_i_40_n_0\,
      \g[3]_INST_0_i_10_1\(1) => \g[3]_INST_0_i_41_n_0\,
      \g[3]_INST_0_i_10_1\(0) => \g[3]_INST_0_i_42_n_0\,
      \g[3]_INST_0_i_10_2\(2) => \g[3]_INST_0_i_48_n_0\,
      \g[3]_INST_0_i_10_2\(1) => \g[3]_INST_0_i_49_n_0\,
      \g[3]_INST_0_i_10_2\(0) => \g[3]_INST_0_i_50_n_0\,
      \g[3]_INST_0_i_11\(2) => \g[3]_INST_0_i_60_n_0\,
      \g[3]_INST_0_i_11\(1) => \g[3]_INST_0_i_61_n_0\,
      \g[3]_INST_0_i_11\(0) => \g[3]_INST_0_i_62_n_0\,
      \g[3]_INST_0_i_11_0\(2) => \g[3]_INST_0_i_56_n_0\,
      \g[3]_INST_0_i_11_0\(1) => \g[3]_INST_0_i_57_n_0\,
      \g[3]_INST_0_i_11_0\(0) => \g[3]_INST_0_i_58_n_0\,
      \g[3]_INST_0_i_11_1\(2) => \g[3]_INST_0_i_64_n_0\,
      \g[3]_INST_0_i_11_1\(1) => \g[3]_INST_0_i_65_n_0\,
      \g[3]_INST_0_i_11_1\(0) => \g[3]_INST_0_i_66_n_0\,
      \g[3]_INST_0_i_12\(2) => \g[3]_INST_0_i_76_n_0\,
      \g[3]_INST_0_i_12\(1) => \g[3]_INST_0_i_77_n_0\,
      \g[3]_INST_0_i_12\(0) => \g[3]_INST_0_i_78_n_0\,
      \g[3]_INST_0_i_12_0\(2) => \g[3]_INST_0_i_84_n_0\,
      \g[3]_INST_0_i_12_0\(1) => \g[3]_INST_0_i_85_n_0\,
      \g[3]_INST_0_i_12_0\(0) => \g[3]_INST_0_i_86_n_0\,
      \g[3]_INST_0_i_12_1\(2) => \g[3]_INST_0_i_72_n_0\,
      \g[3]_INST_0_i_12_1\(1) => \g[3]_INST_0_i_73_n_0\,
      \g[3]_INST_0_i_12_1\(0) => \g[3]_INST_0_i_74_n_0\,
      \g[3]_INST_0_i_12_2\(2) => \g[3]_INST_0_i_80_n_0\,
      \g[3]_INST_0_i_12_2\(1) => \g[3]_INST_0_i_81_n_0\,
      \g[3]_INST_0_i_12_2\(0) => \g[3]_INST_0_i_82_n_0\,
      \g[3]_INST_0_i_8\(2) => \g[3]_INST_0_i_36_n_0\,
      \g[3]_INST_0_i_8\(1) => \g[3]_INST_0_i_37_n_0\,
      \g[3]_INST_0_i_8\(0) => \g[3]_INST_0_i_38_n_0\,
      \g[3]_INST_0_i_8_0\(2) => \g[3]_INST_0_i_28_n_0\,
      \g[3]_INST_0_i_8_0\(1) => \g[3]_INST_0_i_29_n_0\,
      \g[3]_INST_0_i_8_0\(0) => \g[3]_INST_0_i_30_n_0\,
      \g[3]_INST_0_i_8_1\(2) => \g[3]_INST_0_i_32_n_0\,
      \g[3]_INST_0_i_8_1\(1) => \g[3]_INST_0_i_33_n_0\,
      \g[3]_INST_0_i_8_1\(0) => \g[3]_INST_0_i_34_n_0\,
      hs => hs,
      left_o => left_o,
      pixel_clk => pixel_clk,
      r(1 downto 0) => \^r\(1 downto 0),
      \r[1]_INST_0_i_20\(2) => \r[1]_INST_0_i_90_n_0\,
      \r[1]_INST_0_i_20\(1) => \r[1]_INST_0_i_91_n_0\,
      \r[1]_INST_0_i_20\(0) => \r[1]_INST_0_i_92_n_0\,
      \r[1]_INST_0_i_20_0\(2) => \r[1]_INST_0_i_86_n_0\,
      \r[1]_INST_0_i_20_0\(1) => \r[1]_INST_0_i_87_n_0\,
      \r[1]_INST_0_i_20_0\(0) => \r[1]_INST_0_i_88_n_0\,
      \r[1]_INST_0_i_20_1\(2) => \r[1]_INST_0_i_82_n_0\,
      \r[1]_INST_0_i_20_1\(1) => \r[1]_INST_0_i_83_n_0\,
      \r[1]_INST_0_i_20_1\(0) => \r[1]_INST_0_i_84_n_0\,
      \r[1]_INST_0_i_20_2\(2) => \r[1]_INST_0_i_78_n_0\,
      \r[1]_INST_0_i_20_2\(1) => \r[1]_INST_0_i_79_n_0\,
      \r[1]_INST_0_i_20_2\(0) => \r[1]_INST_0_i_80_n_0\,
      \r[1]_INST_0_i_21\(2) => \r[1]_INST_0_i_98_n_0\,
      \r[1]_INST_0_i_21\(1) => \r[1]_INST_0_i_99_n_0\,
      \r[1]_INST_0_i_21\(0) => \r[1]_INST_0_i_100_n_0\,
      \r[1]_INST_0_i_21_0\(2) => \r[1]_INST_0_i_106_n_0\,
      \r[1]_INST_0_i_21_0\(1) => \r[1]_INST_0_i_107_n_0\,
      \r[1]_INST_0_i_21_0\(0) => \r[1]_INST_0_i_108_n_0\,
      \r[1]_INST_0_i_21_1\(2) => \r[1]_INST_0_i_102_n_0\,
      \r[1]_INST_0_i_21_1\(1) => \r[1]_INST_0_i_103_n_0\,
      \r[1]_INST_0_i_21_1\(0) => \r[1]_INST_0_i_104_n_0\,
      \r[1]_INST_0_i_21_2\(2) => \r[1]_INST_0_i_94_n_0\,
      \r[1]_INST_0_i_21_2\(1) => \r[1]_INST_0_i_95_n_0\,
      \r[1]_INST_0_i_21_2\(0) => \r[1]_INST_0_i_96_n_0\,
      \r[1]_INST_0_i_22\(2) => \r[1]_INST_0_i_122_n_0\,
      \r[1]_INST_0_i_22\(1) => \r[1]_INST_0_i_123_n_0\,
      \r[1]_INST_0_i_22\(0) => \r[1]_INST_0_i_124_n_0\,
      \r[1]_INST_0_i_22_0\(2) => \r[1]_INST_0_i_114_n_0\,
      \r[1]_INST_0_i_22_0\(1) => \r[1]_INST_0_i_115_n_0\,
      \r[1]_INST_0_i_22_0\(0) => \r[1]_INST_0_i_116_n_0\,
      \r[1]_INST_0_i_22_1\(2) => \r[1]_INST_0_i_118_n_0\,
      \r[1]_INST_0_i_22_1\(1) => \r[1]_INST_0_i_119_n_0\,
      \r[1]_INST_0_i_22_1\(0) => \r[1]_INST_0_i_120_n_0\,
      \r[1]_INST_0_i_22_2\(2) => \r[1]_INST_0_i_110_n_0\,
      \r[1]_INST_0_i_22_2\(1) => \r[1]_INST_0_i_111_n_0\,
      \r[1]_INST_0_i_22_2\(0) => \r[1]_INST_0_i_112_n_0\,
      \r[1]_INST_0_i_27\(2) => \r[1]_INST_0_i_135_n_0\,
      \r[1]_INST_0_i_27\(1) => \r[1]_INST_0_i_136_n_0\,
      \r[1]_INST_0_i_27\(0) => \r[1]_INST_0_i_137_n_0\,
      \r[1]_INST_0_i_27_0\(2) => \r[1]_INST_0_i_127_n_0\,
      \r[1]_INST_0_i_27_0\(1) => \r[1]_INST_0_i_128_n_0\,
      \r[1]_INST_0_i_27_0\(0) => \r[1]_INST_0_i_129_n_0\,
      \r[1]_INST_0_i_27_1\(2) => \r[1]_INST_0_i_131_n_0\,
      \r[1]_INST_0_i_27_1\(1) => \r[1]_INST_0_i_132_n_0\,
      \r[1]_INST_0_i_27_1\(0) => \r[1]_INST_0_i_133_n_0\,
      \r[1]_INST_0_i_27_2\(2) => \r[1]_INST_0_i_139_n_0\,
      \r[1]_INST_0_i_27_2\(1) => \r[1]_INST_0_i_140_n_0\,
      \r[1]_INST_0_i_27_2\(0) => \r[1]_INST_0_i_141_n_0\,
      \r[1]_INST_0_i_6\(2) => \r[1]_INST_0_i_57_n_0\,
      \r[1]_INST_0_i_6\(1) => \r[1]_INST_0_i_58_n_0\,
      \r[1]_INST_0_i_6\(0) => \r[1]_INST_0_i_59_n_0\,
      \r[1]_INST_0_i_6_0\(2) => \r[1]_INST_0_i_65_n_0\,
      \r[1]_INST_0_i_6_0\(1) => \r[1]_INST_0_i_66_n_0\,
      \r[1]_INST_0_i_6_0\(0) => \r[1]_INST_0_i_67_n_0\,
      \r[1]_INST_0_i_6_1\(2) => \r[1]_INST_0_i_61_n_0\,
      \r[1]_INST_0_i_6_1\(1) => \r[1]_INST_0_i_62_n_0\,
      \r[1]_INST_0_i_6_1\(0) => \r[1]_INST_0_i_63_n_0\,
      right_o => right_o,
      rotate_o => rotate_o,
      sw_pause => sw_pause,
      sw_pause_o => sw_pause_o,
      sw_rst => sw_rst,
      sw_rst_o => sw_rst_o,
      vs => vs
    );
\r[1]_INST_0_i_100\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \r[1]_INST_0_i_100_n_0\
    );
\r[1]_INST_0_i_102\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_102_n_0\
    );
\r[1]_INST_0_i_103\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_103_n_0\
    );
\r[1]_INST_0_i_104\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_104_n_0\
    );
\r[1]_INST_0_i_106\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_106_n_0\
    );
\r[1]_INST_0_i_107\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_107_n_0\
    );
\r[1]_INST_0_i_108\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(3),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \r[1]_INST_0_i_108_n_0\
    );
\r[1]_INST_0_i_110\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_110_n_0\
    );
\r[1]_INST_0_i_111\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_111_n_0\
    );
\r[1]_INST_0_i_112\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_112_n_0\
    );
\r[1]_INST_0_i_114\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_114_n_0\
    );
\r[1]_INST_0_i_115\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_115_n_0\
    );
\r[1]_INST_0_i_116\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(3),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \r[1]_INST_0_i_116_n_0\
    );
\r[1]_INST_0_i_118\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_118_n_0\
    );
\r[1]_INST_0_i_119\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_119_n_0\
    );
\r[1]_INST_0_i_120\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_120_n_0\
    );
\r[1]_INST_0_i_122\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_122_n_0\
    );
\r[1]_INST_0_i_123\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_123_n_0\
    );
\r[1]_INST_0_i_124\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \r[1]_INST_0_i_124_n_0\
    );
\r[1]_INST_0_i_127\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_127_n_0\
    );
\r[1]_INST_0_i_128\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_128_n_0\
    );
\r[1]_INST_0_i_129\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_129_n_0\
    );
\r[1]_INST_0_i_131\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_131_n_0\
    );
\r[1]_INST_0_i_132\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_132_n_0\
    );
\r[1]_INST_0_i_133\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_133_n_0\
    );
\r[1]_INST_0_i_135\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_135_n_0\
    );
\r[1]_INST_0_i_136\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_136_n_0\
    );
\r[1]_INST_0_i_137\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_137_n_0\
    );
\r[1]_INST_0_i_139\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_139_n_0\
    );
\r[1]_INST_0_i_140\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_140_n_0\
    );
\r[1]_INST_0_i_141\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_141_n_0\
    );
\r[1]_INST_0_i_57\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_57_n_0\
    );
\r[1]_INST_0_i_58\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_58_n_0\
    );
\r[1]_INST_0_i_59\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_59_n_0\
    );
\r[1]_INST_0_i_61\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_61_n_0\
    );
\r[1]_INST_0_i_62\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_62_n_0\
    );
\r[1]_INST_0_i_63\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \r[1]_INST_0_i_63_n_0\
    );
\r[1]_INST_0_i_65\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_65_n_0\
    );
\r[1]_INST_0_i_66\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_66_n_0\
    );
\r[1]_INST_0_i_67\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_67_n_0\
    );
\r[1]_INST_0_i_78\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_78_n_0\
    );
\r[1]_INST_0_i_79\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_79_n_0\
    );
\r[1]_INST_0_i_80\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_80_n_0\
    );
\r[1]_INST_0_i_82\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_82_n_0\
    );
\r[1]_INST_0_i_83\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_83_n_0\
    );
\r[1]_INST_0_i_84\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(3),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \r[1]_INST_0_i_84_n_0\
    );
\r[1]_INST_0_i_86\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_86_n_0\
    );
\r[1]_INST_0_i_87\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_87_n_0\
    );
\r[1]_INST_0_i_88\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(3),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \r[1]_INST_0_i_88_n_0\
    );
\r[1]_INST_0_i_90\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_90_n_0\
    );
\r[1]_INST_0_i_91\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_91_n_0\
    );
\r[1]_INST_0_i_92\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_92_n_0\
    );
\r[1]_INST_0_i_94\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_94_n_0\
    );
\r[1]_INST_0_i_95\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_95_n_0\
    );
\r[1]_INST_0_i_96\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_96_n_0\
    );
\r[1]_INST_0_i_98\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_98_n_0\
    );
\r[1]_INST_0_i_99\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_99_n_0\
    );
end STRUCTURE;
