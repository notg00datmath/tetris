-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Thu Mar 19 19:47:00 2020
-- Host        : notg00datmath running 64-bit Ubuntu 18.04.4 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ tetris_tetris_0_0_sim_netlist.vhdl
-- Design      : tetris_tetris_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_GetRandomPiece is
  port (
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    pixel_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_GetRandomPiece;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_GetRandomPiece is
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \random_tetris[0]_i_1_n_0\ : STD_LOGIC;
  signal \random_tetris[1]_i_1_n_0\ : STD_LOGIC;
  signal \random_tetris[2]_i_1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \random_tetris[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \random_tetris[2]_i_1\ : label is "soft_lutpair0";
begin
  Q(2 downto 0) <= \^q\(2 downto 0);
\random_tetris[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B3"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => \random_tetris[0]_i_1_n_0\
    );
\random_tetris[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      O => \random_tetris[1]_i_1_n_0\
    );
\random_tetris[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => \random_tetris[2]_i_1_n_0\
    );
\random_tetris_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => \random_tetris[0]_i_1_n_0\,
      Q => \^q\(0),
      R => '0'
    );
\random_tetris_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => \random_tetris[1]_i_1_n_0\,
      Q => \^q\(1),
      R => '0'
    );
\random_tetris_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => \random_tetris[2]_i_1_n_0\,
      Q => \^q\(2),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce is
  port (
    down_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_down : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce is
  signal \PB_cnt[0]_i_1_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal PB_state_i_1_n_0 : STD_LOGIC;
  signal PB_sync_0 : STD_LOGIC;
  signal PB_sync_0_i_1_n_0 : STD_LOGIC;
  signal PB_sync_1 : STD_LOGIC;
  signal down_o_INST_0_i_2_n_0 : STD_LOGIC;
  signal down_o_INST_0_i_3_n_0 : STD_LOGIC;
  signal down_o_INST_0_i_4_n_0 : STD_LOGIC;
  signal p_2_in : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of PB_state_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of down_o_INST_0 : label is "soft_lutpair1";
begin
\PB_cnt[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1,
      O => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1_n_0\
    );
\PB_cnt_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1_n_0\
    );
PB_state_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => PB_state_i_1_n_0
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_state_i_1_n_0,
      Q => PB_state,
      R => '0'
    );
PB_sync_0_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => btn_down,
      O => PB_sync_0_i_1_n_0
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_i_1_n_0,
      Q => PB_sync_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0,
      Q => PB_sync_1,
      R => '0'
    );
down_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => down_o
    );
down_o_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => down_o_INST_0_i_2_n_0,
      I1 => PB_cnt_reg(2),
      I2 => PB_cnt_reg(3),
      I3 => PB_cnt_reg(4),
      I4 => PB_cnt_reg(15),
      I5 => down_o_INST_0_i_3_n_0,
      O => p_2_in
    );
down_o_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => down_o_INST_0_i_2_n_0
    );
down_o_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1_n_0\,
      I1 => down_o_INST_0_i_4_n_0,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => down_o_INST_0_i_3_n_0
    );
down_o_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => down_o_INST_0_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_0 is
  port (
    drop_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_drop : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_0 : entity is "debounce";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_0 is
  signal \PB_cnt[0]_i_1__3_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3__3_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2__3_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__3_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__3_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__3_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__3_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal \PB_state_i_1__3_n_0\ : STD_LOGIC;
  signal \PB_sync_0_i_1__3_n_0\ : STD_LOGIC;
  signal PB_sync_0_reg_n_0 : STD_LOGIC;
  signal PB_sync_1_reg_n_0 : STD_LOGIC;
  signal drop_o_INST_0_i_2_n_0 : STD_LOGIC;
  signal drop_o_INST_0_i_3_n_0 : STD_LOGIC;
  signal drop_o_INST_0_i_4_n_0 : STD_LOGIC;
  signal p_2_in : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \PB_state_i_1__3\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of drop_o_INST_0 : label is "soft_lutpair2";
begin
\PB_cnt[0]_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1_reg_n_0,
      O => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt[0]_i_3__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3__3_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__3_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[0]_i_2__3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2__3_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2__3_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2__3_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2__3_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2__3_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2__3_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2__3_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3__3_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__3_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__3_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__3_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[12]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1__3_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1__3_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1__3_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1__3_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1__3_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1__3_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1__3_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1__3_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__3_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__3_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__3_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__3_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__3_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__3_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__3_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[4]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2__3_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1__3_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1__3_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1__3_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1__3_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1__3_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1__3_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1__3_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__3_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__3_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__3_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__3_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_cnt_reg[8]_i_1__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1__3_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1__3_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1__3_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1__3_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1__3_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1__3_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1__3_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1__3_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__3_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1__3_n_0\
    );
\PB_state_i_1__3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => \PB_state_i_1__3_n_0\
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_state_i_1__3_n_0\,
      Q => PB_state,
      R => '0'
    );
\PB_sync_0_i_1__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => btn_drop,
      O => \PB_sync_0_i_1__3_n_0\
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_sync_0_i_1__3_n_0\,
      Q => PB_sync_0_reg_n_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_reg_n_0,
      Q => PB_sync_1_reg_n_0,
      R => '0'
    );
drop_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => drop_o
    );
drop_o_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => drop_o_INST_0_i_2_n_0,
      I1 => PB_cnt_reg(2),
      I2 => PB_cnt_reg(3),
      I3 => PB_cnt_reg(4),
      I4 => PB_cnt_reg(15),
      I5 => drop_o_INST_0_i_3_n_0,
      O => p_2_in
    );
drop_o_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => drop_o_INST_0_i_2_n_0
    );
drop_o_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1__3_n_0\,
      I1 => drop_o_INST_0_i_4_n_0,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => drop_o_INST_0_i_3_n_0
    );
drop_o_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => drop_o_INST_0_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_1 is
  port (
    left_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_left : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_1 : entity is "debounce";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_1;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_1 is
  signal \PB_cnt[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3__0_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__0_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal \PB_state_i_1__0_n_0\ : STD_LOGIC;
  signal \PB_sync_0_i_1__0_n_0\ : STD_LOGIC;
  signal PB_sync_0_reg_n_0 : STD_LOGIC;
  signal PB_sync_1_reg_n_0 : STD_LOGIC;
  signal left_o_INST_0_i_2_n_0 : STD_LOGIC;
  signal left_o_INST_0_i_3_n_0 : STD_LOGIC;
  signal left_o_INST_0_i_4_n_0 : STD_LOGIC;
  signal p_2_in : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \PB_state_i_1__0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of left_o_INST_0 : label is "soft_lutpair3";
begin
\PB_cnt[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1_reg_n_0,
      O => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt[0]_i_3__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3__0_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__0_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[0]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2__0_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2__0_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2__0_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2__0_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2__0_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2__0_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2__0_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3__0_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__0_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__0_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__0_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1__0_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1__0_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1__0_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1__0_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1__0_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1__0_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1__0_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1__0_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__0_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__0_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__0_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__0_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__0_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__0_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__0_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2__0_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1__0_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1__0_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1__0_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1__0_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1__0_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1__0_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1__0_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__0_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__0_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__0_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__0_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_cnt_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1__0_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1__0_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1__0_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1__0_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1__0_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1__0_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1__0_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__0_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1__0_n_0\
    );
\PB_state_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => \PB_state_i_1__0_n_0\
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_state_i_1__0_n_0\,
      Q => PB_state,
      R => '0'
    );
\PB_sync_0_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => btn_left,
      O => \PB_sync_0_i_1__0_n_0\
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_sync_0_i_1__0_n_0\,
      Q => PB_sync_0_reg_n_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_reg_n_0,
      Q => PB_sync_1_reg_n_0,
      R => '0'
    );
left_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => left_o
    );
left_o_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => left_o_INST_0_i_2_n_0,
      I1 => PB_cnt_reg(2),
      I2 => PB_cnt_reg(3),
      I3 => PB_cnt_reg(4),
      I4 => PB_cnt_reg(15),
      I5 => left_o_INST_0_i_3_n_0,
      O => p_2_in
    );
left_o_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => left_o_INST_0_i_2_n_0
    );
left_o_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1__0_n_0\,
      I1 => left_o_INST_0_i_4_n_0,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => left_o_INST_0_i_3_n_0
    );
left_o_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => left_o_INST_0_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_2 is
  port (
    right_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_right : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_2 : entity is "debounce";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_2;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_2 is
  signal \PB_cnt[0]_i_1__1_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3__1_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2__1_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__1_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__1_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__1_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__1_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal \PB_state_i_1__1_n_0\ : STD_LOGIC;
  signal \PB_sync_0_i_1__1_n_0\ : STD_LOGIC;
  signal PB_sync_0_reg_n_0 : STD_LOGIC;
  signal PB_sync_1_reg_n_0 : STD_LOGIC;
  signal p_2_in : STD_LOGIC;
  signal right_o_INST_0_i_2_n_0 : STD_LOGIC;
  signal right_o_INST_0_i_3_n_0 : STD_LOGIC;
  signal right_o_INST_0_i_4_n_0 : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \PB_state_i_1__1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of right_o_INST_0 : label is "soft_lutpair4";
begin
\PB_cnt[0]_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1_reg_n_0,
      O => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt[0]_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3__1_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__1_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[0]_i_2__1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2__1_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2__1_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2__1_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2__1_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2__1_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2__1_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2__1_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3__1_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__1_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__1_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__1_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[12]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1__1_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1__1_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1__1_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1__1_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1__1_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1__1_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1__1_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1__1_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__1_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__1_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__1_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__1_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__1_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__1_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__1_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[4]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2__1_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1__1_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1__1_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1__1_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1__1_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1__1_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1__1_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1__1_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__1_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__1_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__1_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__1_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_cnt_reg[8]_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1__1_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1__1_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1__1_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1__1_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1__1_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1__1_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1__1_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1__1_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__1_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1__1_n_0\
    );
\PB_state_i_1__1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => \PB_state_i_1__1_n_0\
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_state_i_1__1_n_0\,
      Q => PB_state,
      R => '0'
    );
\PB_sync_0_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => btn_right,
      O => \PB_sync_0_i_1__1_n_0\
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_sync_0_i_1__1_n_0\,
      Q => PB_sync_0_reg_n_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_reg_n_0,
      Q => PB_sync_1_reg_n_0,
      R => '0'
    );
right_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => right_o
    );
right_o_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => right_o_INST_0_i_2_n_0,
      I1 => PB_cnt_reg(2),
      I2 => PB_cnt_reg(3),
      I3 => PB_cnt_reg(4),
      I4 => PB_cnt_reg(15),
      I5 => right_o_INST_0_i_3_n_0,
      O => p_2_in
    );
right_o_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => right_o_INST_0_i_2_n_0
    );
right_o_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1__1_n_0\,
      I1 => right_o_INST_0_i_4_n_0,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => right_o_INST_0_i_3_n_0
    );
right_o_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => right_o_INST_0_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_3 is
  port (
    rotate_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_rotate : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_3 : entity is "debounce";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_3;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_3 is
  signal \PB_cnt[0]_i_1__2_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3__2_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2__2_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__2_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__2_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__2_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__2_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal \PB_state_i_1__2_n_0\ : STD_LOGIC;
  signal \PB_sync_0_i_1__2_n_0\ : STD_LOGIC;
  signal PB_sync_0_reg_n_0 : STD_LOGIC;
  signal PB_sync_1_reg_n_0 : STD_LOGIC;
  signal p_2_in : STD_LOGIC;
  signal rotate_o_INST_0_i_2_n_0 : STD_LOGIC;
  signal rotate_o_INST_0_i_3_n_0 : STD_LOGIC;
  signal rotate_o_INST_0_i_4_n_0 : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \PB_state_i_1__2\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of rotate_o_INST_0 : label is "soft_lutpair5";
begin
\PB_cnt[0]_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1_reg_n_0,
      O => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt[0]_i_3__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3__2_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__2_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[0]_i_2__2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2__2_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2__2_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2__2_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2__2_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2__2_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2__2_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2__2_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3__2_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__2_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__2_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__2_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[12]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1__2_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1__2_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1__2_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1__2_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1__2_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1__2_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1__2_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1__2_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__2_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__2_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__2_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__2_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__2_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__2_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__2_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[4]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2__2_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1__2_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1__2_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1__2_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1__2_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1__2_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1__2_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1__2_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__2_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__2_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__2_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__2_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_cnt_reg[8]_i_1__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1__2_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1__2_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1__2_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1__2_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1__2_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1__2_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1__2_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1__2_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__2_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1__2_n_0\
    );
\PB_state_i_1__2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => \PB_state_i_1__2_n_0\
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_state_i_1__2_n_0\,
      Q => PB_state,
      R => '0'
    );
\PB_sync_0_i_1__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => btn_rotate,
      O => \PB_sync_0_i_1__2_n_0\
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_sync_0_i_1__2_n_0\,
      Q => PB_sync_0_reg_n_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_reg_n_0,
      Q => PB_sync_1_reg_n_0,
      R => '0'
    );
rotate_o_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_2_in,
      I1 => PB_state,
      O => rotate_o
    );
rotate_o_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => rotate_o_INST_0_i_2_n_0,
      I1 => PB_cnt_reg(2),
      I2 => PB_cnt_reg(3),
      I3 => PB_cnt_reg(4),
      I4 => PB_cnt_reg(15),
      I5 => rotate_o_INST_0_i_3_n_0,
      O => p_2_in
    );
rotate_o_INST_0_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => rotate_o_INST_0_i_2_n_0
    );
rotate_o_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1__2_n_0\,
      I1 => rotate_o_INST_0_i_4_n_0,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => rotate_o_INST_0_i_3_n_0
    );
rotate_o_INST_0_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => rotate_o_INST_0_i_4_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_4 is
  port (
    sw_pause_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    sw_pause : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_4 : entity is "debounce";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_4;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_4 is
  signal \PB_cnt[0]_i_1__5_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3__5_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2__5_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__5_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__5_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__5_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__5_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal \PB_state_i_1__5_n_0\ : STD_LOGIC;
  signal \PB_sync_0_i_1__5_n_0\ : STD_LOGIC;
  signal PB_sync_0_reg_n_0 : STD_LOGIC;
  signal PB_sync_1_reg_n_0 : STD_LOGIC;
  signal \sw_flipped_i_1__0_n_0\ : STD_LOGIC;
  signal \sw_flipped_i_2__0_n_0\ : STD_LOGIC;
  signal \sw_flipped_i_3__0_n_0\ : STD_LOGIC;
  signal \sw_flipped_i_4__0_n_0\ : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
\PB_cnt[0]_i_1__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1_reg_n_0,
      O => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt[0]_i_3__5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3__5_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__5_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[0]_i_2__5\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2__5_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2__5_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2__5_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2__5_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2__5_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2__5_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2__5_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3__5_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__5_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__5_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__5_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[12]_i_1__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1__5_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1__5_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1__5_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1__5_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1__5_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1__5_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1__5_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1__5_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__5_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__5_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__5_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__5_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__5_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__5_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__5_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[4]_i_1__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2__5_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1__5_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1__5_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1__5_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1__5_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1__5_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1__5_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1__5_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__5_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__5_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__5_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__5_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_cnt_reg[8]_i_1__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1__5_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1__5_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1__5_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1__5_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1__5_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1__5_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1__5_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1__5_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__5_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1__5_n_0\
    );
\PB_state_i_1__5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \sw_flipped_i_1__0_n_0\,
      I1 => PB_state,
      O => \PB_state_i_1__5_n_0\
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_state_i_1__5_n_0\,
      Q => PB_state,
      R => '0'
    );
\PB_sync_0_i_1__5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sw_pause,
      O => \PB_sync_0_i_1__5_n_0\
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_sync_0_i_1__5_n_0\,
      Q => PB_sync_0_reg_n_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_reg_n_0,
      Q => PB_sync_1_reg_n_0,
      R => '0'
    );
\sw_flipped_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => \sw_flipped_i_2__0_n_0\,
      I1 => PB_cnt_reg(2),
      I2 => PB_cnt_reg(3),
      I3 => PB_cnt_reg(4),
      I4 => PB_cnt_reg(15),
      I5 => \sw_flipped_i_3__0_n_0\,
      O => \sw_flipped_i_1__0_n_0\
    );
\sw_flipped_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => \sw_flipped_i_2__0_n_0\
    );
\sw_flipped_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1__5_n_0\,
      I1 => \sw_flipped_i_4__0_n_0\,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => \sw_flipped_i_3__0_n_0\
    );
\sw_flipped_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => \sw_flipped_i_4__0_n_0\
    );
sw_flipped_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \sw_flipped_i_1__0_n_0\,
      Q => sw_pause_o,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_5 is
  port (
    sw_rst_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    sw_rst : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_5 : entity is "debounce";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_5;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_5 is
  signal \PB_cnt[0]_i_1__4_n_0\ : STD_LOGIC;
  signal \PB_cnt[0]_i_3__4_n_0\ : STD_LOGIC;
  signal PB_cnt_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \PB_cnt_reg[0]_i_2__4_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[0]_i_2__4_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[12]_i_1__4_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[4]_i_1__4_n_7\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_0\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_1\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_2\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_3\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_4\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_5\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_6\ : STD_LOGIC;
  signal \PB_cnt_reg[8]_i_1__4_n_7\ : STD_LOGIC;
  signal PB_state : STD_LOGIC;
  signal \PB_state_i_1__4_n_0\ : STD_LOGIC;
  signal \PB_sync_0_i_1__4_n_0\ : STD_LOGIC;
  signal PB_sync_0_reg_n_0 : STD_LOGIC;
  signal PB_sync_1_reg_n_0 : STD_LOGIC;
  signal sw_flipped_i_1_n_0 : STD_LOGIC;
  signal sw_flipped_i_2_n_0 : STD_LOGIC;
  signal sw_flipped_i_3_n_0 : STD_LOGIC;
  signal sw_flipped_i_4_n_0 : STD_LOGIC;
  signal \NLW_PB_cnt_reg[12]_i_1__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
\PB_cnt[0]_i_1__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => PB_state,
      I1 => PB_sync_1_reg_n_0,
      O => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt[0]_i_3__4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => PB_cnt_reg(0),
      O => \PB_cnt[0]_i_3__4_n_0\
    );
\PB_cnt_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__4_n_7\,
      Q => PB_cnt_reg(0),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[0]_i_2__4\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \PB_cnt_reg[0]_i_2__4_n_0\,
      CO(2) => \PB_cnt_reg[0]_i_2__4_n_1\,
      CO(1) => \PB_cnt_reg[0]_i_2__4_n_2\,
      CO(0) => \PB_cnt_reg[0]_i_2__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \PB_cnt_reg[0]_i_2__4_n_4\,
      O(2) => \PB_cnt_reg[0]_i_2__4_n_5\,
      O(1) => \PB_cnt_reg[0]_i_2__4_n_6\,
      O(0) => \PB_cnt_reg[0]_i_2__4_n_7\,
      S(3 downto 1) => PB_cnt_reg(3 downto 1),
      S(0) => \PB_cnt[0]_i_3__4_n_0\
    );
\PB_cnt_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__4_n_5\,
      Q => PB_cnt_reg(10),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__4_n_4\,
      Q => PB_cnt_reg(11),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__4_n_7\,
      Q => PB_cnt_reg(12),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[12]_i_1__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[8]_i_1__4_n_0\,
      CO(3) => \NLW_PB_cnt_reg[12]_i_1__4_CO_UNCONNECTED\(3),
      CO(2) => \PB_cnt_reg[12]_i_1__4_n_1\,
      CO(1) => \PB_cnt_reg[12]_i_1__4_n_2\,
      CO(0) => \PB_cnt_reg[12]_i_1__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[12]_i_1__4_n_4\,
      O(2) => \PB_cnt_reg[12]_i_1__4_n_5\,
      O(1) => \PB_cnt_reg[12]_i_1__4_n_6\,
      O(0) => \PB_cnt_reg[12]_i_1__4_n_7\,
      S(3 downto 0) => PB_cnt_reg(15 downto 12)
    );
\PB_cnt_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__4_n_6\,
      Q => PB_cnt_reg(13),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__4_n_5\,
      Q => PB_cnt_reg(14),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[12]_i_1__4_n_4\,
      Q => PB_cnt_reg(15),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__4_n_6\,
      Q => PB_cnt_reg(1),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__4_n_5\,
      Q => PB_cnt_reg(2),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[0]_i_2__4_n_4\,
      Q => PB_cnt_reg(3),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__4_n_7\,
      Q => PB_cnt_reg(4),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[4]_i_1__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[0]_i_2__4_n_0\,
      CO(3) => \PB_cnt_reg[4]_i_1__4_n_0\,
      CO(2) => \PB_cnt_reg[4]_i_1__4_n_1\,
      CO(1) => \PB_cnt_reg[4]_i_1__4_n_2\,
      CO(0) => \PB_cnt_reg[4]_i_1__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[4]_i_1__4_n_4\,
      O(2) => \PB_cnt_reg[4]_i_1__4_n_5\,
      O(1) => \PB_cnt_reg[4]_i_1__4_n_6\,
      O(0) => \PB_cnt_reg[4]_i_1__4_n_7\,
      S(3 downto 0) => PB_cnt_reg(7 downto 4)
    );
\PB_cnt_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__4_n_6\,
      Q => PB_cnt_reg(5),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__4_n_5\,
      Q => PB_cnt_reg(6),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[4]_i_1__4_n_4\,
      Q => PB_cnt_reg(7),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__4_n_7\,
      Q => PB_cnt_reg(8),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_cnt_reg[8]_i_1__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \PB_cnt_reg[4]_i_1__4_n_0\,
      CO(3) => \PB_cnt_reg[8]_i_1__4_n_0\,
      CO(2) => \PB_cnt_reg[8]_i_1__4_n_1\,
      CO(1) => \PB_cnt_reg[8]_i_1__4_n_2\,
      CO(0) => \PB_cnt_reg[8]_i_1__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \PB_cnt_reg[8]_i_1__4_n_4\,
      O(2) => \PB_cnt_reg[8]_i_1__4_n_5\,
      O(1) => \PB_cnt_reg[8]_i_1__4_n_6\,
      O(0) => \PB_cnt_reg[8]_i_1__4_n_7\,
      S(3 downto 0) => PB_cnt_reg(11 downto 8)
    );
\PB_cnt_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_cnt_reg[8]_i_1__4_n_6\,
      Q => PB_cnt_reg(9),
      R => \PB_cnt[0]_i_1__4_n_0\
    );
\PB_state_i_1__4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => sw_flipped_i_1_n_0,
      I1 => PB_state,
      O => \PB_state_i_1__4_n_0\
    );
PB_state_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_state_i_1__4_n_0\,
      Q => PB_state,
      R => '0'
    );
\PB_sync_0_i_1__4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sw_rst,
      O => \PB_sync_0_i_1__4_n_0\
    );
PB_sync_0_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \PB_sync_0_i_1__4_n_0\,
      Q => PB_sync_0_reg_n_0,
      R => '0'
    );
PB_sync_1_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => PB_sync_0_reg_n_0,
      Q => PB_sync_1_reg_n_0,
      R => '0'
    );
sw_flipped_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => sw_flipped_i_2_n_0,
      I1 => PB_cnt_reg(2),
      I2 => PB_cnt_reg(3),
      I3 => PB_cnt_reg(4),
      I4 => PB_cnt_reg(15),
      I5 => sw_flipped_i_3_n_0,
      O => sw_flipped_i_1_n_0
    );
sw_flipped_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(5),
      I1 => PB_cnt_reg(8),
      I2 => PB_cnt_reg(1),
      I3 => PB_cnt_reg(9),
      O => sw_flipped_i_2_n_0
    );
sw_flipped_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \PB_cnt[0]_i_1__4_n_0\,
      I1 => sw_flipped_i_4_n_0,
      I2 => PB_cnt_reg(10),
      I3 => PB_cnt_reg(12),
      I4 => PB_cnt_reg(11),
      I5 => PB_cnt_reg(0),
      O => sw_flipped_i_3_n_0
    );
sw_flipped_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => PB_cnt_reg(6),
      I1 => PB_cnt_reg(13),
      I2 => PB_cnt_reg(7),
      I3 => PB_cnt_reg(14),
      O => sw_flipped_i_4_n_0
    );
sw_flipped_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => sw_flipped_i_1_n_0,
      Q => sw_rst_o,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga is
  port (
    O : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \counterX_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    r : out STD_LOGIC_VECTOR ( 2 downto 0 );
    vs : out STD_LOGIC;
    cur_blk_index : out STD_LOGIC_VECTOR ( 0 to 0 );
    b : out STD_LOGIC_VECTOR ( 1 downto 0 );
    hs : out STD_LOGIC;
    g : out STD_LOGIC_VECTOR ( 3 downto 0 );
    pixel_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_18_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_18_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_18_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_19_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_19_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_19_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_19_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_14_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_14_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_14_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_14_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_8_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_8_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_8_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_15_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_15_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_15_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_15_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_16_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_16_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_16_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_16_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_11_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_11_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_11_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_11_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_2_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_2_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_2_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_1_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \r[1]_INST_0_i_1_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \r[1]_INST_0_i_1_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \r[1]_INST_0_i_1_3\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    r_2_sp_1 : in STD_LOGIC;
    g_1_sp_1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga is
  signal A : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal C : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \^o\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \b[0]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \counterX[4]_i_1_n_0\ : STD_LOGIC;
  signal \counterX[8]_i_2_n_0\ : STD_LOGIC;
  signal \counterX[9]_i_1_n_0\ : STD_LOGIC;
  signal \counterX[9]_i_3_n_0\ : STD_LOGIC;
  signal \counterX[9]_i_4_n_0\ : STD_LOGIC;
  signal counterX_reg : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \^counterx_reg[8]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal counterY : STD_LOGIC;
  signal \counterY[0]_i_1_n_0\ : STD_LOGIC;
  signal \counterY[1]_i_1_n_0\ : STD_LOGIC;
  signal \counterY[2]_i_1_n_0\ : STD_LOGIC;
  signal \counterY[3]_i_1_n_0\ : STD_LOGIC;
  signal \counterY[6]_i_2_n_0\ : STD_LOGIC;
  signal \counterY[9]_i_3_n_0\ : STD_LOGIC;
  signal \counterY[9]_i_4_n_0\ : STD_LOGIC;
  signal counterY_reg : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal cur_blk_index0 : STD_LOGIC_VECTOR ( 9 downto 2 );
  signal \cur_blk_index__0\ : STD_LOGIC_VECTOR ( 2 downto 1 );
  signal \g[0]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \g[2]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \g[2]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \g[2]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_10_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_11_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_12_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_13_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_14_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_15_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_15_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_15_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_16_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_16_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_16_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_17_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_17_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_17_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_18_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_19_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_20_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_20_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_20_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_21_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_21_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_21_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_22_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_22_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_22_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_23_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_23_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_23_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_27_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_31_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_35_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_36_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_36_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_36_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_37_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_37_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_37_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_38_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_38_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_38_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_39_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_39_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_39_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_40_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_40_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_40_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_41_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_41_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_41_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_42_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_42_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_42_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_43_n_1\ : STD_LOGIC;
  signal \g[3]_INST_0_i_43_n_2\ : STD_LOGIC;
  signal \g[3]_INST_0_i_43_n_3\ : STD_LOGIC;
  signal \g[3]_INST_0_i_47_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_51_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_55_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_59_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_63_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_67_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_71_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_75_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_79_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_83_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_87_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_91_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_9_n_0\ : STD_LOGIC;
  signal g_1_sn_1 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 9 downto 4 );
  signal \r[0]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \r[0]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \r[0]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \r[0]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \r[0]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \r[0]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_102_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_106_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_10_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_10_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_10_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_110_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_114_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_118_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_11_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_122_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_126_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_128_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_129_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_12_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_12_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_12_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_130_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_131_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_133_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_135_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_136_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_137_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_13_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_13_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_13_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_141_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_142_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_143_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_145_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_146_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_147_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_148_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_14_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_14_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_14_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_15_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_16_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_17_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_17_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_17_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_17_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_18_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_19_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_21_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_23_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_25_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_28_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_29_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_31_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_32_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_33_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_35_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_36_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_37_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_39_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_40_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_41_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_42_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_42_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_42_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_43_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_43_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_43_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_44_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_44_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_44_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_45_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_45_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_45_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_49_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_53_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_57_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_58_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_58_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_58_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_59_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_59_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_59_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_60_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_60_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_60_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_61_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_61_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_61_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_62_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_62_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_62_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_63_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_63_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_63_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_64_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_64_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_64_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_65_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_65_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_65_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_66_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_66_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_66_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_66_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_67_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_67_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_68_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_68_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_68_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_68_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_69_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_70_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_71_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_72_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_74_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_75_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_76_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_77_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_7_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_7_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_7_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_82_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_86_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_8_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_8_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_8_n_3\ : STD_LOGIC;
  signal \r[1]_INST_0_i_90_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_94_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_98_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_9_n_1\ : STD_LOGIC;
  signal \r[1]_INST_0_i_9_n_2\ : STD_LOGIC;
  signal \r[1]_INST_0_i_9_n_3\ : STD_LOGIC;
  signal r_2_sn_1 : STD_LOGIC;
  signal rgb10 : STD_LOGIC;
  signal rgb1011_out : STD_LOGIC;
  signal rgb11 : STD_LOGIC;
  signal rgb1112_out : STD_LOGIC;
  signal rgb12 : STD_LOGIC;
  signal rgb1213_out : STD_LOGIC;
  signal rgb13 : STD_LOGIC;
  signal rgb1314_out : STD_LOGIC;
  signal rgb14 : STD_LOGIC;
  signal rgb1415_out : STD_LOGIC;
  signal rgb14_out : STD_LOGIC;
  signal rgb15 : STD_LOGIC;
  signal rgb1516_out : STD_LOGIC;
  signal rgb16 : STD_LOGIC;
  signal rgb1617_out : STD_LOGIC;
  signal rgb1618_out : STD_LOGIC;
  signal rgb163_out : STD_LOGIC;
  signal rgb2 : STD_LOGIC;
  signal rgb228_in : STD_LOGIC;
  signal rgb3 : STD_LOGIC;
  signal rgb31_out : STD_LOGIC;
  signal rgb327_in : STD_LOGIC;
  signal rgb34_out : STD_LOGIC;
  signal rgb4 : STD_LOGIC;
  signal rgb40_in : STD_LOGIC;
  signal rgb40_out : STD_LOGIC;
  signal rgb429_in : STD_LOGIC;
  signal rgb42_out : STD_LOGIC;
  signal rgb43_in : STD_LOGIC;
  signal rgb45_out : STD_LOGIC;
  signal rgb5 : STD_LOGIC;
  signal rgb56_out : STD_LOGIC;
  signal rgb6 : STD_LOGIC;
  signal rgb67_out : STD_LOGIC;
  signal rgb7 : STD_LOGIC;
  signal rgb78_out : STD_LOGIC;
  signal rgb8 : STD_LOGIC;
  signal rgb89_out : STD_LOGIC;
  signal rgb9 : STD_LOGIC;
  signal rgb910_out : STD_LOGIC;
  signal v_enabled : STD_LOGIC;
  signal vs_INST_0_i_1_n_0 : STD_LOGIC;
  signal \NLW_g[3]_INST_0_i_15_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_16_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_17_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_20_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_21_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_22_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_23_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_36_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_37_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_38_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_39_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_40_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_41_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_42_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_g[3]_INST_0_i_43_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_10_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_12_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_13_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_14_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_42_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_43_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_44_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_45_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_58_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_59_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_60_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_61_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_62_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_63_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_64_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_65_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_66_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_r[1]_INST_0_i_67_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_r[1]_INST_0_i_67_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_r[1]_INST_0_i_7_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_78_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_78_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_r[1]_INST_0_i_8_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_r[1]_INST_0_i_9_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \b[0]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \b[0]_INST_0_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \counterX[1]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \counterX[2]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \counterX[3]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \counterX[6]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \counterX[7]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \counterX[8]_i_2\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \counterX[9]_i_3\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \counterY[0]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \counterY[4]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \counterY[8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \counterY[9]_i_2\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \g[0]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \g[0]_INST_0_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \g[2]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \g[2]_INST_0_i_2\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \g[2]_INST_0_i_3\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \g[3]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \g[3]_INST_0_i_11\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \g[3]_INST_0_i_12\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \g[3]_INST_0_i_3\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_148\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_22\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_24\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_3\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_76\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of vs_INST_0_i_1 : label is "soft_lutpair6";
begin
  O(1 downto 0) <= \^o\(1 downto 0);
  \counterX_reg[8]_0\(3 downto 0) <= \^counterx_reg[8]_0\(3 downto 0);
  g_1_sn_1 <= g_1_sp_1;
  r_2_sn_1 <= r_2_sp_1;
\b[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0E"
    )
        port map (
      I0 => \b[0]_INST_0_i_1_n_0\,
      I1 => \g[3]_INST_0_i_2_n_0\,
      I2 => \r[1]_INST_0_i_6_n_0\,
      O => b(1)
    );
\b[0]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04004044"
    )
        port map (
      I0 => \r[1]_INST_0_i_5_n_0\,
      I1 => \r[1]_INST_0_i_1_n_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => Q(2),
      O => \b[0]_INST_0_i_1_n_0\
    );
\b[1]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAA888AA"
    )
        port map (
      I0 => \g[2]_INST_0_i_1_n_0\,
      I1 => \r[1]_INST_0_i_5_n_0\,
      I2 => Q(0),
      I3 => Q(2),
      I4 => Q(1),
      I5 => \r[1]_INST_0_i_6_n_0\,
      O => b(0)
    );
\counterX[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => counterX_reg(0),
      O => p_0_in(0)
    );
\counterX[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => counterX_reg(0),
      I1 => counterX_reg(1),
      O => p_0_in(1)
    );
\counterX[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => counterX_reg(0),
      I1 => counterX_reg(1),
      I2 => counterX_reg(2),
      O => p_0_in(2)
    );
\counterX[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => counterX_reg(1),
      I1 => counterX_reg(0),
      I2 => counterX_reg(2),
      I3 => counterX_reg(3),
      O => p_0_in(3)
    );
\counterX[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => counterX_reg(2),
      I1 => counterX_reg(0),
      I2 => counterX_reg(1),
      I3 => counterX_reg(3),
      I4 => counterX_reg(4),
      O => \counterX[4]_i_1_n_0\
    );
\counterX[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => counterX_reg(3),
      I1 => counterX_reg(1),
      I2 => counterX_reg(0),
      I3 => counterX_reg(2),
      I4 => counterX_reg(4),
      I5 => counterX_reg(5),
      O => p_0_in(5)
    );
\counterX[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => counterX_reg(4),
      I1 => \counterX[8]_i_2_n_0\,
      I2 => counterX_reg(5),
      I3 => counterX_reg(6),
      O => p_0_in(6)
    );
\counterX[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => counterX_reg(5),
      I1 => \counterX[8]_i_2_n_0\,
      I2 => counterX_reg(4),
      I3 => counterX_reg(6),
      I4 => counterX_reg(7),
      O => p_0_in(7)
    );
\counterX[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => counterX_reg(6),
      I1 => counterX_reg(4),
      I2 => \counterX[8]_i_2_n_0\,
      I3 => counterX_reg(5),
      I4 => counterX_reg(7),
      I5 => counterX_reg(8),
      O => p_0_in(8)
    );
\counterX[8]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => counterX_reg(3),
      I1 => counterX_reg(1),
      I2 => counterX_reg(0),
      I3 => counterX_reg(2),
      O => \counterX[8]_i_2_n_0\
    );
\counterX[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888088888888"
    )
        port map (
      I0 => counterX_reg(9),
      I1 => counterX_reg(8),
      I2 => counterX_reg(5),
      I3 => counterX_reg(7),
      I4 => counterX_reg(6),
      I5 => \counterX[9]_i_3_n_0\,
      O => \counterX[9]_i_1_n_0\
    );
\counterX[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => counterX_reg(7),
      I1 => \counterX[9]_i_4_n_0\,
      I2 => counterX_reg(6),
      I3 => counterX_reg(8),
      I4 => counterX_reg(9),
      O => p_0_in(9)
    );
\counterX[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => counterX_reg(3),
      I1 => counterX_reg(4),
      I2 => counterX_reg(1),
      I3 => counterX_reg(2),
      I4 => counterX_reg(0),
      O => \counterX[9]_i_3_n_0\
    );
\counterX[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counterX_reg(5),
      I1 => counterX_reg(3),
      I2 => counterX_reg(1),
      I3 => counterX_reg(0),
      I4 => counterX_reg(2),
      I5 => counterX_reg(4),
      O => \counterX[9]_i_4_n_0\
    );
\counterX_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(0),
      Q => counterX_reg(0),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(1),
      Q => counterX_reg(1),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(2),
      Q => counterX_reg(2),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(3),
      Q => counterX_reg(3),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => \counterX[4]_i_1_n_0\,
      Q => counterX_reg(4),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(5),
      Q => counterX_reg(5),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(6),
      Q => counterX_reg(6),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(7),
      Q => counterX_reg(7),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(8),
      Q => counterX_reg(8),
      R => \counterX[9]_i_1_n_0\
    );
\counterX_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => p_0_in(9),
      Q => counterX_reg(9),
      R => \counterX[9]_i_1_n_0\
    );
\counterY[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00005777"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => \counterY[9]_i_3_n_0\,
      I2 => counterY_reg(3),
      I3 => counterY_reg(2),
      I4 => counterY_reg(0),
      O => \counterY[0]_i_1_n_0\
    );
\counterY[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000577757770000"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => \counterY[9]_i_3_n_0\,
      I2 => counterY_reg(3),
      I3 => counterY_reg(2),
      I4 => counterY_reg(0),
      I5 => counterY_reg(1),
      O => \counterY[1]_i_1_n_0\
    );
\counterY[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0077570057005700"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => \counterY[9]_i_3_n_0\,
      I2 => counterY_reg(3),
      I3 => counterY_reg(2),
      I4 => counterY_reg(0),
      I5 => counterY_reg(1),
      O => \counterY[2]_i_1_n_0\
    );
\counterY[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0770507050705070"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => \counterY[9]_i_3_n_0\,
      I2 => counterY_reg(3),
      I3 => counterY_reg(2),
      I4 => counterY_reg(1),
      I5 => counterY_reg(0),
      O => \counterY[3]_i_1_n_0\
    );
\counterY[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => counterY_reg(2),
      I1 => counterY_reg(0),
      I2 => counterY_reg(1),
      I3 => counterY_reg(3),
      I4 => counterY_reg(4),
      O => \p_0_in__0\(4)
    );
\counterY[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => counterY_reg(3),
      I1 => counterY_reg(1),
      I2 => counterY_reg(0),
      I3 => counterY_reg(2),
      I4 => counterY_reg(4),
      I5 => counterY_reg(5),
      O => \p_0_in__0\(5)
    );
\counterY[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => counterY_reg(4),
      I1 => counterY_reg(2),
      I2 => \counterY[6]_i_2_n_0\,
      I3 => counterY_reg(3),
      I4 => counterY_reg(5),
      I5 => counterY_reg(6),
      O => \p_0_in__0\(6)
    );
\counterY[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => counterY_reg(1),
      I1 => counterY_reg(0),
      O => \counterY[6]_i_2_n_0\
    );
\counterY[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \counterY[9]_i_4_n_0\,
      I1 => counterY_reg(6),
      I2 => counterY_reg(7),
      O => \p_0_in__0\(7)
    );
\counterY[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => counterY_reg(6),
      I1 => \counterY[9]_i_4_n_0\,
      I2 => counterY_reg(7),
      I3 => counterY_reg(8),
      O => \p_0_in__0\(8)
    );
\counterY[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88808080"
    )
        port map (
      I0 => v_enabled,
      I1 => counterY_reg(9),
      I2 => \counterY[9]_i_3_n_0\,
      I3 => counterY_reg(3),
      I4 => counterY_reg(2),
      O => counterY
    );
\counterY[9]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => counterY_reg(7),
      I1 => \counterY[9]_i_4_n_0\,
      I2 => counterY_reg(6),
      I3 => counterY_reg(8),
      I4 => counterY_reg(9),
      O => \p_0_in__0\(9)
    );
\counterY[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => counterY_reg(4),
      I1 => counterY_reg(7),
      I2 => counterY_reg(8),
      I3 => counterY_reg(5),
      I4 => counterY_reg(6),
      O => \counterY[9]_i_3_n_0\
    );
\counterY[9]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => counterY_reg(5),
      I1 => counterY_reg(3),
      I2 => counterY_reg(1),
      I3 => counterY_reg(0),
      I4 => counterY_reg(2),
      I5 => counterY_reg(4),
      O => \counterY[9]_i_4_n_0\
    );
\counterY_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \counterY[0]_i_1_n_0\,
      Q => counterY_reg(0),
      R => '0'
    );
\counterY_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \counterY[1]_i_1_n_0\,
      Q => counterY_reg(1),
      R => '0'
    );
\counterY_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \counterY[2]_i_1_n_0\,
      Q => counterY_reg(2),
      R => '0'
    );
\counterY_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \counterY[3]_i_1_n_0\,
      Q => counterY_reg(3),
      R => '0'
    );
\counterY_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(4),
      Q => counterY_reg(4),
      R => counterY
    );
\counterY_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(5),
      Q => counterY_reg(5),
      R => counterY
    );
\counterY_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(6),
      Q => counterY_reg(6),
      R => counterY
    );
\counterY_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(7),
      Q => counterY_reg(7),
      R => counterY
    );
\counterY_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(8),
      Q => counterY_reg(8),
      R => counterY
    );
\counterY_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => v_enabled,
      D => \p_0_in__0\(9),
      Q => counterY_reg(9),
      R => counterY
    );
\g[0]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B888"
    )
        port map (
      I0 => \g[3]_INST_0_i_1_n_0\,
      I1 => \r[1]_INST_0_i_6_n_0\,
      I2 => \g[2]_INST_0_i_1_n_0\,
      I3 => \g[0]_INST_0_i_1_n_0\,
      O => g(0)
    );
\g[0]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDDF"
    )
        port map (
      I0 => Q(2),
      I1 => \r[1]_INST_0_i_5_n_0\,
      I2 => Q(0),
      I3 => Q(1),
      O => \g[0]_INST_0_i_1_n_0\
    );
\g[1]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DD8DDDDD88888888"
    )
        port map (
      I0 => \r[1]_INST_0_i_6_n_0\,
      I1 => \g[3]_INST_0_i_1_n_0\,
      I2 => g_1_sn_1,
      I3 => \r[1]_INST_0_i_5_n_0\,
      I4 => Q(2),
      I5 => \g[2]_INST_0_i_1_n_0\,
      O => g(1)
    );
\g[2]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B888"
    )
        port map (
      I0 => \g[3]_INST_0_i_1_n_0\,
      I1 => \r[1]_INST_0_i_6_n_0\,
      I2 => \g[2]_INST_0_i_1_n_0\,
      I3 => \g[2]_INST_0_i_2_n_0\,
      O => g(2)
    );
\g[2]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B888FFFFB8880000"
    )
        port map (
      I0 => \r[1]_INST_0_i_3_n_0\,
      I1 => \r[1]_INST_0_i_2_n_0\,
      I2 => rgb14_out,
      I3 => \g[2]_INST_0_i_3_n_0\,
      I4 => \r[1]_INST_0_i_5_n_0\,
      I5 => \r[1]_INST_0_i_1_n_0\,
      O => \g[2]_INST_0_i_1_n_0\
    );
\g[2]_INST_0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EBBF"
    )
        port map (
      I0 => \r[1]_INST_0_i_5_n_0\,
      I1 => Q(1),
      I2 => Q(2),
      I3 => Q(0),
      O => \g[2]_INST_0_i_2_n_0\
    );
\g[2]_INST_0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \^counterx_reg[8]_0\(1),
      I1 => \g[3]_INST_0_i_9_n_0\,
      I2 => \^counterx_reg[8]_0\(2),
      O => \g[2]_INST_0_i_3_n_0\
    );
\g[3]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DDD8"
    )
        port map (
      I0 => \r[1]_INST_0_i_6_n_0\,
      I1 => \g[3]_INST_0_i_1_n_0\,
      I2 => \g[3]_INST_0_i_2_n_0\,
      I3 => \g[3]_INST_0_i_3_n_0\,
      O => g(3)
    );
\g[3]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEEEEEEEF"
    )
        port map (
      I0 => rgb228_in,
      I1 => rgb429_in,
      I2 => counterX_reg(9),
      I3 => counterX_reg(5),
      I4 => \g[3]_INST_0_i_6_n_0\,
      I5 => rgb327_in,
      O => \g[3]_INST_0_i_1_n_0\
    );
\g[3]_INST_0_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFFF"
    )
        port map (
      I0 => counterY_reg(6),
      I1 => counterY_reg(2),
      I2 => counterY_reg(4),
      I3 => counterY_reg(8),
      O => \g[3]_INST_0_i_10_n_0\
    );
\g[3]_INST_0_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => counterX_reg(2),
      I1 => counterX_reg(3),
      I2 => counterX_reg(0),
      I3 => counterX_reg(1),
      O => \g[3]_INST_0_i_11_n_0\
    );
\g[3]_INST_0_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => counterX_reg(5),
      I1 => counterX_reg(6),
      O => \g[3]_INST_0_i_12_n_0\
    );
\g[3]_INST_0_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => counterY_reg(3),
      I1 => counterY_reg(7),
      I2 => counterY_reg(2),
      I3 => counterY_reg(5),
      O => \g[3]_INST_0_i_13_n_0\
    );
\g[3]_INST_0_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => rgb6,
      I1 => rgb5,
      I2 => rgb8,
      I3 => rgb7,
      O => \g[3]_INST_0_i_14_n_0\
    );
\g[3]_INST_0_i_15\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb163_out,
      CO(2) => \g[3]_INST_0_i_15_n_1\,
      CO(1) => \g[3]_INST_0_i_15_n_2\,
      CO(0) => \g[3]_INST_0_i_15_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_15_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_8_2\(2 downto 0),
      S(0) => \g[3]_INST_0_i_27_n_0\
    );
\g[3]_INST_0_i_16\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb42_out,
      CO(2) => \g[3]_INST_0_i_16_n_1\,
      CO(1) => \g[3]_INST_0_i_16_n_2\,
      CO(0) => \g[3]_INST_0_i_16_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_16_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_8_0\(2 downto 0),
      S(0) => \g[3]_INST_0_i_31_n_0\
    );
\g[3]_INST_0_i_17\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb31_out,
      CO(2) => \g[3]_INST_0_i_17_n_1\,
      CO(1) => \g[3]_INST_0_i_17_n_2\,
      CO(0) => \g[3]_INST_0_i_17_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_17_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_8_1\(2 downto 0),
      S(0) => \g[3]_INST_0_i_35_n_0\
    );
\g[3]_INST_0_i_18\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => rgb14,
      I1 => rgb13,
      I2 => rgb16,
      I3 => rgb15,
      O => \g[3]_INST_0_i_18_n_0\
    );
\g[3]_INST_0_i_19\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => rgb10,
      I1 => rgb9,
      I2 => rgb12,
      I3 => rgb11,
      O => \g[3]_INST_0_i_19_n_0\
    );
\g[3]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000200000"
    )
        port map (
      I0 => \r[1]_INST_0_i_5_n_0\,
      I1 => \r[1]_INST_0_i_2_n_0\,
      I2 => rgb14_out,
      I3 => \^counterx_reg[8]_0\(2),
      I4 => \g[3]_INST_0_i_9_n_0\,
      I5 => \^counterx_reg[8]_0\(1),
      O => \g[3]_INST_0_i_2_n_0\
    );
\g[3]_INST_0_i_20\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb6,
      CO(2) => \g[3]_INST_0_i_20_n_1\,
      CO(1) => \g[3]_INST_0_i_20_n_2\,
      CO(0) => \g[3]_INST_0_i_20_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_20_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_14_2\(2 downto 0),
      S(0) => \g[3]_INST_0_i_47_n_0\
    );
\g[3]_INST_0_i_21\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb5,
      CO(2) => \g[3]_INST_0_i_21_n_1\,
      CO(1) => \g[3]_INST_0_i_21_n_2\,
      CO(0) => \g[3]_INST_0_i_21_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_21_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_14_3\(2 downto 0),
      S(0) => \g[3]_INST_0_i_51_n_0\
    );
\g[3]_INST_0_i_22\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb8,
      CO(2) => \g[3]_INST_0_i_22_n_1\,
      CO(1) => \g[3]_INST_0_i_22_n_2\,
      CO(0) => \g[3]_INST_0_i_22_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_22_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_14_0\(2 downto 0),
      S(0) => \g[3]_INST_0_i_55_n_0\
    );
\g[3]_INST_0_i_23\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb7,
      CO(2) => \g[3]_INST_0_i_23_n_1\,
      CO(1) => \g[3]_INST_0_i_23_n_2\,
      CO(0) => \g[3]_INST_0_i_23_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_23_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_14_1\(2 downto 0),
      S(0) => \g[3]_INST_0_i_59_n_0\
    );
\g[3]_INST_0_i_27\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \g[3]_INST_0_i_27_n_0\
    );
\g[3]_INST_0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01150000"
    )
        port map (
      I0 => \r[1]_INST_0_i_5_n_0\,
      I1 => Q(0),
      I2 => Q(2),
      I3 => Q(1),
      I4 => \r[1]_INST_0_i_1_n_0\,
      O => \g[3]_INST_0_i_3_n_0\
    );
\g[3]_INST_0_i_31\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \g[3]_INST_0_i_31_n_0\
    );
\g[3]_INST_0_i_35\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(1),
      I2 => \cur_blk_index__0\(2),
      O => \g[3]_INST_0_i_35_n_0\
    );
\g[3]_INST_0_i_36\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb14,
      CO(2) => \g[3]_INST_0_i_36_n_1\,
      CO(1) => \g[3]_INST_0_i_36_n_2\,
      CO(0) => \g[3]_INST_0_i_36_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_36_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_18_1\(2 downto 0),
      S(0) => \g[3]_INST_0_i_63_n_0\
    );
\g[3]_INST_0_i_37\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb13,
      CO(2) => \g[3]_INST_0_i_37_n_1\,
      CO(1) => \g[3]_INST_0_i_37_n_2\,
      CO(0) => \g[3]_INST_0_i_37_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_37_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_18_2\(2 downto 0),
      S(0) => \g[3]_INST_0_i_67_n_0\
    );
\g[3]_INST_0_i_38\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb16,
      CO(2) => \g[3]_INST_0_i_38_n_1\,
      CO(1) => \g[3]_INST_0_i_38_n_2\,
      CO(0) => \g[3]_INST_0_i_38_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_38_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => S(2 downto 0),
      S(0) => \g[3]_INST_0_i_71_n_0\
    );
\g[3]_INST_0_i_39\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb15,
      CO(2) => \g[3]_INST_0_i_39_n_1\,
      CO(1) => \g[3]_INST_0_i_39_n_2\,
      CO(0) => \g[3]_INST_0_i_39_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_39_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_18_0\(2 downto 0),
      S(0) => \g[3]_INST_0_i_75_n_0\
    );
\g[3]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(3),
      I2 => counterY_reg(7),
      I3 => counterY_reg(5),
      I4 => \counterY[6]_i_2_n_0\,
      I5 => \g[3]_INST_0_i_10_n_0\,
      O => rgb228_in
    );
\g[3]_INST_0_i_40\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb10,
      CO(2) => \g[3]_INST_0_i_40_n_1\,
      CO(1) => \g[3]_INST_0_i_40_n_2\,
      CO(0) => \g[3]_INST_0_i_40_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_40_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_19_2\(2 downto 0),
      S(0) => \g[3]_INST_0_i_79_n_0\
    );
\g[3]_INST_0_i_41\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb9,
      CO(2) => \g[3]_INST_0_i_41_n_1\,
      CO(1) => \g[3]_INST_0_i_41_n_2\,
      CO(0) => \g[3]_INST_0_i_41_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_41_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_19_3\(2 downto 0),
      S(0) => \g[3]_INST_0_i_83_n_0\
    );
\g[3]_INST_0_i_42\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb12,
      CO(2) => \g[3]_INST_0_i_42_n_1\,
      CO(1) => \g[3]_INST_0_i_42_n_2\,
      CO(0) => \g[3]_INST_0_i_42_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_42_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_19_0\(2 downto 0),
      S(0) => \g[3]_INST_0_i_87_n_0\
    );
\g[3]_INST_0_i_43\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb11,
      CO(2) => \g[3]_INST_0_i_43_n_1\,
      CO(1) => \g[3]_INST_0_i_43_n_2\,
      CO(0) => \g[3]_INST_0_i_43_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_g[3]_INST_0_i_43_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \g[3]_INST_0_i_19_1\(2 downto 0),
      S(0) => \g[3]_INST_0_i_91_n_0\
    );
\g[3]_INST_0_i_47\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \g[3]_INST_0_i_47_n_0\
    );
\g[3]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => counterX_reg(9),
      I1 => counterX_reg(8),
      I2 => \g[3]_INST_0_i_11_n_0\,
      I3 => counterX_reg(7),
      I4 => counterX_reg(4),
      I5 => \g[3]_INST_0_i_12_n_0\,
      O => rgb429_in
    );
\g[3]_INST_0_i_51\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \g[3]_INST_0_i_51_n_0\
    );
\g[3]_INST_0_i_55\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \g[3]_INST_0_i_55_n_0\
    );
\g[3]_INST_0_i_59\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \g[3]_INST_0_i_59_n_0\
    );
\g[3]_INST_0_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFF7"
    )
        port map (
      I0 => counterX_reg(8),
      I1 => counterX_reg(7),
      I2 => counterX_reg(4),
      I3 => counterX_reg(6),
      I4 => \g[3]_INST_0_i_11_n_0\,
      O => \g[3]_INST_0_i_6_n_0\
    );
\g[3]_INST_0_i_63\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(1),
      I2 => \cur_blk_index__0\(2),
      O => \g[3]_INST_0_i_63_n_0\
    );
\g[3]_INST_0_i_67\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \cur_blk_index__0\(2),
      I2 => \counterX[4]_i_1_n_0\,
      O => \g[3]_INST_0_i_67_n_0\
    );
\g[3]_INST_0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010000000"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(8),
      I2 => counterY_reg(4),
      I3 => counterY_reg(6),
      I4 => \counterY[6]_i_2_n_0\,
      I5 => \g[3]_INST_0_i_13_n_0\,
      O => rgb327_in
    );
\g[3]_INST_0_i_71\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \g[3]_INST_0_i_71_n_0\
    );
\g[3]_INST_0_i_75\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(1),
      I2 => \cur_blk_index__0\(2),
      O => \g[3]_INST_0_i_75_n_0\
    );
\g[3]_INST_0_i_79\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \g[3]_INST_0_i_79_n_0\
    );
\g[3]_INST_0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \g[3]_INST_0_i_14_n_0\,
      I1 => rgb163_out,
      I2 => rgb42_out,
      I3 => rgb31_out,
      I4 => \g[3]_INST_0_i_18_n_0\,
      I5 => \g[3]_INST_0_i_19_n_0\,
      O => rgb14_out
    );
\g[3]_INST_0_i_83\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \g[3]_INST_0_i_83_n_0\
    );
\g[3]_INST_0_i_87\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \g[3]_INST_0_i_87_n_0\
    );
\g[3]_INST_0_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4404000020221800"
    )
        port map (
      I0 => \^counterx_reg[8]_0\(0),
      I1 => \^o\(1),
      I2 => \^o\(0),
      I3 => \cur_blk_index__0\(1),
      I4 => \counterX[4]_i_1_n_0\,
      I5 => \cur_blk_index__0\(2),
      O => \g[3]_INST_0_i_9_n_0\
    );
\g[3]_INST_0_i_91\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \g[3]_INST_0_i_91_n_0\
    );
hs_INST_0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEFEFE"
    )
        port map (
      I0 => counterX_reg(8),
      I1 => counterX_reg(9),
      I2 => counterX_reg(7),
      I3 => counterX_reg(6),
      I4 => counterX_reg(5),
      O => hs
    );
\r[0]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000004"
    )
        port map (
      I0 => \r[1]_INST_0_i_6_n_0\,
      I1 => \r[0]_INST_0_i_1_n_0\,
      I2 => \r[1]_INST_0_i_5_n_0\,
      I3 => \r[0]_INST_0_i_2_n_0\,
      I4 => Q(1),
      O => r(0)
    );
\r[0]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000800000"
    )
        port map (
      I0 => \r[0]_INST_0_i_3_n_0\,
      I1 => rgb43_in,
      I2 => \r[0]_INST_0_i_4_n_0\,
      I3 => counterY_reg(8),
      I4 => \r[0]_INST_0_i_5_n_0\,
      I5 => counterY_reg(9),
      O => \r[0]_INST_0_i_1_n_0\
    );
\r[0]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"666666666666666F"
    )
        port map (
      I0 => Q(2),
      I1 => Q(0),
      I2 => \r[1]_INST_0_i_7_n_0\,
      I3 => rgb4,
      I4 => rgb40_out,
      I5 => rgb2,
      O => \r[0]_INST_0_i_2_n_0\
    );
\r[0]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7F7F7F7F7F7FFF"
    )
        port map (
      I0 => counterY_reg(6),
      I1 => counterY_reg(7),
      I2 => counterY_reg(5),
      I3 => counterY_reg(3),
      I4 => counterY_reg(4),
      I5 => counterY_reg(2),
      O => \r[0]_INST_0_i_3_n_0\
    );
\r[0]_INST_0_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000FF7F"
    )
        port map (
      I0 => counterX_reg(6),
      I1 => counterX_reg(8),
      I2 => counterX_reg(7),
      I3 => \r[0]_INST_0_i_6_n_0\,
      I4 => counterX_reg(9),
      O => \r[0]_INST_0_i_4_n_0\
    );
\r[0]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA88888880"
    )
        port map (
      I0 => counterY_reg(7),
      I1 => counterY_reg(5),
      I2 => counterY_reg(4),
      I3 => \counterY[6]_i_2_n_0\,
      I4 => \r[1]_INST_0_i_76_n_0\,
      I5 => counterY_reg(6),
      O => \r[0]_INST_0_i_5_n_0\
    );
\r[0]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => counterX_reg(3),
      I1 => counterX_reg(2),
      I2 => counterX_reg(5),
      I3 => counterX_reg(4),
      I4 => counterX_reg(1),
      I5 => counterX_reg(0),
      O => \r[0]_INST_0_i_6_n_0\
    );
\r[1]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000C0C0AA00"
    )
        port map (
      I0 => \r[1]_INST_0_i_1_n_0\,
      I1 => \r[1]_INST_0_i_2_n_0\,
      I2 => \r[1]_INST_0_i_3_n_0\,
      I3 => r_2_sn_1,
      I4 => \r[1]_INST_0_i_5_n_0\,
      I5 => \r[1]_INST_0_i_6_n_0\,
      O => r(2)
    );
\r[1]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \r[1]_INST_0_i_7_n_0\,
      I1 => rgb4,
      I2 => rgb40_out,
      I3 => rgb2,
      I4 => \r[0]_INST_0_i_1_n_0\,
      O => \r[1]_INST_0_i_1_n_0\
    );
\r[1]_INST_0_i_10\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb2,
      CO(2) => \r[1]_INST_0_i_10_n_1\,
      CO(1) => \r[1]_INST_0_i_10_n_2\,
      CO(0) => \r[1]_INST_0_i_10_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_10_O_UNCONNECTED\(3 downto 0),
      S(3) => \r[1]_INST_0_i_1_2\(0),
      S(2) => \r[1]_INST_0_i_39_n_0\,
      S(1) => \r[1]_INST_0_i_40_n_0\,
      S(0) => \r[1]_INST_0_i_41_n_0\
    );
\r[1]_INST_0_i_102\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_102_n_0\
    );
\r[1]_INST_0_i_106\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_106_n_0\
    );
\r[1]_INST_0_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => rgb67_out,
      I1 => rgb56_out,
      I2 => rgb89_out,
      I3 => rgb78_out,
      O => \r[1]_INST_0_i_11_n_0\
    );
\r[1]_INST_0_i_110\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_110_n_0\
    );
\r[1]_INST_0_i_114\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_114_n_0\
    );
\r[1]_INST_0_i_118\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_118_n_0\
    );
\r[1]_INST_0_i_12\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1618_out,
      CO(2) => \r[1]_INST_0_i_12_n_1\,
      CO(1) => \r[1]_INST_0_i_12_n_2\,
      CO(0) => \r[1]_INST_0_i_12_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_12_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_2_2\(2 downto 0),
      S(0) => \r[1]_INST_0_i_49_n_0\
    );
\r[1]_INST_0_i_122\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_122_n_0\
    );
\r[1]_INST_0_i_126\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_126_n_0\
    );
\r[1]_INST_0_i_127\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"15555555EAAAAAAA"
    )
        port map (
      I0 => counterX_reg(4),
      I1 => counterX_reg(3),
      I2 => counterX_reg(1),
      I3 => counterX_reg(0),
      I4 => counterX_reg(2),
      I5 => counterX_reg(5),
      O => C(1)
    );
\r[1]_INST_0_i_128\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAA9555"
    )
        port map (
      I0 => counterX_reg(8),
      I1 => counterX_reg(7),
      I2 => \r[1]_INST_0_i_145_n_0\,
      I3 => counterX_reg(6),
      I4 => cur_blk_index0(4),
      O => \r[1]_INST_0_i_128_n_0\
    );
\r[1]_INST_0_i_129\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"99955555666AAAAA"
    )
        port map (
      I0 => counterX_reg(7),
      I1 => counterX_reg(6),
      I2 => \counterX[8]_i_2_n_0\,
      I3 => counterX_reg(4),
      I4 => counterX_reg(5),
      I5 => cur_blk_index0(3),
      O => \r[1]_INST_0_i_129_n_0\
    );
\r[1]_INST_0_i_13\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb45_out,
      CO(2) => \r[1]_INST_0_i_13_n_1\,
      CO(1) => \r[1]_INST_0_i_13_n_2\,
      CO(0) => \r[1]_INST_0_i_13_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_13_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_2_0\(2 downto 0),
      S(0) => \r[1]_INST_0_i_53_n_0\
    );
\r[1]_INST_0_i_130\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9995666A"
    )
        port map (
      I0 => counterX_reg(6),
      I1 => counterX_reg(5),
      I2 => counterX_reg(4),
      I3 => \counterX[8]_i_2_n_0\,
      I4 => cur_blk_index0(2),
      O => \r[1]_INST_0_i_130_n_0\
    );
\r[1]_INST_0_i_131\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999999969696"
    )
        port map (
      I0 => C(1),
      I1 => counterY_reg(4),
      I2 => counterY_reg(2),
      I3 => counterY_reg(1),
      I4 => counterY_reg(0),
      I5 => counterY_reg(3),
      O => \r[1]_INST_0_i_131_n_0\
    );
\r[1]_INST_0_i_132\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000111"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(7),
      I2 => counterY_reg(6),
      I3 => \r[1]_INST_0_i_147_n_0\,
      I4 => counterY_reg(8),
      O => A(6)
    );
\r[1]_INST_0_i_133\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000111"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(7),
      I2 => counterY_reg(6),
      I3 => \r[1]_INST_0_i_147_n_0\,
      I4 => counterY_reg(8),
      O => \r[1]_INST_0_i_133_n_0\
    );
\r[1]_INST_0_i_134\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAA999"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(7),
      I2 => counterY_reg(6),
      I3 => \r[1]_INST_0_i_147_n_0\,
      I4 => counterY_reg(8),
      O => A(5)
    );
\r[1]_INST_0_i_135\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAA888"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(7),
      I2 => counterY_reg(6),
      I3 => \r[1]_INST_0_i_147_n_0\,
      I4 => counterY_reg(8),
      O => \r[1]_INST_0_i_135_n_0\
    );
\r[1]_INST_0_i_136\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCCCC222"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(8),
      I2 => \r[1]_INST_0_i_147_n_0\,
      I3 => counterY_reg(6),
      I4 => counterY_reg(7),
      O => \r[1]_INST_0_i_136_n_0\
    );
\r[1]_INST_0_i_137\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5AA6A6A6"
    )
        port map (
      I0 => counterY_reg(9),
      I1 => counterY_reg(8),
      I2 => counterY_reg(7),
      I3 => counterY_reg(6),
      I4 => \r[1]_INST_0_i_147_n_0\,
      O => \r[1]_INST_0_i_137_n_0\
    );
\r[1]_INST_0_i_138\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA95"
    )
        port map (
      I0 => counterY_reg(8),
      I1 => \r[1]_INST_0_i_147_n_0\,
      I2 => counterY_reg(6),
      I3 => counterY_reg(7),
      O => A(4)
    );
\r[1]_INST_0_i_139\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"95"
    )
        port map (
      I0 => counterY_reg(7),
      I1 => counterY_reg(6),
      I2 => \r[1]_INST_0_i_147_n_0\,
      O => A(3)
    );
\r[1]_INST_0_i_14\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb34_out,
      CO(2) => \r[1]_INST_0_i_14_n_1\,
      CO(1) => \r[1]_INST_0_i_14_n_2\,
      CO(0) => \r[1]_INST_0_i_14_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_14_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_2_1\(2 downto 0),
      S(0) => \r[1]_INST_0_i_57_n_0\
    );
\r[1]_INST_0_i_140\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"11111115EEEEEEEA"
    )
        port map (
      I0 => counterY_reg(5),
      I1 => counterY_reg(4),
      I2 => counterY_reg(3),
      I3 => \counterY[6]_i_2_n_0\,
      I4 => counterY_reg(2),
      I5 => counterY_reg(6),
      O => A(2)
    );
\r[1]_INST_0_i_141\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A669"
    )
        port map (
      I0 => counterY_reg(8),
      I1 => counterY_reg(7),
      I2 => counterY_reg(6),
      I3 => \r[1]_INST_0_i_147_n_0\,
      O => \r[1]_INST_0_i_141_n_0\
    );
\r[1]_INST_0_i_142\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"699A"
    )
        port map (
      I0 => counterY_reg(7),
      I1 => counterY_reg(6),
      I2 => counterY_reg(5),
      I3 => \r[1]_INST_0_i_148_n_0\,
      O => \r[1]_INST_0_i_142_n_0\
    );
\r[1]_INST_0_i_143\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5959595959595996"
    )
        port map (
      I0 => counterY_reg(6),
      I1 => counterY_reg(5),
      I2 => counterY_reg(4),
      I3 => counterY_reg(2),
      I4 => \counterY[6]_i_2_n_0\,
      I5 => counterY_reg(3),
      O => \r[1]_INST_0_i_143_n_0\
    );
\r[1]_INST_0_i_144\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999999959595"
    )
        port map (
      I0 => counterY_reg(5),
      I1 => counterY_reg(4),
      I2 => counterY_reg(3),
      I3 => counterY_reg(0),
      I4 => counterY_reg(1),
      I5 => counterY_reg(2),
      O => A(1)
    );
\r[1]_INST_0_i_145\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A888888888888888"
    )
        port map (
      I0 => counterX_reg(5),
      I1 => counterX_reg(4),
      I2 => counterX_reg(3),
      I3 => counterX_reg(1),
      I4 => counterX_reg(0),
      I5 => counterX_reg(2),
      O => \r[1]_INST_0_i_145_n_0\
    );
\r[1]_INST_0_i_146\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEAAA00001555"
    )
        port map (
      I0 => counterX_reg(8),
      I1 => counterX_reg(7),
      I2 => \r[1]_INST_0_i_145_n_0\,
      I3 => counterX_reg(6),
      I4 => counterX_reg(9),
      I5 => cur_blk_index0(9),
      O => \r[1]_INST_0_i_146_n_0\
    );
\r[1]_INST_0_i_147\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFEA0000"
    )
        port map (
      I0 => counterY_reg(2),
      I1 => counterY_reg(1),
      I2 => counterY_reg(0),
      I3 => counterY_reg(3),
      I4 => counterY_reg(4),
      I5 => counterY_reg(5),
      O => \r[1]_INST_0_i_147_n_0\
    );
\r[1]_INST_0_i_148\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAA888"
    )
        port map (
      I0 => counterY_reg(4),
      I1 => counterY_reg(3),
      I2 => counterY_reg(0),
      I3 => counterY_reg(1),
      I4 => counterY_reg(2),
      O => \r[1]_INST_0_i_148_n_0\
    );
\r[1]_INST_0_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => rgb1415_out,
      I1 => rgb1314_out,
      I2 => rgb1617_out,
      I3 => rgb1516_out,
      O => \r[1]_INST_0_i_15_n_0\
    );
\r[1]_INST_0_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => rgb1011_out,
      I1 => rgb910_out,
      I2 => rgb1213_out,
      I3 => rgb1112_out,
      O => \r[1]_INST_0_i_16_n_0\
    );
\r[1]_INST_0_i_17\: unisim.vcomponents.CARRY4
     port map (
      CI => \r[1]_INST_0_i_66_n_0\,
      CO(3) => \r[1]_INST_0_i_17_n_0\,
      CO(2) => \r[1]_INST_0_i_17_n_1\,
      CO(1) => \r[1]_INST_0_i_17_n_2\,
      CO(0) => \r[1]_INST_0_i_17_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => cur_blk_index0(8 downto 5),
      O(3 downto 0) => \^counterx_reg[8]_0\(3 downto 0),
      S(3) => \r[1]_INST_0_i_69_n_0\,
      S(2) => \r[1]_INST_0_i_70_n_0\,
      S(1) => \r[1]_INST_0_i_71_n_0\,
      S(0) => \r[1]_INST_0_i_72_n_0\
    );
\r[1]_INST_0_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1800042000002242"
    )
        port map (
      I0 => \^counterx_reg[8]_0\(0),
      I1 => \^o\(1),
      I2 => \^o\(0),
      I3 => \cur_blk_index__0\(1),
      I4 => \counterX[4]_i_1_n_0\,
      I5 => \cur_blk_index__0\(2),
      O => \r[1]_INST_0_i_18_n_0\
    );
\r[1]_INST_0_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"11111115FFFFFFFF"
    )
        port map (
      I0 => counterY_reg(6),
      I1 => counterY_reg(5),
      I2 => counterY_reg(3),
      I3 => counterY_reg(4),
      I4 => counterY_reg(2),
      I5 => counterY_reg(7),
      O => \r[1]_INST_0_i_19_n_0\
    );
\r[1]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \r[1]_INST_0_i_11_n_0\,
      I1 => rgb1618_out,
      I2 => rgb45_out,
      I3 => rgb34_out,
      I4 => \r[1]_INST_0_i_15_n_0\,
      I5 => \r[1]_INST_0_i_16_n_0\,
      O => \r[1]_INST_0_i_2_n_0\
    );
\r[1]_INST_0_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAAAAAAAAA"
    )
        port map (
      I0 => counterX_reg(9),
      I1 => counterX_reg(8),
      I2 => counterX_reg(6),
      I3 => counterX_reg(4),
      I4 => counterX_reg(5),
      I5 => counterX_reg(7),
      O => rgb43_in
    );
\r[1]_INST_0_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000055555557"
    )
        port map (
      I0 => \r[1]_INST_0_i_74_n_0\,
      I1 => counterX_reg(2),
      I2 => counterX_reg(3),
      I3 => counterX_reg(1),
      I4 => counterX_reg(0),
      I5 => counterX_reg(9),
      O => \r[1]_INST_0_i_21_n_0\
    );
\r[1]_INST_0_i_22\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEEE"
    )
        port map (
      I0 => counterY_reg(8),
      I1 => counterY_reg(7),
      I2 => counterY_reg(6),
      I3 => \r[1]_INST_0_i_75_n_0\,
      I4 => counterY_reg(9),
      O => rgb3
    );
\r[1]_INST_0_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1555FFFFFFFFFFFF"
    )
        port map (
      I0 => counterY_reg(6),
      I1 => counterY_reg(5),
      I2 => counterY_reg(4),
      I3 => \r[1]_INST_0_i_76_n_0\,
      I4 => counterY_reg(8),
      I5 => counterY_reg(7),
      O => \r[1]_INST_0_i_23_n_0\
    );
\r[1]_INST_0_i_24\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFAAEA"
    )
        port map (
      I0 => counterX_reg(8),
      I1 => counterX_reg(6),
      I2 => counterX_reg(5),
      I3 => \r[1]_INST_0_i_77_n_0\,
      I4 => counterX_reg(9),
      O => rgb40_in
    );
\r[1]_INST_0_i_25\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001FFFFFF"
    )
        port map (
      I0 => counterX_reg(4),
      I1 => counterX_reg(6),
      I2 => counterX_reg(5),
      I3 => counterX_reg(8),
      I4 => counterX_reg(7),
      I5 => counterX_reg(9),
      O => \r[1]_INST_0_i_25_n_0\
    );
\r[1]_INST_0_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008001000005541"
    )
        port map (
      I0 => \^o\(0),
      I1 => Q(2),
      I2 => Q(0),
      I3 => Q(1),
      I4 => \^counterx_reg[8]_0\(0),
      I5 => \^o\(1),
      O => \r[1]_INST_0_i_28_n_0\
    );
\r[1]_INST_0_i_29\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5448000000100201"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => Q(1),
      I2 => Q(2),
      I3 => Q(0),
      I4 => \cur_blk_index__0\(2),
      I5 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_29_n_0\
    );
\r[1]_INST_0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \^counterx_reg[8]_0\(1),
      I1 => \r[1]_INST_0_i_18_n_0\,
      I2 => \^counterx_reg[8]_0\(2),
      O => \r[1]_INST_0_i_3_n_0\
    );
\r[1]_INST_0_i_31\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000A699"
    )
        port map (
      I0 => \^counterx_reg[8]_0\(1),
      I1 => Q(2),
      I2 => Q(1),
      I3 => Q(0),
      I4 => \^counterx_reg[8]_0\(3),
      I5 => \^counterx_reg[8]_0\(2),
      O => \r[1]_INST_0_i_31_n_0\
    );
\r[1]_INST_0_i_32\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0820000000005145"
    )
        port map (
      I0 => \^o\(0),
      I1 => Q(0),
      I2 => Q(1),
      I3 => Q(2),
      I4 => \^counterx_reg[8]_0\(0),
      I5 => \^o\(1),
      O => \r[1]_INST_0_i_32_n_0\
    );
\r[1]_INST_0_i_33\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8AA0000010040001"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => Q(1),
      I2 => Q(0),
      I3 => Q(2),
      I4 => \cur_blk_index__0\(2),
      I5 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_33_n_0\
    );
\r[1]_INST_0_i_35\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000005559"
    )
        port map (
      I0 => \^counterx_reg[8]_0\(1),
      I1 => Q(2),
      I2 => Q(1),
      I3 => Q(0),
      I4 => \^counterx_reg[8]_0\(3),
      I5 => \^counterx_reg[8]_0\(2),
      O => \r[1]_INST_0_i_35_n_0\
    );
\r[1]_INST_0_i_36\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8000001"
    )
        port map (
      I0 => \^o\(0),
      I1 => Q(0),
      I2 => Q(1),
      I3 => \^counterx_reg[8]_0\(0),
      I4 => \^o\(1),
      O => \r[1]_INST_0_i_36_n_0\
    );
\r[1]_INST_0_i_37\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8100000028680001"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => Q(1),
      I2 => Q(0),
      I3 => Q(2),
      I4 => \cur_blk_index__0\(2),
      I5 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_37_n_0\
    );
\r[1]_INST_0_i_39\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000A9"
    )
        port map (
      I0 => \^counterx_reg[8]_0\(1),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \^counterx_reg[8]_0\(3),
      I4 => \^counterx_reg[8]_0\(2),
      O => \r[1]_INST_0_i_39_n_0\
    );
\r[1]_INST_0_i_40\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"020000080000A891"
    )
        port map (
      I0 => \^o\(0),
      I1 => Q(0),
      I2 => Q(1),
      I3 => Q(2),
      I4 => \^counterx_reg[8]_0\(0),
      I5 => \^o\(1),
      O => \r[1]_INST_0_i_40_n_0\
    );
\r[1]_INST_0_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"021000080000A881"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => Q(0),
      I2 => Q(1),
      I3 => Q(2),
      I4 => \cur_blk_index__0\(2),
      I5 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_41_n_0\
    );
\r[1]_INST_0_i_42\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb67_out,
      CO(2) => \r[1]_INST_0_i_42_n_1\,
      CO(1) => \r[1]_INST_0_i_42_n_2\,
      CO(0) => \r[1]_INST_0_i_42_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_42_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_11_2\(2 downto 0),
      S(0) => \r[1]_INST_0_i_82_n_0\
    );
\r[1]_INST_0_i_43\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb56_out,
      CO(2) => \r[1]_INST_0_i_43_n_1\,
      CO(1) => \r[1]_INST_0_i_43_n_2\,
      CO(0) => \r[1]_INST_0_i_43_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_43_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_11_3\(2 downto 0),
      S(0) => \r[1]_INST_0_i_86_n_0\
    );
\r[1]_INST_0_i_44\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb89_out,
      CO(2) => \r[1]_INST_0_i_44_n_1\,
      CO(1) => \r[1]_INST_0_i_44_n_2\,
      CO(0) => \r[1]_INST_0_i_44_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_44_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_11_0\(2 downto 0),
      S(0) => \r[1]_INST_0_i_90_n_0\
    );
\r[1]_INST_0_i_45\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb78_out,
      CO(2) => \r[1]_INST_0_i_45_n_1\,
      CO(1) => \r[1]_INST_0_i_45_n_2\,
      CO(0) => \r[1]_INST_0_i_45_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_45_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_11_1\(2 downto 0),
      S(0) => \r[1]_INST_0_i_94_n_0\
    );
\r[1]_INST_0_i_49\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \cur_blk_index__0\(2),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_49_n_0\
    );
\r[1]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0200000000000000"
    )
        port map (
      I0 => \r[1]_INST_0_i_19_n_0\,
      I1 => counterY_reg(9),
      I2 => counterY_reg(8),
      I3 => rgb43_in,
      I4 => \r[1]_INST_0_i_21_n_0\,
      I5 => rgb3,
      O => \r[1]_INST_0_i_5_n_0\
    );
\r[1]_INST_0_i_53\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(2),
      I2 => \cur_blk_index__0\(1),
      O => \r[1]_INST_0_i_53_n_0\
    );
\r[1]_INST_0_i_57\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_57_n_0\
    );
\r[1]_INST_0_i_58\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1415_out,
      CO(2) => \r[1]_INST_0_i_58_n_1\,
      CO(1) => \r[1]_INST_0_i_58_n_2\,
      CO(0) => \r[1]_INST_0_i_58_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_58_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_15_2\(2 downto 0),
      S(0) => \r[1]_INST_0_i_98_n_0\
    );
\r[1]_INST_0_i_59\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1314_out,
      CO(2) => \r[1]_INST_0_i_59_n_1\,
      CO(1) => \r[1]_INST_0_i_59_n_2\,
      CO(0) => \r[1]_INST_0_i_59_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_59_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_15_3\(2 downto 0),
      S(0) => \r[1]_INST_0_i_102_n_0\
    );
\r[1]_INST_0_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \r[1]_INST_0_i_23_n_0\,
      I1 => counterY_reg(9),
      I2 => rgb40_in,
      I3 => \r[1]_INST_0_i_25_n_0\,
      I4 => rgb3,
      O => \r[1]_INST_0_i_6_n_0\
    );
\r[1]_INST_0_i_60\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1617_out,
      CO(2) => \r[1]_INST_0_i_60_n_1\,
      CO(1) => \r[1]_INST_0_i_60_n_2\,
      CO(0) => \r[1]_INST_0_i_60_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_60_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_15_0\(2 downto 0),
      S(0) => \r[1]_INST_0_i_106_n_0\
    );
\r[1]_INST_0_i_61\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1516_out,
      CO(2) => \r[1]_INST_0_i_61_n_1\,
      CO(1) => \r[1]_INST_0_i_61_n_2\,
      CO(0) => \r[1]_INST_0_i_61_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_61_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_15_1\(2 downto 0),
      S(0) => \r[1]_INST_0_i_110_n_0\
    );
\r[1]_INST_0_i_62\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1011_out,
      CO(2) => \r[1]_INST_0_i_62_n_1\,
      CO(1) => \r[1]_INST_0_i_62_n_2\,
      CO(0) => \r[1]_INST_0_i_62_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_62_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_16_2\(2 downto 0),
      S(0) => \r[1]_INST_0_i_114_n_0\
    );
\r[1]_INST_0_i_63\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb910_out,
      CO(2) => \r[1]_INST_0_i_63_n_1\,
      CO(1) => \r[1]_INST_0_i_63_n_2\,
      CO(0) => \r[1]_INST_0_i_63_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_63_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_16_3\(2 downto 0),
      S(0) => \r[1]_INST_0_i_118_n_0\
    );
\r[1]_INST_0_i_64\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1213_out,
      CO(2) => \r[1]_INST_0_i_64_n_1\,
      CO(1) => \r[1]_INST_0_i_64_n_2\,
      CO(0) => \r[1]_INST_0_i_64_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_64_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_16_0\(2 downto 0),
      S(0) => \r[1]_INST_0_i_122_n_0\
    );
\r[1]_INST_0_i_65\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb1112_out,
      CO(2) => \r[1]_INST_0_i_65_n_1\,
      CO(1) => \r[1]_INST_0_i_65_n_2\,
      CO(0) => \r[1]_INST_0_i_65_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_65_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => \r[1]_INST_0_i_16_1\(2 downto 0),
      S(0) => \r[1]_INST_0_i_126_n_0\
    );
\r[1]_INST_0_i_66\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \r[1]_INST_0_i_66_n_0\,
      CO(2) => \r[1]_INST_0_i_66_n_1\,
      CO(1) => \r[1]_INST_0_i_66_n_2\,
      CO(0) => \r[1]_INST_0_i_66_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => cur_blk_index0(4 downto 2),
      DI(0) => C(1),
      O(3 downto 2) => \^o\(1 downto 0),
      O(1) => \cur_blk_index__0\(2),
      O(0) => \NLW_r[1]_INST_0_i_66_O_UNCONNECTED\(0),
      S(3) => \r[1]_INST_0_i_128_n_0\,
      S(2) => \r[1]_INST_0_i_129_n_0\,
      S(1) => \r[1]_INST_0_i_130_n_0\,
      S(0) => \r[1]_INST_0_i_131_n_0\
    );
\r[1]_INST_0_i_67\: unisim.vcomponents.CARRY4
     port map (
      CI => \r[1]_INST_0_i_68_n_0\,
      CO(3) => cur_blk_index0(9),
      CO(2) => \NLW_r[1]_INST_0_i_67_CO_UNCONNECTED\(2),
      CO(1) => \r[1]_INST_0_i_67_n_2\,
      CO(0) => \r[1]_INST_0_i_67_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => A(6),
      DI(1) => \r[1]_INST_0_i_133_n_0\,
      DI(0) => A(5),
      O(3) => \NLW_r[1]_INST_0_i_67_O_UNCONNECTED\(3),
      O(2 downto 0) => cur_blk_index0(8 downto 6),
      S(3) => '1',
      S(2) => \r[1]_INST_0_i_135_n_0\,
      S(1) => \r[1]_INST_0_i_136_n_0\,
      S(0) => \r[1]_INST_0_i_137_n_0\
    );
\r[1]_INST_0_i_68\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \r[1]_INST_0_i_68_n_0\,
      CO(2) => \r[1]_INST_0_i_68_n_1\,
      CO(1) => \r[1]_INST_0_i_68_n_2\,
      CO(0) => \r[1]_INST_0_i_68_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => A(4 downto 2),
      DI(0) => '0',
      O(3 downto 0) => cur_blk_index0(5 downto 2),
      S(3) => \r[1]_INST_0_i_141_n_0\,
      S(2) => \r[1]_INST_0_i_142_n_0\,
      S(1) => \r[1]_INST_0_i_143_n_0\,
      S(0) => A(1)
    );
\r[1]_INST_0_i_69\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEAAA00001555"
    )
        port map (
      I0 => counterX_reg(8),
      I1 => counterX_reg(7),
      I2 => \r[1]_INST_0_i_145_n_0\,
      I3 => counterX_reg(6),
      I4 => counterX_reg(9),
      I5 => cur_blk_index0(8),
      O => \r[1]_INST_0_i_69_n_0\
    );
\r[1]_INST_0_i_7\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \r[1]_INST_0_i_7_n_0\,
      CO(2) => \r[1]_INST_0_i_7_n_1\,
      CO(1) => \r[1]_INST_0_i_7_n_2\,
      CO(0) => \r[1]_INST_0_i_7_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_7_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => \r[1]_INST_0_i_1_1\(1 downto 0),
      S(1) => \r[1]_INST_0_i_28_n_0\,
      S(0) => \r[1]_INST_0_i_29_n_0\
    );
\r[1]_INST_0_i_70\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEAAA00001555"
    )
        port map (
      I0 => counterX_reg(8),
      I1 => counterX_reg(7),
      I2 => \r[1]_INST_0_i_145_n_0\,
      I3 => counterX_reg(6),
      I4 => counterX_reg(9),
      I5 => cur_blk_index0(7),
      O => \r[1]_INST_0_i_70_n_0\
    );
\r[1]_INST_0_i_71\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEAAA00001555"
    )
        port map (
      I0 => counterX_reg(8),
      I1 => counterX_reg(7),
      I2 => \r[1]_INST_0_i_145_n_0\,
      I3 => counterX_reg(6),
      I4 => counterX_reg(9),
      I5 => cur_blk_index0(6),
      O => \r[1]_INST_0_i_71_n_0\
    );
\r[1]_INST_0_i_72\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55556AAAAAAA9555"
    )
        port map (
      I0 => counterX_reg(9),
      I1 => counterX_reg(6),
      I2 => \r[1]_INST_0_i_145_n_0\,
      I3 => counterX_reg(7),
      I4 => counterX_reg(8),
      I5 => cur_blk_index0(5),
      O => \r[1]_INST_0_i_72_n_0\
    );
\r[1]_INST_0_i_73\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999999999969696"
    )
        port map (
      I0 => C(1),
      I1 => counterY_reg(4),
      I2 => counterY_reg(2),
      I3 => counterY_reg(1),
      I4 => counterY_reg(0),
      I5 => counterY_reg(3),
      O => \cur_blk_index__0\(1)
    );
\r[1]_INST_0_i_74\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => counterX_reg(4),
      I1 => counterX_reg(7),
      I2 => counterX_reg(8),
      I3 => counterX_reg(5),
      I4 => counterX_reg(6),
      O => \r[1]_INST_0_i_74_n_0\
    );
\r[1]_INST_0_i_75\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEAAAAAAAA"
    )
        port map (
      I0 => counterY_reg(5),
      I1 => counterY_reg(2),
      I2 => counterY_reg(1),
      I3 => counterY_reg(0),
      I4 => counterY_reg(3),
      I5 => counterY_reg(4),
      O => \r[1]_INST_0_i_75_n_0\
    );
\r[1]_INST_0_i_76\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => counterY_reg(2),
      I1 => counterY_reg(3),
      O => \r[1]_INST_0_i_76_n_0\
    );
\r[1]_INST_0_i_77\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555555557FFFFFFF"
    )
        port map (
      I0 => counterX_reg(7),
      I1 => counterX_reg(2),
      I2 => counterX_reg(1),
      I3 => counterX_reg(0),
      I4 => counterX_reg(3),
      I5 => counterX_reg(4),
      O => \r[1]_INST_0_i_77_n_0\
    );
\r[1]_INST_0_i_78\: unisim.vcomponents.CARRY4
     port map (
      CI => \r[1]_INST_0_i_17_n_0\,
      CO(3 downto 0) => \NLW_r[1]_INST_0_i_78_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_r[1]_INST_0_i_78_O_UNCONNECTED\(3 downto 1),
      O(0) => cur_blk_index(0),
      S(3 downto 1) => B"000",
      S(0) => \r[1]_INST_0_i_146_n_0\
    );
\r[1]_INST_0_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb4,
      CO(2) => \r[1]_INST_0_i_8_n_1\,
      CO(1) => \r[1]_INST_0_i_8_n_2\,
      CO(0) => \r[1]_INST_0_i_8_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_8_O_UNCONNECTED\(3 downto 0),
      S(3) => \r[1]_INST_0_i_1_0\(0),
      S(2) => \r[1]_INST_0_i_31_n_0\,
      S(1) => \r[1]_INST_0_i_32_n_0\,
      S(0) => \r[1]_INST_0_i_33_n_0\
    );
\r[1]_INST_0_i_82\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \cur_blk_index__0\(2),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_82_n_0\
    );
\r[1]_INST_0_i_86\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(1),
      I1 => \cur_blk_index__0\(2),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_86_n_0\
    );
\r[1]_INST_0_i_9\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => rgb40_out,
      CO(2) => \r[1]_INST_0_i_9_n_1\,
      CO(1) => \r[1]_INST_0_i_9_n_2\,
      CO(0) => \r[1]_INST_0_i_9_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_r[1]_INST_0_i_9_O_UNCONNECTED\(3 downto 0),
      S(3) => \r[1]_INST_0_i_1_3\(0),
      S(2) => \r[1]_INST_0_i_35_n_0\,
      S(1) => \r[1]_INST_0_i_36_n_0\,
      S(0) => \r[1]_INST_0_i_37_n_0\
    );
\r[1]_INST_0_i_90\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_90_n_0\
    );
\r[1]_INST_0_i_94\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \counterX[4]_i_1_n_0\,
      I1 => \cur_blk_index__0\(1),
      I2 => \cur_blk_index__0\(2),
      O => \r[1]_INST_0_i_94_n_0\
    );
\r[1]_INST_0_i_98\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \cur_blk_index__0\(2),
      I1 => \cur_blk_index__0\(1),
      I2 => \counterX[4]_i_1_n_0\,
      O => \r[1]_INST_0_i_98_n_0\
    );
\r[2]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000C0C0AA00"
    )
        port map (
      I0 => \r[1]_INST_0_i_1_n_0\,
      I1 => \r[1]_INST_0_i_2_n_0\,
      I2 => \r[1]_INST_0_i_3_n_0\,
      I3 => r_2_sn_1,
      I4 => \r[1]_INST_0_i_5_n_0\,
      I5 => \r[1]_INST_0_i_6_n_0\,
      O => r(1)
    );
v_enabled_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => \counterX[9]_i_1_n_0\,
      Q => v_enabled,
      R => '0'
    );
vs_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => vs_INST_0_i_1_n_0,
      I1 => counterY_reg(3),
      I2 => counterY_reg(2),
      I3 => counterY_reg(5),
      I4 => counterY_reg(4),
      I5 => counterY_reg(1),
      O => vs
    );
vs_INST_0_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => counterY_reg(7),
      I1 => counterY_reg(6),
      I2 => counterY_reg(9),
      I3 => counterY_reg(8),
      O => vs_INST_0_i_1_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_tetris is
  port (
    sw_rst_o : out STD_LOGIC;
    sw_pause_o : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \counterX_reg[8]\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    cur_blk_index : out STD_LOGIC_VECTOR ( 1 downto 0 );
    r : out STD_LOGIC_VECTOR ( 2 downto 0 );
    vs : out STD_LOGIC;
    b : out STD_LOGIC_VECTOR ( 1 downto 0 );
    hs : out STD_LOGIC;
    g : out STD_LOGIC_VECTOR ( 3 downto 0 );
    down_o : out STD_LOGIC;
    left_o : out STD_LOGIC;
    right_o : out STD_LOGIC;
    rotate_o : out STD_LOGIC;
    drop_o : out STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    S : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_18\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_18_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_18_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_19\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_19_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_19_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_19_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_14\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_14_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_14_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_14_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_8\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_8_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \g[3]_INST_0_i_8_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_15\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_15_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_15_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_15_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_16\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_16_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_16_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_16_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_11\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_11_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_11_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_11_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_2_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_2_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \r[1]_INST_0_i_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \r[1]_INST_0_i_1_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \r[1]_INST_0_i_1_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \r[1]_INST_0_i_1_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    btn_down : in STD_LOGIC;
    btn_left : in STD_LOGIC;
    btn_right : in STD_LOGIC;
    btn_rotate : in STD_LOGIC;
    btn_drop : in STD_LOGIC;
    sw_rst : in STD_LOGIC;
    sw_pause : in STD_LOGIC;
    r_2_sp_1 : in STD_LOGIC;
    g_1_sp_1 : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_tetris;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_tetris is
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal g_1_sn_1 : STD_LOGIC;
  signal next_tetris : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal r_2_sn_1 : STD_LOGIC;
begin
  Q(2 downto 0) <= \^q\(2 downto 0);
  g_1_sn_1 <= g_1_sp_1;
  r_2_sn_1 <= r_2_sp_1;
GetRandomPiece_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_GetRandomPiece
     port map (
      Q(2 downto 0) => next_tetris(2 downto 0),
      pixel_clk => pixel_clk
    );
debounce_down: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce
     port map (
      btn_down => btn_down,
      down_o => down_o,
      pixel_clk => pixel_clk
    );
debounce_drop: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_0
     port map (
      btn_drop => btn_drop,
      drop_o => drop_o,
      pixel_clk => pixel_clk
    );
debounce_left: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_1
     port map (
      btn_left => btn_left,
      left_o => left_o,
      pixel_clk => pixel_clk
    );
debounce_right: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_2
     port map (
      btn_right => btn_right,
      pixel_clk => pixel_clk,
      right_o => right_o
    );
debounce_rotate: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_3
     port map (
      btn_rotate => btn_rotate,
      pixel_clk => pixel_clk,
      rotate_o => rotate_o
    );
debounce_sw_pause: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_4
     port map (
      pixel_clk => pixel_clk,
      sw_pause => sw_pause,
      sw_pause_o => sw_pause_o
    );
debounce_sw_rst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_debounce_5
     port map (
      pixel_clk => pixel_clk,
      sw_rst => sw_rst,
      sw_rst_o => sw_rst_o
    );
\next_tetris_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '0',
      D => '0',
      Q => \^q\(0),
      R => '0'
    );
\next_tetris_reg_reg[0]__0\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '0',
      D => next_tetris(0),
      Q => \^q\(0),
      R => '0'
    );
\next_tetris_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '0',
      D => '0',
      Q => \^q\(1),
      R => '0'
    );
\next_tetris_reg_reg[1]__0\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '0',
      D => next_tetris(1),
      Q => \^q\(1),
      R => '0'
    );
\next_tetris_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '0',
      D => '0',
      Q => \^q\(2),
      R => '0'
    );
\next_tetris_reg_reg[2]__0\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '0',
      D => next_tetris(2),
      Q => \^q\(2),
      R => '0'
    );
vga_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga
     port map (
      O(1 downto 0) => \counterX_reg[8]\(1 downto 0),
      Q(2 downto 0) => \^q\(2 downto 0),
      S(2 downto 0) => S(2 downto 0),
      b(1 downto 0) => b(1 downto 0),
      \counterX_reg[8]_0\(3) => cur_blk_index(0),
      \counterX_reg[8]_0\(2 downto 0) => \counterX_reg[8]\(4 downto 2),
      cur_blk_index(0) => cur_blk_index(1),
      g(3 downto 0) => g(3 downto 0),
      \g[3]_INST_0_i_14_0\(2 downto 0) => \g[3]_INST_0_i_14\(2 downto 0),
      \g[3]_INST_0_i_14_1\(2 downto 0) => \g[3]_INST_0_i_14_0\(2 downto 0),
      \g[3]_INST_0_i_14_2\(2 downto 0) => \g[3]_INST_0_i_14_1\(2 downto 0),
      \g[3]_INST_0_i_14_3\(2 downto 0) => \g[3]_INST_0_i_14_2\(2 downto 0),
      \g[3]_INST_0_i_18_0\(2 downto 0) => \g[3]_INST_0_i_18\(2 downto 0),
      \g[3]_INST_0_i_18_1\(2 downto 0) => \g[3]_INST_0_i_18_0\(2 downto 0),
      \g[3]_INST_0_i_18_2\(2 downto 0) => \g[3]_INST_0_i_18_1\(2 downto 0),
      \g[3]_INST_0_i_19_0\(2 downto 0) => \g[3]_INST_0_i_19\(2 downto 0),
      \g[3]_INST_0_i_19_1\(2 downto 0) => \g[3]_INST_0_i_19_0\(2 downto 0),
      \g[3]_INST_0_i_19_2\(2 downto 0) => \g[3]_INST_0_i_19_1\(2 downto 0),
      \g[3]_INST_0_i_19_3\(2 downto 0) => \g[3]_INST_0_i_19_2\(2 downto 0),
      \g[3]_INST_0_i_8_0\(2 downto 0) => \g[3]_INST_0_i_8\(2 downto 0),
      \g[3]_INST_0_i_8_1\(2 downto 0) => \g[3]_INST_0_i_8_0\(2 downto 0),
      \g[3]_INST_0_i_8_2\(2 downto 0) => \g[3]_INST_0_i_8_1\(2 downto 0),
      g_1_sp_1 => g_1_sn_1,
      hs => hs,
      pixel_clk => pixel_clk,
      r(2 downto 0) => r(2 downto 0),
      \r[1]_INST_0_i_11_0\(2 downto 0) => \r[1]_INST_0_i_11\(2 downto 0),
      \r[1]_INST_0_i_11_1\(2 downto 0) => \r[1]_INST_0_i_11_0\(2 downto 0),
      \r[1]_INST_0_i_11_2\(2 downto 0) => \r[1]_INST_0_i_11_1\(2 downto 0),
      \r[1]_INST_0_i_11_3\(2 downto 0) => \r[1]_INST_0_i_11_2\(2 downto 0),
      \r[1]_INST_0_i_15_0\(2 downto 0) => \r[1]_INST_0_i_15\(2 downto 0),
      \r[1]_INST_0_i_15_1\(2 downto 0) => \r[1]_INST_0_i_15_0\(2 downto 0),
      \r[1]_INST_0_i_15_2\(2 downto 0) => \r[1]_INST_0_i_15_1\(2 downto 0),
      \r[1]_INST_0_i_15_3\(2 downto 0) => \r[1]_INST_0_i_15_2\(2 downto 0),
      \r[1]_INST_0_i_16_0\(2 downto 0) => \r[1]_INST_0_i_16\(2 downto 0),
      \r[1]_INST_0_i_16_1\(2 downto 0) => \r[1]_INST_0_i_16_0\(2 downto 0),
      \r[1]_INST_0_i_16_2\(2 downto 0) => \r[1]_INST_0_i_16_1\(2 downto 0),
      \r[1]_INST_0_i_16_3\(2 downto 0) => \r[1]_INST_0_i_16_2\(2 downto 0),
      \r[1]_INST_0_i_1_0\(0) => \r[1]_INST_0_i_1\(0),
      \r[1]_INST_0_i_1_1\(1 downto 0) => \r[1]_INST_0_i_1_0\(1 downto 0),
      \r[1]_INST_0_i_1_2\(0) => \r[1]_INST_0_i_1_1\(0),
      \r[1]_INST_0_i_1_3\(0) => \r[1]_INST_0_i_1_2\(0),
      \r[1]_INST_0_i_2_0\(2 downto 0) => \r[1]_INST_0_i_2\(2 downto 0),
      \r[1]_INST_0_i_2_1\(2 downto 0) => \r[1]_INST_0_i_2_0\(2 downto 0),
      \r[1]_INST_0_i_2_2\(2 downto 0) => \r[1]_INST_0_i_2_1\(2 downto 0),
      r_2_sp_1 => r_2_sn_1,
      vs => vs
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk : in STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_down : in STD_LOGIC;
    btn_left : in STD_LOGIC;
    btn_right : in STD_LOGIC;
    btn_rotate : in STD_LOGIC;
    btn_drop : in STD_LOGIC;
    sw_rst : in STD_LOGIC;
    sw_pause : in STD_LOGIC;
    rotate_o : out STD_LOGIC;
    down_o : out STD_LOGIC;
    left_o : out STD_LOGIC;
    right_o : out STD_LOGIC;
    drop_o : out STD_LOGIC;
    sw_rst_o : out STD_LOGIC;
    sw_pause_o : out STD_LOGIC;
    r : out STD_LOGIC_VECTOR ( 3 downto 0 );
    g : out STD_LOGIC_VECTOR ( 3 downto 0 );
    b : out STD_LOGIC_VECTOR ( 3 downto 0 );
    hs : out STD_LOGIC;
    vs : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "tetris_tetris_0_0,tetris,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "package_project";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "tetris,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \^b\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \g[1]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_24_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_25_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_26_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_28_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_29_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_30_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_32_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_33_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_34_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_44_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_45_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_46_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_48_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_49_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_50_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_52_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_53_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_54_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_56_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_57_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_58_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_60_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_61_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_62_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_64_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_65_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_66_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_68_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_69_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_70_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_72_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_73_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_74_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_76_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_77_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_78_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_80_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_81_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_82_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_84_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_85_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_86_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_88_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_89_n_0\ : STD_LOGIC;
  signal \g[3]_INST_0_i_90_n_0\ : STD_LOGIC;
  signal next_tetris_reg : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^r\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \r[1]_INST_0_i_100_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_101_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_103_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_104_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_105_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_107_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_108_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_109_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_111_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_112_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_113_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_115_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_116_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_117_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_119_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_120_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_121_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_123_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_124_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_125_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_26_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_27_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_30_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_34_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_38_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_46_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_47_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_48_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_50_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_51_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_52_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_54_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_55_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_56_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_79_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_80_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_81_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_83_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_84_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_85_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_87_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_88_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_89_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_91_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_92_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_93_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_95_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_96_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_97_n_0\ : STD_LOGIC;
  signal \r[1]_INST_0_i_99_n_0\ : STD_LOGIC;
  signal \vga_inst/cur_blk_index\ : STD_LOGIC_VECTOR ( 9 downto 8 );
  signal \vga_inst/cur_blk_index__0\ : STD_LOGIC_VECTOR ( 7 downto 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \g[1]_INST_0_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \r[1]_INST_0_i_4\ : label is "soft_lutpair20";
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 pixel_clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME pixel_clk, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN tetris_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of pixel_clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER of pixel_clk : signal is "XIL_INTERFACENAME clk, FREQ_HZ 25173010, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of sw_rst : signal is "xilinx.com:signal:reset:1.0 sw_rst RST";
  attribute X_INTERFACE_PARAMETER of sw_rst : signal is "XIL_INTERFACENAME sw_rst, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  b(3) <= \^b\(0);
  b(2) <= \^b\(0);
  b(1 downto 0) <= \^b\(1 downto 0);
  r(3) <= \^r\(1);
  r(2 downto 0) <= \^r\(2 downto 0);
\g[1]_INST_0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => next_tetris_reg(0),
      I1 => next_tetris_reg(1),
      O => \g[1]_INST_0_i_1_n_0\
    );
\g[3]_INST_0_i_24\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_24_n_0\
    );
\g[3]_INST_0_i_25\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_25_n_0\
    );
\g[3]_INST_0_i_26\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \g[3]_INST_0_i_26_n_0\
    );
\g[3]_INST_0_i_28\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_28_n_0\
    );
\g[3]_INST_0_i_29\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_29_n_0\
    );
\g[3]_INST_0_i_30\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_30_n_0\
    );
\g[3]_INST_0_i_32\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_32_n_0\
    );
\g[3]_INST_0_i_33\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_33_n_0\
    );
\g[3]_INST_0_i_34\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_34_n_0\
    );
\g[3]_INST_0_i_44\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_44_n_0\
    );
\g[3]_INST_0_i_45\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_45_n_0\
    );
\g[3]_INST_0_i_46\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \g[3]_INST_0_i_46_n_0\
    );
\g[3]_INST_0_i_48\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_48_n_0\
    );
\g[3]_INST_0_i_49\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_49_n_0\
    );
\g[3]_INST_0_i_50\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_50_n_0\
    );
\g[3]_INST_0_i_52\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_52_n_0\
    );
\g[3]_INST_0_i_53\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_53_n_0\
    );
\g[3]_INST_0_i_54\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \g[3]_INST_0_i_54_n_0\
    );
\g[3]_INST_0_i_56\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_56_n_0\
    );
\g[3]_INST_0_i_57\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_57_n_0\
    );
\g[3]_INST_0_i_58\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \g[3]_INST_0_i_58_n_0\
    );
\g[3]_INST_0_i_60\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_60_n_0\
    );
\g[3]_INST_0_i_61\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_61_n_0\
    );
\g[3]_INST_0_i_62\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_62_n_0\
    );
\g[3]_INST_0_i_64\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_64_n_0\
    );
\g[3]_INST_0_i_65\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_65_n_0\
    );
\g[3]_INST_0_i_66\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_66_n_0\
    );
\g[3]_INST_0_i_68\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_68_n_0\
    );
\g[3]_INST_0_i_69\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_69_n_0\
    );
\g[3]_INST_0_i_70\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \g[3]_INST_0_i_70_n_0\
    );
\g[3]_INST_0_i_72\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_72_n_0\
    );
\g[3]_INST_0_i_73\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_73_n_0\
    );
\g[3]_INST_0_i_74\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \g[3]_INST_0_i_74_n_0\
    );
\g[3]_INST_0_i_76\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_76_n_0\
    );
\g[3]_INST_0_i_77\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_77_n_0\
    );
\g[3]_INST_0_i_78\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \g[3]_INST_0_i_78_n_0\
    );
\g[3]_INST_0_i_80\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_80_n_0\
    );
\g[3]_INST_0_i_81\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_81_n_0\
    );
\g[3]_INST_0_i_82\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_82_n_0\
    );
\g[3]_INST_0_i_84\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_84_n_0\
    );
\g[3]_INST_0_i_85\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_85_n_0\
    );
\g[3]_INST_0_i_86\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \g[3]_INST_0_i_86_n_0\
    );
\g[3]_INST_0_i_88\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \g[3]_INST_0_i_88_n_0\
    );
\g[3]_INST_0_i_89\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \g[3]_INST_0_i_89_n_0\
    );
\g[3]_INST_0_i_90\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \g[3]_INST_0_i_90_n_0\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_tetris
     port map (
      Q(2 downto 0) => next_tetris_reg(2 downto 0),
      S(2) => \g[3]_INST_0_i_68_n_0\,
      S(1) => \g[3]_INST_0_i_69_n_0\,
      S(0) => \g[3]_INST_0_i_70_n_0\,
      b(1) => \^b\(0),
      b(0) => \^b\(1),
      btn_down => btn_down,
      btn_drop => btn_drop,
      btn_left => btn_left,
      btn_right => btn_right,
      btn_rotate => btn_rotate,
      clk => clk,
      \counterX_reg[8]\(4 downto 0) => \vga_inst/cur_blk_index__0\(7 downto 3),
      cur_blk_index(1 downto 0) => \vga_inst/cur_blk_index\(9 downto 8),
      down_o => down_o,
      drop_o => drop_o,
      g(3 downto 0) => g(3 downto 0),
      \g[3]_INST_0_i_14\(2) => \g[3]_INST_0_i_52_n_0\,
      \g[3]_INST_0_i_14\(1) => \g[3]_INST_0_i_53_n_0\,
      \g[3]_INST_0_i_14\(0) => \g[3]_INST_0_i_54_n_0\,
      \g[3]_INST_0_i_14_0\(2) => \g[3]_INST_0_i_56_n_0\,
      \g[3]_INST_0_i_14_0\(1) => \g[3]_INST_0_i_57_n_0\,
      \g[3]_INST_0_i_14_0\(0) => \g[3]_INST_0_i_58_n_0\,
      \g[3]_INST_0_i_14_1\(2) => \g[3]_INST_0_i_44_n_0\,
      \g[3]_INST_0_i_14_1\(1) => \g[3]_INST_0_i_45_n_0\,
      \g[3]_INST_0_i_14_1\(0) => \g[3]_INST_0_i_46_n_0\,
      \g[3]_INST_0_i_14_2\(2) => \g[3]_INST_0_i_48_n_0\,
      \g[3]_INST_0_i_14_2\(1) => \g[3]_INST_0_i_49_n_0\,
      \g[3]_INST_0_i_14_2\(0) => \g[3]_INST_0_i_50_n_0\,
      \g[3]_INST_0_i_18\(2) => \g[3]_INST_0_i_72_n_0\,
      \g[3]_INST_0_i_18\(1) => \g[3]_INST_0_i_73_n_0\,
      \g[3]_INST_0_i_18\(0) => \g[3]_INST_0_i_74_n_0\,
      \g[3]_INST_0_i_18_0\(2) => \g[3]_INST_0_i_60_n_0\,
      \g[3]_INST_0_i_18_0\(1) => \g[3]_INST_0_i_61_n_0\,
      \g[3]_INST_0_i_18_0\(0) => \g[3]_INST_0_i_62_n_0\,
      \g[3]_INST_0_i_18_1\(2) => \g[3]_INST_0_i_64_n_0\,
      \g[3]_INST_0_i_18_1\(1) => \g[3]_INST_0_i_65_n_0\,
      \g[3]_INST_0_i_18_1\(0) => \g[3]_INST_0_i_66_n_0\,
      \g[3]_INST_0_i_19\(2) => \g[3]_INST_0_i_84_n_0\,
      \g[3]_INST_0_i_19\(1) => \g[3]_INST_0_i_85_n_0\,
      \g[3]_INST_0_i_19\(0) => \g[3]_INST_0_i_86_n_0\,
      \g[3]_INST_0_i_19_0\(2) => \g[3]_INST_0_i_88_n_0\,
      \g[3]_INST_0_i_19_0\(1) => \g[3]_INST_0_i_89_n_0\,
      \g[3]_INST_0_i_19_0\(0) => \g[3]_INST_0_i_90_n_0\,
      \g[3]_INST_0_i_19_1\(2) => \g[3]_INST_0_i_76_n_0\,
      \g[3]_INST_0_i_19_1\(1) => \g[3]_INST_0_i_77_n_0\,
      \g[3]_INST_0_i_19_1\(0) => \g[3]_INST_0_i_78_n_0\,
      \g[3]_INST_0_i_19_2\(2) => \g[3]_INST_0_i_80_n_0\,
      \g[3]_INST_0_i_19_2\(1) => \g[3]_INST_0_i_81_n_0\,
      \g[3]_INST_0_i_19_2\(0) => \g[3]_INST_0_i_82_n_0\,
      \g[3]_INST_0_i_8\(2) => \g[3]_INST_0_i_28_n_0\,
      \g[3]_INST_0_i_8\(1) => \g[3]_INST_0_i_29_n_0\,
      \g[3]_INST_0_i_8\(0) => \g[3]_INST_0_i_30_n_0\,
      \g[3]_INST_0_i_8_0\(2) => \g[3]_INST_0_i_32_n_0\,
      \g[3]_INST_0_i_8_0\(1) => \g[3]_INST_0_i_33_n_0\,
      \g[3]_INST_0_i_8_0\(0) => \g[3]_INST_0_i_34_n_0\,
      \g[3]_INST_0_i_8_1\(2) => \g[3]_INST_0_i_24_n_0\,
      \g[3]_INST_0_i_8_1\(1) => \g[3]_INST_0_i_25_n_0\,
      \g[3]_INST_0_i_8_1\(0) => \g[3]_INST_0_i_26_n_0\,
      g_1_sp_1 => \g[1]_INST_0_i_1_n_0\,
      hs => hs,
      left_o => left_o,
      pixel_clk => pixel_clk,
      r(2) => \^r\(1),
      r(1) => \^r\(2),
      r(0) => \^r\(0),
      \r[1]_INST_0_i_1\(0) => \r[1]_INST_0_i_30_n_0\,
      \r[1]_INST_0_i_11\(2) => \r[1]_INST_0_i_87_n_0\,
      \r[1]_INST_0_i_11\(1) => \r[1]_INST_0_i_88_n_0\,
      \r[1]_INST_0_i_11\(0) => \r[1]_INST_0_i_89_n_0\,
      \r[1]_INST_0_i_11_0\(2) => \r[1]_INST_0_i_91_n_0\,
      \r[1]_INST_0_i_11_0\(1) => \r[1]_INST_0_i_92_n_0\,
      \r[1]_INST_0_i_11_0\(0) => \r[1]_INST_0_i_93_n_0\,
      \r[1]_INST_0_i_11_1\(2) => \r[1]_INST_0_i_79_n_0\,
      \r[1]_INST_0_i_11_1\(1) => \r[1]_INST_0_i_80_n_0\,
      \r[1]_INST_0_i_11_1\(0) => \r[1]_INST_0_i_81_n_0\,
      \r[1]_INST_0_i_11_2\(2) => \r[1]_INST_0_i_83_n_0\,
      \r[1]_INST_0_i_11_2\(1) => \r[1]_INST_0_i_84_n_0\,
      \r[1]_INST_0_i_11_2\(0) => \r[1]_INST_0_i_85_n_0\,
      \r[1]_INST_0_i_15\(2) => \r[1]_INST_0_i_103_n_0\,
      \r[1]_INST_0_i_15\(1) => \r[1]_INST_0_i_104_n_0\,
      \r[1]_INST_0_i_15\(0) => \r[1]_INST_0_i_105_n_0\,
      \r[1]_INST_0_i_15_0\(2) => \r[1]_INST_0_i_107_n_0\,
      \r[1]_INST_0_i_15_0\(1) => \r[1]_INST_0_i_108_n_0\,
      \r[1]_INST_0_i_15_0\(0) => \r[1]_INST_0_i_109_n_0\,
      \r[1]_INST_0_i_15_1\(2) => \r[1]_INST_0_i_95_n_0\,
      \r[1]_INST_0_i_15_1\(1) => \r[1]_INST_0_i_96_n_0\,
      \r[1]_INST_0_i_15_1\(0) => \r[1]_INST_0_i_97_n_0\,
      \r[1]_INST_0_i_15_2\(2) => \r[1]_INST_0_i_99_n_0\,
      \r[1]_INST_0_i_15_2\(1) => \r[1]_INST_0_i_100_n_0\,
      \r[1]_INST_0_i_15_2\(0) => \r[1]_INST_0_i_101_n_0\,
      \r[1]_INST_0_i_16\(2) => \r[1]_INST_0_i_119_n_0\,
      \r[1]_INST_0_i_16\(1) => \r[1]_INST_0_i_120_n_0\,
      \r[1]_INST_0_i_16\(0) => \r[1]_INST_0_i_121_n_0\,
      \r[1]_INST_0_i_16_0\(2) => \r[1]_INST_0_i_123_n_0\,
      \r[1]_INST_0_i_16_0\(1) => \r[1]_INST_0_i_124_n_0\,
      \r[1]_INST_0_i_16_0\(0) => \r[1]_INST_0_i_125_n_0\,
      \r[1]_INST_0_i_16_1\(2) => \r[1]_INST_0_i_111_n_0\,
      \r[1]_INST_0_i_16_1\(1) => \r[1]_INST_0_i_112_n_0\,
      \r[1]_INST_0_i_16_1\(0) => \r[1]_INST_0_i_113_n_0\,
      \r[1]_INST_0_i_16_2\(2) => \r[1]_INST_0_i_115_n_0\,
      \r[1]_INST_0_i_16_2\(1) => \r[1]_INST_0_i_116_n_0\,
      \r[1]_INST_0_i_16_2\(0) => \r[1]_INST_0_i_117_n_0\,
      \r[1]_INST_0_i_1_0\(1) => \r[1]_INST_0_i_26_n_0\,
      \r[1]_INST_0_i_1_0\(0) => \r[1]_INST_0_i_27_n_0\,
      \r[1]_INST_0_i_1_1\(0) => \r[1]_INST_0_i_38_n_0\,
      \r[1]_INST_0_i_1_2\(0) => \r[1]_INST_0_i_34_n_0\,
      \r[1]_INST_0_i_2\(2) => \r[1]_INST_0_i_50_n_0\,
      \r[1]_INST_0_i_2\(1) => \r[1]_INST_0_i_51_n_0\,
      \r[1]_INST_0_i_2\(0) => \r[1]_INST_0_i_52_n_0\,
      \r[1]_INST_0_i_2_0\(2) => \r[1]_INST_0_i_54_n_0\,
      \r[1]_INST_0_i_2_0\(1) => \r[1]_INST_0_i_55_n_0\,
      \r[1]_INST_0_i_2_0\(0) => \r[1]_INST_0_i_56_n_0\,
      \r[1]_INST_0_i_2_1\(2) => \r[1]_INST_0_i_46_n_0\,
      \r[1]_INST_0_i_2_1\(1) => \r[1]_INST_0_i_47_n_0\,
      \r[1]_INST_0_i_2_1\(0) => \r[1]_INST_0_i_48_n_0\,
      r_2_sp_1 => \r[1]_INST_0_i_4_n_0\,
      right_o => right_o,
      rotate_o => rotate_o,
      sw_pause => sw_pause,
      sw_pause_o => sw_pause_o,
      sw_rst => sw_rst,
      sw_rst_o => sw_rst_o,
      vs => vs
    );
\r[1]_INST_0_i_100\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_100_n_0\
    );
\r[1]_INST_0_i_101\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \r[1]_INST_0_i_101_n_0\
    );
\r[1]_INST_0_i_103\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_103_n_0\
    );
\r[1]_INST_0_i_104\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_104_n_0\
    );
\r[1]_INST_0_i_105\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \r[1]_INST_0_i_105_n_0\
    );
\r[1]_INST_0_i_107\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_107_n_0\
    );
\r[1]_INST_0_i_108\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_108_n_0\
    );
\r[1]_INST_0_i_109\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \r[1]_INST_0_i_109_n_0\
    );
\r[1]_INST_0_i_111\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_111_n_0\
    );
\r[1]_INST_0_i_112\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_112_n_0\
    );
\r[1]_INST_0_i_113\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_113_n_0\
    );
\r[1]_INST_0_i_115\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_115_n_0\
    );
\r[1]_INST_0_i_116\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_116_n_0\
    );
\r[1]_INST_0_i_117\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_117_n_0\
    );
\r[1]_INST_0_i_119\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_119_n_0\
    );
\r[1]_INST_0_i_120\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_120_n_0\
    );
\r[1]_INST_0_i_121\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \r[1]_INST_0_i_121_n_0\
    );
\r[1]_INST_0_i_123\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_123_n_0\
    );
\r[1]_INST_0_i_124\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_124_n_0\
    );
\r[1]_INST_0_i_125\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(4),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_125_n_0\
    );
\r[1]_INST_0_i_26\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_26_n_0\
    );
\r[1]_INST_0_i_27\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000A9"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(6),
      I1 => next_tetris_reg(1),
      I2 => next_tetris_reg(0),
      I3 => \vga_inst/cur_blk_index\(8),
      I4 => \vga_inst/cur_blk_index__0\(7),
      O => \r[1]_INST_0_i_27_n_0\
    );
\r[1]_INST_0_i_30\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_30_n_0\
    );
\r[1]_INST_0_i_34\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_34_n_0\
    );
\r[1]_INST_0_i_38\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_38_n_0\
    );
\r[1]_INST_0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"CB"
    )
        port map (
      I0 => next_tetris_reg(1),
      I1 => next_tetris_reg(0),
      I2 => next_tetris_reg(2),
      O => \r[1]_INST_0_i_4_n_0\
    );
\r[1]_INST_0_i_46\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_46_n_0\
    );
\r[1]_INST_0_i_47\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_47_n_0\
    );
\r[1]_INST_0_i_48\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(5),
      I2 => \vga_inst/cur_blk_index__0\(4),
      O => \r[1]_INST_0_i_48_n_0\
    );
\r[1]_INST_0_i_50\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_50_n_0\
    );
\r[1]_INST_0_i_51\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_51_n_0\
    );
\r[1]_INST_0_i_52\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_52_n_0\
    );
\r[1]_INST_0_i_54\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_54_n_0\
    );
\r[1]_INST_0_i_55\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_55_n_0\
    );
\r[1]_INST_0_i_56\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_56_n_0\
    );
\r[1]_INST_0_i_79\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_79_n_0\
    );
\r[1]_INST_0_i_80\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_80_n_0\
    );
\r[1]_INST_0_i_81\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \r[1]_INST_0_i_81_n_0\
    );
\r[1]_INST_0_i_83\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_83_n_0\
    );
\r[1]_INST_0_i_84\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_84_n_0\
    );
\r[1]_INST_0_i_85\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_85_n_0\
    );
\r[1]_INST_0_i_87\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_87_n_0\
    );
\r[1]_INST_0_i_88\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_88_n_0\
    );
\r[1]_INST_0_i_89\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \r[1]_INST_0_i_89_n_0\
    );
\r[1]_INST_0_i_91\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_91_n_0\
    );
\r[1]_INST_0_i_92\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_92_n_0\
    );
\r[1]_INST_0_i_93\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(3),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(5),
      O => \r[1]_INST_0_i_93_n_0\
    );
\r[1]_INST_0_i_95\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_95_n_0\
    );
\r[1]_INST_0_i_96\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(8),
      I1 => \vga_inst/cur_blk_index__0\(7),
      I2 => \vga_inst/cur_blk_index__0\(6),
      O => \r[1]_INST_0_i_96_n_0\
    );
\r[1]_INST_0_i_97\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \vga_inst/cur_blk_index__0\(5),
      I1 => \vga_inst/cur_blk_index__0\(4),
      I2 => \vga_inst/cur_blk_index__0\(3),
      O => \r[1]_INST_0_i_97_n_0\
    );
\r[1]_INST_0_i_99\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \vga_inst/cur_blk_index\(9),
      O => \r[1]_INST_0_i_99_n_0\
    );
end STRUCTURE;
