-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Wed Mar 18 10:43:04 2020
-- Host        : notg00datmath running 64-bit Ubuntu 18.04.4 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ tetris_pl_interrupt_0_3_stub.vhdl
-- Design      : tetris_pl_interrupt_0_3
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    up : in STD_LOGIC;
    down : in STD_LOGIC;
    left : in STD_LOGIC;
    right : in STD_LOGIC;
    mid : in STD_LOGIC;
    sw_rst : in STD_LOGIC;
    sw_pause : in STD_LOGIC;
    interrupt_up : out STD_LOGIC;
    interrupt_down : out STD_LOGIC;
    interrupt_left : out STD_LOGIC;
    interrupt_right : out STD_LOGIC;
    interrupt_mid : out STD_LOGIC;
    interrupt_sw_rst : out STD_LOGIC;
    interrupt_sw_pause : out STD_LOGIC;
    pl_interrupt_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pl_interrupt_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    pl_interrupt_awvalid : in STD_LOGIC;
    pl_interrupt_awready : out STD_LOGIC;
    pl_interrupt_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    pl_interrupt_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pl_interrupt_wvalid : in STD_LOGIC;
    pl_interrupt_wready : out STD_LOGIC;
    pl_interrupt_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pl_interrupt_bvalid : out STD_LOGIC;
    pl_interrupt_bready : in STD_LOGIC;
    pl_interrupt_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pl_interrupt_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    pl_interrupt_arvalid : in STD_LOGIC;
    pl_interrupt_arready : out STD_LOGIC;
    pl_interrupt_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    pl_interrupt_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pl_interrupt_rvalid : out STD_LOGIC;
    pl_interrupt_rready : in STD_LOGIC;
    pl_interrupt_aclk : in STD_LOGIC;
    pl_interrupt_aresetn : in STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "up,down,left,right,mid,sw_rst,sw_pause,interrupt_up,interrupt_down,interrupt_left,interrupt_right,interrupt_mid,interrupt_sw_rst,interrupt_sw_pause,pl_interrupt_awaddr[3:0],pl_interrupt_awprot[2:0],pl_interrupt_awvalid,pl_interrupt_awready,pl_interrupt_wdata[31:0],pl_interrupt_wstrb[3:0],pl_interrupt_wvalid,pl_interrupt_wready,pl_interrupt_bresp[1:0],pl_interrupt_bvalid,pl_interrupt_bready,pl_interrupt_araddr[3:0],pl_interrupt_arprot[2:0],pl_interrupt_arvalid,pl_interrupt_arready,pl_interrupt_rdata[31:0],pl_interrupt_rresp[1:0],pl_interrupt_rvalid,pl_interrupt_rready,pl_interrupt_aclk,pl_interrupt_aresetn";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "pl_interrupt_v1_0,Vivado 2019.1";
begin
end;
