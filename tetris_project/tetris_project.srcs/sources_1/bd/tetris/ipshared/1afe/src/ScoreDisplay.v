`include "tetris_define.vh"

module ScoreDisplay (
	input 		[`BITS_X_POS-1:0]					cur_pos_x,
 	input 		[`BITS_Y_POS-1:0]					cur_pos_y,
 	input		[3:0]								digit,
 	output		reg [`BITS_BLK_POS-1:0]				blk1,
 	output		reg [`BITS_BLK_POS-1:0]				blk2,
 	output		reg [`BITS_BLK_POS-1:0]				blk3,
 	output		reg [`BITS_BLK_POS-1:0]				blk4,
 	output		reg [`BITS_BLK_POS-1:0]				blk5,
 	output		reg [`BITS_BLK_POS-1:0]				blk6,
 	output		reg [`BITS_BLK_POS-1:0]				blk7,
 	output		reg [`BITS_BLK_POS-1:0]				blk8,
 	output		reg [`BITS_BLK_POS-1:0]				blk9,
 	output		reg [`BITS_BLK_POS-1:0]				blk10,
 	output		reg [`BITS_BLK_POS-1:0]				blk11,
 	output		reg [`BITS_BLK_POS-1:0]				blk12,
 	output		reg [`BITS_BLK_POS-1:0]				blk13,
 	output		reg [`BITS_BLK_POS-1:0]				blk14,
 	output		reg [`BITS_BLK_POS-1:0]				blk15,
 	output reg 	[`BLOCKS_WIDTH*`BLOCKS_HEIGHT-1:0]	score_board
);

	always@(*) begin 
		blk1  = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;
	 	blk2  = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;
	 	blk3  = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 2;
	 	blk4  = (1 + cur_pos_y) * `BLOCKS_WIDTH + cur_pos_x;
	 	blk5  = (1 + cur_pos_y) * `BLOCKS_WIDTH + cur_pos_x + 1;
	 	blk6  = (1 + cur_pos_y) * `BLOCKS_WIDTH + cur_pos_x + 2;
	 	blk7  = (2 + cur_pos_y) * `BLOCKS_WIDTH + cur_pos_x;
	 	blk8  = (2 + cur_pos_y) * `BLOCKS_WIDTH + cur_pos_x + 1;
	 	blk9  = (2 + cur_pos_y) * `BLOCKS_WIDTH + cur_pos_x + 2;
	 	blk10 = (3 + cur_pos_y) * `BLOCKS_WIDTH + cur_pos_x;
	 	blk11 = (3 + cur_pos_y) * `BLOCKS_WIDTH + cur_pos_x + 1;
	 	blk12 = (3 + cur_pos_y) * `BLOCKS_WIDTH + cur_pos_x + 2;
	 	blk13 = (4 + cur_pos_y) * `BLOCKS_WIDTH + cur_pos_x;
	 	blk14 = (4 + cur_pos_y) * `BLOCKS_WIDTH + cur_pos_x + 1;
	 	blk15 = (4 + cur_pos_y) * `BLOCKS_WIDTH + cur_pos_x + 2;
	end	

		always@(*) begin 
		score_board = 'b0;
		// if(digit == 0) begin 
		// 	score_board[blk1] = 0;
		// 	score_board[blk2] = 1;
		// 	score_board[blk3] = 0;
		// 	score_board[blk4] = 1;
		// 	score_board[blk5] = 0;
		// 	score_board[blk6] = 1;
		// 	score_board[blk7] = 1;
		// 	score_board[blk8] = 0;
		// 	score_board[blk9] = 1;
		// 	score_board[blk10] = 1;
		// 	score_board[blk11] = 0;
		// 	score_board[blk12] = 1;
		// 	score_board[blk13] = 0;
		// 	score_board[blk14] = 1;
		// 	score_board[blk15] = 0;
		// end
		// else if(digit == 1) begin 
		// 	score_board[blk1] = 0;
		// 	score_board[blk2] = 1;
		// 	score_board[blk3] = 0;
		// 	score_board[blk4] = 0;
		// 	score_board[blk5] = 1;
		// 	score_board[blk6] = 0;
		// 	score_board[blk7] = 0;
		// 	score_board[blk8] = 1;
		// 	score_board[blk9] = 0;
		// 	score_board[blk10] = 0;
		// 	score_board[blk11] = 1;
		// 	score_board[blk12] = 0;
		// 	score_board[blk13] = 0;
		// 	score_board[blk14] = 1;
		// 	score_board[blk15] = 0;			
		// end
		// else if(digit == 2) begin 
		// 	score_board[blk1] = 1;
		// 	score_board[blk2] = 1;
		// 	score_board[blk3] = 1;
		// 	score_board[blk4] = 0;
		// 	score_board[blk5] = 0;
		// 	score_board[blk6] = 1;
		// 	score_board[blk7] = 1;
		// 	score_board[blk8] = 1;
		// 	score_board[blk9] = 1;
		// 	score_board[blk10] = 1;
		// 	score_board[blk11] = 0;
		// 	score_board[blk12] = 0;
		// 	score_board[blk13] = 1;
		// 	score_board[blk14] = 1;
		// 	score_board[blk15] = 1;
		// end
		// else if(digit == 3) begin 
		// 	score_board[blk1] = 1;
		// 	score_board[blk2] = 1;
		// 	score_board[blk3] = 1;
		// 	score_board[blk4] = 0;
		// 	score_board[blk5] = 0;
		// 	score_board[blk6] = 1;
		// 	score_board[blk7] = 1;
		// 	score_board[blk8] = 1;
		// 	score_board[blk9] = 1;
		// 	score_board[blk10] = 0;
		// 	score_board[blk11] = 0;
		// 	score_board[blk12] = 1;
		// 	score_board[blk13] = 1;
		// 	score_board[blk14] = 1;
		// 	score_board[blk15] = 1;
		// end
		// else if(digit == 4) begin 
		// 	score_board[blk1] = 1;
		// 	score_board[blk2] = 0;
		// 	score_board[blk3] = 1;
		// 	score_board[blk4] = 1;
		// 	score_board[blk5] = 0;
		// 	score_board[blk6] = 1;
		// 	score_board[blk7] = 1;
		// 	score_board[blk8] = 1;
		// 	score_board[blk9] = 1;
		// 	score_board[blk10] = 0;
		// 	score_board[blk11] = 0;
		// 	score_board[blk12] = 1;
		// 	score_board[blk13] = 0;
		// 	score_board[blk14] = 0;
		// 	score_board[blk15] = 1;
		// end
		// else if(digit == 5) begin 
		// 	score_board[blk1] = 1;
		// 	score_board[blk2] = 1;
		// 	score_board[blk3] = 1;
		// 	score_board[blk4] = 1;
		// 	score_board[blk5] = 0;
		// 	score_board[blk6] = 0;
		// 	score_board[blk7] = 1;
		// 	score_board[blk8] = 1;
		// 	score_board[blk9] = 1;
		// 	score_board[blk10] = 0;
		// 	score_board[blk11] = 0;
		// 	score_board[blk12] = 1;
		// 	score_board[blk13] = 1;
		// 	score_board[blk14] = 1;
		// 	score_board[blk15] = 1;
		// end
		// else if(digit == 6) begin 
		// 	score_board[blk1] = 1;
		// 	score_board[blk2] = 1;
		// 	score_board[blk3] = 1;
		// 	score_board[blk4] = 1;
		// 	score_board[blk5] = 0;
		// 	score_board[blk6] = 0;
		// 	score_board[blk7] = 1;
		// 	score_board[blk8] = 1;
		// 	score_board[blk9] = 1;
		// 	score_board[blk10] = 1;
		// 	score_board[blk11] = 0;
		// 	score_board[blk12] = 1;
		// 	score_board[blk13] = 1;
		// 	score_board[blk14] = 1;
		// 	score_board[blk15] = 1;
		// end
		// else if(digit == 7) begin 
		// 	score_board[blk1] = 1;
		// 	score_board[blk2] = 1;
		// 	score_board[blk3] = 1;
		// 	score_board[blk4] = 0;
		// 	score_board[blk5] = 0;
		// 	score_board[blk6] = 1;
		// 	score_board[blk7] = 0;
		// 	score_board[blk8] = 0;
		// 	score_board[blk9] = 1;
		// 	score_board[blk10] = 0;
		// 	score_board[blk11] = 0;
		// 	score_board[blk12] = 1;
		// 	score_board[blk13] = 0;
		// 	score_board[blk14] = 0;
		// 	score_board[blk15] = 1;
		// end
		// else if(digit == 8) begin 
		// 	score_board[blk1] = 1;
		// 	score_board[blk2] = 1;
		// 	score_board[blk3] = 1;
		// 	score_board[blk4] = 1;
		// 	score_board[blk5] = 0;
		// 	score_board[blk6] = 1;
		// 	score_board[blk7] = 1;
		// 	score_board[blk8] = 1;
		// 	score_board[blk9] = 1;
		// 	score_board[blk10] = 1;
		// 	score_board[blk11] = 0;
		// 	score_board[blk12] = 1;
		// 	score_board[blk13] = 1;
		// 	score_board[blk14] = 1;
		// 	score_board[blk15] = 1;
		// end
		// else begin 
		// 	score_board[blk1] = 1;
		// 	score_board[blk2] = 1;
		// 	score_board[blk3] = 1;
		// 	score_board[blk4] = 1;
		// 	score_board[blk5] = 0;
		// 	score_board[blk6] = 1;
		// 	score_board[blk7] = 1;
		// 	score_board[blk8] = 1;
		// 	score_board[blk9] = 1;
		// 	score_board[blk10] = 0;
		// 	score_board[blk11] = 0;
		// 	score_board[blk12] = 1;
		// 	score_board[blk13] = 0;
		// 	score_board[blk14] = 0;
		// 	score_board[blk15] = 1;
		// end	
		case(digit)
			0: begin
				//15'b010101101101010;
				score_board[blk1] = 0;
				score_board[blk2] = 1;
				score_board[blk3] = 0;
				score_board[blk4] = 1;
				score_board[blk5] = 0;
				score_board[blk6] = 1;
				score_board[blk7] = 1;
				score_board[blk8] = 0;
				score_board[blk9] = 1;
				score_board[blk10] = 1;
				score_board[blk11] = 0;
				score_board[blk12] = 1;
				score_board[blk13] = 0;
				score_board[blk14] = 1;
				score_board[blk15] = 0;
			end
			1: begin
				//15'b010 010 010 010 010;
				score_board[blk1] = 0;
				score_board[blk2] = 1;
				score_board[blk3] = 0;
				score_board[blk4] = 0;
				score_board[blk5] = 1;
				score_board[blk6] = 0;
				score_board[blk7] = 0;
				score_board[blk8] = 1;
				score_board[blk9] = 0;
				score_board[blk10] = 0;
				score_board[blk11] = 1;
				score_board[blk12] = 0;
				score_board[blk13] = 0;
				score_board[blk14] = 1;
				score_board[blk15] = 0;
			end
			2: begin
				//15'b111 001 111 100 111;
				score_board[blk1] = 1;
				score_board[blk2] = 1;
				score_board[blk3] = 1;
				score_board[blk4] = 0;
				score_board[blk5] = 0;
				score_board[blk6] = 1;
				score_board[blk7] = 1;
				score_board[blk8] = 1;
				score_board[blk9] = 1;
				score_board[blk10] = 1;
				score_board[blk11] = 0;
				score_board[blk12] = 0;
				score_board[blk13] = 1;
				score_board[blk14] = 1;
				score_board[blk15] = 1;
			end
			3: begin
				//15'b111 001 111 001 111;
				score_board[blk1] = 1;
				score_board[blk2] = 1;
				score_board[blk3] = 1;
				score_board[blk4] = 0;
				score_board[blk5] = 0;
				score_board[blk6] = 1;
				score_board[blk7] = 1;
				score_board[blk8] = 1;
				score_board[blk9] = 1;
				score_board[blk10] = 0;
				score_board[blk11] = 0;
				score_board[blk12] = 1;
				score_board[blk13] = 1;
				score_board[blk14] = 1;
				score_board[blk15] = 1;
			end
			4: begin
				//15'b101 101 111 001 001;
				score_board[blk1] = 1;
				score_board[blk2] = 0;
				score_board[blk3] = 1;
				score_board[blk4] = 1;
				score_board[blk5] = 0;
				score_board[blk6] = 1;
				score_board[blk7] = 1;
				score_board[blk8] = 1;
				score_board[blk9] = 1;
				score_board[blk10] = 0;
				score_board[blk11] = 0;
				score_board[blk12] = 1;
				score_board[blk13] = 0;
				score_board[blk14] = 0;
				score_board[blk15] = 1;
			end
			5: begin
				//15'b111 100 111 001 111;
				score_board[blk1] = 1;
				score_board[blk2] = 1;
				score_board[blk3] = 1;
				score_board[blk4] = 1;
				score_board[blk5] = 0;
				score_board[blk6] = 0;
				score_board[blk7] = 1;
				score_board[blk8] = 1;
				score_board[blk9] = 1;
				score_board[blk10] = 0;
				score_board[blk11] = 0;
				score_board[blk12] = 1;
				score_board[blk13] = 1;
				score_board[blk14] = 1;
				score_board[blk15] = 1;
			end
			6: begin
				//15'b111 100 111 101 111;
				score_board[blk1] = 1;
				score_board[blk2] = 1;
				score_board[blk3] = 1;
				score_board[blk4] = 1;
				score_board[blk5] = 0;
				score_board[blk6] = 0;
				score_board[blk7] = 1;
				score_board[blk8] = 1;
				score_board[blk9] = 1;
				score_board[blk10] = 1;
				score_board[blk11] = 0;
				score_board[blk12] = 1;
				score_board[blk13] = 1;
				score_board[blk14] = 1;
				score_board[blk15] = 1;
			end
			7: begin
				//15'b111 001 001 001 001;
				score_board[blk1] = 1;
				score_board[blk2] = 1;
				score_board[blk3] = 1;
				score_board[blk4] = 0;
				score_board[blk5] = 0;
				score_board[blk6] = 1;
				score_board[blk7] = 0;
				score_board[blk8] = 0;
				score_board[blk9] = 1;
				score_board[blk10] = 0;
				score_board[blk11] = 0;
				score_board[blk12] = 1;
				score_board[blk13] = 0;
				score_board[blk14] = 0;
				score_board[blk15] = 1;
			end
			8: begin
				//15'b111 101 111 101 111;
				score_board[blk1] = 1;
				score_board[blk2] = 1;
				score_board[blk3] = 1;
				score_board[blk4] = 1;
				score_board[blk5] = 0;
				score_board[blk6] = 1;
				score_board[blk7] = 1;
				score_board[blk8] = 1;
				score_board[blk9] = 1;
				score_board[blk10] = 1;
				score_board[blk11] = 0;
				score_board[blk12] = 1;
				score_board[blk13] = 1;
				score_board[blk14] = 1;
				score_board[blk15] = 1;
			end
			9: begin
				//15'b111 101 111 001 001;
				score_board[blk1] = 1;
				score_board[blk2] = 1;
				score_board[blk3] = 1;
				score_board[blk4] = 1;
				score_board[blk5] = 0;
				score_board[blk6] = 1;
				score_board[blk7] = 1;
				score_board[blk8] = 1;
				score_board[blk9] = 1;
				score_board[blk10] = 0;
				score_board[blk11] = 0;
				score_board[blk12] = 1;
				score_board[blk13] = 0;
				score_board[blk14] = 0;
				score_board[blk15] = 1;
			end
			default: begin 
				score_board[blk1] = 0;
				score_board[blk2] = 1;
				score_board[blk3] = 0;
				score_board[blk4] = 1;
				score_board[blk5] = 0;
				score_board[blk6] = 1;
				score_board[blk7] = 1;
				score_board[blk8] = 0;
				score_board[blk9] = 1;
				score_board[blk10] = 1;
				score_board[blk11] = 0;
				score_board[blk12] = 1;
				score_board[blk13] = 0;
				score_board[blk14] = 1;
				score_board[blk15] = 0;
			end	
		endcase // digit
	end
endmodule
