module GetScoreDigits (
	input				clk,    // Clock
	input 				rst,
	input				increment,
	output	reg [3:0]	digit0,
	output	reg [3:0]	digit1
);

	always@(posedge clk) begin 
		if(rst) begin 
			digit1 <= 0;
			digit0 <= 0;
		end
		else begin
			if(!increment) begin 
				digit1 <= digit1;
				digit0 <= digit0;
			end
			else begin 
				if(digit0 == 9) begin 
					digit1 <= digit1 + 1;
					digit0 <= 0;
				end
				else if(digit1 == 9 && digit0 == 9) begin 
					digit1 <= 0;
					digit0 <= 0;
				end
				else begin 
					digit1 <= digit1;
					digit0 <= digit0 + 1;
				end
			end
	   end
	end
endmodule