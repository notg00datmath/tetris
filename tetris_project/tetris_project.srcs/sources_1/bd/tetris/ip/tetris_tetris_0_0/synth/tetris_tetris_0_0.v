// (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: user.org:user:tetris:1.0
// IP Revision: 135

(* X_CORE_INFO = "tetris,Vivado 2019.1" *)
(* CHECK_LICENSE_TYPE = "tetris_tetris_0_0,tetris,{}" *)
(* IP_DEFINITION_SOURCE = "package_project" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module tetris_tetris_0_0 (
  clk,
  pixel_clk,
  btn_down,
  btn_left,
  btn_right,
  btn_rotate,
  btn_drop,
  sw_rst,
  sw_pause,
  rotate_o,
  down_o,
  left_o,
  right_o,
  drop_o,
  sw_rst_o,
  sw_pause_o,
  r,
  g,
  b,
  hs,
  vs,
  level
);

(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME pixel_clk, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN tetris_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 pixel_clk CLK" *)
input wire clk;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, FREQ_HZ 25173010, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *)
input wire pixel_clk;
input wire btn_down;
input wire btn_left;
input wire btn_right;
input wire btn_rotate;
input wire btn_drop;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sw_rst, POLARITY ACTIVE_LOW, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 sw_rst RST" *)
input wire sw_rst;
input wire sw_pause;
output wire rotate_o;
output wire down_o;
output wire left_o;
output wire right_o;
output wire drop_o;
output wire sw_rst_o;
output wire sw_pause_o;
output wire [3 : 0] r;
output wire [3 : 0] g;
output wire [3 : 0] b;
output wire hs;
output wire vs;
output wire [3 : 0] level;

  tetris inst (
    .clk(clk),
    .pixel_clk(pixel_clk),
    .btn_down(btn_down),
    .btn_left(btn_left),
    .btn_right(btn_right),
    .btn_rotate(btn_rotate),
    .btn_drop(btn_drop),
    .sw_rst(sw_rst),
    .sw_pause(sw_pause),
    .rotate_o(rotate_o),
    .down_o(down_o),
    .left_o(left_o),
    .right_o(right_o),
    .drop_o(drop_o),
    .sw_rst_o(sw_rst_o),
    .sw_pause_o(sw_pause_o),
    .r(r),
    .g(g),
    .b(b),
    .hs(hs),
    .vs(vs),
    .level(level)
  );
endmodule
