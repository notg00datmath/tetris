// (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: user.org:user:pl_interrupt:1.0
// IP Revision: 8

(* X_CORE_INFO = "pl_interrupt_v1_0,Vivado 2019.1" *)
(* CHECK_LICENSE_TYPE = "tetris_pl_interrupt_0_3,pl_interrupt_v1_0,{}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module tetris_pl_interrupt_0_3 (
  up,
  down,
  left,
  right,
  mid,
  sw_rst,
  sw_pause,
  level,
  interrupt_up,
  interrupt_down,
  interrupt_left,
  interrupt_right,
  interrupt_mid,
  interrupt_sw_rst,
  interrupt_sw_pause,
  pl_interrupt_awaddr,
  pl_interrupt_awprot,
  pl_interrupt_awvalid,
  pl_interrupt_awready,
  pl_interrupt_wdata,
  pl_interrupt_wstrb,
  pl_interrupt_wvalid,
  pl_interrupt_wready,
  pl_interrupt_bresp,
  pl_interrupt_bvalid,
  pl_interrupt_bready,
  pl_interrupt_araddr,
  pl_interrupt_arprot,
  pl_interrupt_arvalid,
  pl_interrupt_arready,
  pl_interrupt_rdata,
  pl_interrupt_rresp,
  pl_interrupt_rvalid,
  pl_interrupt_rready,
  pl_interrupt_aclk,
  pl_interrupt_aresetn
);

input wire up;
input wire down;
input wire left;
input wire right;
input wire mid;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sw_rst, POLARITY ACTIVE_LOW, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 sw_rst RST" *)
input wire sw_rst;
input wire sw_pause;
input wire [3 : 0] level;
output wire interrupt_up;
output wire interrupt_down;
output wire interrupt_left;
output wire interrupt_right;
output wire interrupt_mid;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME interrupt_sw_rst, POLARITY ACTIVE_LOW, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 interrupt_sw_rst RST" *)
output wire interrupt_sw_rst;
output wire interrupt_sw_pause;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt AWADDR" *)
input wire [3 : 0] pl_interrupt_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt AWPROT" *)
input wire [2 : 0] pl_interrupt_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt AWVALID" *)
input wire pl_interrupt_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt AWREADY" *)
output wire pl_interrupt_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt WDATA" *)
input wire [31 : 0] pl_interrupt_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt WSTRB" *)
input wire [3 : 0] pl_interrupt_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt WVALID" *)
input wire pl_interrupt_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt WREADY" *)
output wire pl_interrupt_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt BRESP" *)
output wire [1 : 0] pl_interrupt_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt BVALID" *)
output wire pl_interrupt_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt BREADY" *)
input wire pl_interrupt_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt ARADDR" *)
input wire [3 : 0] pl_interrupt_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt ARPROT" *)
input wire [2 : 0] pl_interrupt_arprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt ARVALID" *)
input wire pl_interrupt_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt ARREADY" *)
output wire pl_interrupt_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt RDATA" *)
output wire [31 : 0] pl_interrupt_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt RRESP" *)
output wire [1 : 0] pl_interrupt_rresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt RVALID" *)
output wire pl_interrupt_rvalid;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME pl_interrupt, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 1e+08, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN tetris_processing_syst\
em7_0_0_FCLK_CLK0, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 pl_interrupt RREADY" *)
input wire pl_interrupt_rready;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME pl_interrupt_CLK, ASSOCIATED_BUSIF pl_interrupt, ASSOCIATED_RESET pl_interrupt_aresetn:pl_interrupt_RST, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN tetris_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 pl_interrupt_CLK CLK" *)
input wire pl_interrupt_aclk;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME pl_interrupt_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 pl_interrupt_RST RST" *)
input wire pl_interrupt_aresetn;

  pl_interrupt_v1_0 #(
    .C_pl_interrupt_DATA_WIDTH(32),  // Width of S_AXI data bus
    .C_pl_interrupt_ADDR_WIDTH(4)  // Width of S_AXI address bus
  ) inst (
    .up(up),
    .down(down),
    .left(left),
    .right(right),
    .mid(mid),
    .sw_rst(sw_rst),
    .sw_pause(sw_pause),
    .level(level),
    .interrupt_up(interrupt_up),
    .interrupt_down(interrupt_down),
    .interrupt_left(interrupt_left),
    .interrupt_right(interrupt_right),
    .interrupt_mid(interrupt_mid),
    .interrupt_sw_rst(interrupt_sw_rst),
    .interrupt_sw_pause(interrupt_sw_pause),
    .pl_interrupt_awaddr(pl_interrupt_awaddr),
    .pl_interrupt_awprot(pl_interrupt_awprot),
    .pl_interrupt_awvalid(pl_interrupt_awvalid),
    .pl_interrupt_awready(pl_interrupt_awready),
    .pl_interrupt_wdata(pl_interrupt_wdata),
    .pl_interrupt_wstrb(pl_interrupt_wstrb),
    .pl_interrupt_wvalid(pl_interrupt_wvalid),
    .pl_interrupt_wready(pl_interrupt_wready),
    .pl_interrupt_bresp(pl_interrupt_bresp),
    .pl_interrupt_bvalid(pl_interrupt_bvalid),
    .pl_interrupt_bready(pl_interrupt_bready),
    .pl_interrupt_araddr(pl_interrupt_araddr),
    .pl_interrupt_arprot(pl_interrupt_arprot),
    .pl_interrupt_arvalid(pl_interrupt_arvalid),
    .pl_interrupt_arready(pl_interrupt_arready),
    .pl_interrupt_rdata(pl_interrupt_rdata),
    .pl_interrupt_rresp(pl_interrupt_rresp),
    .pl_interrupt_rvalid(pl_interrupt_rvalid),
    .pl_interrupt_rready(pl_interrupt_rready),
    .pl_interrupt_aclk(pl_interrupt_aclk),
    .pl_interrupt_aresetn(pl_interrupt_aresetn)
  );
endmodule
