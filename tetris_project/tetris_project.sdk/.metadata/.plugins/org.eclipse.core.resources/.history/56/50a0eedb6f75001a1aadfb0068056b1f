/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */
#include <stdio.h>
#include "xil_printf.h"
#include "xbasic_types.h"
#include "xscugic.h"
#include "xil_exception.h"
#include "platform.h"

#define INTC_INTERRUPT_ID_0 61 	//up
#define INTC_INTERRUPT_ID_1 62	//down
#define INTC_INTERRUPT_ID_2 63	//left
#define INTC_INTERRUPT_ID_3 64	//right
#define INTC_INTERRUPT_ID_4 65	//mid
#define INTC_INTERRUPT_ID_5 66	//sw_rst
#define INTC_INTERRUPT_ID_6	67	//sw_pause

static XScuGic intc;

Xuint32* slv_reg0 = (Xuint32*) XPAR_PL_INTERRUPT_0_PL_INTERRUPT_BASEADDR;
int setup_interrupt_sysyem();
void isr0 (void *intc_inst_ptr);
void isr1 (void *intc_inst_ptr);
void isr2 (void *intc_inst_ptr);
void isr3 (void *intc_inst_ptr);
void isr4 (void *intc_inst_ptr);
void isr5 (void *intc_inst_ptr);
void isr6 (void *intc_inst_ptr);

int main()
{
    init_platform();
    int status;
    xil_printf("Main Program Starts\r\n");
    status = setup_interrupt_sysyem();
    if(status != XST_SUCCESS) return XST_FAILURE;
    while(1);

    cleanup_platform();
    return 0;
}

void isr0(void *intc_inst_ptr){
	//disable interrupt
	XScuGic_Disable(intc_inst_ptr, INTC_INTERRUPT_ID_0);
	//service routine
	xil_printf("ROTATE\r\n");
	//re-enable interrupt
	XScuGic_Enable(intc_inst_ptr, INTC_INTERRUPT_ID_0);
}

void isr1(void *intc_inst_ptr){
	//disable interrupt
	XScuGic_Disable(intc_inst_ptr, INTC_INTERRUPT_ID_1);
	//service routine
	xil_printf("DROP\r\n");
	//re-enable interrupt
	XScuGic_Enable(intc_inst_ptr, INTC_INTERRUPT_ID_1);
}

void isr2(void *intc_inst_ptr){
	//disable interrupt
	XScuGic_Disable(intc_inst_ptr, INTC_INTERRUPT_ID_2);
	//service routine
	xil_printf("LEFT\r\n");
	//re-enable interrupt
	XScuGic_Enable(intc_inst_ptr, INTC_INTERRUPT_ID_2);
}

void isr3(void *intc_inst_ptr){
	//disable interrupt
	XScuGic_Disable(intc_inst_ptr, INTC_INTERRUPT_ID_3);
	//service routine
	xil_printf("RIGHT\r\n");
	//re-enable interrupt
	XScuGic_Enable(intc_inst_ptr, INTC_INTERRUPT_ID_3);
}

void isr4(void *intc_inst_ptr){
	//disable interrupt
	XScuGic_Disable(intc_inst_ptr, INTC_INTERRUPT_ID_4);
	//service routine
	xil_printf("DOWN 0x%08x\r\n", *slv_reg0);
	//re-enable interrupt
	XScuGic_Enable(intc_inst_ptr, INTC_INTERRUPT_ID_4);
}

void isr5(void *intc_inst_ptr){
	//disable interrupt
	XScuGic_Disable(intc_inst_ptr, INTC_INTERRUPT_ID_5);
	//service routine
	xil_printf("RESET\r\n");
	//re-enable interrupt
	XScuGic_Enable(intc_inst_ptr, INTC_INTERRUPT_ID_5);
}

void isr6(void *intc_inst_ptr){
	//disable interrupt
	XScuGic_Disable(intc_inst_ptr, INTC_INTERRUPT_ID_6);
	//service routine
	xil_printf("PAUSE\r\n");
	//re-enable interrupt
	XScuGic_Enable(intc_inst_ptr, INTC_INTERRUPT_ID_6);
}

int setup_interrupt_sysyem(){
	int status;
	XScuGic *intc_instance_ptr = &intc;
	XScuGic_Config *intc_config;

	//disable interrupt
//	XScuGic_Disable(intc_instance_ptr, INTC_INTERRUPT_ID_0);
	//lookuo configuration
	intc_config = XScuGic_LookupConfig(XPAR_PS7_SCUGIC_0_DEVICE_ID);
	if (intc_config==NULL){
		xil_printf("LookupConfig Failed\r\n");
		return XST_FAILURE;
	}
	//initialize
	status = XScuGic_CfgInitialize(intc_instance_ptr, intc_config, intc_config->CpuBaseAddress);
	if(status!=XST_SUCCESS){
		xil_printf("CgfInitialize Failed\r\n");
		return XST_FAILURE;
	}
	//set priority and trigger type IRQ_F2P[0]
	XScuGic_SetPriorityTriggerType(intc_instance_ptr, INTC_INTERRUPT_ID_0, 0xA0, 0x3);
	status = XScuGic_Connect(intc_instance_ptr, INTC_INTERRUPT_ID_0,(Xil_ExceptionHandler) isr0, (void*) &intc);
	if(status!=XST_SUCCESS){
		xil_printf("GIC Connect Failed\n\r");
		return XST_FAILURE;
	}
	XScuGic_Enable(intc_instance_ptr, INTC_INTERRUPT_ID_0);

	//IRQ_F2P[1]
	XScuGic_SetPriorityTriggerType(intc_instance_ptr, INTC_INTERRUPT_ID_1, 0xA0, 0x3);
	status = XScuGic_Connect(intc_instance_ptr, INTC_INTERRUPT_ID_1,(Xil_ExceptionHandler) isr1, (void*) &intc);
	if(status!=XST_SUCCESS){
		xil_printf("GIC Connect Failed\n\r");
		return XST_FAILURE;
	}
	XScuGic_Enable(intc_instance_ptr, INTC_INTERRUPT_ID_1);

	//IRQ_F2P[2]
	XScuGic_SetPriorityTriggerType(intc_instance_ptr, INTC_INTERRUPT_ID_2, 0xA0, 0x3);
	status = XScuGic_Connect(intc_instance_ptr, INTC_INTERRUPT_ID_2,(Xil_ExceptionHandler) isr2, (void*) &intc);
	if(status!=XST_SUCCESS){
		xil_printf("GIC Connect Failed\n\r");
		return XST_FAILURE;
	}
	XScuGic_Enable(intc_instance_ptr, INTC_INTERRUPT_ID_2);

	//IRQ_F2P[3]
	XScuGic_SetPriorityTriggerType(intc_instance_ptr, INTC_INTERRUPT_ID_3, 0xA0, 0x3);
	status = XScuGic_Connect(intc_instance_ptr, INTC_INTERRUPT_ID_3,(Xil_ExceptionHandler) isr3, (void*) &intc);
	if(status!=XST_SUCCESS){
		xil_printf("GIC Connect Failed\n\r");
		return XST_FAILURE;
	}
	XScuGic_Enable(intc_instance_ptr, INTC_INTERRUPT_ID_3);

	//IRQ_F2P[4]
	XScuGic_SetPriorityTriggerType(intc_instance_ptr, INTC_INTERRUPT_ID_4, 0xA0, 0x3);
	status = XScuGic_Connect(intc_instance_ptr, INTC_INTERRUPT_ID_4,(Xil_ExceptionHandler) isr4, (void*) &intc);
	if(status!=XST_SUCCESS){
		xil_printf("GIC Connect Failed\n\r");
		return XST_FAILURE;
	}
	XScuGic_Enable(intc_instance_ptr, INTC_INTERRUPT_ID_4);

	//IRQ_F2P[5]
	XScuGic_SetPriorityTriggerType(intc_instance_ptr, INTC_INTERRUPT_ID_5, 0xA0, 0x3);
	status = XScuGic_Connect(intc_instance_ptr, INTC_INTERRUPT_ID_5,(Xil_ExceptionHandler) isr5, (void*) &intc);
	if(status!=XST_SUCCESS){
		xil_printf("GIC Connect Failed\n\r");
		return XST_FAILURE;
	}
	XScuGic_Enable(intc_instance_ptr, INTC_INTERRUPT_ID_5);

	//IRQ_F2P[6]
		XScuGic_SetPriorityTriggerType(intc_instance_ptr, INTC_INTERRUPT_ID_6, 0xA0, 0x3);
		status = XScuGic_Connect(intc_instance_ptr, INTC_INTERRUPT_ID_6,(Xil_ExceptionHandler) isr6, (void*) &intc);
		if(status!=XST_SUCCESS){
			xil_printf("GIC Connect Failed\n\r");
			return XST_FAILURE;
		}
		XScuGic_Enable(intc_instance_ptr, INTC_INTERRUPT_ID_6);

	//initialize and enable exception
	Xil_ExceptionInit();
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT, (Xil_ExceptionHandler) XScuGic_InterruptHandler, intc_instance_ptr);
	Xil_ExceptionEnable();

	return XST_SUCCESS;
}
