Tetris-src: contains the HDL for the Tetris IP block
InterruptGenerator-src: contains the HDL for the Interrupt Generator IP block
tetris_project: contains the Vivado project
ip_repo: IPs created for the project
Misc: Miscellaneous files
