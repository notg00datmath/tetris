module debounce (
	input 	clk,    // Clock
	input 	raw,
	output	btn_debounced,	
	output	sw_flipped
);

	reg			debounced, debounced_prev;
	reg	[15:0]	counter;
	wire 		enabled, disabled;

	assign enabled = debounced &&!debounced_prev;
	assign disabled = !debounced && debounced_prev;

	assign btn_debounced = enabled;
	assign sw_flipped = enabled;

	initial begin
		debounced = 0;
		debounced_prev = 0;
		counter = 0;
	end

	always@(posedge clk) begin 
		debounced_prev <= debounced;
	end

	always@(posedge clk) begin
		if (counter == 15000) begin
			counter <= 0;
			debounced <= raw;
		end
		else begin
			counter <= counter + 1;
			// debounced <= debounced;
		end
	end

// 	wire slow_clk_en;
// 	wire Q1, Q2, Q2_bar;
// 	clock_enable clock_enable_inst(
// 		.clk_100M   (clk),
// 		.pb_1       (raw),
// 		.slow_clk_en(slow_clk_en)
// 		);
// 	dff_en dff_en_inst1(
// 		.dff_clk     (clk),
// 		.clock_enable(slow_clk_en),
// 		.D           (raw),
// 		.Q           (Q1)
// 		);
// 	dff_en dff_en_inst2(
// 		.dff_clk     (clk),
// 		.clock_enable(slow_clk_en),
// 		.D           (Q1),
// 		.Q           (Q2)
// 		);
// 	assign Q2_bar = ~Q2;
// 	assign btn_debounced = Q1 & Q2_bar;
// 	assign sw_flipped = Q1 & Q2_bar;
endmodule

// module clock_enable(
// 	input clk_100M,
// 	input pb_1,
// 	output slow_clk_en
// 	);
	
// 	reg [26:0] counter = 0;
// 	always@(posedge clk_100M, negedge pb_1) begin 
// 		if(pb_1 == 0)
// 			counter <= 0;
// 		else 
// 			counter <= (counter >= 15000) ? 0 : counter+1; 
// 	end

// 	assign slow_clk_en = (counter == 15000) ? 1'b1 : 1'b0;
// endmodule

// module dff_en(
// 	input dff_clk,
// 	input clock_enable,
// 	input D,
// 	output reg Q=0
// 	);

// 	always@(posedge dff_clk) begin 
// 		if(clock_enable == 1)
// 			Q <= D;
// 		else
// 			Q <= 0;
// 	end
// endmodule

