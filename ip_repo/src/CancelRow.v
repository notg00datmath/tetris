`include "tetris_define.vh"

module CancelRow (
	input 										clk,
	input 										pause,
	input [`BLOCKS_WIDTH * `BLOCKS_HEIGHT-1:0]	fallen,
	output reg [`BITS_Y_POS-1:0]				row,
	output										enabled
);

	initial begin 
		row = 0;
	end

	assign enabled = &fallen[row*`BLOCKS_WIDTH+:`BLOCKS_WIDTH];

	always@(posedge clk) begin 
		if(!pause) begin 
			if(row == `BLOCKS_HEIGHT - 1) begin 
				row <= 0;
			end
			else begin 
				row <= row + 1;
			end
		end
		else begin 
			row <= row;
		end
	end
endmodule