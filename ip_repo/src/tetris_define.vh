// The width of the screen in pixels
`define PIXEL_WIDTH 640
// The height of the screen in pixels
`define PIXEL_HEIGHT 480

// Used for VGA horizontal and vertical sync
`define HSYNC_FRONT_PORCH 16
`define HSYNC_PULSE_WIDTH 96
`define HSYNC_BACK_PORCH 48
`define VSYNC_FRONT_PORCH 10
`define VSYNC_PULSE_WIDTH 2
`define VSYNC_BACK_PORCH 33

// How many pixels wide/high each block is
`define BLOCK_SIZE 16
// How many blocks wide the game board is
`define BLOCKS_WIDTH 10

// How many blocks high the game board is
`define BLOCKS_HEIGHT 22

// Width of the game board in pixels
`define BOARD_WIDTH (`BLOCKS_WIDTH * `BLOCK_SIZE)
// Starting x pixel for the game board
`define BOARD_X (((`PIXEL_WIDTH - `BOARD_WIDTH) / 2) - 1)

// Height of the game board in pixels
`define BOARD_HEIGHT (`BLOCKS_HEIGHT * `BLOCK_SIZE)
// Starting y pixel for the game board
`define BOARD_Y ((((`PIXEL_HEIGHT - `BOARD_HEIGHT) / 2) - 1) + 20)

//score board dimension in pixels
`define SCORE_WIDTH				(3 * `BLOCK_SIZE)
`define SCORE_HEIGHT 			(5 * `BLOCK_SIZE)
//starting position of the score board in pixels
`define SCORE1_X					(`BOARD_X + `BOARD_WIDTH + 1)
`define SCORE1_Y					`BOARD_Y
`define SCORE0_X					`BOARD_X + `BOARD_WIDTH + `SCORE_WIDTH + 1
`define SCORE0_Y					`SCORE1_Y

//next tetrimino dimension in pixels
`define NEXT_BLOCK_WIDTH		(3 * `BLOCK_SIZE)
`define NEXT_BLOCK_HEIGHT 		(4 * `BLOCK_SIZE)
//starting position of the next tetrimino in pixels
`define NEXT_BLOCK_X			(`SCORE1_X)
`define NEXT_BLOCK_Y			(`BOARD_Y + `SCORE_HEIGHT)

//position of new tetriminos
`define NEW_TETRIS_X `BLOCKS_WIDTH/2 - 1;
`define NEW_TETRIS_Y 0

// The number of bits used to store a block position
`define BITS_BLK_POS 8
// The number of bits used to store an X position
`define BITS_X_POS 4
// The number of bits used to store a Y position
`define BITS_Y_POS 5
// The number of bits used to store a rotation
`define BITS_ROT 2
// The number of bits used to store how wide / long a block is (max of decimal 4)
`define BITS_BLK_SIZE 3
// The number of bits for the score. The score goes up to 10000
`define BITS_SCORE 14
// The number of bits used to store each block
`define BITS_PER_BLOCK 3

// The type of each block
`define EMPTY_BLOCK 3'b000
`define I_BLOCK 3'b001
`define O_BLOCK 3'b010
`define T_BLOCK 3'b011
`define S_BLOCK 3'b100
`define Z_BLOCK 3'b101
`define J_BLOCK 3'b110
`define L_BLOCK 3'b111

// Color mapping
`define WHITE		12'hfff
`define BLACK 		12'h000
`define GRAY		12'hAAA
`define CYAN		12'h0FF
`define YELLOW		12'hEF0
`define PURPLE		12'hE3F
`define GREEN		12'h0F0
`define RED			12'hF00
`define BLUE		12'h00F
`define ORANGE		12'hE72

// Error value
`define ERR_BLK_POS 8'b11111111

// Modes
`define MODE_BITS 5
`define MODE_IDLE 	5'b00001
`define MODE_PLAY 	5'b00010
`define MODE_PAUSE 	5'b00100
`define MODE_DROP 	5'b01000
`define MODE_SHIFT 	5'b10000
// `define MODE_DEAD	5'b00000
// The maximum value of the drop timer
`define DROP_TIMER_MAX 10000
