`include "tetris_define.vh"

module tetris( 
	input wire			clk,        //100 MHz
	input wire 			pixel_clk,	//25.175 MHz
	input wire			btn_down,
	input wire			btn_left,
	input wire			btn_right,
	input wire			btn_rotate,
	input wire			btn_drop,
	input wire			sw_rst,
	input wire			sw_pause,
	output wire         rotate_o,
	output wire         down_o,
	output wire 		left_o,
	output wire			right_o,
	output wire			drop_o,
	output wire         sw_rst_o,
	output wire         sw_pause_o,
	output wire	[3:0]	r, g, b,
	output wire			hs, vs,
	output wire	[3:0]	level
	);

	//Get a random tetrimino index
	wire  [2:0]	next_tetris;
	wire  [2:0] random_tetris;
	reg   [2:0]	next_tetris_reg;
	GetRandomPiece GetRandomPiece_inst(
		.clk(pixel_clk),
		.random_tetris(next_tetris)
		);

	//Get the location information of the next tetrimino 
	/*four blocks of the next tetrimino's flattened coordinates*/
	wire [`BITS_BLK_POS-1:0]				next_blk1;
	wire [`BITS_BLK_POS-1:0]				next_blk2;
	wire [`BITS_BLK_POS-1:0]				next_blk3;
	wire [`BITS_BLK_POS-1:0]				next_blk4;
	GetTetrisInfo GetTetrisInfo_next_tetris(
		.cur_tetris  (next_tetris_reg),
		.cur_pos_x   (`BLOCKS_WIDTH),
		.cur_pos_y   (5),
		.cur_rotation('b0),
		.blk1        (next_blk1),
		.blk2        (next_blk2),
		.blk3        (next_blk3),
		.blk4        (next_blk4)
		);

	//debounce all buttons 
	wire	btn_down_en;
	wire	btn_left_en;
	wire	btn_right_en;
	wire	btn_rotate_en;
	wire	btn_drop_en;
	wire	sw_rst_en;
	wire	sw_pause_en;
	debounce debounce_down(
		.clk  			(pixel_clk),
		.raw		    (btn_down),
		.btn_debounced	(btn_down_en)
		);

	debounce debounce_left(
		.clk  			(pixel_clk),
		.raw	     	(btn_left),
		.btn_debounced	(btn_left_en)
		);

	debounce debounce_right(
		.clk  			(pixel_clk),
		.raw	     	(btn_right),
		.btn_debounced	(btn_right_en)
		);

	debounce debounce_rotate(
		.clk  			(pixel_clk),
		.raw	     	(btn_rotate),
		.btn_debounced	(btn_rotate_en)
		);

	debounce debounce_drop(
		.clk  			(pixel_clk),
		.raw      		(btn_drop),
		.btn_debounced	(btn_drop_en)
		);

	debounce debounce_sw_rst(
		.clk 			(pixel_clk),
		.raw            (sw_rst),
		.sw_flipped     (sw_rst_en)
		);

	debounce debounce_sw_pause(
		.clk 			(pixel_clk),
		.raw            (sw_pause),
		.sw_flipped     (sw_pause_en)
		);
    //output interrupt assignment
    assign rotate_o = btn_rotate_en;
    assign down_o = btn_down_en;
    assign left_o = btn_left_en;
    assign right_o = btn_right_en;
    assign drop_o = btn_drop_en;
    assign sw_rst_o = sw_rst_en;
    assign sw_pause_o = sw_pause_en;
	/*register to hold position fallen tetriminos*/
	reg [`BLOCKS_WIDTH*`BLOCKS_HEIGHT-1:0]	fallen;
	wire [`BLOCKS_WIDTH*`BLOCKS_HEIGHT-1:0]	score_board0, score_board1;
	/*current falling tetrimino*/
	reg [`BITS_PER_BLOCK-1:0]				cur_tetris;
	/*current tetrimino rotation (0 90 180 270)*/
	reg [`BITS_ROT-1:0]						cur_rot;
	/*current tetrimino x coordinate*/
	reg [`BITS_X_POS-1:0]					cur_pos_x;
	/*current tetrimino y coordinate*/
	reg [`BITS_Y_POS-1:0]					cur_pos_y;
	/*four blocks of the current tetrimino's flattened coordinates*/
	wire [`BITS_BLK_POS-1:0]				cur_blk1;
	wire [`BITS_BLK_POS-1:0]				cur_blk2;
	wire [`BITS_BLK_POS-1:0]				cur_blk3;
	wire [`BITS_BLK_POS-1:0]				cur_blk4;
	/*width and height of the current tetrimino*/
	wire [`BITS_BLK_SIZE-1:0]				cur_width;
	wire [`BITS_BLK_SIZE-1:0]				cur_height;
	wire [11:0]								rgb;
	assign r = rgb[11:8];
	assign g = rgb[7:4];
	assign b = rgb[3:0];
	//calculate current tetrimino's width, height and flattened location
	GetTetrisInfo GetTetrisInfo_inst(
		.cur_tetris  (cur_tetris),
		.cur_pos_x   (cur_pos_x),
		.cur_pos_y   (cur_pos_y),
		.cur_rotation(cur_rot),
		.blk1        (cur_blk1),
		.blk2        (cur_blk2),
		.blk3        (cur_blk3),
		.blk4        (cur_blk4),
		.width       (cur_width),
		.height      (cur_height)
		);

	//move tester
	reg [`MODE_BITS-1:0]					mode;
	reg [`MODE_BITS-1:0]					prev_mode;
	wire [`BITS_X_POS-1:0]					test_pos_x;
	wire [`BITS_Y_POS-1:0]					test_pos_y;
	wire [`BITS_ROT-1:0]					test_rot;
	wire [`BITS_BLK_POS-1:0]				test_blk1;
	wire [`BITS_BLK_POS-1:0]				test_blk2;
	wire [`BITS_BLK_POS-1:0]				test_blk3;
	wire [`BITS_BLK_POS-1:0]				test_blk4;
	wire [`BITS_BLK_SIZE-1:0]				test_width;
	wire [`BITS_BLK_SIZE-1:0]				test_height;
	wire 									test_intesects;
	wire									gameover;

	//tetriminos fall automatically 
	wire auto_fall;
	reg  auto_fall_rst;
	auto_fall_timer auto_fall_timer_inst(
		.clk(pixel_clk), 
		.rst(auto_fall_rst), 
		.pause(mode != `MODE_PLAY), 
		.auto_fall(auto_fall)
		);

	GetTestInfo GetTestInfo_inst(
		.mode         	(mode),
		.auto_fall     	(auto_fall),
		.auto_fall_rst 	(auto_fall_rst),
		.btn_left_en  	(btn_left_en),
		.btn_right_en (btn_right_en),
		.btn_rotate_en(btn_rotate_en),
		.btn_down_en  (btn_down_en),
		.btn_drop_en  (btn_drop_en),
		.cur_pos_x    (cur_pos_x),
		.cur_pos_y    (cur_pos_y),
		.cur_rot      (cur_rot),
		.test_pos_x   (test_pos_x),
		.test_pos_y   (test_pos_y),
		.test_rot     (test_rot));

	//calculate the test tetriminos' info
	GetTetrisInfo GetTetrisInfo_TestBlocks(
		.cur_tetris  (cur_tetris),
		.cur_pos_x   (test_pos_x),
		.cur_pos_y   (test_pos_y),
		.cur_rotation(test_rot),
		.blk1        (test_blk1),
		.blk2        (test_blk2),
		.blk3        (test_blk3),
		.blk4        (test_blk4),
		.width       (test_width),
		.height      (test_height));

	//task to check if intersection happens
	function intersects;
		input [`BITS_BLK_POS-1:0]			blk1, blk2, blk3, blk4;
		begin 
			intersects = fallen[blk1] || fallen[blk2] || fallen[blk3] || fallen[blk4];
		end		
	endfunction
	
	assign test_intesects = intersects(test_blk1,test_blk2,  test_blk3, test_blk4);
	assign gameover = cur_pos_y == 0 && test_intesects;

	//task to move current tetrimino to the left
	task left;
		begin
			if(cur_pos_x > 0 && !test_intesects) 
				cur_pos_x <= cur_pos_x - 1;
		end
	endtask

	//task to move current tetrimino to the right
	task right;
		begin
			if(cur_pos_x + test_width < `BLOCKS_WIDTH && !test_intesects) 
				cur_pos_x <= cur_pos_x + 1;
		end
	endtask

	//task to rotate curret tetrimino
	task rotate;
		begin
			if(cur_pos_x + test_width <= `BLOCKS_WIDTH &&
			   cur_pos_y + test_height <= `BLOCKS_HEIGHT &&
			   !test_intesects)
			   cur_rot <= cur_rot + 1;	
		end
	endtask

	//task to add fallen tetriminos
	task add_fallen;
		begin
			fallen[cur_blk1] <= 1;
			fallen[cur_blk2] <= 1;
			fallen[cur_blk3] <= 1;
			fallen[cur_blk4] <= 1;
		end
	endtask

	//task to move current tetrimino down
	//call get_new if move would intersect with fallen or go out of game board
	task down;
		begin 
			if(cur_pos_y + test_height < `BLOCKS_HEIGHT && !test_intesects) begin
				cur_pos_y <= cur_pos_y + 1;
			end
			else begin 
				add_fallen();
				get_new();
			end
		end
	endtask

	//task to get a new tetrimino
	reg  		new_piece_enable;
	task get_new;
		begin
			new_piece_enable <= 1;
			cur_tetris <= random_tetris;
			cur_pos_x <= `NEW_TETRIS_X;
			cur_pos_y <= `NEW_TETRIS_Y;
			cur_rot <= 0;
			//reset the auto fall counter
			auto_fall_rst <= 1;
		end
	endtask

	//calculate the row number to be removed
	wire [`BITS_Y_POS-1:0]					cancel_row;
	reg  [`BITS_Y_POS-1:0]					shift_row;
	wire 									cancel_row_en;
	wire	[5:0]							count;
	CancelRow CancelRow_inst(
		.clk(pixel_clk),
		.pause(mode != `MODE_PLAY),
		.fallen(fallen),
		.row(cancel_row),
		.enabled(cancel_row_en)
		);
		
	wire [3:0] digit0, digit1;
	wire 	   increment;
	wire [`BITS_BLK_POS-1:0]	score1_blk1;
	wire [`BITS_BLK_POS-1:0]	score1_blk2;
	wire [`BITS_BLK_POS-1:0]	score1_blk3;
	wire [`BITS_BLK_POS-1:0]	score1_blk4;
	wire [`BITS_BLK_POS-1:0]	score1_blk5;
	wire [`BITS_BLK_POS-1:0]	score1_blk6;
	wire [`BITS_BLK_POS-1:0]	score1_blk7;
	wire [`BITS_BLK_POS-1:0]	score1_blk8;
	wire [`BITS_BLK_POS-1:0]	score1_blk9;
	wire [`BITS_BLK_POS-1:0]	score1_blk10;
	wire [`BITS_BLK_POS-1:0]	score1_blk11;
	wire [`BITS_BLK_POS-1:0]	score1_blk12;
	wire [`BITS_BLK_POS-1:0]	score1_blk13;
	wire [`BITS_BLK_POS-1:0]	score1_blk14;
	wire [`BITS_BLK_POS-1:0]	score1_blk15;
	wire [`BITS_BLK_POS-1:0]	score0_blk1;
	wire [`BITS_BLK_POS-1:0]	score0_blk2;
	wire [`BITS_BLK_POS-1:0]	score0_blk3;
	wire [`BITS_BLK_POS-1:0]	score0_blk4;
	wire [`BITS_BLK_POS-1:0]	score0_blk5;
	wire [`BITS_BLK_POS-1:0]	score0_blk6;
	wire [`BITS_BLK_POS-1:0]	score0_blk7;
	wire [`BITS_BLK_POS-1:0]	score0_blk8;
	wire [`BITS_BLK_POS-1:0]	score0_blk9;
	wire [`BITS_BLK_POS-1:0]	score0_blk10;
	wire [`BITS_BLK_POS-1:0]	score0_blk11;
	wire [`BITS_BLK_POS-1:0]	score0_blk12;
	wire [`BITS_BLK_POS-1:0]	score0_blk13;
	wire [`BITS_BLK_POS-1:0]	score0_blk14;
	wire [`BITS_BLK_POS-1:0]	score0_blk15;
	reg cancel_row_en_ff1, cancel_row_en_ff2;

	always@(posedge pixel_clk) begin 
		cancel_row_en_ff1 <= cancel_row_en;
		cancel_row_en_ff2 <= cancel_row_en_ff1;
	end

	assign increment = cancel_row_en_ff1 && !cancel_row_en_ff2;
	assign level = digit1;
	//calculate the current score
	GetScoreDigits GetScoreDigits_inst(
		.clk           (pixel_clk),
		.rst           (sw_rst_en),
		.increment     (increment),
		.digit0(digit0),
		.digit1(digit1)
		);

	//score 1
	ScoreDisplay ScoreDisplay_1(
		.cur_pos_x(`BLOCKS_WIDTH),
		.cur_pos_y('b0),
		.digit      (digit1),
		.score_board(score_board1),
		.blk1     (score1_blk1),
		.blk2     (score1_blk2),
		.blk3     (score1_blk3),
		.blk4     (score1_blk4),
		.blk5     (score1_blk5),
		.blk6     (score1_blk6),
		.blk7     (score1_blk7),
		.blk8     (score1_blk8),
		.blk9     (score1_blk9),
		.blk10     (score1_blk10),
		.blk11     (score1_blk11),
		.blk12     (score1_blk12),
		.blk13     (score1_blk13),
		.blk14     (score1_blk14),
		.blk15     (score1_blk15)
	);

	//score 0
	ScoreDisplay ScoreDisplay_0(
		.cur_pos_x(3+`BLOCKS_WIDTH),
		.cur_pos_y('b0),
		.digit      (digit0),
		.score_board(score_board0),
		.blk1     (score0_blk1),
		.blk2     (score0_blk2),
		.blk3     (score0_blk3),
		.blk4     (score0_blk4),
		.blk5     (score0_blk5),
		.blk6     (score0_blk6),
		.blk7     (score0_blk7),
		.blk8     (score0_blk8),
		.blk9     (score0_blk9),
		.blk10     (score0_blk10),
		.blk11     (score0_blk11),
		.blk12     (score0_blk12),
		.blk13     (score0_blk13),
		.blk14     (score0_blk14),
		.blk15     (score0_blk15)
	);
	
	//VGA controller
	vga vga_inst(
		.pixel_clk	(pixel_clk),
		.cur_tetris	(cur_tetris),
		.next_tetris(next_tetris_reg),
		.blk1 		(cur_blk1), 
		.blk2 		(cur_blk2), 
		.blk3 		(cur_blk3), 
		.blk4 		(cur_blk4), 
		.next_blk1	(next_blk1),
		.next_blk2	(next_blk2),
		.next_blk3	(next_blk3),
		.next_blk4	(next_blk4),
		.score1_blk1(score1_blk1),
		.score1_blk2(score1_blk2),
		.score1_blk3(score1_blk3),
		.score1_blk4(score1_blk4),
		.score1_blk5(score1_blk5),
		.score1_blk6(score1_blk6),
		.score1_blk7(score1_blk7),
		.score1_blk8(score1_blk8),
		.score1_blk9(score1_blk9),
		.score1_blk10(score1_blk10),
		.score1_blk11(score1_blk11),
		.score1_blk12(score1_blk12),
		.score1_blk13(score1_blk13),
		.score1_blk14(score1_blk14),
		.score1_blk15(score1_blk15),
		.score0_blk1(score0_blk1),
		.score0_blk2(score0_blk2),
		.score0_blk3(score0_blk3),
		.score0_blk4(score0_blk4),
		.score0_blk5(score0_blk5),
		.score0_blk6(score0_blk6),
		.score0_blk7(score0_blk7),
		.score0_blk8(score0_blk8),
		.score0_blk9(score0_blk9),
		.score0_blk10(score0_blk10),
		.score0_blk11(score0_blk11),
		.score0_blk12(score0_blk12),
		.score0_blk13(score0_blk13),
		.score0_blk14(score0_blk14),
		.score0_blk15(score0_blk15),
		.fallen		(fallen),
		.score_board1(score_board1),
		.score_board0(score_board0),
		// .digit0		(digit0),
		// .digit1		(digit1),
		.rgb		(rgb),
		.hs 		(hs),
		.vs 		(vs)
		);

	//task to start the game
	task start;
		begin 
			fallen <= 0;
			get_new();
		end
	endtask : start

	initial begin 
		mode = `MODE_IDLE;
		fallen = 'b0;
		cur_tetris = `EMPTY_BLOCK;
		cur_pos_x = 0;
		cur_pos_y = 0;
		cur_rot = 0;
	end

	//next_tetris 
	always@(posedge clk)begin 
		if(new_piece_enable)
			next_tetris_reg <= next_tetris;
		else
			next_tetris_reg <= next_tetris_reg;
	end
	assign random_tetris = next_tetris_reg;

	//state machine
	always@(posedge pixel_clk) begin 
		case(mode)
			`MODE_IDLE: begin 
				if(sw_rst_en) begin 
					mode <= `MODE_PLAY;
				end
				else begin 
					mode <= mode;
				end
			end

			`MODE_PAUSE:begin 
				if(sw_rst_en) begin 
					//reset switch flipped
					mode <= `MODE_IDLE;
				end
				else if(sw_pause_en) begin 
					//pause switch flipped again
					mode <= prev_mode;
				end
				else begin 
					//nothing changes
					mode <= mode;
				end
			end
			
			`MODE_PLAY:begin 
				if(sw_rst_en) begin 
					mode <= `MODE_IDLE;
				end
				else if(gameover) begin
					mode <= `MODE_IDLE;
				end
				else if(sw_pause_en) begin 
					mode <= `MODE_PAUSE;
				end
				else if(btn_drop_en) begin 
					mode <= `MODE_DROP;
				end
				else if(cancel_row_en) begin 
					mode <= `MODE_SHIFT;
				end
				else begin
					mode <= mode;
				end
			end
			
			`MODE_DROP:begin 
				if(auto_fall_rst) begin 
					//reached buttom
					mode <= `MODE_PLAY;
				end
				else if(sw_rst_en || gameover)
					mode <= `MODE_IDLE;
				else begin 
					//stay in drop if hasnt reached button
					mode <= mode;
				end
			end

			`MODE_SHIFT:begin 
				if(shift_row == 0) begin 
					//shifted all the rows 
					mode <= `MODE_PLAY;
				end
				else if(sw_rst_en)
					mode <= `MODE_IDLE;
				else begin 
					//keep shifting
					mode <= mode;
				end
			end

			// `MODE_DEAD: begin 
			// 	if(sw_rst_en)
			// 		mode <= `MODE_IDLE;
			// 	else
			// 		mode <= `MODE_DEAD;
			// end
		endcase // mode
	end

	//game logic
	always@(posedge pixel_clk) begin 
		auto_fall_rst <= 0;
		new_piece_enable <= 0;
		case(mode)
			`MODE_IDLE: begin 
				if(sw_rst_en) begin
					start();
				end
				else begin
					cur_tetris <= `EMPTY_BLOCK;
					fallen <= 0;
				end
			end

			`MODE_PAUSE:begin 
				// update nothing
				cur_tetris <= cur_tetris;
				cur_pos_x <= cur_pos_x;
				cur_pos_y <= cur_pos_y;
				cur_rot <= cur_rot;
				fallen <= fallen;
			end
			
			`MODE_PLAY:begin 
				if (auto_fall) 
					down();
				else if(sw_pause_en) begin 
					//save the current mode
					prev_mode <= mode;
				end
				else if(btn_left_en)  
					left();
				else if(btn_right_en)  
					right();
				else if(btn_rotate_en)  
					rotate();
				else if(btn_down_en)  
					down();
				else if(cancel_row_en)  
					shift_row <= cancel_row;
			end
			
			`MODE_DROP:begin 
				down();
			end

			`MODE_SHIFT:begin 
				if(shift_row == 0) begin 
					//shifted all the rows 
					fallen[0+:`BLOCKS_WIDTH] <= 0;
					shift_row <= shift_row;
				end
				else begin 
					//keep shifting
					fallen[shift_row*`BLOCKS_WIDTH+:`BLOCKS_WIDTH] <= fallen[(shift_row-1)*`BLOCKS_WIDTH+:`BLOCKS_WIDTH];
					shift_row <= shift_row - 1;
				end
			end

			// `MODE_DEAD: begin
			// 	cur_tetris <= `EMPTY_BLOCK;
			// 	fallen <= 0;
			// end
		endcase // mode
	end

endmodule : tetris
