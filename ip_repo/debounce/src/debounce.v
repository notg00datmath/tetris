`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/11/2020 09:24:43 PM
// Design Name: 
// Module Name: debounce
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module debounce(
    input i_clk,
    input [4:0] i_pb,    //push button input
//    output[4:0] o_pb_dbd, //debounced push button output
    output IRQ_F2P
    );
    
//    wire [4:0] pb_ff1, pb_ff2;
//    wire       clk_en;
//    wire [4:0]  pb_dbd;
    
    reg [4:0] ff1, ff2, ff3;
    reg [16:0] counter;
    reg output_enable;
    
    assign IRQ_F2P = (output_enable==1'b1)? (|ff3):1'b0;
    
    always@(posedge i_clk) begin
        ff1 <= i_pb;
        ff2 <= ff1;
    end 
    
    always@(posedge i_clk) begin
        if(ff1 == ff2 && ff1 != 0) 
            counter <= counter + 1;
        else 
            counter <= 0;
    end
    
    always@(posedge i_clk) begin
        if(counter == 50000) begin
            output_enable <= 1'b1;
            ff3 <= ff2;
        end
        else begin 
            output_enable <= 1'b0;
            ff3 <= 0;
        end    
    end
    
//    clk_enable clk_enable_inst(.i_clk(i_clk), .i_pb(i_pb), .o_clk_en(clk_en));
//    DFF DFF_inst1(.i_clk(i_clk), .i_data(i_pb), .i_clk_en(clk_en), .o_data(pb_ff1));
//    DFF DFF_inst2(.i_clk(i_clk), .i_data(pb_ff1), .i_clk_en(clk_en), .o_data(pb_ff2));
    
//    assign pb_dbd = pb_ff1 & !pb_ff2;    
//    assign IRQ_F2P = pb_dbd==1? 1'b1:1'b0;//|pb_dbd;         //interrupt is asserted if c button is pressed
//endmodule

//module clk_enable(
//    input i_clk,
//    input [4:0] i_pb, 
//    output o_clk_en
//    );

//    reg [16:0]  counter;    
//    assign o_clk_en = counter==100000? 1'b1:1'b0;
    
//    always@(posedge i_clk) begin
//        if(i_pb == 0)
//            counter <= 0;           //Tick counter after a button press
//        else 
//            counter <= counter>100000? 0:counter+1;
//    end
//endmodule

//module DFF(
//    input i_clk,
//    input [4:0] i_data,
//    input i_clk_en,
//    output reg [4:0] o_data
//);

//    always@(posedge i_clk) begin
//        if(i_clk_en == 1)
//            o_data <= i_data;
//        else 
//            o_data <= o_data;
//    end
endmodule












