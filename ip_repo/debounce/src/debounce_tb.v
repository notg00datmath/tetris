`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/12/2020 12:38:53 AM
// Design Name: 
// Module Name: debounce_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module debounce_tb;

    reg clk;
    reg [4:0] i_pb;
    wire IRQ_F2P;
    
    debounce debounce_tb(.i_clk(clk), .i_pb(i_pb), .IRQ_F2P(IRQ_F2P));
    
    initial 
    begin
        clk = 0;
        i_pb = 0;
        #50 i_pb = 1;
    end
    
    always
        #5 clk = !clk;
        
       
endmodule
