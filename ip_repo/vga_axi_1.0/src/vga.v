`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/31/2020 03:52:20 PM
// Design Name: 
// Module Name: vga
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module vga # (
    parameter vga_data_width = 32)
(
	input wire pixel_clk,
	input wire pixel_rstn,
	input wire [vga_data_width-1:0] rdata,
    input wire fifo_empty,
	output wire [3:0] r,g,b,
	output wire hsync, vsync,
	output wire fifo_rd_en
    );

	reg     [3:0] r_reg, g_reg, b_reg;
	reg     hsync_reg, vsync_reg;
	reg [10:0] counterX, counterY;
    reg v_enabled;
	reg	fifo_rd_en_reg;
    reg fifo_en_h;
    reg fifo_en_v;

    assign r = r_reg;
    assign g = g_reg;
    assign b = b_reg;
    assign hsync = hsync_reg;
    assign vsync = vsync_reg;
    assign fifo_rd_en = fifo_rd_en_reg;
    
    always@(counterX) begin
	   if(counterX<799)
            v_enabled = 1'b0;
	   else 
            v_enabled = 1'b1;
    end

    always@(posedge pixel_clk) begin
        if(pixel_rstn==0) begin
        	counterX <= 0;
        end
        else begin
            if(fifo_empty==1'b1) begin
                counterX <= counterX;
            end
        	else if(counterX < 799) begin
        		counterX <= counterX + 1;
        	end
        	else begin
        		counterX <= 0;
        	end
        end
    end

    always@(posedge pixel_clk) begin
    	if(pixel_rstn==0) begin
    		counterY <= 0;
    	end
    	else begin
            if(v_enabled==1) begin
    			if(counterY < 524) begin
	    			counterY <= counterY + 1;
	    		end
	    		else begin
	    			counterY <= 0;
	    		end
    		end
    		else begin
    			counterY <= counterY;
    		end
    	end
    end

    always@(counterX,counterY,rdata) begin
        if(counterX<784 && counterX>143 && counterY < 515 && counterY > 34) begin
//          if(counterX<1640 && counterX>359 && counterY < 1065 && counterY > 40) begin 
            r_reg = rdata[7:4];
            g_reg = rdata[15:12];
            b_reg = rdata[23:20];
        end
        else begin
            g_reg = 4'b0;
            b_reg = 4'b0;
            r_reg = 4'b0;
        end
    end

    always@(counterX) begin
        if(counterX<784 && counterX>143)
//          if(counterX<1640 && counterX>359)
            fifo_en_h = 1'b1;
        else 
            fifo_en_h = 1'b0;
    end
    
    always@(counterY) begin
        if(counterY < 515 && counterY > 34) begin
//          if(counterY < 1065 && counterY > 40) begin
            fifo_en_v = 1'b1;
        end
        else begin
            fifo_en_v = 1'b0;
        end
    end

    always@(fifo_en_h,fifo_en_v) begin
        fifo_rd_en_reg = fifo_en_v && fifo_en_h && (~fifo_empty) && (pixel_rstn==1'b1);
    end

    always@(counterX,counterY) begin
        hsync_reg = (counterX < 96) ? 1'b0:1'b1;
//        hsync_reg = (counterX < 112) ? 1'b0:1'b1;
        vsync_reg = (counterY < 2)  ? 1'b0:1'b1;
//        vsync_reg = (counterY < 3)  ? 1'b0:1'b1;
    end
endmodule
