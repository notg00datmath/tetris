module debounce (
	input 	clk,    // Clock
	input 	raw,
	output	btn_debounced,	
	output	sw_flipped
);

	reg			debounced, debounced_prev;
	reg	[15:0]	counter;

	assign btn_debounced = debounced && !debounced_prev;
	assign sw_flipped = debounced ^ debounced_prev;

	initial begin
		debounced = 0;
		debounced_prev = 0;
		counter = 0;
	end

	always@(posedge clk) begin
		debounced_prev <= debounced;
		if (counter == 15000) begin
			counter <= 0;
			debounced <= raw;
		end
		else begin
			counter <= counter + 1;
			debounced <= debounced;
		end
	end
endmodule