/* 
This module calculates the heithgt, 
width and flattened locations of the current tetrimino 
from input tetrimino type, xy coordinates and current rotation
*/

`include "tetris_define.vh"

 module GetTetrisInfo (
 	input [`BITS_PER_BLOCK-1:0]		cur_tetris,
 	input [`BITS_X_POS-1:0]			cur_pos_x,
 	input [`BITS_Y_POS-1:0]			cur_pos_y,
 	input [`BITS_ROT-1:0]			cur_rotation,
 	output reg [`BITS_BLK_POS-1:0]	blk1,
 	output reg [`BITS_BLK_POS-1:0]	blk2,
 	output reg [`BITS_BLK_POS-1:0]	blk3,
 	output reg [`BITS_BLK_POS-1:0]	blk4,
	output reg [`BITS_BLK_SIZE-1:0] width,
	output reg [`BITS_BLK_SIZE-1:0] height
 );

 	always@(*) begin
 		case(cur_tetris)
 			`EMPTY_BLOCK: begin
 				blk1 = 'b1;
 				blk2 = 'b1;
 				blk3 = 'b1;
 				blk4 = 'b1;
 				width = 0;
 				height = 0;
 			end

 			`I_BLOCK: begin
 				if(cur_rotation == 0 || cur_rotation == 2) begin 				// *
 					blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;				// *
 					blk2 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;			// *
 					blk3 = (cur_pos_y + 2) * `BLOCKS_WIDTH + cur_pos_x;			// *
 					blk4 = (cur_pos_y + 3) * `BLOCKS_WIDTH + cur_pos_x;
 					width = 1;
 					height = 4;
 				end
 				else begin 														// * * * * 						
					blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;
					blk2 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1; 
 					blk3 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 2;
 					blk4 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 3;
 					width = 4;	
 					height = 1;
 				end
 			end

 			`O_BLOCK: begin
				blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;
				blk2 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;			//	* *
				blk3 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;			//	* *
				blk4 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;
				width = 2;
				height = 2;
 			end

 			`T_BLOCK: begin
				case(cur_rotation)
					0:begin
						blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;			//   *
						blk2 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;			// * * *
						blk3 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;
						blk4 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 2;
						width = 3;
						height = 2;
					end
					1: begin
						blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;				// *
						blk2 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;			// * *
						blk3 = (cur_pos_y + 2) * `BLOCKS_WIDTH + cur_pos_x;			// *
						blk4 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;
						width = 2;
						height = 3;
					end
					2: begin
						blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;				
						blk2 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;			// * * *
						blk3 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 2;			//   *
						blk4 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;
						width = 3;
						height = 2;
					end
					3:begin
						blk1 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;			// 	 *
						blk2 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;			// * * 
						blk3 = (cur_pos_y + 2) * `BLOCKS_WIDTH + cur_pos_x + 1;		// 	 *
						blk4 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;
						width = 2;
						height = 3;
					end
				endcase
 			end

 			`S_BLOCK: begin
 				if(cur_rotation == 0 || cur_rotation == 2) begin
 					blk1 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;				//   * *
					blk2 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;					// * *
					blk3 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;					
					blk4 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 2;
					width = 3;
					height = 2;
 				end
 				else begin
 					blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;						// *
					blk2 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;				// * *
					blk3 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;					//   *	
					blk4 = (cur_pos_y + 2) * `BLOCKS_WIDTH + cur_pos_x + 1;
					width = 2;
					height = 3;
 				end
 			end

 			`Z_BLOCK: begin
 				if(cur_rotation == 0 || cur_rotation == 2) begin
 					blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;						// * *
					blk2 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;					//   * *
					blk3 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;					
					blk4 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 2;
					width = 3;
					height = 2;
 				end
 				else begin
 					blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;					//   *
					blk2 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;					// * *
					blk3 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;				// *	
					blk4 = (cur_pos_y + 2) * `BLOCKS_WIDTH + cur_pos_x + 1;
					width = 2;
					height = 3;
 				end
 			end

 			`J_BLOCK: begin
				case(cur_rotation)
					0:begin
						blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;				// *
						blk2 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;			// * * *
						blk3 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;
						blk4 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 2;
						width = 3;
						height = 2;
					end
					1: begin
						blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;				// * *
						blk2 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;			// * 
						blk3 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;			// *
						blk4 = (cur_pos_y + 2) * `BLOCKS_WIDTH + cur_pos_x;
						width = 2;
						height = 3;
					end
					2: begin
						blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;				
						blk2 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;			// * * *
						blk3 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 2;			//     *
						blk4 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 2;
						width = 3;
						height = 2;
					end
					3:begin
						blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;			//	 *
						blk2 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;		//   * 
						blk3 = (cur_pos_y + 2) * `BLOCKS_WIDTH + cur_pos_x + 1;		// * *
						blk4 = (cur_pos_y + 2) * `BLOCKS_WIDTH + cur_pos_x;
						width = 2;
						height = 3;
					end
				endcase
 			end

 			`L_BLOCK: begin
				case(cur_rotation)
					0:begin
						blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 2;			//     *
						blk2 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;			// * * *
						blk3 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;
						blk4 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 2;
						width = 3;
						height = 2;
					end
					1: begin
						blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;				// * 
						blk2 = (cur_pos_y + 2) * `BLOCKS_WIDTH + cur_pos_x + 1;		// * 
						blk3 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;			// * *
						blk4 = (cur_pos_y + 2) * `BLOCKS_WIDTH + cur_pos_x;
						width = 2;
						height = 3;
					end
					2: begin
						blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;				
						blk2 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;			// * * *
						blk3 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 2;			// *
						blk4 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x;
						width = 3;
						height = 2;
					end
					3:begin
						blk1 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x + 1;			// * *
						blk2 = (cur_pos_y + 1) * `BLOCKS_WIDTH + cur_pos_x + 1;		//   * 
						blk3 = (cur_pos_y + 2) * `BLOCKS_WIDTH + cur_pos_x + 1;		//   *
						blk4 = cur_pos_y * `BLOCKS_WIDTH + cur_pos_x;
						width = 2;
						height = 3;
					end
				endcase
 			end
 		endcase // cur_tetris
 	end
endmodule