`include "tetris_define.vh"

module tetris(
	input wire			clk,        //100 MHz
	input wire 			pixel_clk,	//25.175 MHz
	input wire			btn_down,
	input wire			btn_left,
	input wire			btn_right,
	input wire			btn_rotate,
	input wire			btn_drop,
	input wire			sw_rst,
	input wire			sw_pause,
	output wire	[4:0]	r, g, b,
	output wire			hs, vs
	);

	//timer used to automatically drop blocks
	reg [31:0]	drop_counter;
	initial begin
		drop_counter = 0;
	end

	//Get a random tetrimino index
	wire [2:0]	random_tetris;
	GetRandomPiece GetRandomPiece_inst(
		.clk(clk),
		.random_tetris(random_tetris)
		);

	//debounce all buttons 
	wire	btn_down_en;
	wire	btn_left_en;
	wire	btn_right_en;
	wire	btn_rotate_en;
	wire	btn_drop_en;
	wire	sw_rst_en;
	wire	sw_pause_en;
	debounce debounce_down(
		.clk  			(pixel_clk),
		.raw		    (btn_down),
		.btn_debounced	(btn_down_en)
		);

	debounce debounce_left(
		.clk  			(pixel_clk),
		.raw	     	(btn_left),
		.btn_debounced	(btn_left_en)
		);

	debounce debounce_right(
		.clk  			(pixel_clk),
		.raw	     	(btn_right),
		.btn_debounced	(btn_right_en)
		);

	debounce debounce_rotate(
		.clk  			(pixel_clk),
		.raw	     	(btn_rotate),
		.btn_debounced	(btn_rotate_en)
		);

	debounce debounce_drop(
		.clk  			(pixel_clk),
		.raw      		(btn_drop),
		.btn_debounced	(btn_drop_en)
		);

	debounce debounce_sw_rst(
		.clk 			(pixel_clk),
		.raw            (sw_rst),
		.sw_flipped     (sw_rst_en)
		);

	debounce debounce_sw_pause(
		.clk 			(pixel_clk),
		.raw            (sw_pause),
		.sw_flipped     (sw_pause_en)
		);

	/*register to hold position fallen tetriminos*/
	reg [`BLOCKS_WIDTH*`BLOCKS_HEIGHT-1:0]	fallen;
	/*current falling tetrimino*/
	reg [`BITS_PER_BLOCK-1:0]				cur_tetris;
	/*current tetrimino rotation (0 90 180 270)*/
	reg [`BITS_ROT-1:0]						cur_rot;
	/*current tetrimino x coordinate*/
	reg [`BITS_X_POS-1:0]					cur_pos_x;
	/*current tetrimino y coordinate*/
	reg [`BITS_X_POS-1:0]					cur_pos_y;
	/*four blocks of the current tetrimino's flattened coordinates*/
	wire [`BITS_BLK_POS-1:0]				cur_blk1;
	wire [`BITS_BLK_POS-1:0]				cur_blk2;
	wire [`BITS_BLK_POS-1:0]				cur_blk3;
	wire [`BITS_BLK_POS-1:0]				cur_blk4;
	/*width and height of the current tetrimino*/
	wire [`BITS_BLK_SIZE-1:0]				cur_width;
	wire [`BITS_BLK_SIZE-1:0]				cur_height;

	//calculate current tetrimino's width, height and flattened location
	GetTetrisInfo GetTetrisInfo_inst(
		.cur_tetris  (cur_tetris),
		.cur_pos_x   (cur_pos_x),
		.cur_pos_y   (cur_pos_y),
		.cur_rotation(cur_rot),
		.blk1        (cur_blk1),
		.blk2        (cur_blk2),
		.blk3        (cur_blk3),
		.blk4        (cur_blk4),
		.width       (cur_width),
		.height      (cur_height)
		);

	//VGA controller
	vga vga_inst(
		.pixel_clk	(pixel_clk),
		.cur_tetris	(cur_tetris),
		.blk1 		(cur_blk1), 
		.blk2 		(cur_blk2), 
		.blk3 		(cur_blk3), 
		.blk4 		(cur_blk4), 
		.fallen		(fallen),
		.rgb		({r,g,b}),
		.hs 		(hs),
		.vs 		(vs)
		);

	//move tester
	reg [`MODE_BITS-1:0]					mode;
	reg [`MODE_BITS-1:0]					prev_mode;
	wire [`BITS_X_POS-1:0]					test_pos_x;
	wire [`BITS_Y_POS-1:0]					test_pos_y;
	wire [`BITS_ROT-1:0]					test_rot;
	wire [`BITS_BLK_POS-1:0]				test_blk1;
	wire [`BITS_BLK_POS-1:0]				test_blk2;
	wire [`BITS_BLK_POS-1:0]				test_blk3;
	wire [`BITS_BLK_POS-1:0]				test_blk4;
	wire [`BITS_BLK_SIZE-1:0]				test_width;
	wire [`BITS_BLK_SIZE-1:0]				test_height;
	wire 									test_intesects;
	wire									gameover;

	//tetriminos fall automatically 
	wire auto_fall;
	reg  auto_fall_rst;
	drop_timer drop_timer_inst(.clk(pixel_clk), 
							   .rst(auto_fall_rst), 
							   .pause(mode != `MODE_PLAY), 
							   .drop_timer_o(auto_fall)
							  );

	GetTestInfo GetTestInfo_inst(
		.mode         	(mode),
		.auto_fall     	(auto_fall),
		.auto_fall_rst 	(auto_fall_rst),
		.btn_left_en  	(btn_left_en),
		.btn_right_en (btn_right_en),
		.btn_rotate_en(btn_rotate_en),
		.btn_down_en  (btn_down_en),
		.btn_drop_en  (btn_drop_en),
		.cur_pos_x    (cur_pos_x),
		.cur_pos_y    (cur_pos_y),
		.cur_rot      (cur_rot),
		.test_pos_x   (test_pos_x),
		.test_pos_y   (test_pos_y),
		.test_rot     (test_rot));

	//calculate the test tetriminos' info
	GetTetrisInfo GetTetrisInfo_TestBlocks(
		.cur_tetris  (cur_tetris),
		.cur_pos_x   (test_pos_x),
		.cur_pos_y   (test_pos_y),
		.cur_rotation(test_rot),
		.blk1        (test_blk1),
		.blk2        (test_blk2),
		.blk3        (test_blk3),
		.blk4        (test_blk4),
		.width       (test_width),
		.height      (test_height));

	//task to check if intersection happens
	function intersects;
		input [`BITS_BLK_POS-1:0]			blk1, blk2, blk3, blk4;
		begin 
			intersects = fallen[blk1] || fallen[blk2] || fallen[blk3] || fallen[blk4];
		end		
	endfunction
	
	assign test_intesects = intersects(test_blk1,test_blk2,  test_blk3, test_blk4);
	assign gameover = cur_pos_y == 0 && test_intesects;

	//task to move current tetrimino to the left
	task left;
		begin
			if(cur_pos_x > 0 && !test_intesects) 
				cur_pos_x = cur_pos_x - 1;
		end
	endtask

	//task to move current tetrimino to the right
	task right;
		begin
			if(cur_pos_x < `BLOCKS_WIDTH && !test_intesects) 
				cur_pos_x = cur_pos_x + 1;
		end
	endtask

	//task to rotate curret tetrimino
	task rotate;
		begin
			if(cur_pos_x + test_width <= `BLOCKS_WIDTH &&
			   cur_pos_y + test_height <= `BLOCKS_HEIGHT &&
			   !test_intesects)
			   cur_rot = cur_rot + 1;	
		end
	endtask

	//task to add fallen tetriminos
	task add_fallen;
		begin
			fallen[cur_blk1] = 1;
			fallen[cur_blk2] = 1;
			fallen[cur_blk3] = 1;
			fallen[cur_blk4] = 1;
		end
	endtask

	//task to get a new tetrimino
	task get_new;
		begin
			//reset drop timer
			drop_counter = 0;
			cur_tetris = random_tetris;
			cur_pos_x = `NEW_TETRIS_X;
			cur_pos_y = `NEW_TETRIS_Y;
			cur_rot = 0;
			//reset the auto fall counter
			auto_fall_rst = 1;

		end
	endtask

	//task to move current tetrimino down
	//call get_new if move would intersect with fallen or go out of game board
	task down;
		begin 
			if(cur_pos_y + cur_height < `BLOCKS_HEIGHT && !test_intesects) begin
				cur_pos_y = cur_pos_y + 1;
			end
			else begin 
				add_fallen();
				get_new();
			end
		end
	endtask

	//task to go into drop mode
	task drop;
		begin 
			mode = `MODE_DROP;
		end
	endtask : drop

	//calculate the row number to be removed
	wire [`BITS_Y_POS-1:0]					cancel_row;
	wire 									cancel_row_en;
	CancelRow CancelRow_inst(
		.clk(pixel_clk),
		.pause(mode != `MODE_PLAY),
		.fallen(fallen),
		.row(cancel_row),
		.enabled(cancel_row_en)
		);

	//task to remove row 
	reg [`BITS_Y_POS-1:0]					shift_row;
	task remove_row;
		begin 
			mode = `MODE_SHIFT;
			shift_row = cancel_row;
		end
	endtask

	//task to start the game
	task start;
		begin 
			mode = `MODE_PLAY;
			fallen = 0;
			get_new();
		end
	endtask : start

	initial begin 
		mode = `MODE_IDLE;
		fallen = 'b0;
		cur_tetris = `EMPTY_BLOCK;
		cur_pos_x = 0;
		cur_pos_y = 0;
		cur_rot = 0;
	end

	//increment drop timer
	always@(posedge pixel_clk) begin 
		if(drop_counter < `DROP_TIMER_MAX) begin 
			drop_counter <= drop_counter + 1;
		end
		else
			drop_counter <= drop_counter;
	end

	//game state machine
	always@(posedge pixel_clk) begin
		auto_fall_rst <= 0;
		case(mode)
			`MODE_IDLE: begin 
				if(sw_rst_en) begin 
					//reset switch flipped
					start();
				end
				else begin
					//reset or game over
					mode <= `MODE_IDLE;
					cur_tetris <= `EMPTY_BLOCK;
					add_fallen();
				end
			end

			`MODE_PAUSE:begin 
				if(sw_pause_en) begin 
					mode <= prev_mode;
				end
				else begin 
					mode <= mode;
				end
			end
			
			`MODE_PLAY:begin 
				if(sw_pause_en) begin 
					prev_mode <= mode;
				end
				else begin 
					prev_mode <= prev_mode;
				end

				if(auto_fall)
					down();
				else if(btn_left_en)
					left();
				else if(btn_right_en)
					right();
				else if(btn_rotate_en)
					rotate();
				else if(btn_down_en)
					down();
				else if(btn_drop_en && drop_counter == `DROP_TIMER_MAX)
					drop();
				else if(cancel_row_en)
					remove_row();
			end
			
			`MODE_DROP:begin 
				if(!auto_fall_rst) begin 
					//keep going down untill it hits a fallen or goes out of game board
					down();
				end
				else begin 
					mode <= `MODE_PLAY;
				end
			end

			`MODE_SHIFT:begin 
				if(shift_row == 0) begin 
					mode <= `MODE_PLAY;
					fallen[0 +: `BLOCKS_WIDTH] <= 0;
					shift_row <= shift_row;
				end
				else begin 
					mode <= mode;
					fallen[shift_row*`BLOCKS_WIDTH+:`BLOCKS_WIDTH] <= fallen[(shift_row-1)*`BLOCKS_WIDTH+:`BLOCKS_WIDTH];
					shift_row <= shift_row - 1;
				end
			end
		endcase // mode
	end

endmodule : tetris
