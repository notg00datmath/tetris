module drop_timer (
	input		clk,    // Clock
	input		rst,
	input		pause,
	output reg	drop_timer_o 	
);

	reg	[31:0]	counter;
	always@(posedge clk) begin
		if(!pause) begin
			if(rst) begin
				counter <= 0;
				drop_timer_o <= 0;
			end
			else begin
				if(counter == 25000000) begin
					counter <= 0;
					drop_timer_o <= 1;
				end
				else begin
					counter <= counter + 1;
					drop_timer_o <= 0;
				end	
			end	
		end
		else begin
			drop_timer_o <= drop_timer_o;
			counter <= counter;
		end	
	end
endmodule