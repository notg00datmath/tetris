`include "tetris_define.vh"

module GetRandomPiece (
	input				clk,    // Clock
	output reg [`BITS_PER_BLOCK-1:0]	random_tetris 
);
	initial begin 
		random_tetris = 1;
	end

	always@(posedge clk) begin
		if(random_tetris==7) 
			random_tetris <= 1;
		else 
			random_tetris <= random_tetris + 1;
	end
endmodule