`include "tetris_define.vh"

module vga (
	input 											pixel_clk,    // Clock	
	input 	wire [`BITS_PER_BLOCK-1:0]				cur_tetris,
	input 	wire [`BITS_BLK_POS-1:0]				blk1,	
	input 	wire [`BITS_BLK_POS-1:0]				blk2,	
	input 	wire [`BITS_BLK_POS-1:0]				blk3,	
	input 	wire [`BITS_BLK_POS-1:0]				blk4,	
	input	wire [`BLOCKS_WIDTH*`BLOCKS_HEIGHT-1:0]	fallen,
	output	reg	 [11:0]								rgb,
	output											hs, vs
);

	reg  [9:0] counterX = 0;
	reg  [9:0] counterY = 0;
	wire [9:0] cur_blk_index; 
	reg		   v_enabled;

	//synchronization signals
	assign hs = (counterX < `HSYNC_PULSE_WIDTH) ? 1'b0 : 1'b1;
	assign vs = (counterY < `VSYNC_PULSE_WIDTH) ? 1'b0 : 1'b1;

	//select current pixel
	assign cur_blk_index = ((counterX-`BOARD_X)/`BLOCK_SIZE) + (((counterY-`BOARD_Y)/`BLOCK_SIZE)*`BLOCKS_WIDTH);

	always@(*) begin
		//with in the game board
		if(counterX >= `BOARD_X && counterX <= `BOARD_X + `BOARD_WIDTH && 
		    counterY >= `BOARD_Y && counterY <= `BOARD_Y + `BOARD_HEIGHT) begin
			if(counterX == `BOARD_X || counterX == `BOARD_X + `BOARD_WIDTH || 
		       counterY == `BOARD_Y || counterY == `BOARD_Y + `BOARD_HEIGHT) begin
		   	//the game board border is white
		   		rgb = `WHITE;
		    end
		    else begin
		   		//if current tetrimino is reached, drive rgb
		   		if(cur_blk_index == blk1 ||
		   		   cur_blk_index == blk2 ||
		   		   cur_blk_index == blk3 ||
		   		   cur_blk_index == blk4) begin
		   		   	case(cur_tetris) 
		   		   		`EMPTY_BLOCK: rgb = `GRAY;
		                `I_BLOCK: rgb = `CYAN;
		                `O_BLOCK: rgb = `YELLOW;
		                `T_BLOCK: rgb = `PURPLE;
		                `S_BLOCK: rgb = `GREEN;
		                `Z_BLOCK: rgb = `RED;
		                `J_BLOCK: rgb = `BLUE;
		                `L_BLOCK: rgb = `ORANGE;
		   		   	endcase 
			    end
			    else begin
			    	//if fallen tetriminos reached, drive monochrome
			    	rgb = fallen[cur_blk_index] ? `WHITE : `GRAY;
			    end
		    end
		end
		else begin
			rgb = `BLACK;
		end
	end

    always@(posedge pixel_clk) begin
		if(counterX<799) begin
			counterX <= counterX + 1;
			v_enabled <= 0;
		end
		else begin
			counterX <= 0;
			v_enabled <= 1;
		end
    end

    always@(posedge pixel_clk) begin
    	if(v_enabled==1'b1) begin
    		if(counterY<524) 
    			counterY <= counterY + 1;
    		else
    			counterY <= 0;
    	end
    	else 
    		counterY <= counterY;
    end

endmodule