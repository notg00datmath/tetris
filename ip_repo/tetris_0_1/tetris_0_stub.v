// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Wed Feb 26 11:21:40 2020
// Host        : ensc-rcl-07.engineering.sfu.ca running 64-bit Fedora release 30 (Thirty)
// Command     : write_verilog -force -mode synth_stub /home/yuhui/tetris/ip_repo/tetris_0_1/tetris_0_stub.v
// Design      : tetris_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "tetris,Vivado 2019.1" *)
module tetris_0(clk, pixel_clk, btn_down, btn_left, btn_right, 
  btn_rotate, btn_drop, sw_rst, sw_pause, r, g, b, hs, vs)
/* synthesis syn_black_box black_box_pad_pin="clk,pixel_clk,btn_down,btn_left,btn_right,btn_rotate,btn_drop,sw_rst,sw_pause,r[4:0],g[4:0],b[4:0],hs,vs" */;
  input clk;
  input pixel_clk;
  input btn_down;
  input btn_left;
  input btn_right;
  input btn_rotate;
  input btn_drop;
  input sw_rst;
  input sw_pause;
  output [4:0]r;
  output [4:0]g;
  output [4:0]b;
  output hs;
  output vs;
endmodule
