vlib work
vlib riviera

vlib riviera/xil_defaultlib

vmap xil_defaultlib riviera/xil_defaultlib

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../tetris_0_1/src" \
"../../../../tetris_0_1/src/CancelRow.v" \
"../../../../tetris_0_1/src/GetRandomPiece.v" \
"../../../../tetris_0_1/src/GetTestInfo.v" \
"../../../../tetris_0_1/src/GetTetrisInfo.v" \
"../../../../tetris_0_1/src/debounce.v" \
"../../../../tetris_0_1/src/drop_timer.v" \
"../../../../tetris_0_1/src/vga.v" \
"../../../../tetris_0_1/src/tetris.v" \
"../../../../tetris_0_1/sim/tetris_0.v" \


vlog -work xil_defaultlib \
"glbl.v"

