vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xil_defaultlib

vmap xil_defaultlib questa_lib/msim/xil_defaultlib

vlog -work xil_defaultlib -64 "+incdir+../../../../tetris_0_1/src" \
"../../../../tetris_0_1/src/CancelRow.v" \
"../../../../tetris_0_1/src/GetRandomPiece.v" \
"../../../../tetris_0_1/src/GetTestInfo.v" \
"../../../../tetris_0_1/src/GetTetrisInfo.v" \
"../../../../tetris_0_1/src/debounce.v" \
"../../../../tetris_0_1/src/drop_timer.v" \
"../../../../tetris_0_1/src/vga.v" \
"../../../../tetris_0_1/src/tetris.v" \
"../../../../tetris_0_1/sim/tetris_0.v" \


vlog -work xil_defaultlib \
"glbl.v"

