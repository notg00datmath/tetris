-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Wed Feb 26 11:21:41 2020
-- Host        : ensc-rcl-07.engineering.sfu.ca running 64-bit Fedora release 30 (Thirty)
-- Command     : write_vhdl -force -mode synth_stub /home/yuhui/tetris/ip_repo/tetris_0_1/tetris_0_stub.vhdl
-- Design      : tetris_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tetris_0 is
  Port ( 
    clk : in STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    btn_down : in STD_LOGIC;
    btn_left : in STD_LOGIC;
    btn_right : in STD_LOGIC;
    btn_rotate : in STD_LOGIC;
    btn_drop : in STD_LOGIC;
    sw_rst : in STD_LOGIC;
    sw_pause : in STD_LOGIC;
    r : out STD_LOGIC_VECTOR ( 4 downto 0 );
    g : out STD_LOGIC_VECTOR ( 4 downto 0 );
    b : out STD_LOGIC_VECTOR ( 4 downto 0 );
    hs : out STD_LOGIC;
    vs : out STD_LOGIC
  );

end tetris_0;

architecture stub of tetris_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,pixel_clk,btn_down,btn_left,btn_right,btn_rotate,btn_drop,sw_rst,sw_pause,r[4:0],g[4:0],b[4:0],hs,vs";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "tetris,Vivado 2019.1";
begin
end;
