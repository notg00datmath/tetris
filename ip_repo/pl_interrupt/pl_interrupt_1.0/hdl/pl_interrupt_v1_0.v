
`timescale 1 ns / 1 ps

	module pl_interrupt_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface pl_interrupt
		parameter integer C_pl_interrupt_DATA_WIDTH	= 32,
		parameter integer C_pl_interrupt_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here
        input wire up,down,left,right,mid,sw_rst,sw_pause,
        input wire [3:0] level,
        output wire interrupt_up, interrupt_down, interrupt_left, interrupt_right, interrupt_mid,interrupt_sw_rst,interrupt_sw_pause,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface pl_interrupt
		input wire  pl_interrupt_aclk,
		input wire  pl_interrupt_aresetn,
		input wire [C_pl_interrupt_ADDR_WIDTH-1 : 0] pl_interrupt_awaddr,
		input wire [2 : 0] pl_interrupt_awprot,
		input wire  pl_interrupt_awvalid,
		output wire  pl_interrupt_awready,
		input wire [C_pl_interrupt_DATA_WIDTH-1 : 0] pl_interrupt_wdata,
		input wire [(C_pl_interrupt_DATA_WIDTH/8)-1 : 0] pl_interrupt_wstrb,
		input wire  pl_interrupt_wvalid,
		output wire  pl_interrupt_wready,
		output wire [1 : 0] pl_interrupt_bresp,
		output wire  pl_interrupt_bvalid,
		input wire  pl_interrupt_bready,
		input wire [C_pl_interrupt_ADDR_WIDTH-1 : 0] pl_interrupt_araddr,
		input wire [2 : 0] pl_interrupt_arprot,
		input wire  pl_interrupt_arvalid,
		output wire  pl_interrupt_arready,
		output wire [C_pl_interrupt_DATA_WIDTH-1 : 0] pl_interrupt_rdata,
		output wire [1 : 0] pl_interrupt_rresp,
		output wire  pl_interrupt_rvalid,
		input wire  pl_interrupt_rready
	);
// Instantiation of Axi Bus Interface pl_interrupt
	pl_interrupt_v1_0_pl_interrupt # ( 
		.C_S_AXI_DATA_WIDTH(C_pl_interrupt_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_pl_interrupt_ADDR_WIDTH)
	) pl_interrupt_v1_0_pl_interrupt_inst (
	    .level(level),
	    .up(up),
	    .down(down),
	    .left(left),
	    .right(right),
	    .mid(mid),
	    .sw_rst(sw_rst),
	    .sw_pause(sw_pause),
	    .interrupt_up(interrupt_up), 
	    .interrupt_down(interrupt_down), 
	    .interrupt_left(interrupt_left), 
	    .interrupt_right(interrupt_right), 
	    .interrupt_mid(interrupt_mid),
	    .interrupt_sw_rst(interrupt_sw_rst),
	    .interrupt_sw_pause(interrupt_sw_pause),
		.S_AXI_ACLK(pl_interrupt_aclk),
		.S_AXI_ARESETN(pl_interrupt_aresetn),
		.S_AXI_AWADDR(pl_interrupt_awaddr),
		.S_AXI_AWPROT(pl_interrupt_awprot),
		.S_AXI_AWVALID(pl_interrupt_awvalid),
		.S_AXI_AWREADY(pl_interrupt_awready),
		.S_AXI_WDATA(pl_interrupt_wdata),
		.S_AXI_WSTRB(pl_interrupt_wstrb),
		.S_AXI_WVALID(pl_interrupt_wvalid),
		.S_AXI_WREADY(pl_interrupt_wready),
		.S_AXI_BRESP(pl_interrupt_bresp),
		.S_AXI_BVALID(pl_interrupt_bvalid),
		.S_AXI_BREADY(pl_interrupt_bready),
		.S_AXI_ARADDR(pl_interrupt_araddr),
		.S_AXI_ARPROT(pl_interrupt_arprot),
		.S_AXI_ARVALID(pl_interrupt_arvalid),
		.S_AXI_ARREADY(pl_interrupt_arready),
		.S_AXI_RDATA(pl_interrupt_rdata),
		.S_AXI_RRESP(pl_interrupt_rresp),
		.S_AXI_RVALID(pl_interrupt_rvalid),
		.S_AXI_RREADY(pl_interrupt_rready)
	);

	// Add user logic here

	// User logic ends

	endmodule
