# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  ipgui::add_page $IPINST -name "Page 0"


}

proc update_PARAM_VALUE.ADDRESS_READY { PARAM_VALUE.ADDRESS_READY } {
	# Procedure called to update ADDRESS_READY when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ADDRESS_READY { PARAM_VALUE.ADDRESS_READY } {
	# Procedure called to validate ADDRESS_READY
	return true
}

proc update_PARAM_VALUE.IDLE { PARAM_VALUE.IDLE } {
	# Procedure called to update IDLE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.IDLE { PARAM_VALUE.IDLE } {
	# Procedure called to validate IDLE
	return true
}

proc update_PARAM_VALUE.READ { PARAM_VALUE.READ } {
	# Procedure called to update READ when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.READ { PARAM_VALUE.READ } {
	# Procedure called to validate READ
	return true
}

proc update_PARAM_VALUE.READ_REQUEST { PARAM_VALUE.READ_REQUEST } {
	# Procedure called to update READ_REQUEST when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.READ_REQUEST { PARAM_VALUE.READ_REQUEST } {
	# Procedure called to validate READ_REQUEST
	return true
}


proc update_MODELPARAM_VALUE.READ_REQUEST { MODELPARAM_VALUE.READ_REQUEST PARAM_VALUE.READ_REQUEST } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.READ_REQUEST}] ${MODELPARAM_VALUE.READ_REQUEST}
}

proc update_MODELPARAM_VALUE.ADDRESS_READY { MODELPARAM_VALUE.ADDRESS_READY PARAM_VALUE.ADDRESS_READY } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ADDRESS_READY}] ${MODELPARAM_VALUE.ADDRESS_READY}
}

proc update_MODELPARAM_VALUE.READ { MODELPARAM_VALUE.READ PARAM_VALUE.READ } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.READ}] ${MODELPARAM_VALUE.READ}
}

proc update_MODELPARAM_VALUE.IDLE { MODELPARAM_VALUE.IDLE PARAM_VALUE.IDLE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.IDLE}] ${MODELPARAM_VALUE.IDLE}
}

