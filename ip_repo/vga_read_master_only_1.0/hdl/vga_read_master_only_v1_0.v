module VGA_MASTER(
    input wire pixel_clk,
    // input wire pixel_rstn,
    input wire fifo_rst,
    input wire M_AXI_ACLK,                                                                                    
    input wire M_AXI_ARESETN,                                                                                                              
    output wire [31:0] M_AXI_ARADDR,    
    output wire [7:0] M_AXI_ARLEN,     
    output wire [2:0] M_AXI_ARSIZE,    
    output wire [1:0] M_AXI_ARBURST,   
    output wire [2:0] M_AXI_ARPROT,    
    output wire M_AXI_ARVALID,   
    input wire M_AXI_ARREADY,   
    input wire [31:0] M_AXI_RDATA,     
    input wire M_AXI_RLAST,     
    input wire M_AXI_RVALID,    
    output wire M_AXI_RREADY,    
    output wire [3:0] r,               
    output wire [3:0] g,               
    output wire [3:0] b,              
    output wire hs,              
    output wire vs              
    );

    parameter [1:0] READ_REQUEST = 2'b00, 
                    ADDRESS_READY = 2'b01, 
                    READ = 2'b10, 
                    IDLE = 2'b11;

    reg [1:0] state;
    localparam [31:0] base_addr = 32'h01000000;
    reg [31:0] next_addr;
    localparam [31:0] max_addr = base_addr + 640*480*4-128;
    reg arvalid;
    reg rready;
    reg [31:0] araddr;
    wire [31:0] fifo_din;
    reg [31:0] fifo_din_reg;
    wire[31:0] fifo_dout;
    reg [3:0] r_reg, g_reg, b_reg;
    reg fifo_en_h;
    reg fifo_en_v;
    reg fifo_rd_en_reg, fifo_wr_en_reg;
    wire fifo_rd_en;
    wire fifo_full;
    wire fifo_wr_en;
    wire fifo_empty;
    reg v_enabled;
    reg hsync_reg, vsync_reg;

    assign r = r_reg;
    assign g = g_reg;
    assign b = b_reg;
    assign vs = vsync_reg;
    assign hs = hsync_reg;
    assign fifo_rd_en = fifo_rd_en_reg;
    assign fifo_wr_en = fifo_wr_en_reg;
    assign fifo_din = fifo_din_reg;
    
    reg [5:0] pixle_cnt = 'b0;
    reg [10:0] h_cnt = 'b0;
    reg [10:0] v_cnt = 'b0;

    assign M_AXI_ARSIZE = 3'b010;
    assign M_AXI_ARBURST = 2'b01;
    assign M_AXI_ARLEN = 8'h1f; //burst length 32
    assign M_AXI_ARVALID = arvalid;
    assign M_AXI_ARPROT = 'b0;
    assign M_AXI_RREADY = rready;       
    assign M_AXI_ARADDR = araddr;
    
//  assign fifo_wr_en = fifo_full==1? 0:1;

    fifo_generator_0 fifo_inst(
        .rst(fifo_rst),
        .wr_clk(M_AXI_ACLK),
        .rd_clk(pixel_clk),
        .din(fifo_din),
        .wr_en(fifo_wr_en),
        .rd_en(fifo_rd_en),
        .dout(fifo_dout),
        .full(fifo_full),
        .empty(fifo_empty)
        );

    always@(posedge M_AXI_ACLK) begin
        if(M_AXI_ARESETN==0)
            fifo_wr_en_reg <= 0;
        else 
            fifo_wr_en_reg <= M_AXI_RVALID && rready;
            if(fifo_full==1'b0) 
                rready <= 1'b1;
            else 
                rready <= 1'b0;
    end

    always@(posedge M_AXI_ACLK) begin
        if(M_AXI_ARESETN==0) begin
            state <= READ_REQUEST;
            arvalid <= 1'b0;
            next_addr <= base_addr;
        end
        else begin
            case(state)
                READ_REQUEST: begin
                       araddr <= next_addr;
                       arvalid <= 1'b1;
                       state <= ADDRESS_READY;
                end
                ADDRESS_READY:begin
                    if(M_AXI_ARREADY==1'b1) begin
                        arvalid <= 1'b0;
                        // rready <= 1'b1;
                        state <= READ;
                    end
                    else begin
                       arvalid <= 1'b1;
                       // rready <= 1'b0;
                       state <= ADDRESS_READY;
                    end
                end
                READ: begin
                    if(M_AXI_RVALID==1'b1) begin
                        fifo_din_reg <= M_AXI_RDATA;
                        if(M_AXI_RLAST==1'b1) begin
                            state <= IDLE;
                        end
                    end
                end
                IDLE: begin
                    if(next_addr>=max_addr)
                        next_addr <= base_addr;
                    else
                        next_addr <= next_addr + 128;
                    state <= READ_REQUEST;
                end
                default: state <= READ_REQUEST;
            endcase
        end
    end
/*--------------------------vga-------------------*/
    always@(posedge pixel_clk) begin
		if(h_cnt<799) begin
			h_cnt <= h_cnt + 1;
			v_enabled <= 0;
		end
		else begin
			h_cnt <= 0;
			v_enabled <= 1;
		end
    end

    always@(posedge pixel_clk) begin
    	if(v_enabled==1'b1) begin
    		if(v_cnt<524) 
    			v_cnt <= v_cnt + 1;
    		else
    			v_cnt <= 0;
    	end
    	else 
    		v_cnt <= v_cnt;
    end

    always@(h_cnt,v_cnt,fifo_dout) begin
        if(h_cnt < 784 && h_cnt > 143 && v_cnt < 515 && v_cnt > 34) begin
            r_reg = fifo_dout[7:4];
            g_reg = fifo_dout[15:12];
            b_reg = fifo_dout[23:20];
        end
        else begin
            g_reg = 4'b0;
            b_reg = 4'b0;
            r_reg = 4'b0;
        end
    end
    
    always@(h_cnt) begin
        if(h_cnt<784 && h_cnt>143)
            fifo_en_h = 1'b1;
        else 
            fifo_en_h = 1'b0;
    end
    
    always@(v_cnt) begin
        if(v_cnt < 515 && v_cnt > 34) begin
            fifo_en_v = 1'b1;
        end
        else begin
            fifo_en_v = 1'b0;
        end
    end

    always@(fifo_en_h,fifo_en_v,fifo_empty) begin
        fifo_rd_en_reg = fifo_en_v && fifo_en_h && (~fifo_empty);
    end

    always@(h_cnt,v_cnt) begin
        hsync_reg = (h_cnt < 96) ? 1'b0:1'b1;
        vsync_reg = (v_cnt < 2)  ? 1'b0:1'b1;
    end
//----------------------------------------------------------------------------------------

endmodule